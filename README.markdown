# SHARON

## Overview

- Download sharon package
- Download apache storm and install
- Download kafka and install
- Configure MongoDB
- Compile and execute sharon

## Configuration file format

The following parameters in `sharon.properties` specify your kafka installation. Kafka must exist for the system to run.

```
kafka.brokerlist = localhost:9092
```

## Running Kafka

Start zookeeper:

```
bin/zookeeper-server-start.sh config/zookeeper.properties &
```

Copy `config/server.properties` to `config/server1.properties`, so as to preserve the original `server.properties` file.

Add/edit the following lines to `config/server1.properties`:

```
brokerid=1
port=9092
advertised.host.name=server.example.com
log.dirs=/tmp/kafka-logs-1
log.retention.hours=24
```

You may put the log directory to wherever is appropriate on your system. The log retention hours is important as the log files will include all tweets received by the system over the last log retention hours.

Start server:

```
env JMX_PORT=9999 bin/kafka-server-start.sh config/server1.properties &
```

## Add topics

```
for topic in commands keyword-topic
do
	bin/kafka-topics.sh --create --topic "$topic" --partitions 1 --replication-factor 1 --zookeeper localhost:2181
done 
```

# Configuring MongoDB

Requires mongodb 2.6 or above.

## Connect to MongoDB and add an admin user

```
mongo --port 27017
use admin
db.createUser(
  {
    user: "myUserAdmin",
    pwd: "abc123",
    roles: [ {role: "clusterAdmin", db: "admin"}, { role: "userAdminAnyDatabase", db: "admin" } ] 
  }
)
```

## Set MongoDB configuration to use authentication

Set `auth=true` in `mongodb.conf` (version 2.6) or `security.authentication` to `enable` in `mongod.conf` (version 3.0+) and restart MongoDB.

## Set sharon user in MongoDB

```
mongo --port 27017 -u "myUserAdmin" -p "abc123" --authenticationDatabase "admin"
use admin
db.createUser(
  {
    user: "sharon",
    pwd: "sharon123",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" },{role:"readWrite",db:"sharonDB"} ]
  }
)
```

## Set sharon.properties to contain database user

```
database.user = sharon
database.password = sharon123
```

# Execution of Sharon

## Compiling Sharon

To compile as a full package:

```
mvn package
```

## Storm logging

If running in local mode then logging for new versions of Storm requires:

```
export STORM_JAR_JVM_OPTS="-Dlog4j.configurationFile=path/to/cluster2.xml"
```

And older versions of Storm requires:

```
export STORM_JAR_JVM_OPTS="-Dlogback.configurationFile=path/to/cluster.xml"
```

An example `cluster.xml` is given in `logback/cluster.xml`, based on the `cluster.xml` supplied with Storm. Copy across relevant logging statements as desired.

## Format of the credentials file

For the purposes of development you would like to run the entire system yourself. To do this you will need to create an application in Twitter and create an access token for your twitter account.

Go here: [Twitter API](https://dev.twitter.com/overview/api)

Click on "Manage My Apps". Follow the instructions for creating an application. Please call the application anything you like, don't worry about fields that are not needed. When you've created the application, the tab "Keys and Access Tokens" for your application provides a button at the bottom to add your own twitter account as a user of the application. After all of this, you will have 4 tokens, 2 for the application and 2 for the user. We call this a credential.

Create directory `TwitterCredentials` and put one credential per file:

```
consumerKey = EXAMPLE
consumerSecret = EXAMPLE
accessToken = EXAMPLE
accessTokenSecret = EXAMPLE
```

## Run command

### Running locally

```
storm jar server/target/rapidserver-*.jar storm.sharon.TwitterTrackerTopologyOne
```

## Community GUI

```
java -cp sharon-*.jar storm.sharon.CommunityGUI
```

## Sharon GUI

```
java -cp sharon-*.jar storm.sharon.GuiApp1
```

## kafka examples

### List message queues

```
bin/kafka-topics.sh --list --zookeeper localhost:2181
```

### Start Consumer 

Start a consumer to listen to the message queue:

```
bin/kafka-console-consumer.sh --zookeeper localhost:2181 --topic keyword-topic --from-beginning
```





