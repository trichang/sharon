

sharon.page.header = {}

sharon.page.header.init = function(){
	$('#page-header').toolbar().enhanceWithin()
	$("#navbar-topics").on("click",function(){
		$.mobile.pageContainer.pagecontainer("change",$("#topics"),{transition:"none"});
	})
	$("#navbar-tweets").on("click",function(){
		$.mobile.pageContainer.pagecontainer("change",$("#tweets"),{transition:"none"});
	})
	$("#navbar-discussions").on("click",function(){
		$.mobile.pageContainer.pagecontainer("change",$("#discussions"),{transition:"none"});
	})
	console.log("sharon.page.header.init: initialized")
}