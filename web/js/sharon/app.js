/*
 * App for Sharon interface
 */

sharon = {}

// application
sharon.app = {}

// storage
sharon.storage = {}

// maintains each page
sharon.page = {
	// utility functions
	util : {
		populateField : function(field,data){
			if(data!=null){
				$(field).val(data)
			} else {
				$(field).val("")
			}
		},
		readField : function(field) {
			var val=$(field).val()
			if(val==""){
				return(null)
			} else {
				return(val)
			}
		}
	}
}

// popups for information and confirmation dialogs
sharon.popups = {}

// manages results
sharon.result = {}

// maintains collections
sharon.collection = {}

// maintains result streams
sharon.stream = {}

// maintains result queries
sharon.query = {}


sharon.app.prodname = "Sharon Workbench"

sharon.app.init = function() {
	console.log('sharon.app.init: starting...')
	console.log('sharon.app.init: running on '+navigator.userAgent)
	
	// initialize storage
	sharon.storage.init()
	
	sharon.storage.state.hostname=location.hostname;
	
	// initialize page handlers
	sharon.page.splash.init()
	sharon.page.header.init()
	sharon.page.footer.init()
	sharon.page.settings.init()
	sharon.page.tweets.init()
	sharon.page.discussions.init()
	sharon.popups.init()
	
	// initialize document event listeners
	document.addEventListener("pause", sharon.app.onPause, false)
	document.addEventListener("resume", sharon.app.onResume, false)
	document.addEventListener("online", sharon.app.onOnline, false)
	document.addEventListener("offline", sharon.app.onOffline, false)
	document.addEventListener("backbutton", sharon.app.onBackbutton, false)
	$.mobile.pageContainer.on( "pagecontainerbeforeshow", sharon.app.onPageContainerBeforeShow)
	$.mobile.pageContainer.on( "pagecontainershow", sharon.app.onPageContainerShow)
	$.mobile.pageContainer.on( "pagecontainerbeforehide", sharon.app.onPageContainerBeforeHide)
	$.mobile.pageContainer.on( "pagecontainertransition", sharon.app.onPageContainerTransition)
	
	/*//Chrome only
	navigator.webkitTemporaryStorage.queryUsageAndQuota ( 
	    function(usedBytes, grantedBytes) {  
	        console.log('we are using ', usedBytes, ' of ', grantedBytes, 'bytes');
	    }, 
    	function(e) { console.log('Error', e);  }
	);
	*/
	
	// transition to the first page
	$.mobile.pageContainer.pagecontainer("change",$("#settings"),{transition:"none"});
	console.log('sharon.app.init: ready')
}

sharon.app.onPageContainerBeforeShow = function(evt, ui) {
	
	 var toPage = $(ui.toPage).attr("id")
	 var fromPage = $(ui.prevPage).attr("id")
	 if(typeof toPage == 'undefined'){
		 return
	 }
	 console.log("sharon.app.onPageContainerBeforeShow: "+toPage)
	 if (sharon.page.hasOwnProperty(toPage)){
		sharon.page[toPage].onPageBeforeShow(evt,ui)
	 }
}

sharon.app.onPageContainerBeforeHide = function(evt,ui) {
	
	 var toPage = $(ui.toPage).attr("id")
	 var fromPage = $(ui.prevPage).attr("id")
	 if(typeof fromPage == 'undefined'){
		 return
	 }
	 console.log("sharon.app.onPageContainerBeforeHide: "+fromPage)
	 if (sharon.page.hasOwnProperty(fromPage)){
 		sharon.page[fromPage].onPageBeforeHide(evt,ui)
 	 }
}

sharon.app.onPageContainerShow = function(evt, ui){
	
	var toPage = $(ui.toPage).attr("id")
	var fromPage = $(ui.prevPage).attr("id")
	if(typeof toPage == 'undefined'){
		return
	}
	console.log("sharon.app.onPageContainerShow: "+toPage)
 	 if (sharon.page.hasOwnProperty(toPage)){
  		sharon.page[toPage].onPageShow(evt,ui)
  	 }
}

sharon.app.onPageContainerTransition = function(evt, ui){
	 
	var toPage = $(ui.toPage).attr("id")
	var fromPage = $(ui.prevPage).attr("id")
	if(typeof toPage == 'undefined'){
		return
	}
	console.log("sharon.app.onPageContainerTransition: "+toPage)
 	 if (sharon.page.hasOwnProperty(toPage)){
  		sharon.page[toPage].onTransition(evt,ui)
  	 }
}

sharon.app.onPause = function() {
	// application is being put in background
	console.log("sharon.app.onPause: pausing...")
}

sharon.app.onResume = function() {
	// application is resuming from background
	console.log("sharon.app.onResume: resuming...")
}

sharon.app.onOnline = function() {
	// application is now online
	console.log("sharon.app.onOnline: now online...")
}

sharon.app.onOffline = function() {
	// application is now offline
	console.log("sharon.app.onOffline: now offline...")
}

sharon.app.nextID = function() {
	sharon.storage.state.idcount+=1
	sharon.storage.writeState()
	return sharon.storage.state.idcount
}

