

sharon.page.settings = {}

sharon.page.settings.init = function(evt,ui) {
	$("#page-header-setup").on("click",function(){
		$.mobile.pageContainer.pagecontainer("change",$("#settings"),{transition:"none"});
	})
	$("#settings-addcredentialbutton").on("click",sharon.page.settings.onAddCredential);
	console.log('sharon.page.settings.init: initialized');
}

sharon.page.settings.onPageBeforeShow = function(evt,ui){
	$("#settings-tweetresults-summary").html("<b>"+sharon.storage.state.tweetresultids.length+"</b> Tweet result(s) in local storage.");
	$("#settings-statuscachesize").val(sharon.storage.state.statuscachesize)
	$("#settings-resultsetsize").val(sharon.storage.state.resultsetsize)
	$("#settings-hostname").val(sharon.storage.state.hostname)
	$("#settings-webport").val(sharon.storage.state.webport)
	sharon.control.getCredentials("#settings-credentiallist")
	$("#page-header-heading").html("Settings")
}

sharon.page.settings.onPageShow = function(evt,ui){
	setTimeout(function(){sharon.popups.popupInfo("sharon_app_welcome",
			"Welcome to "+sharon.app.prodname,
			"<p>This is a big data collection analysis workbench in the browser.<BR><BR>"+
			"<i>ALPHA Test Version</i>.<BR><BR>"+
			"Computing and Information Systems, The University of Melbourne.<BR><BR>"+
			"If you are not authorized to run this application please terminate and uninstall it now.</p>",
			function(){
				sharon.popups.popupInfo("sharon_page_settings",
					"Settings Page",
					"<p>On this page you can see an overview of global state and settings. "+
					"At the top of the page you can select to work on Tweets or Discussions.</p>",
					function(){},true,true);
			},true,true)},1000);
}

sharon.page.settings.onTransition = function(evt,ui){
	
}


sharon.page.settings.onPageBeforeHide = function(evt,ui){


}

sharon.page.settings.onAddCredential = function(evt,ui){
	$("#popupCredential-filename").val("")
	$("#popupCredential-consumerKey").val("")
	$("#popupCredential-consumerSecret").val("")
	$("#popupCredential-accessToken").val("")
	$("#popupCredential-accessTokenSecret").val("")
	$("#popupAddCredential").popup("open")
}

sharon.page.settings.onDeleteCredential = function(evt,ui){
	filename=$(this).find(".creditemfilename").first().html()
	sharon.popups.popupConfirmation("sharon_page_settings_deletecred",
			"Delete Credential?",
			"<p>This action will permanently remove the credential from the system.</p>",
			function(){},(function(filename){
				return function(){	
					sharon.control.deleteCredentials("#settings-credentiallist",filename)
				}
			})(filename)
			,false,false)
}