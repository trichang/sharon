
sharon.result.tweets = {}

sharon.result.tweets.createTweet = function(status){
	// additional meta data about the status can be included in the wrapper object
	var tweet={
		status:status
	}
	return tweet
}

sharon.result.tweets.createResult = function(){
	var result={
		id:null, // used for internal storage
		name:null, // displayed to user
		tweets:[], // just tweet id strings
		tids:{},
		parameter : {
			id_str : null,
			screenname : null,
			id_str_inreplyto : null,
			screenname_inreplyto : null,
			id_str_retweeted : null,
			screenname_retweeted : null,
			statusescount_atleast : null,
			statusescount_atmost : null,
			followerscount_atleast : null,
			followerscount_atmost : null,
			friendscount_atleast : null,
			friendscount_atmost : null,
			favouritescount_atleast : null,
			favouritescount_atmost : null,
			retweetedcount_atleast : null,
			retweetedcount_atmost : null,
			language : null,
			hashtags : null,
			mentions : null,
			exclude_id_str : null,
			exclude_screenname : null,
			exclude_id_str_inreplyto : null,
			exclude_screenname_inreplyto : null,
			exclude_id_str_retweeted : null,
			exclude_screenname_retweeted : null,
			exclude_statusescount_atleast : null,
			exclude_statusescount_atmost : null,
			exclude_followerscount_atleast : null,
			exclude_followerscount_atmost : null,
			exclude_friendscount_atleast : null,
			exclude_friendscount_atmost : null,
			exclude_favouritescount_atleast : null,
			exclude_favouritescount_atmost : null,
			exclude_retweetedcount_atleast : null,
			exclude_retweetedcount_atmost : null,
			exclude_language : null,
			exclude_hashtags : null,
			exclude_mentions : null
		}
	}
	result.id=sharon.app.nextID().toString()
	result.name=result.id
	return result
}

sharon.result.tweets.renderCollapsible = function(result){
	var html='<div data-resultid="'+result.id+'" class="tweetcollapsible tweetresultcollapsible'+result.id+
	'" data-role="collapsible" data-mini="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u"><h4><span class="tweetresultcollapsibleheading'+result.id+
	'">Result set: ['+result.name+'] ('+result.tweets.length+' tweets)</span></h4>'
	html+='<ul class="tweetlistitem tweetresultlist'+result.id+'" data-role="listview" data-filter="true"></ul></div>'
	return html
}

sharon.result.tweets.getFormattedText = function(status) {
	var text=status.text
	var newtext=""
    var cur=0
    var ht_idx=0
    var url_idx=0
    var user_idx=0
	while(ht_idx<status.entities.hashtags.length ||
		  url_idx<status.entities.urls.length ||
		  user_idx<status.entities.user_mentions.length){
		var ht_start=1000
		var url_start=1000
		var user_start=1000
		
		if(ht_idx<status.entities.hashtags.length){
			ht_start=status.entities.hashtags[ht_idx].indices[0]
		}
		if(url_idx<status.entities.urls.length){
			url_start=status.entities.urls[url_idx].indices[0]
		}
		if(user_idx<status.entities.user_mentions.length){
			user_start=status.entities.user_mentions[user_idx].indices[0]
		}
		
		if(ht_start<url_start && ht_start<user_start){
			var hashtag = status.entities.hashtags[ht_idx].text
			var end = status.entities.hashtags[ht_idx].indices[1]
			newtext=newtext+text.substring(cur,ht_start)+'<span class="twitterhashtag">#'+hashtag+'</span>'
			cur=end
			ht_idx+=1
			continue
		}
		if(url_start<ht_start && url_start<user_start){
			var url = status.entities.urls[url_idx].url
			var end = status.entities.urls[url_idx].indices[1]
			newtext=newtext+text.substring(cur,url_start)+'<span class="twitterurl">'+url+'</span>'
			cur=end
			url_idx+=1
			continue
		}
		if(user_start<ht_start && user_start<url_start){
			var user = status.entities.user_mentions[user_idx].screen_name
			var end = status.entities.user_mentions[user_idx].indices[1]
			newtext=newtext+text.substring(cur,user_start)+'<span class="twitteruser">@'+user+'</span>'
			cur=end
			user_idx+=1
			continue
		}
	}
	if(cur<text.length-1){
		newtext=newtext+text.substring(cur,text.length)
	}
	text=newtext
	return text
}

sharon.result.tweets.renderTweetAsLI = function(tweet){
	// tweet is just the id str
	var html='<li><a href="#" style="padding-left:3.25em !important;" data-tid="'+tweet+'" class="status'+tweet+'">'
	html+='</a></li>'
	return html
}

sharon.result.tweets.fillTweetFields = function(selector,status){
	var html=""
	html+='<img src="'+status.user.profile_image_url+'">'
	html+='<h2><span class="twitteruser">@'+status.user.screen_name+'</span> '+status.user.name+'</h2>'
	html+='<p style="white-space:normal !important;"><span class="fielddescriptor">Text:</span> '
	if(status.hasOwnProperty("retweeted_status")){
		html+='<span class="twitterretweet">'
	}
	html+=sharon.result.tweets.getFormattedText(status)
	if(status.hasOwnProperty("retweeted_status")){
		html+='</span>'
	}
	html+='</p>'
	html+='<p><span class="fielddescriptor">TID:</span> <span class="tweetid">'+status.id_str+'</span>'
	if(status.in_reply_to_status_id_str!=null){
		html+=' <span class="fielddescriptor">In reply to tweet:</span> <span class="tweetid">'+status.in_reply_to_status_id_str+'</span>'
	}
	if(status.in_reply_to_screen_name!=null){
		html+=' <span class="fielddescriptor">In reply to user:</span> <span class="twitteruser">@'+status.in_reply_to_screen_name+'</span>'
	}
	html+='</p>'
	html+='<p><span class="fielddescriptor">User description:</span> '+status.user.description+'</span></p>'
	html+='<p>'
	html+='<span class="fielddescriptor">Statuses:</span> '+status.user.statuses_count+', '
	html+='<span class="fielddescriptor">Followers:</span> '+status.user.followers_count+', '
	html+='<span class="fielddescriptor">Friends:</span> '+status.user.friends_count+', '
	html+='<span class="fielddescriptor">Favourites:</span> '+status.user.favourites_count
	html+='</p>'
	html+='<p class="ui-li-aside"><strong>'+status.created_at+'</strong></p>'
	$(selector).html(html)
}

sharon.result.tweets.renderResult = function(ui,result) {
	var html = sharon.result.tweets.renderCollapsible(result)
	$(ui).prepend(html)
	var list = ".tweetresultlist"+result.id
	for(var i=0;i<result.tweets.length;i+=1){
		var html = sharon.result.tweets.renderTweetAsLI(result.tweets[i])
		var def = sharon.storage.readStatus(result.tweets[i])
		def.progress((function(selector){
			return function(status){
				sharon.result.tweets.fillTweetFields(selector,status)
			}
		})(".status"+result.tweets[i]))
		$(list).append(html)
		$(".status"+result.tweets[i]).on("click",sharon.result.tweets.displayStatus)
	}
	$(list).listview()
	$(list).enhanceWithin()
	$(ui).enhanceWithin()
	$(ui).find(".tweetresultcollapsible"+result.id).find(".ui-collapsible-heading").on('click',function(evt,ui){
		var rid=$(this).parent().attr("data-resultid")
		sharon.page.tweets.onClickedResultCollapsible(rid)
	})
	
}

sharon.result.tweets.newTweet = function(result) {
	var list = ".tweetresultlist"+result.id
	var html = sharon.result.tweets.renderTweetAsLI(result.tweets[0])
	var def = sharon.storage.readStatus(result.tweets[0])
	def.progress((function(selector){
		return function(status){
			sharon.result.tweets.fillTweetFields(selector,status)
		}
	})(".status"+result.tweets[0]))
	$(list).prepend(html)
	$(".status"+result.tweets[0]).on("click",sharon.result.tweets.displayStatus)
	$(".tweetresultcollapsibleheading"+result.id).html('Result set: ['+result.name+'] ('+result.tweets.length+' tweets)')
	$(list).listview("refresh")
	$(".tweetresultcollapsible"+result.id).enhanceWithin()
	
}

sharon.result.tweets.displayStatus = function(evt,ui){
	var tid=$(this).attr("data-tid")
	var resultid=$(this).closest(".tweetcollapsible").attr("data-resultid")
	var result=sharon.storage.readTweetResult(resultid)
	if(result!=null){
		for(var i=0;i<result.tweets.length;i+=1){
			if(tid==result.tweets[i].status.id_str){
				var html=sharon.result.tweets.tojQM(result.tweets[i].status,"viewtweet")
				$("#popupViewTweetContent").html(html).enhanceWithin()
				$("#popupViewTweet").popup("open")
				$("#popupViewTweet").popup("reposition", {positionTo: 'window'});
				break
			}
		}
	} else {
		alert("ALPHA: error, tweet result could not be found")
	}
}

sharon.result.tweets.tojQM = function (o,basename){
    var html="";
    for(var i in o){
        if(o.hasOwnProperty(i)){
            var name = basename+"-"+i;
            switch(typeof(o[i])){
            case "string":
                html+='<form><input name="'+name+'" id="'+name+'" placeholder="'+i+'" value="'+$('<div/>').text(o[i]).html()+'" type="text"></form>';
                break;
            case "number":
                html+='<form><input name="'+name+'" id="'+name+'" placeholder="'+i+'" value="'+$('<div/>').text(o[i]).html()+'" type="text"></form>';
                break;
            case "boolean":
               // html+='<input name="'+name+'" id="'+name+'" type="checkbox">';
                break;
            case "object":
                if ($.isArray(o[i])){
                	if(o[i].length>0){
	                   // html+='<ul data-role="listview">';
	                    for(var j=0;j<o[i].length;j+=1){
	                            html+='<div>'+sharon.result.tweets.tojQM(o[i][j],name)+'</div>';
	                    }
	                    //html+='</ul>';
                	}
                } else {
                    html+='<div data-role="collapsible">';
                    html+='<h3>'+i+'</h3>';
                    html+=sharon.result.tweets.tojQM(o[i],name);
                    html+='</div>';
                }
            }
        }
    }
    return html;
}

