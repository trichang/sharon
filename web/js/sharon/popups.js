/*
 * confirmation
 */

sharon.popups.dialogMemory = {}

sharon.popups.dialogDeferred = null

sharon.popups.init = function() {
	var swatch="a"
	$( "#popupConfirm").popup({
        dismissible : false,
        theme : swatch,
        overlayTheme : swatch,
        transition : "pop",
        positionTo : "origin"		
    }).enhanceWithin();
	$( "#popupConfirmCancel" ).on('click', sharon.popups.onPopupConfirmCancel);
	$( "#popupConfirmContinue" ).on('click', sharon.popups.onPopupConfirmContinue);
	$( "#popupConfirm" ).on({
        popupafterclose: sharon.popups.onPopupConfirmClose
    });
	
	$( "#popupInfo").popup({
        dismissible : false,
        theme : swatch,
        overlayTheme : swatch,
        transition : "pop",
        positionTo : "window"		
    }).enhanceWithin();
	$( "#popupInfoOK" ).on('click', sharon.popups.onPopupInfoOK);
	$( "#popupInfo" ).on({
        popupafterclose: sharon.popups.onPopupInfoClose
    });
	
	$( "#popupViewTweet").popup({
		beforeposition: function () {
            $(this).css({
                width: window.innerWidth*0.8
                
            });
        },
        x: 0,
        y: 0,
        dismissible : true,
        theme : swatch,
        overlayTheme : swatch,
        transition : "pop",
        positionTo : "window"		
    }).enhanceWithin();
	
	$( "#popupAddCredential").popup({
        x: 0,
        y: 0,
        dismissible : true,
        theme : swatch,
        overlayTheme : swatch,
        transition : "pop",
        positionTo : "window"		
    }).enhanceWithin();
	$("#popupAddCredentialAdd").on("click",sharon.popups.onAddCredentialAdd);
	$("#popupAddCredentialCancel").on("click",sharon.popups.onAddCredentialCancel);
	//height: window.innerHeight - 14
}

sharon.popups.popupConfirmation = function(ref,ques,info,funcCancel,funcContinue,def,first) {
	/*
	 * ref: unique string for reference to state of confirmation
	 * ques: question to ask in bold for confirmation
	 * info: further information to ask
	 * funcCancel: called if user cancels action
	 * funcContinue: called if use wishes to continue
	 * def: default "don't show this again" behavior 
	 * first: if currently showing, show only once each run of the application
	 */
	if(!sharon.storage.state.hasOwnProperty("dialogMemory")){
		sharon.storage.state.dialogMemory={};
		sharon.storage.writeState();
	}
	if(sharon.storage.state.dialogMemory.hasOwnProperty(ref)){
		if(sharon.storage.state.dialogMemory[ref]==true){
			return funcContinue();
		}
	} else {
		sharon.storage.state.dialogMemory[ref]=def;
		sharon.storage.writeState();
	}
	if(first==true){
		if(sharon.popups.dialogMemory.hasOwnProperty(ref)){
			return funcContinue();
		} else {
			sharon.popups.dialogMemory[ref]=true;
		}
	}
	$("#popupConfirmQuestion").html(ques);
	$("#popupConfirmInfo").html(info);
	$("#popupConfirmMemory").prop("checked",def).checkboxradio("refresh");
	sharon.popups.popupConfirmRef = ref;
	sharon.popups.popupConfirmCallbackCancel = funcCancel;
	sharon.popups.popupConfirmCallbackContinue = funcContinue;
	$("#popupConfirm").popup("open");
};
		
sharon.popups.onPopupConfirmCancel = function (evt) {
	sharon.storage.state.dialogMemory[sharon.popups.popupConfirmRef]=$("#popupConfirmMemory").is(":checked");
	sharon.storage.writeState();
	sharon.popups.dialogDeferred=$.Deferred();
	$("#popupConfirm").popup("close");
	$.when(sharon.popups.dialogDeferred).done(function(){
		return sharon.popups.popupConfirmCallbackCancel();
	});
};
		
sharon.popups.onPopupConfirmContinue = function (evt) {
	sharon.storage.state.dialogMemory[sharon.popups.popupConfirmRef]=$("#popupConfirmMemory").is(":checked");
	sharon.storage.writeState();
	sharon.popups.dialogDeferred=$.Deferred();
	$("#popupConfirm").popup("close");
	$.when(sharon.popups.dialogDeferred).done(function(){
		return sharon.popups.popupConfirmCallbackContinue();
	});
};
		
sharon.popups.onPopupConfirmClose = function (evt) {
	sharon.popups.dialogDeferred.resolve();
};
		
/*
 * information
 */

sharon.popups.popupInfo = function(ref,heading,content,funcOK,def,first) {
	/*
	 * ref: unique string for reference to state of information
	 * heading: title for the information
	 * content: further information 
	 * funcOK: called when user presses OK
	 * def: default "don't show this again" behavior 
	 * first: if currently showing, show only once each run of the application
	 */
	if(!sharon.storage.state.hasOwnProperty("dialogMemory")){
		sharon.storage.state.dialogMemory={};
		sharon.storage.writeState();
	}
	if(sharon.storage.state.dialogMemory.hasOwnProperty(ref)){
		if(sharon.storage.state.dialogMemory[ref]==true){
			return funcOK();
		}
	} else {
		sharon.storage.state.dialogMemory[ref]=def;
		sharon.storage.writeState();
	}
	if(first==true){
		if(sharon.popups.dialogMemory.hasOwnProperty(ref)){
			return funcOK();
		} else {
			sharon.popups.dialogMemory[ref]=true;
		}
	}
	$("#popupInfoHeading").html(heading);
	$("#popupInfoContent").html(content);
	$("#popupInfoMemory").prop("checked",def).checkboxradio("refresh");
	sharon.popups.popupConfirmRef = ref;
	sharon.popups.popupConfirmCallbackOK = funcOK;
	$("#popupInfo").popup("open");
};

sharon.popups.onPopupInfoOK = function (evt) {
	sharon.storage.state.dialogMemory[sharon.popups.popupConfirmRef]=$("#popupInfoMemory").is(":checked");
	sharon.storage.writeState();
	sharon.popups.dialogDeferred=$.Deferred();
	$("#popupInfo").popup("close");
	$.when(sharon.popups.dialogDeferred).done(function(){
		return sharon.popups.popupConfirmCallbackOK();
	});
};

sharon.popups.onPopupInfoClose = function (evt) {
	sharon.popups.dialogDeferred.resolve();
};

/*
* add credential
*/

sharon.popups.onAddCredentialAdd = function(evt) {
	var cred={}
	cred.filename=$("#popupCredential-filename").val()
	cred.consumerKey=$("#popupCredential-consumerKey").val()
	cred.consumerSecret=$("#popupCredential-consumerSecret").val()
	cred.accessToken=$("#popupCredential-accessToken").val()
	cred.accessTokenSecret=$("#popupCredential-accessTokenSecret").val()
	if(cred.filename!=""){
		sharon.control.putCredentials("#settings-credentiallist",cred)
	}
	$("#popupAddCredential").popup("close")
}

sharon.popups.onAddCredentialCancel = function(evt) {
	
	$("#popupAddCredential").popup("close")
}