
sharon.storage.state = {}

sharon.storage.results = {}

sharon.storage.statuscache = {}

sharon.storage.init = function(){
	// try to read state from local storage
	sharon.storage.readState()
	console.log("sharon.storage.init: initialized")
}

// basic application state 
sharon.storage.readState = function(){
	if(window.localStorage.getItem('state')===null){
		// use default state
		console.log("sharon.storage.init: using default state")
		sharon.storage.state={
			idcount:0,
			hostname:"localhost",
			webport:8000,
			tweetstreamport:8001,
			tweetqueryport:8002,
			controlport:8003,
			tweetresultids:[],
			statuscachesize:100,
			resultsetsize:100
		}
		
	} else {
		sharon.storage.state = JSON.parse(window.localStorage.getItem('state'))
	}
}

sharon.storage.writeState = function(){
	window.localStorage.setItem('state',JSON.stringify(sharon.storage.state))
}

// raw status cache
sharon.storage.readStatus = function(tid){
	var def = $.Deferred()
	sharon.query.tweet.querycallback(def,{id_str:tid})
	return def
}

sharon.storage.writeStatus = function(status){
	
}

sharon.storage.cacheStatus = function(status){
	console.log("caching "+status.id_str)
}


// result objects
sharon.storage.readTweetResult = function(rid){
	var name="tweetresult"+rid;
	if(sharon.storage.results.hasOwnProperty(name)){
		return sharon.storage.results[name];
	}
	if(window.localStorage.getItem(name)===null){
		console.log("sharon.storage.readTweetResult: result "+rid+" does not exist")
		return null
	}
	sharon.storage.results[name]=JSON.parse(window.localStorage.getItem(name))
	return sharon.storage.results[name];
}

sharon.storage.writeTweetResult = function(result){
	var name="tweetresult"+result.id;
	window.localStorage.setItem(name,JSON.stringify(result))
	// update the master list of tweet result ids, if necessary
	var idx=sharon.storage.state.tweetresultids.indexOf(result.name)
	if(idx==-1){
		sharon.storage.state.tweetresultids.unshift(result.name)
		sharon.storage.writeState()
	}
}

sharon.storage.removeTweetResult = function(rid){
	var name="tweetresult"+rid;
	var result=sharon.storage.readTweetResult(rid)
	window.localStorage.removeItem(name)
	delete sharon.storage.results[name]
	// update the master list of tweet result ids, if necessary
	var idx=sharon.storage.state.tweetresultids.indexOf(result.name)
	if(idx>-1){
		sharon.storage.state.tweetresultids.splice(idx,1)
		sharon.storage.writeState()
	}
}




