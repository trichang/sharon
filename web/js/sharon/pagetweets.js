
sharon.page.tweets = {}

sharon.page.tweets.currentResult = null

sharon.page.tweets.init = function(evt,ui) {
	$("#tweets-resultpanel-deletebutton").on("click",sharon.page.tweets.onResultPanelDeleteButton);
	$("#tweets-resultpanel-querybutton").on("click",sharon.page.tweets.onResultPanelQueryButton)
	$("#tweets-resultpanel-tweetsresultid").on("change",sharon.page.tweets.onResultPanelResultIDChange)
	$("#tweets-resultpanel-streambutton").on("click",sharon.page.tweets.onResultPanelStreamButton)
	$("#tweets-resultpanel-controlstream").on("click",sharon.page.tweets.onResultPanelControlStream)
	$("#tweets-resultpanel-controlpause").on("click",sharon.page.tweets.onResultPanelControlPause)
	$("#tweets-resultpanel-controlstop").on("click",sharon.page.tweets.onResultPanelControlStop)
	
	// load all existing tweet results and render them
	setTimeout(function(){
	for(var i=sharon.storage.state.tweetresultids.length-1;i>=0;i-=1){
		var result=sharon.storage.readTweetResult(md5(sharon.storage.state.tweetresultids[i]))
		sharon.page.tweets.renderResult(result)
	}
	},5000)
	console.log('sharon.page.tweets.init: initialized');
}

sharon.page.tweets.onPageBeforeShow = function(evt,ui){
	$("#page-header-heading").html("Tweet Results")
	if(sharon.page.tweets.currentResult==null){
		sharon.page.tweets.clearParameters()
	} else {
		sharon.page.tweets.populateParameters(sharon.page.tweets.currentResult)
	}
}

sharon.page.tweets.onPageShow = function(evt,ui){
	sharon.popups.popupInfo("sharon_page_tweets",
			"Tweet Results Page",
			"<p>On this page you can query the database for tweets and receive live streams of tweets.</p>",
			function(){},true,true);
}

sharon.page.tweets.onTransition = function(evt,ui){
	
}


sharon.page.tweets.onPageBeforeHide = function(evt,ui){

}

sharon.page.tweets.onResultPanelQueryButton = function(evt,ui) {	
	sharon.popups.popupConfirmation("sharon_page_tweets_query",
			"Query?",
			"<p>This action will undertake a query and add any returned tweets to the specified Result Set. "+
			"If the specified Result Set does not already exist or is left unspecified, a new one will be created. Returned tweets "+
			"are only added if they are unique.</p>",
			function(){},function(){
				// get the intended result set id
				var newname = sharon.page.util.readField("#tweets-resultid")
				var newid = md5(newname)
			
				// if the id matches an existing id then make sure it is loaded
				var idx=sharon.storage.state.tweetresultids.indexOf(newname)
				if(idx>-1){
					sharon.page.tweets.currentResult=sharon.storage.readTweetResult(newid)
					// grab any new parameters
					sharon.page.tweets.getParameters(sharon.page.tweets.currentResult)
					sharon.storage.writeTweetResult(sharon.page.tweets.currentResult)
				} else {
					// the newid field is referring to a non-existent result set
					var result = sharon.result.tweets.createResult()
					if(newname!=null){
						result.id=newid
						result.name=newname
					}
					sharon.page.util.populateField("#tweets-resultid",result.name)
					sharon.page.tweets.getParameters(result)
					sharon.page.tweets.renderResult(result)
					sharon.storage.writeTweetResult(result)
					sharon.page.tweets.currentResult = result
				}
				// make the query
				sharon.query.tweet.query(sharon.page.tweets.currentResult)
			},true,false)
}

sharon.page.tweets.onResultPanelStreamButton = function(evt,ui) {
	$("#tweets-resultpanel-streamingcontrols").show(2)
}

sharon.page.tweets.onResultPanelDeleteButton = function(evt,ui) {
	sharon.page.tweets.currentResult=sharon.storage.readTweetResult(
		md5(sharon.page.util.readField("#tweets-resultid"))	
	)
	if(sharon.page.tweets.currentResult==null){
		// attempting to delete non-existent result
		sharon.popups.popupInfo("sharon_page_tweets_deletebutton",
				"Attempted to delete non-existent result",
				"<p>The result id parameter does not match any known result.</p>",
				function(){},false,false);
		return
	}
	sharon.popups.popupConfirmation("sharon_page_tweets_delete",
		"Delete?",
		"<p>This action will permanently remove the result from local storage.</p>",
		function(){},function(){
			sharon.page.tweets.removeResult(sharon.page.tweets.currentResult)
			sharon.storage.removeTweetResult(sharon.page.tweets.currentResult.id)
			sharon.page.tweets.clearParameters()
		},false,false);
}


sharon.page.tweets.onResultPanelControlStream = function(evt,ui){
	
}

sharon.page.tweets.onResultPanelControlPause = function(evt,ui){
	
}

sharon.page.tweets.onResultPanelControlStop  = function(evt,ui){
	$("#tweets-resultpanel-streamingcontrols").hide(2)
}

sharon.page.tweets.onClickedResultCollapsible = function(id){
	sharon.popups.popupInfo("sharon_page_tweets_collapsible",
			"Clicking on a Result Set",
			"<p>When you click on a Result Set it will collapse or expand and the parameters "+
			"last used for the Result Set will be loaded into the parameter fields.</p>",
			function(){
				sharon.page.tweets.checkAndStore()
				sharon.page.tweets.currentResult=sharon.storage.readTweetResult(id)
				sharon.page.tweets.populateParameters(sharon.page.tweets.currentResult)
			},true,true);
}

sharon.page.tweets.onResultPanelResultIDChange = function(){
	sharon.page.tweets.checkAndStore()
	var newid = md5(sharon.page.util.readField("#tweets-resultsid"))
	sharon.page.tweets.currentResult=sharon.storage.readTweetResult(newid)
	if(sharon.page.tweets.currentResult!=null){
		sharon.page.tweets.populateParameters(sharon.page.tweets.currentResult)
	} else {
		sharon.page.tweets.clearParameters()
	}
}

sharon.page.tweets.checkAndStore = function(){
	// if a current result is loaded then update it with current form elements
	// and store it if it exists in the list of results
	if(sharon.page.tweets.currentResult!=null){
		sharon.page.tweets.getParameters(sharon.page.tweets.currentResult)
		var id = sharon.page.tweets.currentResult.name
		var idx=sharon.storage.state.tweetresultids.indexOf(id)
		if(idx>-1){
			sharon.storage.writeTweetResult(sharon.page.tweets.currentResult)
		}
	}
}

////////////////////////////////////
// functions for modifying the DOM//
////////////////////////////////////
sharon.page.tweets.clearParameters = function(){
	sharon.page.util.populateField("#tweets-resultid",null)
	
	// must match
	sharon.page.util.populateField("#tweets-id_str",null)
	sharon.page.util.populateField("#tweets-screenname",null)
	sharon.page.util.populateField("#tweets-id_str-inreplyto",null)
	sharon.page.util.populateField("#tweets-screenname-inreplyto",null)
	sharon.page.util.populateField("#tweets-id_str-retweeted",null)
	sharon.page.util.populateField("#tweets-screenname-retweeted",null)
	sharon.page.util.populateField("#tweets-statusescount-atleast",null)
	sharon.page.util.populateField("#tweets-statusescount-atmost",null)
	sharon.page.util.populateField("#tweets-followerscount-atleast",null)
	sharon.page.util.populateField("#tweets-followerscount-atmost",null)
	sharon.page.util.populateField("#tweets-friendscount-atleast",null)
	sharon.page.util.populateField("#tweets-friendscount-atmost",null)
	sharon.page.util.populateField("#tweets-favouritescount-atleast",null)
	sharon.page.util.populateField("#tweets-favouritescount-atmost",null)
	sharon.page.util.populateField("#tweets-retweetedcount-atleast",null)
	sharon.page.util.populateField("#tweets-retweetedcount-atmost",null)
	sharon.page.util.populateField("#tweets-language",null)
	sharon.page.util.populateField("#tweets-hashtags",null)
	sharon.page.util.populateField("#tweets-mentions",null)
	
	// exclude
	sharon.page.util.populateField("#tweets-exclude-id_str",null)
	sharon.page.util.populateField("#tweets-exclude-screenname",null)
	sharon.page.util.populateField("#tweets-exclude-id_str-inreplyto",null)
	sharon.page.util.populateField("#tweets-exclude-screenname-inreplyto",null)
	sharon.page.util.populateField("#tweets-exclude-id_str-retweeted",null)
	sharon.page.util.populateField("#tweets-exclude-screenname-retweeted",null)
	sharon.page.util.populateField("#tweets-exclude-statusescount-atleast",null)
	sharon.page.util.populateField("#tweets-exclude-statusescount-atmost",null)
	sharon.page.util.populateField("#tweets-exclude-followerscount-atleast",null)
	sharon.page.util.populateField("#tweets-exclude-followerscount-atmost",null)
	sharon.page.util.populateField("#tweets-exclude-friendscount-atleast",null)
	sharon.page.util.populateField("#tweets-exclude-friendscount-atmost",null)
	sharon.page.util.populateField("#tweets-exclude-favouritescount-atleast",null)
	sharon.page.util.populateField("#tweets-exclude-favouritescount-atmost",null)
	sharon.page.util.populateField("#tweets-exclude-retweetedcount-atleast",null)
	sharon.page.util.populateField("#tweets-exclude-retweetedcount-atmost",null)
	sharon.page.util.populateField("#tweets-exclude-language",null)
	sharon.page.util.populateField("#tweets-exclude-hashtags",null)
	sharon.page.util.populateField("#tweets-exclude-mentions",null)
}

sharon.page.tweets.populateParameters = function(result){
	// put all of the parameters from the result object into the page
	sharon.page.util.populateField("#tweets-resultid",result.name)
	
	// must match
	sharon.page.util.populateField("#tweets-id_str",result.parameter.id_str)
	sharon.page.util.populateField("#tweets-screenname",result.parameter.screenname)
	sharon.page.util.populateField("#tweets-id_str-inreplyto",result.parameter.id_str_inreplyto)
	sharon.page.util.populateField("#tweets-screenname-inreplyto",result.parameter.screenname_inreplyto)
	sharon.page.util.populateField("#tweets-id_str-retweeted",result.parameter.id_str_retweeted)
	sharon.page.util.populateField("#tweets-screenname-retweeted",result.parameter.screenname_retweeted)
	sharon.page.util.populateField("#tweets-statusescount-atleast",result.parameter.statusescount_atleast)
	sharon.page.util.populateField("#tweets-statusescount-atmost",result.parameter.statusescount_atmost)
	sharon.page.util.populateField("#tweets-followerscount-atleast",result.parameter.followerscount_atleast)
	sharon.page.util.populateField("#tweets-followerscount-atmost",result.parameter.followerscount_atmost)
	sharon.page.util.populateField("#tweets-friendscount-atleast",result.parameter.friendscount_atleast)
	sharon.page.util.populateField("#tweets-friendscount-atmost",result.parameter.friendscount_atmost)
	sharon.page.util.populateField("#tweets-favouritescount-atleast",result.parameter.favouritescount_atleast)
	sharon.page.util.populateField("#tweets-favouritescount-atmost",result.parameter.favouritescount_atmost)
	sharon.page.util.populateField("#tweets-retweetedcount-atleast",result.parameter.retweetedcount_atleast)
	sharon.page.util.populateField("#tweets-retweetedcount-atmost",result.parameter.retweetedcount_atmost)
	sharon.page.util.populateField("#tweets-language",result.parameter.language)
	sharon.page.util.populateField("#tweets-hashtags",result.parameter.hashtags)
	sharon.page.util.populateField("#tweets-mentions",result.parameter.mentions)
	
	// exclude 
	sharon.page.util.populateField("#tweets-exclude-id_str",result.parameter.exclude_id_str)
	sharon.page.util.populateField("#tweets-exclude-screenname",result.parameter.exclude_screenname)
	sharon.page.util.populateField("#tweets-exclude-id_str-inreplyto",result.parameter.exclude_id_str_inreplyto)
	sharon.page.util.populateField("#tweets-exclude-screenname-inreplyto",result.parameter.exclude_screenname_inreplyto)
	sharon.page.util.populateField("#tweets-exclude-id_str-retweeted",result.parameter.exclude_id_str_retweeted)
	sharon.page.util.populateField("#tweets-exclude-screenname-retweeted",result.parameter.exclude_screenname_retweeted)
	sharon.page.util.populateField("#tweets-exclude-statusescount-atleast",result.parameter.exclude_statusescount_atleast)
	sharon.page.util.populateField("#tweets-exclude-statusescount-atmost",result.parameter.exclude_statusescount_atmost)
	sharon.page.util.populateField("#tweets-exclude-followerscount-atleast",result.parameter.exclude_followerscount_atleast)
	sharon.page.util.populateField("#tweets-exclude-followerscount-atmost",result.parameter.exclude_followerscount_atmost)
	sharon.page.util.populateField("#tweets-exclude-friendscount-atleast",result.parameter.exclude_friendscount_atleast)
	sharon.page.util.populateField("#tweets-exclude-friendscount-atmost",result.parameter.exclude_friendscount_atmost)
	sharon.page.util.populateField("#tweets-exclude-favouritescount-atleast",result.parameter.exclude_favouritescount_atleast)
	sharon.page.util.populateField("#tweets-exclude-favouritescount-atmost",result.parameter.exclude_favouritescount_atmost)
	sharon.page.util.populateField("#tweets-exclude-retweetedcount-atleast",result.parameter.exclude_retweetedcount_atleast)
	sharon.page.util.populateField("#tweets-exclude-retweetedcount-atmost",result.parameter.exclude_retweetedcount_atmost)
	sharon.page.util.populateField("#tweets-exclude-language",result.parameter.exclude_language)
	sharon.page.util.populateField("#tweets-exclude-hashtags",result.parameter.exclude_hashtags)
	sharon.page.util.populateField("#tweets-exclude-mentions",result.parameter.exclude_mentions)
	
}

sharon.page.tweets.getParameters = function(result){
	// get parameters into the result object
	result.name = sharon.page.util.readField("#tweets-resultid")
	result.id = md5(result.name)
	
	// must match
	result.parameter.id_str = sharon.page.util.readField("#tweets-id_str")
	result.parameter.screenname = sharon.page.util.readField("#tweets-screenname")
	result.parameter.id_str_inreplyto= sharon.page.util.readField("#tweets-id_str-inreplyto")
	result.parameter.screenname_inreplyto= sharon.page.util.readField("#tweets-screenname-inreplyto")
	result.parameter.id_str_retweeted= sharon.page.util.readField("#tweets-id_str-retweeted")
	result.parameter.screenname_retweeted= sharon.page.util.readField("#tweets-screenname-retweeted")
	result.parameter.statusescount_atleast= sharon.page.util.readField("#tweets-statusescount-atleast")
	result.parameter.statusescount_atmost= sharon.page.util.readField("#tweets-statusescount-atmost")
	result.parameter.followerscount_atleast= sharon.page.util.readField("#tweets-followerscount-atleast")
	result.parameter.followerscount_atmost= sharon.page.util.readField("#tweets-followerscount-atmost")
	result.parameter.friendscount_atleast= sharon.page.util.readField("#tweets-friendscount-atleast")
	result.parameter.friendscount_atmost= sharon.page.util.readField("#tweets-friendscount-atmost")
	result.parameter.favouritescount_atleast= sharon.page.util.readField("#tweets-favouritescount-atleast")
	result.parameter.favouritescount_atmost= sharon.page.util.readField("#tweets-favouritescount-atmost")
	result.parameter.retweetedcount_atleast= sharon.page.util.readField("#tweets-retweetedcount-atleast")
	result.parameter.retweetedcount_atmost= sharon.page.util.readField("#tweets-retweetedcount-atmost")
	result.parameter.language= sharon.page.util.readField("#tweets-language")
	result.parameter.hashtags= sharon.page.util.readField("#tweets-hashtags")
	result.parameter.mentions= sharon.page.util.readField("#tweets-mentions")
	
	// exclude
	result.parameter.exclude_id_str = sharon.page.util.readField("#tweets-exclude-id_str")
	result.parameter.exclude_screenname = sharon.page.util.readField("#tweets-exclude-screenname")
	result.parameter.exclude_id_str_inreplyto= sharon.page.util.readField("#tweets-exclude-id_str-inreplyto")
	result.parameter.exclude_screenname_inreplyto= sharon.page.util.readField("#tweets-exclude-screenname-inreplyto")
	result.parameter.exclude_id_str_retweeted= sharon.page.util.readField("#tweets-exclude-id_str-retweeted")
	result.parameter.exclude_screenname_retweeted= sharon.page.util.readField("#tweets-exclude-screenname-retweeted")
	result.parameter.exclude_statusescount_atleast= sharon.page.util.readField("#tweets-exclude-statusescount-atleast")
	result.parameter.exclude_statusescount_atmost= sharon.page.util.readField("#tweets-exclude-statusescount-atmost")
	result.parameter.exclude_followerscount_atleast= sharon.page.util.readField("#tweets-exclude-followerscount-atleast")
	result.parameter.exclude_followerscount_atmost= sharon.page.util.readField("#tweets-exclude-followerscount-atmost")
	result.parameter.exclude_friendscount_atleast= sharon.page.util.readField("#tweets-exclude-friendscount-atleast")
	result.parameter.exclude_friendscount_atmost= sharon.page.util.readField("#tweets-exclude-friendscount-atmost")
	result.parameter.exclude_favouritescount_atleast= sharon.page.util.readField("#tweets-exclude-favouritescount-atleast")
	result.parameter.exclude_favouritescount_atmost= sharon.page.util.readField("#tweets-exclude-favouritescount-atmost")
	result.parameter.exclude_retweetedcount_atleast= sharon.page.util.readField("#tweets-exclude-retweetedcount-atleast")
	result.parameter.exclude_retweetedcount_atmost= sharon.page.util.readField("#tweets-exclude-retweetedcount-atmost")
	result.parameter.exclude_language= sharon.page.util.readField("#tweets-exclude-language")
	result.parameter.exclude_hashtags= sharon.page.util.readField("#tweets-exclude-hashtags")
	result.parameter.exclude_mentions= sharon.page.util.readField("#tweets-exclude-mentions")
}

sharon.page.tweets.renderResult = function(result){
	// add the result to the top of the results list
	sharon.result.tweets.renderResult("#tweets-resultpanel-results",result)
}

sharon.page.tweets.removeResult = function(result){
	// remove the result from the list
	$("#tweets-resultpanel-results").find(".tweetresultcollapsible"+result.id).remove()
}