

sharon.query.tweet = {}

sharon.query.tweet.endpoints = {}

sharon.query.tweet.makeEndpoint = function(result){
	// make an endpoint to the relevant service
	var URL='ws://'+sharon.storage.state.hostname+':'+sharon.storage.state.tweetqueryport;
	URL=URL+'/websocket/resultservice';
	var socket = new WebSocket(URL);
	var endpoint = {}
	endpoint.result=result
	endpoint.socket=socket
	endpoint.onopen = (function(endpoint){
							return function(){
								// submit the query
								var query = {
									cmd : "query",
									parameters : endpoint.result.parameter
								}
								endpoint.socket.send(JSON.stringify(query))
							}
						})(endpoint);
	endpoint.onclose = (function(endpoint){
							return function(){
								delete sharon.query.tweet.endpoints['tweetresult'+endpoint.result.id]
							}
						})(endpoint);	
	endpoint.onmessage = (function(endpoint){
							return function(e){
								var msg=JSON.parse(e.data)
								if(msg.hasOwnProperty('status')){
									// process the status
									var id = msg.status.id_str
									if(!endpoint.result.tids.hasOwnProperty('t'+id)){
										endpoint.result.tids['t'+id]=true
										endpoint.result.tweets.unshift(id)
										//var newTweet=sharon.result.tweets.createTweet()
										//newTweet.status=msg.status
										//endpoint.result.tweets.unshift(newTweet)
										sharon.result.tweets.newTweet(endpoint.result)
										sharon.storage.writeTweetResult(endpoint.result)
									} else {
										// ignore duplicate
									}
								}
								if(msg.hasOwnProperty('cmd')){
									// process the command
									if(msg.cmd == 'closing'){
										endpoint.socket.close()
									}
								}
								
							}
						})(endpoint);
	socket.onopen=endpoint.onopen 
	socket.onclose=endpoint.onclose 
	socket.onmessage=endpoint.onmessage
	return endpoint
}

sharon.query.tweet.makeEndpointCallback = function(callback,parameters){
	// make an endpoint to the relevant service
	var URL='ws://'+sharon.storage.state.hostname+':'+sharon.storage.state.tweetqueryport;
	URL=URL+'/websocket/resultservice';
	var socket = new WebSocket(URL);
	var endpoint = {}
	endpoint.callback=callback // jquery deferred object
	endpoint.parameter=parameters
	endpoint.socket=socket
	endpoint.onopen = (function(endpoint){
							return function(){
								// submit the query
								var query = {
									cmd : "query",
									parameters : endpoint.parameter
								}
								endpoint.socket.send(JSON.stringify(query))
							}
						})(endpoint);
	endpoint.onclose = (function(endpoint){
							return function(){
								endpoint.callback.done()
								delete sharon.query.tweet.endpoints['tweetcallback'+endpoint.parameter.id_str]
							}
						})(endpoint);	
	endpoint.onmessage = (function(endpoint){
							return function(e){
								var msg=JSON.parse(e.data)
								if(msg.hasOwnProperty('status')){
									// process the status
									endpoint.callback.notify(msg.status)
								}
								if(msg.hasOwnProperty('cmd')){
									// process the command
									if(msg.cmd == 'closing'){
										endpoint.socket.close()
									}
								}
								
							}
						})(endpoint);
	socket.onopen=endpoint.onopen 
	socket.onclose=endpoint.onclose 
	socket.onmessage=endpoint.onmessage
	return endpoint
}

sharon.query.tweet.query = function(result) {
	if(sharon.query.tweet.endpoints.hasOwnProperty('tweetresult'+result.id)){
		
	} else {
		var endpoint = sharon.query.tweet.makeEndpoint(result)
		sharon.query.tweet.endpoints['tweetresult'+result.id]=endpoint
	}
}

sharon.query.tweet.querycallback = function(callback,parameters) {
	if(sharon.query.tweet.endpoints.hasOwnProperty('tweetcallback'+parameters.id_str)){
		
	} else {
		var endpoint = sharon.query.tweet.makeEndpointCallback(callback,parameters)
		sharon.query.tweet.endpoints['tweetcallback'+parameters.id_str]=endpoint
	}
}
