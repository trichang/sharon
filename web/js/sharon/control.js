sharon.control = {}

sharon.control.endpoints = {}

sharon.control.makeEndpoint = function(ui){
	// make an endpoint to the relevant service
	var URL='ws://'+sharon.storage.state.hostname+':'+sharon.storage.state.controlport;
	URL=URL+'/websocket/resultservice';
	var socket = new WebSocket(URL);
	var endpoint = {}
	endpoint.ui=ui
	endpoint.socket=socket
	endpoint.onopen = (function(endpoint){
							return function(){
								// submit the query
								var query = {
									cmd : "getcredentials",
								}
								console.log("sending "+JSON.stringify(query))
								endpoint.socket.send(JSON.stringify(query))
							}
						})(endpoint);
	endpoint.onclose = (function(endpoint){
							return function(){
								delete sharon.control.endpoints['getcredentials']
							}
						})(endpoint);	
	endpoint.onmessage = (function(endpoint){
							return function(e){
								var msg=JSON.parse(e.data)
								if(msg.hasOwnProperty('credentials')){
									// process the message
									var creds = msg.credentials
									if(creds.length>0){
										var html='<ul class="credlist" data-role="listview" data-inset="true">'
										for(var i=0;i<creds.length;i+=1){
											var cred = creds[i]
											html+='<li data-icon="delete"><a href="#" class="creditem">'
											html+='<h4 class="creditemfilename">'+cred.filename+'</h4>'
											html+='<p>consumerKey: '+cred.consumerKey+'</p>'
											html+='<p>consumerSecret: '+cred.consumerSecret+'</p>'
											html+='<p>accessToken: '+cred.accessToken+'</p>'
											html+='<p>accessTokenSecret: '+cred.accessTokenSecret+'</p>'
											html+='</a></li>'
										}	
										html+='</ul>'
										$(endpoint.ui).html(html)
										$(endpoint.ui).find(".credlist").listview()
										$(endpoint.ui).enhanceWithin()
										$(".creditem").on("click",sharon.page.settings.onDeleteCredential)
									} else {
										html="<b>Warning: using default credential. Please add a credential of your own.</b>"
										$(endpoint.ui).html(html)
									}
								}
								endpoint.socket.close()
							}
						})(endpoint);
	socket.onopen=endpoint.onopen 
	socket.onclose=endpoint.onclose 
	socket.onmessage=endpoint.onmessage
	return endpoint
}

sharon.control.makeEndpointPut = function(ui,cred){
	// make an endpoint to the relevant service
	var URL='ws://'+sharon.storage.state.hostname+':'+sharon.storage.state.controlport;
	URL=URL+'/websocket/resultservice';
	var socket = new WebSocket(URL);
	var endpoint = {}
	endpoint.ui=ui
	endpoint.cred=cred
	endpoint.socket=socket
	endpoint.onopen = (function(endpoint){
							return function(){
								// submit the query
								var query = {
									cmd : "putcredential",
									credential : endpoint.cred
								}
								console.log("sending "+JSON.stringify(query))
								endpoint.socket.send(JSON.stringify(query))
							}
						})(endpoint);
	endpoint.onclose = (function(endpoint){
							return function(){
								sharon.control.getCredentials(endpoint.ui)
								delete sharon.control.endpoints['putcredential']
							}
						})(endpoint);	
	endpoint.onmessage = (function(endpoint){
							return function(e){
								var msg=JSON.parse(e.data)
								
								endpoint.socket.close()
							}
						})(endpoint);
	socket.onopen=endpoint.onopen 
	socket.onclose=endpoint.onclose 
	socket.onmessage=endpoint.onmessage
	return endpoint
}

sharon.control.makeEndpointDelete = function(ui,filename){
	// make an endpoint to the relevant service
	var URL='ws://'+sharon.storage.state.hostname+':'+sharon.storage.state.controlport;
	URL=URL+'/websocket/resultservice';
	var socket = new WebSocket(URL);
	var endpoint = {}
	endpoint.ui=ui
	endpoint.filename=filename
	endpoint.socket=socket
	endpoint.onopen = (function(endpoint){
							return function(){
								// submit the query
								var query = {
									cmd : "deletecredential",
									filename : endpoint.filename
								}
								console.log("sending "+JSON.stringify(query))
								endpoint.socket.send(JSON.stringify(query))
							}
						})(endpoint);
	endpoint.onclose = (function(endpoint){
							return function(){
								sharon.control.getCredentials(endpoint.ui)
								delete sharon.control.endpoints['deletecredential']
							}
						})(endpoint);	
	endpoint.onmessage = (function(endpoint){
							return function(e){
								var msg=JSON.parse(e.data)
								
								endpoint.socket.close()
							}
						})(endpoint);
	socket.onopen=endpoint.onopen 
	socket.onclose=endpoint.onclose 
	socket.onmessage=endpoint.onmessage
	return endpoint
}

sharon.control.getCredentials = function(ui) {
	if(sharon.control.endpoints.hasOwnProperty('getcredentials')){
		// TODO: already an outstanding request
	} else {
		var endpoint = sharon.control.makeEndpoint(ui);
		sharon.control.endpoints['getcredentials']=endpoint
	}
}

sharon.control.putCredentials = function(ui,cred) {
	if(sharon.control.endpoints.hasOwnProperty('putcredential')){
		// TODO: already an outstanding request
	} else {
		var endpoint = sharon.control.makeEndpointPut(ui,cred);
		sharon.control.endpoints['putcredential']=endpoint
	}
}

sharon.control.deleteCredentials = function(ui,filename) {
	if(sharon.control.endpoints.hasOwnProperty('deletecredential')){
		// TODO: already an outstanding request
	} else {
		var endpoint = sharon.control.makeEndpointDelete(ui,filename);
		sharon.control.endpoints['deletecredential']=endpoint
	}
}
