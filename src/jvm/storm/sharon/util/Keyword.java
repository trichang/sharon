package storm.sharon.util;

import org.bson.Document;

@SuppressWarnings("rawtypes")
public class Keyword implements Comparable {
	
	
	private String keyword;
	private Integer type; // type 0 - user added; 1 - system selected; 
	private Boolean isBlacklisted; 
	private Boolean isCurrent; // this is true if the keyword is in the current active key list
	
	public Keyword(String keyword, Integer type, Boolean isCurrent) {
		super();
		this.keyword = keyword;
		this.type = type;
		this.isCurrent = isCurrent;
		this.isBlacklisted = false;
	}
	
	public Keyword(String keyword, Integer type, Boolean isCurrent,
			Boolean isBlacklisted) {
		this(keyword, type, isCurrent);
		this.isBlacklisted = isBlacklisted;
	}
	
	public Keyword(Document doc) {
		this.keyword = doc.getString("keyword");
		this.type = doc.getInteger("type");
		this.isCurrent = doc.getBoolean("isCurrent");
		this.isBlacklisted = doc.getBoolean("isBlacklisted");
	}
	
	
	

	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Boolean getIsCurrent() {
		return isCurrent;
	}
	public void setIsCurrent(Boolean isCurrent) {
		this.isCurrent = isCurrent;
	}
	
	public Boolean getIsBlacklisted() {
		return isBlacklisted;
	}
	public void setIsBlacklisted(Boolean isBlacklisted) {
		this.isBlacklisted = isBlacklisted;
	}
	

	@Override
	public int compareTo(Object arg0) {
		Keyword k = (Keyword) arg0;
		return this.getKeyword().compareTo(k.getKeyword());
		//return 0;
	}
	
	
	

}
