package storm.sharon.util;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import storm.sharon.bolt.TopicTrackGrouping;

/** This class provides methods for processing queries
 *  
 * @author karus
 *
 */

public class QueryProcessor {

	private static Log log = LogFactory.getLog(QueryProcessor.class);

	public static boolean applyFilter(Document values, Document query) {
		boolean cont = true;
		
		ArrayList<Document> conditions = (ArrayList<Document>)query.get("SelectConditions");
		
		cont = processCondition(values, conditions);

		return cont;
	}

	private static boolean processCondition(Document values, ArrayList<Document>  conditions) {
		
		boolean output = false;
		
		for (Document condition: conditions) {		
			String type = condition.getString("type");
			if (type.compareTo("String") == 0) {
				output = compareString(values, condition);
			} else if (type.compareTo("StringList") == 0) {
				output = compareInteger(values, condition);
			} else if (type.compareTo("Integer") == 0) {
				output = compareStringList(values, condition);
			}
			String logical = condition.getString("logicalOperator");
			
			if ( (logical.compareTo(null) == 0) ||
					(output == true && logical.compareTo("OR") == 0) ||
					(output == false && logical.compareTo("AND") == 0)) {
				break;
			}
		}
		
		return false;
	}

	
	private static boolean compareString(Document values, Document condition) {
		
		boolean output;
		
		String fieldName = condition.getString("fieldName");
		String relationalOp = condition.getString("relationalOperator");
		String value = condition.getString("fieldValue");
		String actualValue = values.getString(fieldName);
		
		switch (relationalOp) {
			case ("EQ"):
				output = (actualValue.compareTo(value) == 0); 
				break;
			case ("LEQ"):
				output = (actualValue.compareTo(value) <= 0);
				break;
			case ("LT"):
				output = (actualValue.compareTo(value) < 0);
			    break;
			case ("GEQ"):
				output = (actualValue.compareTo(value) >= 0);
			    break;
			case ("GT"):
				output = (actualValue.compareTo(value) > 0);
			    break;
			case ("IN"):
				//TO DO
				output = false;
				break;
			case ("NOT_IN"):
				//TO DO
				output = false;
				break;
			default:
				output = false;
				break;
		}
		return output;
	}

	private static boolean compareStringList(Document values, Document condition) {
		
		boolean output;
		
		String fieldName = condition.getString("fieldName");
		String relationalOp = condition.getString("relationalOperator");
		String value = condition.getString("fieldValue");
		ArrayList<String> actualValues = (ArrayList<String>)values.get(fieldName);
		
		switch (relationalOp) {
			case ("EQ"):
				output = false;
			    for (String actualValue: actualValues) {
			    	if ((actualValue.compareTo(value) == 0)) {
			    	    output = true;
			    	    break;
			    	}
			    }
				break;
			case ("LEQ"):
				output = false;
		    	for (String actualValue: actualValues) {
		    		if ((actualValue.compareTo(value) <= 0)) {
		    			output = true;
		    			break;
		    		}
		    	}
				break;
			case ("LT"):
				output = false;
		    	for (String actualValue: actualValues) {
		    		if ((actualValue.compareTo(value) < 0)) {
		    			output = true;
		    			break;
		    		}
		    	}
		    	break;
			case ("GEQ"):
				output = false;
		    	for (String actualValue: actualValues) {
		    		if ((actualValue.compareTo(value) >= 0)) {
		    			output = true;
		    			break;
		    		}
		    	}
		    	break;
			case ("GT"):
				output = false;
		    	for (String actualValue: actualValues) {
		    		if ((actualValue.compareTo(value) > 0)) {
		    			output = true;
		    			break;
		    		}
		    	}
		    	break;
			case ("IN"):
				//TO DO
				output = false;
				break;
			case ("NOT_IN"):
				//TO DO
				output = false;
				break;
			default:
				output = false;
				break;
		}
		return output;
	}
	
	
	
	
	private static boolean compareInteger(Document values, Document condition) {
		
		boolean output;
		
		String fieldName = condition.getString("fieldName");
		String relationalOp = condition.getString("relationalOperator");
		Integer value = condition.getInteger("fieldValue");
		Integer actualValue = values.getInteger(fieldName);
		
		switch (relationalOp) {
			case ("EQ"):
				output = (actualValue == value); 
				break;
			case ("LEQ"):
				output = (actualValue <= 0);
				break;
			case ("LT"):
				output = (actualValue < 0);
			    break;
			case ("GEQ"):
				output = (actualValue >= 0);
			    break;
			case ("GT"):
				output = (actualValue > 0);
			    break;
			case ("IN"):
				//TO DO
				output = false;
				break;
			case ("NOT_IN"):
				//TO DO
				output = false;
				break;
			default:
				output = false;
				break;
		}
		return output;
	}



	public static Document filterTweet(Document sharon_tweet, Document query) {
		
		boolean matched = false;
		
		matched = applyFilter(sharon_tweet, query);
		Document doc = null;
		if (matched) {
			doc = new Document("QueryId", query.getString("QueryId"));
			ArrayList<Document> selectFields = (ArrayList<Document>)query.get("SelectFields");
			
			for (Document selectField: selectFields) {
				String type = selectField.getString("type");
				String fieldName = selectField.getString("fieldName");
				if ( type.compareTo("String") == 0 ) {
					String fieldValue = sharon_tweet.getString(fieldName);
					doc.append(fieldName, fieldValue);
				} else if ( type.compareTo("Integer") == 0 ) {
					Integer fieldValue = sharon_tweet.getInteger(fieldName);
					doc.append(fieldName, fieldValue);
				} else {
					//To do  - string list
				}
			}
		} else {
			log.info("Tweet does not meet the filter condition - returning null");
		}
		return doc;
	}
	
}
