package storm.sharon.util;

/**
 * Created by tri on 17/04/16.
 */
public class Settings {
    private static Settings instance;

    public final int port = 8088;
    public final String host = "127.0.0.1";

    public final int numGraphBolts = 4; // not limited
    public final int numClusterBolts = 4; // not limited

    protected Settings() {}

    public static Settings getInstance() {
        if (instance == null) instance = new Settings();

        return instance;
    }

}
