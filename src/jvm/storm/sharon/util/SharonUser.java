/**  
* User.java - class for represents a User.  
* @author  Shanika Karunasekera
* @version 1.0 
*/
package storm.sharon.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.bson.Document;


@SuppressWarnings("rawtypes")
public class SharonUser implements Comparable {
	
	public static final String DEFAULT_SHARON_USER = "SharonDefault";
	private String userName;
	private int numActiveSessions = 0; 
	private HashSet<String> tweetQueue = new HashSet<String>();
	private HashSet<String> discussionQueue = new HashSet<String>();
	private HashSet<String> tupleQueue = new HashSet<String>();
	private HashSet<String> communityQueue = new HashSet<String>();
	
	
	
	public int getNumActiveSessions() {
		return numActiveSessions;
	}

	public void setNumActiveSessions(int numActiveSessions) {
		this.numActiveSessions = numActiveSessions;
	}

	public HashSet<String> getTweetQueue() {
		return tweetQueue;
	}

	public void setTweetQueue(HashSet<String> tweetQueue) {
		this.tweetQueue = tweetQueue;
	}

	public HashSet<String> getDiscussionQueue() {
		return discussionQueue;
	}

	public void setDiscussionQueue(HashSet<String> discussionQueue) {
		this.discussionQueue = discussionQueue;
	}

	public HashSet<String> getTupleQueue() {
		return tupleQueue;
	}

	public void setTupleQueue(HashSet<String> tupleQueue) {
		this.tupleQueue = tupleQueue;
	}

	public HashSet<String> getCommunityQueue() {
		return communityQueue;
	}

	public void setCommunityQueue(HashSet<String> communityQueue) {
		this.communityQueue = communityQueue;
	}

	

    public SharonUser(String user_name) {
    	this.userName = user_name;
    	numActiveSessions = 0;
    }
    
	public String getUserName() {
		return userName;
	}
	
	public void setUserName() {
		this.userName = userName;
	}
	
	public int getnumActiveSessions() {
		return numActiveSessions;
	}
	
	public void setSessionActive(int n) {
		this.numActiveSessions = n;
	}
	
	public void incrementActiveSessions() {
		numActiveSessions++;
	}
	
	@Override
	public int compareTo(Object o) {
		SharonUser t = (SharonUser) o;
		return String.valueOf(this.getUserName()).compareTo(String.valueOf(t.getUserName()));
	}

	public void addToTweetQueue(String topic) {
		tweetQueue.add(topic);
	}
	
	public void removeFromTweetQueue(String topic) {
		if (tweetQueue.contains(topic)) {
			tweetQueue.remove(topic);
		}
	}
	
	public void addToCommunityQueue(String topic) {
		communityQueue.add(topic);
	}
	
	public void removeFromCommunityQueue(String topic) {
		if (communityQueue.contains(topic)) {
			communityQueue.remove(topic);
		}
	}
	
	public void addToDiscussionQueue(String topic) {
		discussionQueue.add(topic);
	}
	
	public void removeFromDiscussionQueue(String topic) {
		if (discussionQueue.contains(topic)) {
			discussionQueue.remove(topic);
		}
	}
	
	public void addToTupleQueue(String topic) {
		tupleQueue.add(topic);
	}
	
	public void removeFromTupleQueue(String topic) {
		if (tupleQueue.contains(topic)) {
			tupleQueue.remove(topic);
		}
	}
	
	public void addToAllQueues(String topic) {
		addToTweetQueue(topic);
		addToDiscussionQueue(topic);
		addToCommunityQueue(topic);
		addToTupleQueue(topic);
	}

	public void removeFromAllQueues(String topic) {
		removeFromTweetQueue(topic);
		removeFromDiscussionQueue(topic);
		removeFromCommunityQueue(topic);
		removeFromTupleQueue(topic);
	}
	
	public boolean hasSubscribed(String queueName, String topicName) {
		if (queueName.equals(MessageQueueInterface.TWEET_MESSAGE_QUEUE)) {
			if (tweetQueue.contains(topicName)) {
				return true;
			} else {
				return false;
			}
		} else if (queueName.equals(MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE)) {
			if (communityQueue.contains(topicName)) {
				return true;
			} else {
				return false;
			}
		} else if (queueName.equals(MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE)) {
			if (discussionQueue.contains(topicName)) {
				return true;
			} else {
				return false;
			}
		} else if (queueName.equals(MessageQueueInterface.TUPLE_MESSAGE_QUEUE)) {
			if (tupleQueue.contains(topicName)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public Document toDocument() {
		Document doc = new Document("user_name", userName);
		ArrayList<String> tweetSub = new ArrayList<String>(tweetQueue);
		doc.append("tweet_queue_sub", tweetSub);
		ArrayList<String> discSub = new ArrayList<String>(discussionQueue);
		doc.append("discussion_queue_sub", discSub);
		ArrayList<String> commSub = new ArrayList<String>(communityQueue);
		doc.append("community_queue_sub", commSub);
		ArrayList<String> tupleSub = new ArrayList<String>(tupleQueue);
		doc.append("tuple_queue_sub", tupleSub);
		return doc;
	}
}
