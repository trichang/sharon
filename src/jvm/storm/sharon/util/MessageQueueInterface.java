package storm.sharon.util;

import java.util.ArrayList;

import org.apache.commons.io.monitor.FileAlterationListener;

public interface MessageQueueInterface {
	
	public final String TWEET_MESSAGE_QUEUE = "tweets";
	public final String DISCUSSION_MESSAGE_QUEUE = "discussions";
	public final String KEYWORD_TOPIC_MESSAGE_QUEUE = "keyword-topic";
	public final String COMMAND_QUEUE = "commands";
	public final String COMMUNITY_MESSAGE_QUEUE = "communities";
	public final String TUPLE_MESSAGE_QUEUE = "tuples";
	public final String CUSTOM_QUERY_MESSAGE_QUEUE = "custom_query";
	public final String CUSTOM_STREAM_MESSAGE_QUEUE = "custom_stream";

	public final String GRAPH_QUEUE = "graph_stream";
	public final String CLUSTER_QUEUE = "cluster_stream";
	public final String NODE_QUEUE = "node_stream";
	
	public final String[] QUEUES = {TWEET_MESSAGE_QUEUE,
			DISCUSSION_MESSAGE_QUEUE,
			KEYWORD_TOPIC_MESSAGE_QUEUE,
			COMMAND_QUEUE,
			COMMUNITY_MESSAGE_QUEUE,
			TUPLE_MESSAGE_QUEUE,
			GRAPH_QUEUE,
			CLUSTER_QUEUE,
			NODE_QUEUE
	};
	
	public void write(String queuename, String a);
	public void write(String queuename, String filename, ArrayList<String> a);
	public ArrayList<String> read(String queuename, String filename);
	public void register(String queuename, FileAlterationListener f);
	void delete(String queuename, String value);

}
