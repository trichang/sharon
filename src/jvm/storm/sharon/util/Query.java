package storm.sharon.util;

import java.util.ArrayList;
import java.util.HashMap;

import org.bson.Document;

public class Query implements Comparable {
	
	private String queryString;
	private Integer queryId;
	private boolean isCurrent;
	
	
	public Query(String queryName, Integer QueryId) {
		this.queryString = queryName;
		this.queryId = QueryId;
		this.isCurrent = true;
	}
	
	public Query(String QueryName, Integer QueryId, boolean isCurrent) {
		this(QueryName, QueryId);
		this.isCurrent = isCurrent;
	}
	
	public Query(Document doc) {
		this.queryString =  doc.getString("Query_name");
		this.queryId = doc.getInteger("Query_id");
		this.isCurrent = doc.getBoolean("isCurrent");
	}
	
	public String getQueryName() {
		return queryString;
	}
	public void setQueryName(String QueryName) {
		this.queryString = QueryName;
	}
	public Integer getQueryId() {
		return queryId;
	}
	public void setQueryId(Integer QueryId) {
		this.queryId = QueryId;
	}
	
	public boolean isCurrent() {
		return isCurrent;
	}

	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}
	

	@Override
	public int compareTo(Object o) {
		Query t = (Query) o;
		return String.valueOf(this.getQueryId()).compareTo(String.valueOf(t.getQueryId()));
	}

}
