package storm.sharon.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import com.mongodb.MongoTimeoutException;
import com.mongodb.client.FindIterable;

import storm.sharon.TwitterTrackerTopologyOne;

public class TopicKeywordHandler {
	
	private static TopicKeywordHandler  tkhandler = null; 
	protected HashMap<Integer, Topic> topicIdMap = new HashMap<Integer, Topic>();
	private HashMap<Integer, HashMap<String, Keyword>> topicKeyword = new HashMap<Integer, HashMap<String, Keyword>>(); 
	protected HashMap<String, HashSet<Integer>> allKeyWords = new HashMap<String, HashSet<Integer>>();
	protected HashMap<String, SharonUser> userMap = new HashMap<String, SharonUser>();
	
	protected DBInterface dbConn = null;
	private Log log = LogFactory.getLog(TopicKeywordHandler.class);

	

	/* only called from GUI */
	public static TopicKeywordHandler getInstance(Document doc) {
	
		if (tkhandler == null) {
			tkhandler = new TopicKeywordHandler(doc);
		}	
		
		return tkhandler;
	}
	
	public static TopicKeywordHandler getInstance() {
		
		if (tkhandler == null) {
			System.exit(-1);
		}	
		return tkhandler;
	}
	/* only called from GUI */
	public TopicKeywordHandler(Document doc) {
		
		populateTopics((ArrayList<Document>)doc.get("topics"));
		populateUsers((ArrayList<Document>)doc.get("users"));
		// the real initialization happens when an authenticated session is received
		
	}
	
	/* only called from server */
	public TopicKeywordHandler() {
	
		dbConn = new MongoDBImpl();
	    populateTopics();
	    populateUsers();
		
	}
	
	
	@SuppressWarnings("unchecked")
	private void populateTopics(ArrayList<Document> topiclist){
		for (Document d: topiclist) {
		     Topic t = new Topic(d);
			 Integer topic_id =  t.getTopicId();
			
		
			 List<Document> keywords = (ArrayList<Document>)d.get("keywords");
			 
			 
			 // Populate the topicIdMap, topicKeyWord map, allKeyWords map
			 if (!topicIdMap.containsKey(topic_id)) {
				
				 topicIdMap.put(topic_id, t);
				
				if (keywords!=null) {
					HashMap<String, Keyword> set = new HashMap<String, Keyword>();	
						 
					for (Document keywordDoc: keywords) {
						String keyword = keywordDoc.getString("keyword");
						Integer type = keywordDoc.getInteger("type");
						Boolean isCurrent = keywordDoc.getBoolean("isCurrent");
						Boolean isBlacklisted = keywordDoc.getBoolean("isBlacklisted");
						Keyword keywordObj = new Keyword(keyword, type, isCurrent, isBlacklisted);
						set.put(keyword, keywordObj);
						String keyword_lower = keyword.toLowerCase();
						if (t.isCurrent()) {
							if (isCurrent) {
								if (allKeyWords.containsKey(keyword_lower)) {
									allKeyWords.get(keyword_lower).add(topic_id);
								} else {
									HashSet<Integer> topics = new HashSet<Integer>();
									topics.add(topic_id);
									allKeyWords.put(keyword_lower, topics);
								}
							}
						}
						
					}
				
					topicKeyword.put(topic_id, set); 
					t.setKeyWords(set);				 
				 }

			 }	 
		
		}
	}
	
	private void populateTopics() {
		FindIterable<Document> docList = dbConn.getDocs(DBInterface.TOPIC_KEYWORD_STORE);
		ArrayList<Document> dl = new ArrayList<Document>();
		try{
			for(Document d:docList){
				dl.add(d);
			}
			populateTopics(dl);
		} catch (MongoTimeoutException mte){
			log.error("MongoTimeoutException: " + mte.getMessage());
		}	
	}
	
	@SuppressWarnings("unchecked")
	private void populateUsers(ArrayList<Document> userlist){
		for (Document d: userlist) {
			 String user_name =  d.getString("user_name");
			 SharonUser user = new SharonUser(user_name);
			 if (d.containsKey("tweet_queue_sub")) {
				 user.setTweetQueue(new HashSet<String>((ArrayList<String>)d.get("tweet_queue_sub")));
			 }
			 if (d.containsKey("discussion_queue_sub")) {
				 user.setDiscussionQueue(new HashSet<String>((ArrayList<String>)d.get("discussion_queue_sub")));
			 }
			 if (d.containsKey("community_queue_sub")) {
				 user.setCommunityQueue(new HashSet<String>((ArrayList<String>)d.get("community_queue_sub")));
			 }
			 if (d.containsKey("tuple_queue_sub")) {
				 user.setTupleQueue(new HashSet<String>((ArrayList<String>)d.get("tuple_queue_sub")));
			 }
			 userMap.put(user_name, user);
		}
	}
	
	private void populateUsers() {
		FindIterable<Document> docList = dbConn.getDocs(DBInterface.USER_STORE);
		ArrayList<Document> dl = new ArrayList<Document>();
		log.debug("Populating user information");
		try{
			for(Document d:docList){
				dl.add(d);
			}
			populateUsers(dl);
		} catch (MongoTimeoutException mte){
			log.error("MongoTimeoutException: " + mte.getMessage());
		}
	}
	
	public HashMap<String, HashSet<Integer>> getAllKeyWords() {
		return allKeyWords;
	}
	
	public HashMap<Integer, HashMap<String, Keyword>> getTopicKeyword() {
		return topicKeyword;
	}
	
	public HashMap<Integer, Topic> getTopicIdMap() {
		return topicIdMap;
	}

	

	public void setTopicKeyword(HashMap<Integer, HashMap<String, Keyword>> topicKeyword) {
		this.topicKeyword = topicKeyword;
	}

	public void setAllKeyWords(HashMap<String, HashSet<Integer>> allKeyWords) {
		this.allKeyWords = allKeyWords;
	}

	
	
	public HashMap<String, SharonUser> getUserMap() {
		return userMap;
	}

	public void setUserMap(HashMap<String, SharonUser> userMap) {
		this.userMap = userMap;
	}

	@SuppressWarnings("unchecked")
	public synchronized Document process(Document command) {
		
		Document doc = null;
		
		String command_name = command.getString("command_name");
		
		if (command_name.equals("ADD_TOPIC")) {
			log.error("This class should not receive add topic command");
			/*doc = addTopic(command.getString("topic_name"), command.getInteger("topic_type"), command.getString("topic_owner"));
			subscribeUserTopic(command.getString("topic_owner"), command.getString("topic_name"));*/
		} else if (command_name.equals("NEW_TOPIC")) {
			doc = newTopic(command);
			subscribeUserTopic(command.getString("topic_owner"), command.getString("topic_name"));		
		} else if (command_name.equals("STOP_TOPIC")) {
			doc = stopTopic(command.getInteger("topic_id"));

		} else if (command_name.equals("DELETE_TOPIC")) {
			removeFromUser(command.getInteger("topic_id"));
			doc = deleteTopic(command.getInteger("topic_id"));

		} else if (command_name.equals("START_TOPIC")) {
			doc = startTopic(command.getInteger("topic_id"));
		} else if (command_name.equals("UPDATES_QUERIES")) {
			doc = updateQueries(command.getInteger("topic_id"), (ArrayList<Document>)command.get("queries"));
		} else if (command_name.equals("GET_TOPICS")) {
			doc = getTopics();
		} else if (command_name.equals("ADD_KEYWORDS")) {
			doc = addKeywords(command.getInteger("topic_id"), 
					command.get("keywords"), 0);
		} else if (command_name.equals("ADD_NONCURRENT_KEYWORDS")) {
			doc = addNonCurrentKeywords(command.getInteger("topic_id"), 
					command.get("keywords"), 0);
		} else if (command_name.equals("DELETE_KEYWORDS")) {
			doc = deleteKeywords(command.getInteger("topic_id"), 
					command.get("keywords"));
		} else if (command_name.equals("BLACKLIST_KEYWORDS")) {
			doc = blackListKeywords(command.getInteger("topic_id"), 
					command.get("keywords"));
		} else if (command_name.equals("BLACKLIST_NONCURRENT_KEYWORDS")) {
			doc = blackListKeywords(command.getInteger("topic_id"), 
					command.get("keywords"));
		} else if (command_name.equals("WHITELIST_KEYWORDS")) {
			doc = whiteListKeywords(command.getInteger("topic_id"), 
					command.get("keywords"));
		} else if (command_name.equals("GET_KEYWORDS")) {
			doc = getKeywords(command.getInteger("topic_id"));
		} else if (command_name.equals("NEW_KEYWORDS")) {
			doc = newKeywords(command.getInteger("topic_id"), command.get("keywords"));
		} else if (command_name.equals("CHANGE_TRACK")) {
			// No action is required by the TopicKeywordHandler because this is a command for the streaming spout
			doc = null;
		} else if (command_name.equals("NEW_USER_SESSION")) {
			doc = newUserSession(command.getString("user_name"));
		} else if (command_name.equals("SUBSCRIBE_TOPIC_QUEUE")) {
			subscribeUserTopicQueue(command.getString("user_name"), command.getInteger("topic_id"), command.getString("queue_name"));
		} else if (command_name.equals("UNSUBSCRIBE_TOPIC_QUEUE")) {
			unSubscribeUserTopicQueue(command.getString("user_name"), command.getInteger("topic_id"), command.getString("queue_name"));
		} else if (command_name.equals("SUBSCRIBE_TOPIC")) {
			subscribeUserTopic(command.getString("user_name"), command.getInteger("topic_id"));
		} else if (command_name.equals("UNSUBSCRIBE_TOPIC")) {
			unSubscribeUserTopic(command.getString("user_name"), command.getInteger("topic_id"));
		} else if (command_name.equals("TOPIC_WINDOW_PARM_UPDATE")) {
			doc = updateWindowParm(command);
		} else if (command_name.equals("STARTUP_PROTOCOL")){
			// ignore these commands
		} else if (command_name.equals("MAX_TOPIC_NUM_REACHED")){
			log.info("This is a topic limit reached message: no action is required ");
		} else {
			log.error("Unknown command received: "+ command_name);
		}
		
		return doc;
		
	}

	
	private Document newTopic(Document doc) {
		if (!topicIdMap.containsKey(doc.getInteger("topic_id"))) {
			Topic t = new Topic(doc);
			topicIdMap.put(doc.getInteger("topic_id"), t);
		}
		return doc;
	}


	// get the keywords for a topic id
	@SuppressWarnings("rawtypes")
	public Document getKeywords(Integer topic_id) {
		if (topicIdMap.containsKey(topic_id)) {
			Topic t = topicIdMap.get(topic_id);
			HashMap<String, Keyword> c_keywords = topicKeyword.get(topic_id);
			Document doc = new Document("topic_id", topic_id);
			doc.append("isCurrent", t.isCurrent());
			ArrayList<Document> keywords = new ArrayList<Document>();
			if (c_keywords != null) {
				Set<Entry<String, Keyword>> c_keywords_es = c_keywords.entrySet();
				for (Entry keyword: c_keywords_es) {
				   Document docK = new Document("keyword", keyword.getKey());
				   docK.append("type", ((Keyword)keyword.getValue()).getType());
				   docK.append("isCurrent", ((Keyword)keyword.getValue()).getIsCurrent());
				   docK.append("isBlacklisted", ((Keyword)keyword.getValue()).getIsBlacklisted());
				   keywords.add(docK);
				}
			}
			
			doc.append("keywords", keywords);
			return doc;
		} else {
			log.debug("Topic does not exist");
			return null;
		}
	}

	// delete the supplied keywords for given topic id
	@SuppressWarnings("unchecked")
	protected Document deleteKeywords(Integer topic_id, Object keywordList) {
		
		if (topicIdMap.containsKey(topic_id)) {
			boolean topicIsCurrent = ((Topic)topicIdMap.get(topic_id)).isCurrent();
			ArrayList<String> keywords = (ArrayList<String>)keywordList;
			HashMap<String, Keyword> c_keywords = topicKeyword.get(topic_id);
			log.debug("Deleting keywords for topic ["+topic_id+"]: " + keywords);
			log.debug("All keywords in system: " + allKeyWords);
			log.debug("Topic ["+topic_id+"] currently contains: " + c_keywords);
			if (keywords != null ) {
				for (String s: keywords) {
					String s_lower = s.toLowerCase();
					if (c_keywords.get(s).getIsCurrent() == true) {
						c_keywords.get(s).setIsCurrent(false);
						log.info("Deleting Keyword: newValue:  " + c_keywords.get(s).getKeyword() + " " +c_keywords.get(s).getIsCurrent());
						
						if (topicIsCurrent) {
							HashSet<Integer> topics = allKeyWords.get(s_lower);
							if (topics.contains(topic_id)) {
								topics.remove(topic_id);
							}
							if (topics.size() == 0) {
								allKeyWords.remove(s_lower);
							}
						}
					} else {
						// TODO: what if not current???
					}
					
				}
			} else {
				log.error("No keywords were given for deletion.");
			}
			log.debug("All keywords in system now: " + allKeyWords);
			
			return getKeywords(topic_id);
		} else {
			log.info("Topic id  does not exist");
			return null;
		}
	}
	
	

	// Set keywords for a topic id
	@SuppressWarnings("rawtypes")
	protected Document newKeywords(Integer topic_id, Object keywordList) {
		HashMap<String, Keyword> c_keywords = topicKeyword.get(topic_id);
		if (c_keywords != null ) {
			Set<Entry<String, Keyword>> c_keywords_es = c_keywords.entrySet();
			ArrayList<String> c_k =  new ArrayList<String>();
			for (Entry keyword: c_keywords_es) {
		       c_k.add((String)keyword.getKey());
			}
			deleteKeywords(topic_id, c_k);
		} else {
			log.error("Topic does not contain any keywords.");
		}
		addKeywords(topic_id, keywordList, 1);
		return getKeywords(topic_id);
	}

	// Add keywords to a topic id
	@SuppressWarnings("unchecked")
	protected Document addKeywords(Integer topic_id, Object keywordList, int type) {
	    if (topicIdMap.containsKey(topic_id)) {
			ArrayList<String> keywords = (ArrayList<String>)keywordList;
			boolean topicIsCurrent = ((Topic)topicIdMap.get(topic_id)).isCurrent();
			HashMap<String, Keyword> c_keywords = topicKeyword.get(topic_id);
			if (c_keywords == null) {
				c_keywords = new HashMap<String, Keyword>();
				topicKeyword.put(topic_id, c_keywords);
			}
			
			if (keywords != null) {
				for (String keyword_a: keywords) {
					String keyword = keyword_a.toLowerCase();
					if ( c_keywords.containsKey(keyword)) {
						if (!c_keywords.get(keyword).getIsBlacklisted()) {
							c_keywords.get(keyword).setIsCurrent(true);
						}
						else {
							log.debug("This is a blacklisted keyword - no action required");
						}
					} else {
						c_keywords.put(keyword, new Keyword(keyword, type, true));
					}
					
					if (topicIsCurrent) {
						if (!c_keywords.get(keyword).getIsBlacklisted()) {
							if (allKeyWords.containsKey(keyword)) {
								log.info("Adding topic to an existing keyword: " + keyword + " to topic " + topic_id);
								HashSet<Integer> topics = allKeyWords.get(keyword);
								topics.add(topic_id);
							} else {
								log.info("Adding a new keyword: " + keyword + " to topic " + topic_id);
								HashSet<Integer> topics = new HashSet<Integer>();
								topics.add(topic_id);
								allKeyWords.put(keyword, topics);
							}
						}
						else {
							log.debug("This is a blacklisted keyword - no action required");
						}
					} else {
						// TODO: add keyword to a non-current topic???
					}
					log.debug("All keywords in system now: " + allKeyWords);
				}
			} else {
				log.error("No keywords given for addition.");
			}
	
			return getKeywords(topic_id);
	    } else {
	    	log.info("Topic id does not exist");
	    	return null;
	    }
	}

	// set the topic to current
	@SuppressWarnings("rawtypes")
	protected Document startTopic(Integer topic_id) {
		String topic_name;
		Document doc = null;
		if ( topicIdMap.containsKey(topic_id)) {
			Topic t = topicIdMap.get(topic_id);
			topic_name = t.getTopicName();
			t.setCurrent(true);
			HashMap<String, Keyword> keywords = topicKeyword.get(topic_id);
			if (keywords != null) {
				Set<Entry<String, Keyword>> c_keywords_es = keywords.entrySet();
				for (Entry entry: c_keywords_es) {
					Keyword k = (Keyword)entry.getValue();
					String k_lower = ((String)entry.getKey()).toLowerCase();
					if (k.getIsCurrent()) {
						HashSet<Integer> topics = allKeyWords.get(k_lower);
						if (topics != null) {
							topics.add(topic_id);
						} else {
							topics = new HashSet<Integer>();
							topics.add(topic_id);
							allKeyWords.put(k_lower, topics);
						}
					}
				}
			}
			
			doc = new Document("topic_name", topic_name);
			doc.append("topic_id", topic_id);
			doc.append("isCurrent", true);
		} else {
			log.error("Topic id ["+topic_id+"] not found in database.");
		}
		
        return doc;
	}

	// set topic to non-current
	@SuppressWarnings("rawtypes")
	protected Document stopTopic(Integer topic_id) {
		String topic_name = null;
		if ( topicIdMap.containsKey(topic_id)) {
			topic_name= topicIdMap.get(topic_id).getTopicName();
			topicIdMap.get(topic_id).setCurrent(false);
			HashMap<String, Keyword> keywords = topicKeyword.get(topic_id);
			
			if (keywords != null) {
				Set<Entry<String, Keyword>> c_keywords_es = keywords.entrySet();
				for (Entry entry: c_keywords_es) {
					String k_lower = ((String) entry.getKey()).toLowerCase();
					HashSet<Integer> topics = allKeyWords.get(k_lower);
					if (topics != null) {
						topics.remove(topic_id);
						if (topics.size() == 0 ) {
							allKeyWords.remove(k_lower);
						}
					}
				}
			}
			
			topicIdMap.get(topic_id).setKeyWords(keywords);
		} else {
			log.error("Topic id ["+topic_id+"] not found in database.");
		}
		Document doc = new Document("topic_name", topic_name);
		doc.append("topic_id", topic_id);
		doc.append("isCurrent", false);
        return doc;
	}

	
	protected Document addQuery(Integer topic_id, Document query) {
		
		if ( topicIdMap.containsKey(topic_id)) {
			topicIdMap.get(topic_id).addQuery(query);
			return new Document("queries", topicIdMap.get(topic_id).getQueries());
		} else {
			return null;
		}
	
	}
	


	// return a list of topics
	public Document getTopics() {
		ArrayList<Document> topicList = new ArrayList<Document>();
		Set<Entry<Integer, Topic>> entries = topicIdMap.entrySet();
		for (Entry<Integer, Topic> e: entries) {
			topicList.add(e.getValue().toDoc());
		}
		Document doc = new Document("topics", topicList);
		return doc;
	}

	
	// add a new topic
	/*protected Document addTopic(String topic_name) {
		Integer topic_id;
		Boolean isCurrent;
		Topic t;
		
		topic_id =  getNextAvailable();
		if (topic_id != maxNumTopics) {
			isCurrent = true;
			t =  new Topic(topic_name, topic_id, isCurrent);
			topicIdMap.put(topic_id, t);	
			return t.toDoc();
		} else {
			log.info("Maximum number of topics reached - not adding topic");
			return null;
		}
	}*/
	
	
/*	// check if topic exists
	private int getNextAvailable() {
		int nextTopicId = 0;
		if (topicIdMap.size() == 0) {
			nextTopicId = 0;
		}
		else {
			Set<Entry<Integer, Topic>> entries = topicIdMap.entrySet();	
			ArrayList<Integer> t_ids = new ArrayList<Integer>();
			for (Entry<Integer, Topic> e: entries) {
				t_ids.add(e.getKey());
			}
		
			Collections.sort(t_ids);
			for (Integer t:t_ids) {
				if (t == nextTopicId) {
					nextTopicId++;
				}
				else {
					break;
				}
			}
		}
		log.info("Using topic ID: " + nextTopicId);
		return nextTopicId;
	}*/
	
	// add a new topic

	
	// check if topic exists
	private boolean topicExists(String topic_name) {
		boolean exists = false;
		Set<Entry<Integer, Topic>> entries = topicIdMap.entrySet();
		for (Entry<Integer, Topic> e: entries) {
			if(e.getValue().getTopicName().equals(topic_name)) {
				exists = true;
				break;
			}
		}
		return exists;
	}
	
	public Topic getTopic(Integer topicId) {
		
		Topic t = null;
		
		if (topicIdMap.containsKey(topicId)) {
			t = topicIdMap.get(topicId);
		}
		
		return t;
		
	}
	
	private Document updateQueries(Integer topicId,
			ArrayList<Document> queries) {
		if (topicIdMap.containsKey(topicId)) {
			 topicIdMap.get(topicId).setQueries(queries);
		}
		return null;
	}
	
	// blacklist the supplied keywords for given topic id
	@SuppressWarnings("unchecked")
	protected Document blackListKeywords(Integer topic_id, Object keywordList) {
		boolean topicIsCurrent = ((Topic)topicIdMap.get(topic_id)).isCurrent();
		ArrayList<String> keywords = (ArrayList<String>)keywordList;
		HashMap<String, Keyword> c_keywords = topicKeyword.get(topic_id);
		log.debug("Blacklisting keywords for topic ["+topic_id+"]: " + keywords);
		log.debug("All keywords in system: " + allKeyWords);
		log.debug("Topic ["+topic_id+"] currently contains: " + c_keywords);
		if (keywords != null ) {
			for (String s: keywords) {
				String s_lower = s.toLowerCase();
				if (c_keywords.get(s).getIsCurrent() == true) {
					c_keywords.get(s).setIsCurrent(false);
					c_keywords.get(s).setIsBlacklisted(true);
					log.info("Blacklisting current Keyword:  " + c_keywords.get(s).getKeyword() + " " +c_keywords.get(s).getIsCurrent() + " " +c_keywords.get(s).getIsBlacklisted());
						
					if (topicIsCurrent) {
						HashSet<Integer> topics = allKeyWords.get(s_lower);
						if (topics.contains(topic_id)) {
							topics.remove(topic_id);
						}
						if (topics.size() == 0) {
							allKeyWords.remove(s_lower);
						}
					}
				} else {
					log.info("Blacklisting non-current Keyword:   " + c_keywords.get(s).getKeyword() + " " +c_keywords.get(s).getIsCurrent() + " " +c_keywords.get(s).getIsBlacklisted());
					c_keywords.get(s).setIsBlacklisted(true);
				}		
			}
		} 
		else {
			log.error("No keywords were given for deletion.");
		}
		log.debug("All keywords in system now: " + allKeyWords);
			
		return getKeywords(topic_id);
	}
	
	
	// Whitelist keywords to a topic id
	@SuppressWarnings("unchecked")
	protected Document whiteListKeywords(Integer topic_id, Object keywordList) {
			ArrayList<String> keywords = (ArrayList<String>)keywordList;
			boolean topicIsCurrent = ((Topic)topicIdMap.get(topic_id)).isCurrent();
			HashMap<String, Keyword> c_keywords = topicKeyword.get(topic_id);
			if (c_keywords == null) {
				c_keywords = new HashMap<String, Keyword>();
				topicKeyword.put(topic_id, c_keywords);
			}
			
			if (keywords != null) {
				for (String keyword: keywords) {
					String keyword_lower = keyword.toLowerCase();
					if ( c_keywords.containsKey(keyword)) {
						log.debug("Whitelisting keyword");
						c_keywords.get(keyword).setIsBlacklisted(false);
					} else {
						log.debug("Request to whitelist a non-existant word - no action required");
					}	
					log.debug("All keywords in system now: " + allKeyWords);
				}
			} else {
				log.error("No keywords given for addition.");
			}

			return getKeywords(topic_id);
		}

	
	// Add keywords to a topic id
	@SuppressWarnings("unchecked")
	protected Document addNonCurrentKeywords(Integer topic_id, Object keywordList, int type) {
		ArrayList<String> keywords = (ArrayList<String>)keywordList;
		boolean topicIsCurrent = ((Topic)topicIdMap.get(topic_id)).isCurrent();
		HashMap<String, Keyword> c_keywords = topicKeyword.get(topic_id);
		if (c_keywords == null) {
			c_keywords = new HashMap<String, Keyword>();
			topicKeyword.put(topic_id, c_keywords);
		}
			
		if (keywords != null) {
			for (String keyword: keywords) {
				String keyword_lower = keyword.toLowerCase();
				if ( c_keywords.containsKey(keyword)) {
					c_keywords.get(keyword).setIsCurrent(true);
					c_keywords.get(keyword).setIsBlacklisted(false);
				} else {
					log.info("This condition should not happen because this is an existing keyword");
				}
					
				if (topicIsCurrent) {
					if (allKeyWords.containsKey(keyword_lower)) {
						log.info("Adding topic to an existing keyword: " + keyword + " to topic " + topic_id);
						HashSet<Integer> topics = allKeyWords.get(keyword_lower);
						topics.add(topic_id);
					} else {
						log.info("Adding a new keyword: " + keyword + " to topic " + topic_id);
						HashSet<Integer> topics = new HashSet<Integer>();
						topics.add(topic_id);
						allKeyWords.put(keyword_lower, topics);
					}
				} else {
						// TODO: add keyword to a non-current topic???
				}
				log.debug("All keywords in system now: " + allKeyWords);
			}
		} else {
			log.error("No keywords given for addition.");
		}

		return getKeywords(topic_id);
	}

	
	
	// delete the topic completely, remove all the data related to the topic
	@SuppressWarnings("rawtypes")
	protected Document deleteTopic(Integer topic_id) {
		String topic_name = null;
		boolean isCurrent = false;
		if ( topicIdMap.containsKey(topic_id)) {
			topic_name= topicIdMap.get(topic_id).getTopicName();
			isCurrent = topicIdMap.get(topic_id).isCurrent();
			if ( isCurrent) {
				HashMap<String, Keyword> keywords = topicKeyword.get(topic_id);
				
				if (keywords != null) {
					Set<Entry<String, Keyword>> c_keywords_es = keywords.entrySet();
					for (Entry entry: c_keywords_es) {
						String k_lower = ((String) entry.getKey()).toLowerCase();
						HashSet<Integer> topics = allKeyWords.get(k_lower);
						if (topics != null) {
							topics.remove(topic_id);
							if (topics.size() == 0 ) {
								allKeyWords.remove(k_lower);
							}
						}
					}
				}				
			}
			topicIdMap.remove(topic_id);
			topicKeyword.remove(topic_id);
		} else {
			log.error("Topic id ["+topic_id+"] not found in database.");
		}
		Document doc = new Document("topic_name", topic_name);
		doc.append("topic_id", topic_id);
		doc.append("isCurrent", isCurrent);
	    return doc;
	}
	
	
	// Create a new user session
	protected Document newUserSession(String user_name) {
		if (!userMap.containsKey(user_name)) {
			log.debug("User is not in the map: " + user_name);
			SharonUser u = new SharonUser(user_name);
			u.incrementActiveSessions();
			userMap.put(user_name, u);
		}
		else {
			log.debug("User is already in the map, incrementing session count: " + user_name);
			userMap.get(user_name).incrementActiveSessions();
		}
		log.debug("User: " + user_name + " Number of active sessions: " + userMap.get(user_name).getNumActiveSessions());
		Document doc = new Document("user_name", user_name);
		return doc;
	}

		
	protected Document subscribeUserTopic(String user_name, String topic_name) {
		log.debug("Adding topic to user session: topic " + topic_name + " user: " + user_name);
		if (userMap.containsKey(user_name)) {
			log.debug("UserMap contains the user.");
			userMap.get(user_name).addToAllQueues(topic_name);
			SharonUser u = userMap.get(user_name);
			Document d = u.toDocument();
			log.debug("Returning user subscription information: " + d);
			return d;
		} else {
			log.warn("UserMap doesn not contain the user - this should not be the case");
			return null;
		}
	
	}

	
	private void removeFromUser(int topic_id) {
		Topic topic = topicIdMap.get(topic_id);
		ArrayList<String> userList = topic.getSharedUsersAsList();
		
		for (String user_name: userList) {
			log.debug("User: " + user_name);
			if (userMap.containsKey(user_name)) {
				log.debug("Removing topic from user subscriptions: " + user_name);
				SharonUser u = userMap.get(user_name);
				u.removeFromAllQueues(topic.getTopicName());
			} else {
				log.debug("User does not exist in the list: " + user_name);
			}
		}

	}

	
	protected Document subscribeUserTopicQueue(String user_name, Integer topic_id, String queue_name) {
		// Add topic to specified queue
		if (topicIdMap.containsKey(topic_id)) {
			if (userMap.containsKey(user_name)) {
				log.info("Adding user to topic queue: topic " + topic_id + " user: " + user_name + " queue: " + queue_name);
				switch(queue_name) {
				case MessageQueueInterface.TWEET_MESSAGE_QUEUE:
					userMap.get(user_name).addToTweetQueue(topicIdMap.get(topic_id).getTopicName());
					break;
				case MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE:
					userMap.get(user_name).addToDiscussionQueue(topicIdMap.get(topic_id).getTopicName());
					break;
				case MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE:
					userMap.get(user_name).addToCommunityQueue(topicIdMap.get(topic_id).getTopicName());
					break;
				case MessageQueueInterface.TUPLE_MESSAGE_QUEUE:
					userMap.get(user_name).addToTupleQueue(topicIdMap.get(topic_id).getTopicName());
					break;
				}
				SharonUser u = userMap.get(user_name);
				return u.toDocument();
			} else {
				log.warn("User does not exist - this should not be the case: topic " + topic_id + " user: " + user_name + " queue: " + queue_name);
				return null;
			}
		} else {
			log.info("Topic does not exist");
			return null;
		}
	}
	
	protected Document subscribeUserTopic(String user_name, Integer topic_id) {	
		// Add topic to all queues
		if (topicIdMap.containsKey(topic_id)) {
			String topicName = topicIdMap.get(topic_id).getTopicName();
			Document doc = subscribeUserTopic(user_name, topicName);
			return doc;
		} else {
			log.info("Topic does not exist");
			return null;
		}
	}
	
	protected Document unSubscribeUserTopicQueue(String user_name, Integer topic_id, String queue_name) {
		// Remove topic from the specified queue
		if (topicIdMap.containsKey(topic_id)) {
			if (userMap.containsKey(user_name)) {
				log.debug("Removing user to topic queue: topic " + topic_id + " user: " + user_name + " queue: " + queue_name);
				switch(queue_name) {
				case MessageQueueInterface.TWEET_MESSAGE_QUEUE:
					userMap.get(user_name).removeFromTweetQueue(topicIdMap.get(topic_id).getTopicName());
					break;
				case MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE:
					userMap.get(user_name).removeFromDiscussionQueue(topicIdMap.get(topic_id).getTopicName());
					break;
				case MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE:
					userMap.get(user_name).removeFromCommunityQueue(topicIdMap.get(topic_id).getTopicName());
					break;
				case MessageQueueInterface.TUPLE_MESSAGE_QUEUE:
					userMap.get(user_name).removeFromTupleQueue(topicIdMap.get(topic_id).getTopicName());
					break;
				}
				SharonUser u = userMap.get(user_name);
				return u.toDocument();
			} else {
				log.warn("User does not exist - this should not be the case: topic " + topic_id + " user: " + user_name + " queue: " + queue_name);
				return null;
			}
		} else {
			log.info("Topic does not exist");
			return null;
		}
	}
	
	protected Document unSubscribeUserTopic(String user_name, Integer topic_id) {
		// Remove topic from all the queues
		if (topicIdMap.containsKey(topic_id)) {
			if (userMap.containsKey(user_name)) {
				log.debug("Removing user from topic for all queues: topic " + topic_id + " user: " + user_name);
				userMap.get(user_name).removeFromAllQueues(topicIdMap.get(topic_id).getTopicName());
				SharonUser u = userMap.get(user_name);
				return u.toDocument();
			} else {
				log.warn("User does not exist - this should not be the case: topic " + topic_id + " user: " + user_name);
				return null;
			}
		} else {
			log.info("Topic does not exist");
			return null;
		}
	}
	
	
	public boolean hasSubscribed(String u, Integer topic_id, String queue_name) {
		String topic_name = topicIdMap.get(topic_id).getTopicName();
		boolean hasSubscribed = false;
		SharonUser user = userMap.get(u);
		switch(queue_name) {
		case MessageQueueInterface.TWEET_MESSAGE_QUEUE:
			if (user.getTweetQueue().contains(topic_name)) {
				log.debug("User " + u + " has subscribed to receive " + queue_name + " for topic " + topic_name);
				hasSubscribed = true;
			} else {
				log.debug("User " + user + " has subscribed to receive " + queue_name + " for topic " + topic_name);
			}
		break;
	
		case MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE:
			if (user.getDiscussionQueue().contains(topic_name)) {
				log.debug("User " + u + " has subscribed to receive " + queue_name + " for topic " + topic_name);
				hasSubscribed = true;
			} else {
				log.debug("User " + user + " has subscribed to receive " + queue_name + " for topic " + topic_name);
			}
		break;
		
		case MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE:
			if (user.getCommunityQueue().contains(topic_name)) {
				log.debug("User " + u + " has subscribed to receive " + queue_name + " for topic " + topic_name);
				hasSubscribed = true;
			} else {
				log.debug("User " + user + " has subscribed to receive " + queue_name + " for topic " + topic_name);
			}
			break;
		case MessageQueueInterface.TUPLE_MESSAGE_QUEUE:
			if (user.getTupleQueue().contains(topic_name)) {
				log.debug("User " + u + " has subscribed to receive " + queue_name + " for topic " + topic_name);
				hasSubscribed = true;
			} else {
				log.debug("User " + user + " has subscribed to receive " + queue_name + " for topic " + topic_name);
			}
			break;
		default:
			log.warn("Invalid case");
			break;
		}
		return hasSubscribed;
	}

	
	protected Document updateWindowParm(Document doc) {
		Integer topic_id = doc.getInteger("topic_id");
		String query_id = doc.getString("query_id");
		if (topicIdMap.containsKey(topic_id)) {
		Topic topic = topicIdMap.get(topic_id);
			if (doc.containsKey("window_length")) {
				topic.setWindowLengthMinutes(query_id, doc.getInteger("window_length"));
			}
			if (doc.containsKey("update_frequency")) {
				topic.setUpdateFrequencyMinutes(query_id, doc.getInteger("update_frequency"));
			}
			if (doc.containsKey("threshold")) {
				topic.setRollingCountThreshold(query_id, doc.getInteger("threshold"));
			}
			
			ArrayList<Integer>  a = topic.getWindowParamMap().get(query_id);
			
			doc.append("window_params", a);
	        
			log.debug("Window parameters: " + doc);
			
			return doc;
		} else {
			return null;
		}
	}

}
;