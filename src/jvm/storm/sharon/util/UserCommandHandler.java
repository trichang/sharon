/**  
* UserCommandHandler.java - this class handles commands related to User management
* @author  Shanika Karunasekera
* @version 1.0 
*/


package storm.sharon.util;

import java.util.ArrayList;

import java.util.Properties;


import org.I0Itec.zkclient.ZkClient;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;


import kafka.admin.AdminUtils;

public class UserCommandHandler extends UserHandler {

	private Log log = LogFactory.getLog(UserCommandHandler.class);
	private ZkClient zkClient;
	
	public UserCommandHandler(ZkClient zkClient) {
		super();
		this.zkClient = zkClient;
	}

	public Document process(Document command) {
		
		Document doc = null;
		String command_name = command.getString("command_name");
		
		log.debug("Received command: "+ command.getString("command_name"));
		if (command_name.equals("NEW_USER_SESSION")) {
			createUser(command.getString("user_name"));
			doc = newUserSession(command.getString("user_name"));
			command_name = "NEW_USER_SESSION";
		} else if (command_name.equals("ADD_USER_TOPIC")) {
			subscribeUserTopic(command.getString("user_name"), command.getString("topic_name"));
			doc = command;
		}
		doc.append("command_name", command_name);
		doc.append("command_type", "USER_MGMT");
		return doc;
		
	}
	
	
	private void createUser(String user_name) {
		
		log.debug("Creating user: "+ user_name);
		if (!dbConn.containsDoc(DBInterface.USER_STORE, "user_name", user_name)) {
			log.debug("Adding user ["+ user_name +"] to database.");
			Document doc = new Document("user_name", user_name);
			dbConn.insertDoc(DBInterface.USER_STORE, doc);
		} else {
			log.warn("Topic ["+ user_name +"] already exists in the database");
		}	
		
		createUserMessageQueues(user_name);
	}

	// Create the appropriate kafka message queues for the user
	private void createUserMessageQueues(String user_name) {
       
		ArrayList<String> messageQueues = new ArrayList<String>();
		messageQueues.add(MessageQueueInterface.TWEET_MESSAGE_QUEUE + "_" + user_name);
		messageQueues.add(MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE + "_" + user_name);
		messageQueues.add(MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE + "_" + user_name);
		messageQueues.add(MessageQueueInterface.TUPLE_MESSAGE_QUEUE + "_" + user_name);
		
        int numPartitions = 1;
        int replicationFactor = 1;
        Properties topicConfig = new Properties();

        for (String queueName: messageQueues) {
	        if (!AdminUtils.topicExists(zkClient, queueName)) {
	        	log.debug("Creating kafka message queue: " + queueName);
	        	AdminUtils.createTopic(zkClient, queueName, numPartitions, replicationFactor, topicConfig);
	        } else {
	        	log.debug("Kafka message queue: "+ queueName + " already exists");
	        }
        }
	
		
	}

}
