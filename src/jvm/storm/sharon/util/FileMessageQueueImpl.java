package storm.sharon.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FileMessageQueueImpl implements MessageQueueInterface {
	
	private String messageQueueDirectory;
	private Log log = LogFactory.getLog(FileMessageQueueImpl.class);
	
	public FileMessageQueueImpl() {
		messageQueueDirectory = "message_queues";
	}
	
	// Register with the message queue to receive messages
	@Override
	public void register(String queuename, FileAlterationListener f) {
		
		String queueDirectory = messageQueueDirectory + "/" + queuename;
		final File track_d = new File(queueDirectory);
        FileAlterationObserver fao = new FileAlterationObserver(track_d);
        fao.addListener(f);
        final FileAlterationMonitor monitor = new FileAlterationMonitor();
        monitor.addObserver(fao);
        log.info("Starting monitor: "+queueDirectory+"... CTRL+C to stop.");
        try {
        	monitor.start();
        } catch(Exception e) {
        	log.error("Failed to start monitor: "+queueDirectory);
        }
	}
	
	@Override
	public void write(String queuename, String value) {
		 
		String queueFile = messageQueueDirectory + "/" + queuename +"/" + value;
		try {
			FileWriter fw = new FileWriter(queueFile);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.close();
		}
		catch (Exception e) {
			log.error("Failed to write to file: "+queueFile);
	    }
	}
	
	@Override
	public void delete(String queuename, String value) {
		String queueFile = messageQueueDirectory + "/" + queuename +"/" + value;
		try {
			File file = new File(queueFile);
			file.delete();
		}
		catch (Exception e) {
			log.info("Failed to delete : "+ queueFile);
	    }
	}
	
	
	@Override
	public void write(String queuename, String filename, ArrayList<String> values) {
	
		String queueFile = messageQueueDirectory + "/" + queuename +"/" + filename;
		
		try {
			FileWriter fw = new FileWriter(queueFile );
			BufferedWriter bw = new BufferedWriter(fw);
			for (String s: values) {
				bw.write(s);
				bw.write('\n');
			}
			bw.close();
		}
		catch (Exception e) {
			log.error("Failed to write to file: "+queueFile);
		}
	
	}



	@Override
	public ArrayList<String> read(String queuename, String filename) {
		ArrayList<String> wordList = new ArrayList<String>();
		String word;
		String queueFile = messageQueueDirectory + "/" + queuename +"/" + filename;	
		
		try {
			FileReader fr = new FileReader(queueFile);
			BufferedReader br = new BufferedReader(fr);
			
			while ( (word = br.readLine()) != null) {
				wordList.add(word);
			}
			
			fr.close();
		}
		catch (Exception e) {
			log.error("Failed to read file: "+queueFile);
			return null;
		}
				
		return wordList;
	}

}
