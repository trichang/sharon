package storm.sharon.util;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterObjectFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;



public class TwitterStreamManager extends TwitterManager {
	
	private LinkedBlockingQueue<Document> queue = null;
	private TwitterStream _twitterStream;
	private Log log = LogFactory.getLog(TwitterStreamManager.class);
	
	public TwitterStreamManager(int id) {
		super(id);
	}
	
	public void open(ArrayList<String> keyWords,  ArrayList<String> users, double[][] locations) {
			
		if ( (keyWords.size() != 0) || (users.size() != 0) ||
						locations != null) {
		
			queue = new LinkedBlockingQueue<Document>(1000);
		
			StatusListener listener = new StatusListener() {
				@Override
				public void onStatus(Status status) {
					Document customStatus = Document.parse(TwitterObjectFactory.getRawJSON(status));
					queue.offer(customStatus);
				}

				@Override
				public void onDeletionNotice(StatusDeletionNotice sdn) {
				}

				@Override
				public void onTrackLimitationNotice(int i) {
				}

				@Override
				public void onScrubGeo(long l, long l1) {
				}

				@Override
				public void onException(Exception ex) {
				}

				@Override
				public void onStallWarning(StallWarning arg0) {
					// TODO Auto-generated method stub

				}

			};

		
			TwitterStream twitterStream = new TwitterStreamFactory(_cb.build()).getInstance();
	
			twitterStream.addListener(listener);
		
			
			// Obtain the number of streams
			int numStreams = CredentialsManager.getNumTwitterCredentials();
			log.debug("Number of streams: " + numStreams);
			
			
			ArrayList<String> keyWordsTrimmed = new ArrayList<String>();
			int count = 0;
			
			// Split the keywords across the streams
			// Allocate the keyword to the stream only if  count%numStreams == getId()
			for (String s: keyWords) {
				if ( count%numStreams == getId()) {
					keyWordsTrimmed.add(s);
				}
				count++;
			}
			
			String[] keyWordsArray = new String[keyWordsTrimmed.size()];
			
			count = 0;
			for (String s: keyWordsTrimmed) {			
				keyWordsArray[count] = s;
				log.debug("Spout ID: " + super.getId()  + " KeyWord : " + keyWordsArray[count]);
				count++;
			}

			
			ArrayList<String> usersTrimmed = new ArrayList<String>();
			count = 0;
			
			// Split the users across the streams
			// Allocate the user to the stream only if  count%numStreams == getId()
			for (String s: users) {
				if ( count%numStreams == getId()) {
					usersTrimmed.add(s);
				}
				count++;
			}
			
			long[] userArray = new long[usersTrimmed.size()];
			count = 0;
			for (String s: usersTrimmed) {
				userArray[count] = Long.parseLong(s);
				count++;
			}
			
			FilterQuery query = new FilterQuery().track(keyWordsArray).follow(userArray).locations(locations);
			twitterStream.filter(query);
		
		

			_twitterStream = twitterStream;
		}
		else {
			log.info("Nothing to track - not opening twitter stream");
			_twitterStream = null;
			queue = null;
		}
	}
	
	//public storm.sharon.util.Status nextTweet() {
	public Document nextTweet() {
		if (queue != null) {
			return queue.poll();
		}
		else {
			return null;
		}
	}

	public void close() {
		if (_twitterStream != null ) {
			_twitterStream.shutdown();
		}
	}
}
