package storm.sharon.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import com.mongodb.MongoTimeoutException;
import com.mongodb.client.FindIterable;

public class UserHandler {
	
	private static UserHandler  userhandler = null; 
	private HashMap<String, SharonUser> userMap = new HashMap<String, SharonUser>();
	
	protected DBInterface dbConn = null;
	
	private Log log = LogFactory.getLog(UserHandler.class);
	

	public static UserHandler getInstance(String dbInfo) {
		if (userhandler == null) {
			userhandler = new UserHandler(dbInfo);
		}	
		return userhandler;
	}
	
	public UserHandler(String dbInfo) {
		dbConn = new MongoDBImpl(dbInfo);
		populateUsers();
	}
	
	
	public UserHandler() {
		dbConn = new MongoDBImpl();
	    populateUsers();
		
	}
	
	@SuppressWarnings("unchecked")
	private void populateUsers() {
		FindIterable<Document> docList = dbConn.getDocs(DBInterface.USER_STORE);
		
		try{
			for (Document d: docList) {
				 String user_name =  d.getString("user_name");
				 SharonUser user = new SharonUser("user_name");
				 userMap.put(user_name, user);
			}
		} catch (MongoTimeoutException mte){
			log.error("MongoTimeoutException: " + mte.getMessage());
		}
	}
	
	
	public synchronized Document process(Document command) {
		
		Document doc = null;
		
		String command_name = command.getString("command_name");
		
		if (command_name.equals("NEW_USER_SESSION")) {
			doc = newUserSession(command.getString("user_name"));
		} 
		return doc;
	}

	// Create a new user session
	protected Document newUserSession(String user_name) {
		if (!userMap.containsKey(user_name)) {
			SharonUser u = new SharonUser(user_name);
			u.incrementActiveSessions();
			userMap.put(user_name, u);
		}
		else {
			userMap.get(user_name).incrementActiveSessions();
		}
		Document doc = new Document("user_name", user_name);
		return doc;
	}

	
	protected void subscribeUserTopic(String user_name, String topic_name) {
		if (userMap.containsKey(user_name)) {
			userMap.get(user_name).addToAllQueues(topic_name);
		}	
	}

}
