package storm.sharon.util.window;


public interface Rankable extends Comparable<Rankable> {

    Object getObject();

    long getCount();

}