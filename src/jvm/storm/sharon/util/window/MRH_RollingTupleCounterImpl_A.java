package storm.sharon.util.window;

import java.util.HashMap;
import java.util.Set;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.bson.Document;

import scala.Tuple2;
import scala.Tuple3;
import storm.sharon.bolt.RollingTupleCountBolt;
import storm.sharon.util.DBInterface;
import storm.sharon.util.MongoDBImpl;
import storm.sharon.util.Topic;
import storm.sharon.util.TopicKeywordHandler;
import backtype.storm.task.OutputCollector;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class MRH_RollingTupleCounterImpl_A extends RollingTupleCounterImpl_A {
	private static final Logger log = Logger.getLogger(MRH_RollingTupleCounterImpl_A.class);
	private DBInterface dbConn = null;
	private final String COMMUNITY_COLLECTION_NAME = DBInterface.WINDOWED_COMMUNITY_STORE;;
	
	public MRH_RollingTupleCounterImpl_A (HashMap<Integer, Topic> topicMap, OutputCollector collector) {
		super(topicMap, collector);
		dbConn = new MongoDBImpl();
	}

	
	@Override
	public void initialize(){
		// For each topic in the topic list if the query_id is a windowed_mention_count or windowed reply_count
		// create a create a SlidingWindowObjectTracker and add to the map
	    log.info("Initilizing rolling object couters for replies and mentions");
		Set<Entry<Integer, Topic>>  topicList = topicMap.entrySet();
		
		for (Entry<Integer,Topic> t: topicList) {
			Integer topic_id = t.getKey();
			initializeTopic(topic_id, Topic.WINDOW_MENTION);
			initializeTopic(topic_id, Topic.WINDOW_REPLY);
			initializeTopic(topic_id, Topic.WINDOW_HASH);
		}
		
	}


	@Override
	public void saveInCollection(Integer topic_id, Document doc) {
		// Save the edge in the collections
		String collectionName = COMMUNITY_COLLECTION_NAME + "_" + topic_id; 
		log.debug("Saving in collection: " + collectionName + " document: " + doc);
		dbConn.insertDoc(collectionName, doc);
	}
	
	
	
}
