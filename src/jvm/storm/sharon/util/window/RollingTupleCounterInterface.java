package storm.sharon.util.window;

import backtype.storm.task.OutputCollector;
import backtype.storm.tuple.Tuple;

public interface RollingTupleCounterInterface {
	
	public void  emitCurrentWindowCounts();
	public void countObjectAndAcknowledge(int topic_id, String query_id, Tuple tuple);
	public void initializeTopic(int topic_id, String query_id);
	public void reInitializeTopic(int topic_id, String query_id);
	
}
