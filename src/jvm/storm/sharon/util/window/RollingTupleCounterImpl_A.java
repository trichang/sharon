package storm.sharon.util.window;

import java.util.HashMap;
import java.util.Set;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.bson.Document;

import scala.Tuple2;
import scala.Tuple3;
import storm.sharon.bolt.RollingTupleCountBolt;
import storm.sharon.util.Topic;
import storm.sharon.util.TopicKeywordHandler;
import backtype.storm.task.OutputCollector;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public abstract class RollingTupleCounterImpl_A implements RollingTupleCounterInterface {
	
	private static final Logger log = Logger.getLogger(RollingTupleCounterImpl_A.class);
	protected HashMap<Integer, Topic> topicMap;
	private HashMap<String, SlidingWindowObjectTracker<Tuple3, Tuple3>> windowedMap;
	private HashMap<String, Integer> timeTracker;
	private OutputCollector collector;
	
	public RollingTupleCounterImpl_A (HashMap<Integer, Topic> topicMap, OutputCollector collector) {
		this.topicMap = topicMap;
		this.collector = collector;
		windowedMap = new HashMap<String, SlidingWindowObjectTracker<Tuple3, Tuple3>>();
		timeTracker = new  HashMap<String, Integer>();
		initialize();
	}

	public abstract void initialize();
	public abstract void saveInCollection(Integer topic_id, Document doc);

	@Override 
	public void emitCurrentWindowCounts() {
		
		log.debug("Emitting Current Window Counts");
		Set<Entry<String, SlidingWindowObjectTracker<Tuple3, Tuple3>>>  entrySet = windowedMap.entrySet();
		for (Entry<String, SlidingWindowObjectTracker<Tuple3, Tuple3>> t: entrySet) {
			String topic_query_id = t.getKey();
			Integer indx = topic_query_id.indexOf('_');
			Integer topic_id = Integer.parseInt(topic_query_id.substring(0, indx));
 			String query_id = topic_query_id.substring(indx+1, topic_query_id.length());
			log.debug("Emitting window counts for topic: " + topic_id + " query_id: " + query_id);
 			emitCurrentWindowCounts(topic_id, query_id);
		}
	}
	
	private void emitCurrentWindowCounts(int topic_id, String query_id) {
		String mapKey = getMapKey(topic_id, query_id);
		// Check if it is time to emit the counts for the current windows
		if (topicMap.containsKey(topic_id)){
			Integer updateTime = topicMap.get(topic_id).getUpdateFrequencySeconds(query_id);
			Integer updateThreshold = topicMap.get(topic_id).getRollingCountThreshold(query_id);
			Integer timeElapsed = timeTracker.get(mapKey);
			Integer timeNow = timeElapsed + RollingTupleCountBolt.EMIT_FREQUENCY_IN_SECONDS;
			if (timeNow == updateTime) {
				log.debug("Time to advance the slot, resetting the counter and advancing slot: " + query_id + " " + timeNow + " " + updateTime);
				timeTracker.put(mapKey, 0);
				emitCurrentWindowCounts(windowedMap.get(mapKey), updateThreshold, query_id);
			} else {
				log.debug("Not the time to advance the slot: "  + query_id + " " + timeNow + " " + updateTime);
				timeTracker.put(mapKey, timeNow);
			}
		}

	}

	@Override
	public void countObjectAndAcknowledge(int topic_id, String query_id, Tuple tuple) {
		
		String mapKey = getMapKey(topic_id, query_id);
		if (topicMap.containsKey(topic_id)) {
			SlidingWindowObjectTracker<Tuple3, Tuple3> objectTracker = windowedMap.get(mapKey);
			Integer updateThreshold = topicMap.get(topic_id).getRollingCountThreshold(query_id);
			
			Tuple3<String, String, String> tupleKey = (Tuple3<String, String, String>)tuple.getValueByField("tupleKey");
	    	Tuple3<String, String, String> tupleValue = 
	    			(Tuple3<String, String, String>)tuple.getValueByField("tupleValue");
	       
	    	Map<Tuple3, Long> tupleCounts = objectTracker.addObjectReturnValues(tupleKey, tupleValue, updateThreshold);
	    	log.info("Emitting Tuples: Incremental: " + tupleCounts);
	    	emit(tupleCounts, 0, query_id);
	        collector.ack(tuple);
		}

	}
	
	
	
	@Override
	public void initializeTopic(int topic_id, String query_id) {
		if (topicMap.containsKey(topic_id)) {
			Topic topic = topicMap.get(topic_id);
			String mapKey =  getMapKey(topic_id, query_id);
			Integer numberOfSlots = getNumSlots(topic.getWindowLengthSeconds(query_id), topic.getUpdateFrequencySeconds(query_id));
			log.info("Initializing topic: " + query_id + " " + topic.getTopicName() + " Number of slots: " + numberOfSlots);
			windowedMap.put(mapKey, new SlidingWindowObjectTracker<Tuple3, Tuple3>(numberOfSlots));
			timeTracker.put(mapKey, 0);
		}
	}
	
	@Override
	public void reInitializeTopic(int topic_id, String query_id) {
		log.info("Sending 0 counts for existing nodes : " + query_id + " Topic id:  " + topic_id );
		Document doc = new Document("command", "RESET");
		doc.append("query_id", query_id);
		collector.emit(new Values(doc.toJson(), topic_id));
		initializeTopic(topic_id, query_id);
	}
	
	private String getMapKey(Integer topic_id, String query_id){
		return Integer.toString(topic_id) + "_" + query_id;
	}
	
	private Integer getNumSlots(int windowLengthInSeconds, int windowUpdateFrequencyInSeconds) {
        return windowLengthInSeconds / windowUpdateFrequencyInSeconds;
    }

	
	private void emitCurrentWindowCounts(SlidingWindowObjectTracker<Tuple3, Tuple3> objectTracker, Integer threshold, String query_id) {
	        Map<Tuple3, Long> tupleCounts = objectTracker.advanceWindowReturnValues(threshold);
	        int actualWindowLengthInSeconds = 0;
	        log.debug("Emitting Tuples: Periodic: " + tupleCounts);
	        emit(tupleCounts, actualWindowLengthInSeconds, query_id);
	 }

	 private void emit(Map<Tuple3, Long> tupleCounts, int actualWindowLengthInSeconds, String query_id) {
	        if (tupleCounts != null) {
	        	for (Entry<Tuple3, Long> entry : tupleCounts.entrySet()) {
	        		Tuple3 tuple = entry.getKey();
	        		Integer topic_id = Integer.parseInt((String)tuple._1());
	        		String field_1 = (String)tuple._2();
	        		String field_2 = (String)tuple._3();
	        		int count = (int)((long)entry.getValue());
	        		Document doc = new Document("count", count);
	        		doc.append("actualWindowLength", actualWindowLengthInSeconds);
	        		doc.append("topic_id", topic_id);
	        		doc.append("dest_node", field_1);
	        		doc.append("src_node", field_2);
	        		doc.append("time", System.currentTimeMillis());
	        		doc.append("query_id", query_id);
	        		log.debug("Emitting Tuple: " + doc.toJson());
	        		collector.emit(new Values(doc.toJson(), topic_id));
	        		saveInCollection(topic_id, doc);
	        	}
	        }
	    }

		
}
