package storm.sharon.util.window;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This class provides map of object counts and total count of objects 
 * 
 * @param <T>
 *            The type of those objects we want to count.
 */
public final class ObjectMap<T> implements Serializable {

    private static final long serialVersionUID = 4858185737378394432L;

    private final Map<T, Long> objToCounts = new HashMap<T, Long>();
    private long totalCount;

   

	public ObjectMap(int numSlots) {
       totalCount = 0;
    }
	
	 public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

    public long addObject(T obj) {
    	//System.out.println("ObjectMap: Adding Object");
    	long count;
    	if (objToCounts.containsKey(obj)) {
    		count = objToCounts.get(obj);
    		count = count + 1;
    	} else {
    		count = 1;
    	}
    	objToCounts.put(obj, count);
        totalCount++;
        return count;
    }

    public long getCount(T obj) {
    	long count;
    	if (objToCounts.containsKey(obj)) {
    		count = objToCounts.get(obj);
    	} else {
    		count = 0;
    	}
    	return count;
    }

    public Map<T, Long> getCounts() {
        return objToCounts;
    }

    public void reset() {
    	for (T obj : objToCounts.keySet()) {
            objToCounts.put(obj, (long)0);
        }
    	totalCount = 0;
    }
    

}

   


