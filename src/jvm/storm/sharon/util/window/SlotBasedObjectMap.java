package storm.sharon.util.window;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import storm.sharon.util.TwitterQueryManager;
import backtype.storm.tuple.Values;

/**
 * This class provides per-slot counts of the occurrences of objects.
 * 
 * It can be used, for instance, as a building block for implementing sliding window counting of objects.
 * 
 * @param <T>
 *            The type of those objects we want to count.
 */
public final class SlotBasedObjectMap<K, V> implements Serializable {

    private static final long serialVersionUID = 4858185737378394432L;

    private final Map<K, ArrayList<ObjectMap<V>>> objToCounts = new HashMap<K, ArrayList<ObjectMap<V>>>();
    private final Map<K, Long> totalCounts = new HashMap<K, Long>();
    private Log log = LogFactory.getLog( SlotBasedObjectMap.class);
    
    private final int numSlots;

    public SlotBasedObjectMap(int numSlots) {
        if (numSlots <= 0) {
            throw new IllegalArgumentException("Number of slots must be greater than zero (you requested " + numSlots
                + ")");
        }
        this.numSlots = numSlots;
    }

    public void addObject(K key, V item, int slot) {
    	//System.out.println("SlotBasedObjectMap: Adding object");
    	ArrayList<ObjectMap<V>> value = null;
    	value = objToCounts.get(key);
        if (value == null) {
            value = new ArrayList<ObjectMap<V>>(this.numSlots);
            for (int i = 0; i < numSlots; i++) {
            	value.add(new ObjectMap<V>(numSlots));
            }
            objToCounts.put(key, value);
        }
        ObjectMap<V> element = value.get(slot);
        element.addObject(item);
    }
    
    public Map<V, Long> addObjectReturnValues(K key, V item, long threshold, int slot) {
    	//System.out.println("SlotBasedObjectMap: Adding object");
    	ArrayList<ObjectMap<V>> value = null;
    	value = objToCounts.get(key);
        if (value == null) {
            value = new ArrayList<ObjectMap<V>>(this.numSlots);
            for (int i = 0; i < numSlots; i++) {
            	value.add(new ObjectMap<V>(numSlots));
            }
            objToCounts.put(key, value);
            totalCounts.put(key, (long)0);
        }
        Long currentCount = totalCounts.get(key);
        currentCount++;
        totalCounts.put(key, currentCount);
        ObjectMap<V> element = value.get(slot);
        Long itemCount = element.addObject(item);
        Map<V, Long> c;
        c =  getValueCounts(key);
        log.info("SSSSSS Values for the key: " + c);
        if (currentCount == threshold) {
        	// This key is now considered a significant key
        	c =  getValueCounts(key);
        } else if (currentCount > threshold) {
        	// This is already a significant key
        	c =  new HashMap<V, Long>();
        	c.put(item, itemCount);
        } else {
        	c =  null;
        }
        return c;
    }

    public long getCount(K obj, int slot) {
    	ArrayList<ObjectMap<V>>  counts = objToCounts.get(obj);
        if (counts == null) {
            return 0;
        }
        else {
            return counts.get(slot).getTotalCount();
        }
    }
    
    
    public Map<V, Long>  getValueCounts(K obj) {
    	Map<V, Long> valueCount = new HashMap<V, Long>();
    	ArrayList<ObjectMap<V>>  values = objToCounts.get(obj);
    	for (ObjectMap<V> value: values) {
    		Map<V, Long> x = value.getCounts();
    		Set<Entry<V, Long>> y = x.entrySet();
    		for (Entry<V,Long> e: y) {
    			Long count;
    			if (valueCount.containsKey(e.getKey())) {
    				count = valueCount.get(e.getKey()) + e.getValue();
    			} else {
    				count = e.getValue();
    			}
    			valueCount.put(e.getKey(), count);
    		}
    		
    	}
    	
    	return valueCount;
    	
    }
    
    public Map<V, Long>  getSlotValueCounts(K obj, int slot) {
    	Map<V, Long> valueCount = new HashMap<V, Long>();
    	ArrayList<ObjectMap<V>>  values = objToCounts.get(obj);
    	ObjectMap<V> value = values.get(slot);
    	return value.getCounts();
    }
    
    public Map<V, Long>  getValueCountsLessTail(K obj, int tailSlot, long threshold) {
    	Map<V, Long> valueCounts = getValueCounts(obj);
    	Map<V, Long> newValueCounts = new HashMap<V, Long>();
    	Map<V, Long> slotValueCounts = getSlotValueCounts(obj, tailSlot);
    	long totalCount = computeTotalCount(obj);
    	long tailSlotCount = getCount(obj, tailSlot);
    	
    	log.info("SSSSSS: "  + obj + " Total Count = " + totalCount + 
    										" tailSlotCount "  + tailSlotCount);
    	
    	Set<Entry<V, Long>> entries =  valueCounts.entrySet();
    	for (Entry<V, Long> e: entries) {
    		if((totalCount - tailSlotCount) < threshold) {
        		// Node is not significant - send zero to all links 
    			newValueCounts.put(e.getKey(), (long)0);
        	} else {
	    		if (slotValueCounts.containsKey(e.getKey())) {
	    			long count = slotValueCounts.get(e.getKey());
	    			long currentCount = valueCounts.get(e.getKey());
	    			newValueCounts.put(e.getKey(), currentCount - count);
	    			log.info("SSSSSS: "  + obj + " CurrentCount = " + currentCount + 
							" NextCount "  + count);

	    		}
        	}
    	}
    	
    	return newValueCounts;
    }

    public Map<K, Long> getCounts() {
        Map<K, Long> result = new HashMap<K, Long>();
        for (K obj : objToCounts.keySet()) {
            result.put(obj, computeTotalCount(obj));
        }
        return result;
    }

    private long computeTotalCount(K obj) {
    	ArrayList<ObjectMap<V>> curr = objToCounts.get(obj);
        long total = 0;
        for (ObjectMap<V> l : curr) {
            total += l.getTotalCount();
        }
        return total;
    }

    /**
     * Reset the slot count of any tracked objects to zero for the given slot.
     * 
     * <em>Implementation detail:</em> As an optimization this method will also remove any object from the counter whose
     * total count is zero after the wipe of the slot (to free up memory).
     * 
     * @param slot
     */
    public void wipeSlot(int slot) {
        Set<K> objToBeRemoved = new HashSet<K>();
        for (K obj : objToCounts.keySet()) {
        	
            long totCount = resetSlotCountToZero(obj, slot);
            Long currentCount = totalCounts.get(obj);
            totalCounts.put(obj, currentCount - totCount);
           
            if (shouldBeRemovedFromCounter(obj)) {
                objToBeRemoved.add(obj);
            }
        }
        for (K obj : objToBeRemoved) {
            objToCounts.remove(obj);
            totalCounts.remove(obj);
        }
    }

    private long resetSlotCountToZero(K obj, int slot) {
    	ArrayList<ObjectMap<V>> counts = objToCounts.get(obj);
    	ObjectMap<V> item = counts.get(slot);
    	long totCount = item.getTotalCount();
        item.reset();
        return totCount;
    }

    private boolean shouldBeRemovedFromCounter(K obj) {
        return computeTotalCount(obj) == 0;
    }

}

   


