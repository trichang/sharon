package storm.sharon.util.window;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public final class SlidingWindowObjectTracker<K, V> implements Serializable {

    private static final long serialVersionUID = -2645063988768785810L;

    private SlotBasedObjectMap<K, V> objCounter;
    private int headSlot;
    private int tailSlot;
    private int windowLengthInSlots;

    public SlidingWindowObjectTracker(int windowLengthInSlots) {
        if (windowLengthInSlots < 2) {
            throw new IllegalArgumentException("Window length in slots must be at least two (you requested "
                + windowLengthInSlots + ")");
        }
        this.windowLengthInSlots = windowLengthInSlots;
        this.objCounter = new SlotBasedObjectMap<K, V>(this.windowLengthInSlots);

        this.headSlot = 0;
        this.tailSlot = slotAfter(headSlot);
    }

    public void addObject(K key, V value) {
    	objCounter.addObject(key, value, headSlot);
    }
    
    public Map<V, Long> addObjectReturnValues(K key, V value, long threshold) {
    	Map<V, Long> s = objCounter.addObjectReturnValues(key, value, threshold, headSlot);
    	return s;
    }
    
    public Map<V, Long> advanceWindowReturnValues(long threshold) {
        Map<V, Long> valueCounts = new HashMap<V, Long>();
        ArrayList<K> keys = getKeysGreaterThanEqual(threshold);
        for (K key: keys) {
        	Map<V, Long> m = objCounter.getValueCountsLessTail(key, tailSlot, threshold);
        	valueCounts.putAll(m);
        }
        objCounter.wipeSlot(tailSlot);
        advanceHead();
        return valueCounts;
    }
    
    
    public Map<V, Long> getObjectsThenAdvanceWindow(long threshold) {
        Map<V, Long> valueCounts = new HashMap<V, Long>();
        ArrayList<K> keys = getKeysGreaterThanEqual(threshold);
        for (K key: keys) {
        	Map<V, Long> m = objCounter.getValueCounts(key);
        	valueCounts.putAll(objCounter.getValueCounts(key));
        }
        objCounter.wipeSlot(tailSlot);
        advanceHead();
        return valueCounts;
    }
    
    
 
    public ArrayList<K> getKeysGreaterThanEqual(long count) {
    	ArrayList<K> keys = new ArrayList<K>();
        Map<K, Long> counts = objCounter.getCounts();
        Set<Entry<K, Long>> entries =  counts.entrySet();
        for (Entry entry: entries) {
        	if ((Long)entry.getValue() >= count) {
        		keys.add((K)entry.getKey());
        	}
        }
        return keys;
    }

    private void advanceHead() {
        headSlot = tailSlot;
        tailSlot = slotAfter(tailSlot);
    }

    private int slotAfter(int slot) {
        return (slot + 1) % windowLengthInSlots;
    }

}
