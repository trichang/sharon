/**  
* TopicKeywordCommandHandler.java - this class handles commands related to Topic and Keyword management
* @author  Shanika Karunasekera
* @version 1.0 
*/

package storm.sharon.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import kafka.admin.AdminUtils;
import storm.sharon.TwitterTrackerTopologyOne;

import org.I0Itec.zkclient.ZkClient;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import com.mongodb.MongoTimeoutException;
import com.mongodb.client.FindIterable;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import backtype.storm.task.OutputCollector;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class TopicKeywordCommandHandler extends TopicKeywordHandler {

	private final int MIN_TIME_BETWEEN_WRITES = 1000*60;
	private MessageQueueInterface messageQueue = null;
	private Log log = LogFactory.getLog(TopicKeywordCommandHandler.class);
	private boolean writeToMessageQueue = false;
	private long lastWriteTime = 0;
	private OutputCollector _collector;
	private ZkClient zkClient;
	private int maxNumTopics = 0;
	
	public TopicKeywordCommandHandler(ZkClient zkClient) {
		super();
		messageQueue = new FileMessageQueueImpl();
		writeToMessageQueue = true;
		this.zkClient = zkClient;
		// Set the maximum number of topics
		setNumTopics();
		
	}
	
	public TopicKeywordCommandHandler(OutputCollector collector, ZkClient zkClient) {
		super();
		_collector = collector;
		messageQueue = new FileMessageQueueImpl();
		writeToMessageQueue = true;
		this.zkClient = zkClient;
		// Set the maximum number of topics
		setNumTopics();
	}

	private void setNumTopics() {
		PropertiesConfiguration config = storm.sharon.util.Properties.getConfig();
		if(config!=null){
			if(config.containsKey("maxNumTopics")) { 
				log.debug("Setting the maximum number of topics from configuration file");
				this.maxNumTopics=config.getInt("maxNumTopics");
			} else {
				log.debug("Setting the maximum number of topics to preset value");
				this.maxNumTopics = TwitterTrackerTopologyOne.MAX_NUM_TOPICS;
			}
		} else {
				log.debug("Setting the maximum number of topics to preset value");
				this.maxNumTopics = TwitterTrackerTopologyOne.MAX_NUM_TOPICS;
		}
		log.debug("Maximum number of topics: " + this.maxNumTopics);
		
	}
	public Document process(Document command) {
		
		Document doc = null;
		
		dbConn.insertDoc(DBInterface.USER_ACTIVITY_LOG, command);
		
		String command_name = command.getString("command_name");
		
		log.info("Recevied command: "+command.getString("command_name"));
		if (command_name.equals("ADD_TOPIC")) {
			doc = addTopic(command.getString("topic_name"), command.getInteger("topic_type"), command.getString("topic_owner"));
			log.debug("Topic document: "  + doc);
			if ( doc != null) {
				if (!dbConn.containsDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id", doc.getInteger("topic_id"))) {
					log.info("Adding topic ["+command.getString("topic_name")+"] to database.");
					dbConn.insertDoc(DBInterface.TOPIC_KEYWORD_STORE, doc);
				} else {
					log.warn("Topic ["+command.getString("topic_name")+"] already exists in the database");
				}	
				// topic_owner will subscribe to all queues
				Document doc2 = subscribeUserTopic(command.getString("topic_owner"), command.getString("topic_name"));
				updateUserStore(doc2);
				command_name = "NEW_TOPIC";
			} else {
				log.info("System has reached the maximum number of topics - not adding topic");
				doc = command;
				command_name = "MAX_TOPIC_NUM_REACHED";
			}
		} else if (command_name.equals("STOP_TOPIC")) {
			doc = stopTopic(command.getInteger("topic_id"));
			dbConn.updateDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id",  doc.getInteger("topic_id"),"isCurrent", doc.getBoolean("isCurrent"));
			log.info("Set topic ["+doc.getInteger("topic_id")+"] status to not current.");
			writeToMessageQueue = true;

		} else if (command_name.equals("DELETE_TOPIC")) {
			// Remove user subscriptions to the topic
			removeFromUser(command.getInteger("topic_id"));
			// Remove the topic completely and make the topic id slot available for another topic 
			doc = deleteTopic(command.getInteger("topic_id"));
			// Delete the topic from the topic-keyword table
			// Drop the collections for the topic
			// If the topic was an active topics write the updated keywords for tracking
			dbConn.deleteDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id",  doc.getInteger("topic_id"));
			dbConn.dropCollection(DBInterface.TWEET_STORE + "_" +doc.getInteger("topic_id"));
			dbConn.dropCollection(DBInterface.COMMUNITY_STORE + "_" + doc.getInteger("topic_id"));
			dbConn.dropCollection(DBInterface.DISCUSSION_STORE + "_" + doc.getInteger("topic_id"));
			// TODO drop the collections (tweet, discussion and community collection)
			//log.info("Set topic ["+doc.getInteger("topic_id")+"] status to not current.");
			
			if (doc.getBoolean("isCurrent") == true) {
				writeToMessageQueue = true;
			}

		} else if (command_name.equals("START_TOPIC")) {
			doc = startTopic(command.getInteger("topic_id"));
			dbConn.updateDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id",  doc.getInteger("topic_id"),"isCurrent", doc.getBoolean("isCurrent"));
			log.info("Set topic ["+doc.getInteger("topic_id")+"] status to current.");
			writeToMessageQueue = true;
		} else if (command_name.equals("ADD_QUERY")) {
			doc = addQuery(command.getInteger("topic_id"), (Document)command.get("query"));
			//dbConn.deleteDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id",  doc.getInteger("topic_id") );
			dbConn.updateDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id",  doc.getInteger("topic_id"), 
					"queries", doc.get("queries"));
			command_name = "UPDATE_QUERIES";
		} else if (command_name.equals("GET_TOPICS")) {
			doc = getTopics();
			log.info("Getting topics from database.");
		} else if (command_name.equals("ADD_KEYWORDS")) {
			// Replace the keyword list in the database with the new list
			log.info("Add keywords: " + command.get("keywords"));
			Document allKeywords = addKeywords(command.getInteger("topic_id"),  command.get("keywords"), 0);
			doc = command;
			dbConn.updateDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id", doc.getInteger("topic_id"),"keywords", allKeywords.get("keywords"));
			if (allKeywords.getBoolean("isCurrent")) {
				writeToMessageQueue = true;
			}
		} else if (command_name.equals("ADD_NONCURRENT_KEYWORDS")) {
			// Replace the keyword list in the database with the new list
			log.info("Add non-current keywords: " + command.get("keywords"));
			Document allKeywords = addNonCurrentKeywords(command.getInteger("topic_id"),  command.get("keywords"), 0);
			doc = command;
			dbConn.updateDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id", doc.getInteger("topic_id"),"keywords", allKeywords.get("keywords"));
			if (allKeywords.getBoolean("isCurrent")) {
				writeToMessageQueue = true;
			}
		} else if (command_name.equals("NEW_KEYWORDS")) {
			// Replace the keyword list in the database with the new list
			log.info("New keywords: " + command.get("keywords"));
			Document allKeywords = newKeywords(command.getInteger("topic_id"),  command.get("keywords"));
			doc = command;
			dbConn.updateDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id", doc.getInteger("topic_id"),"keywords", allKeywords.get("keywords"));
			if (allKeywords.getBoolean("isCurrent")) {
				writeToMessageQueue = true;
			}
		} else if (command_name.equals("DELETE_KEYWORDS")) {
			doc = command;
			Document allKeywords = deleteKeywords(command.getInteger("topic_id"),command.get("keywords"));
			// Replace the keyword list in the database with the new list
		    log.info("Delete keywords: " + allKeywords);
			dbConn.updateDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id", doc.getInteger("topic_id"),"keywords", allKeywords.get("keywords"));
			if (allKeywords.getBoolean("isCurrent")) {
				writeToMessageQueue = true;
			}
		} else if (command_name.equals("BLACKLIST_KEYWORDS")) {
			doc = command;
			Document allKeywords = blackListKeywords(command.getInteger("topic_id"),command.get("keywords"));
			// Replace the keyword list in the database with the new list
		    log.info("Blacklist keywords: " + allKeywords);
			dbConn.updateDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id", doc.getInteger("topic_id"),"keywords", allKeywords.get("keywords"));
			if (allKeywords.getBoolean("isCurrent")) {
				writeToMessageQueue = true;
			}
		} else if (command_name.equals("BLACKLIST_NONCURRENT_KEYWORDS")) {
			doc = command;
			Document allKeywords = blackListKeywords(command.getInteger("topic_id"),command.get("keywords"));
			// Replace the keyword list in the database with the new list
		    log.info("Blacklist keywords: " + allKeywords);
			dbConn.updateDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id", doc.getInteger("topic_id"),"keywords", allKeywords.get("keywords"));
			// Track change is not required
		} else if (command_name.equals("WHITELIST_KEYWORDS")) {
			doc = command;
			Document allKeywords = whiteListKeywords(command.getInteger("topic_id"),command.get("keywords"));
			// Replace the keyword list in the database with the new list
		    log.info("Whitelist keywords: " + allKeywords);
			dbConn.updateDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id", doc.getInteger("topic_id"),"keywords", allKeywords.get("keywords"));
		} else if (command_name.equals("GET_KEYWORDS")) {
			doc = getKeywords(command.getInteger("topic_id"));
			log.info("Get keywords from database.");
			command_name = "GET_KEYWORDS";
		} else if (command_name.equals("NEW_USER_SESSION")) {
			boolean isKeyCurrent = createUser(command);
			String sessionid = command.getString("sessionid");
			if (isKeyCurrent) {
				doc = newUserSession(command.getString("user_name"));
				doc.append("sessionid",sessionid);
				doc.append("authenticated", true);
				// append topic information
				FindIterable<Document> docList = dbConn.getDocs(DBInterface.TOPIC_KEYWORD_STORE);
				ArrayList<Document> dl = new ArrayList<Document>();
				try{
					for(Document d:docList){
						dl.add(d);
					}
					doc.append("topics", dl);
				} catch (MongoTimeoutException mte){
					log.error("MongoTimeoutException: " + mte.getMessage());
				}	
				// append the user information
				try{
					ArrayList<Document> users = new ArrayList<Document>();
					Document user = dbConn.getDoc(DBInterface.USER_STORE,"user_name",command.getString("user_name"));
					user.remove("twitter_auth_info"); // don't want to broadcast sensitive information
					users.add(user);
					doc.append("users",users);
				} catch (MongoTimeoutException mte){
					log.error("MongoTimeoutException: " + mte.getMessage());
				}
				command_name = "NEW_USER_SESSION";
			} else {
				doc = new Document("sessionid",sessionid);
				doc.append("authenticated", false);
				command_name = "STARTUP_PROTOCOL";
			}
		} else if (command_name.equals("VALIDATE_LICENSE_KEY")){
			boolean valid = checkLicenceKey(command.getString("license_key"));
			String sessionid = command.getString("sessionid");
			doc = new Document("sessionid", sessionid);
			if(valid){
				doc.append("valid_license_key",true);
			} else {
				doc.append("valid_license_key",false);
				doc.append("license_error","license has expired"); // maybe different kinds of errors
			}
			command_name = "STARTUP_PROTOCOL";
		} else if (command_name.equals("OBTAIN_AUTH_URL")){
			String sessionid = command.getString("sessionid");
			doc = new Document("sessionid", sessionid);
	        Twitter twitterStream = new TwitterFactory().getInstance();
	        Configuration config = storm.sharon.util.Properties.getConfig();
	        twitterStream.setOAuthConsumer(config.getString("RapidConsumerKey"), config.getString("RapidConsumerSecret"));
	        try {
	        	log.debug("Getting twitter URL information for session "+sessionid);
	        	RequestToken requestToken = twitterStream.getOAuthRequestToken(); // get the request token
	        	String twitterURL = requestToken.getAuthorizationURL();
	        	log.debug("URL: " + twitterURL);
	        	doc.append("url", twitterURL);
	        	doc.append("request_token", requestToken.getToken());
	        	doc.append("request_token_secret", requestToken.getTokenSecret());
	        } catch (Exception e){
	        	log.error("Failed to get twitter URL information: "+e);
	        }
	        command_name = "STARTUP_PROTOCOL";
		} else if (command_name.equals("VALIDATE_USER_CREDENTIAL")){
			Document twitter_auth_info = (Document) command.get("twitter_auth_info");
			String sessionid = command.getString("sessionid");
			Twitter twitterStream = new TwitterFactory().getInstance();
			Configuration config = storm.sharon.util.Properties.getConfig();
			twitterStream.setOAuthConsumer(config.getString("RapidConsumerKey"), config.getString("RapidConsumerSecret"));
			twitterStream.setOAuthAccessToken(new AccessToken(twitter_auth_info.getString("twitter_consumerKey"),
					twitter_auth_info.getString("twitter_consumerSecret")));
			doc = new Document("sessionid", sessionid);
			try {
				User user = twitterStream.verifyCredentials();
				if(user!=null && user.getScreenName().equals(command.getString("user_name"))){
					doc.append("valid_credential",true);
				} else {
					doc.append("valid_credential",false);
				}
			} catch (TwitterException e) {
				log.error("Could not verify credentials for sessionid "+sessionid);
				doc.append("valid_credential",false);
			}
			command_name = "STARTUP_PROTOCOL";
		} else if (command_name.equals("SUBSCRIBE_TOPIC_QUEUE")) {
			doc = subscribeUserTopicQueue(command.getString("user_name"), command.getInteger("topic_id"), command.getString("queue_name"));	
			if (doc != null) {
				updateUserStore(doc);
			}
			doc = command;
		} else if (command_name.equals("UNSUBSCRIBE_TOPIC_QUEUE")) {
			doc = unSubscribeUserTopicQueue(command.getString("user_name"), command.getInteger("topic_id"), command.getString("queue_name"));
			if (doc != null) {
				updateUserStore(doc);
			}
			doc = command;
		} else if (command_name.equals("SUBSCRIBE_TOPIC")) {
			doc = subscribeUserTopic(command.getString("user_name"), command.getInteger("topic_id"));
			if (doc != null) {
				updateUserStore(doc);
			}
			doc = command;
		} else if (command_name.equals("UNSUBSCRIBE_TOPIC")) {
			doc = unSubscribeUserTopic(command.getString("user_name"), command.getInteger("topic_id"));
			if (doc != null) {
				updateUserStore(doc);
			}
			doc = command;
		} else if (command_name.equals("TOPIC_WINDOW_PARM_UPDATE")) {
			doc = updateWindowParm(command);
			if (doc != null) {
				dbConn.updateDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id", doc.getInteger("topic_id"), doc.getString("query_id"), doc.get("window_params"));
			}
		} else {
			return null;
		}
		
		
		doc.append("command_name", command_name);
		return doc;
		
	}
	
	

	public void writeMessageQueue(Tuple tuple) {
		
		if (writeToMessageQueue)  {
		
			long elapsedTime = System.currentTimeMillis() - lastWriteTime;
			
			if (elapsedTime >= MIN_TIME_BETWEEN_WRITES) {
			
				// TODO: Shanika check if enough time has lapsed since the
				// last write to the message queue, if not sleep.
				//  Write the keywords to the message queue for tracking 
				Set<Entry<String, HashSet<Integer>>> entries = allKeyWords.entrySet();
					
				ArrayList<String> trackWords = new ArrayList<String>();
				for (Entry<String, HashSet<Integer>> e: entries) {
					String keyword = e.getKey();
					trackWords.add(keyword);
				}
					 
				String a;
				a = String.valueOf(System.currentTimeMillis());
				String filename = "keywords_" + a;
				
				// Replace file message queue with kafka message queue write
				// messageQueue.write(MessageQueueInterface.TRACK_MESSAGE_QUEUE, filename, trackWords);
				
				Document doc_resp = new Document("trackWords", trackWords);
				
				doc_resp.append("command_name", "CHANGE_TRACK");
				String message = doc_resp.toJson();
	    		log.info("Sending message track change message : " + message);
	    		//Tuple tuple = null;
	    		_collector.emit(tuple, new Values(message));
				//_collector.emit("stream_spout",  new Values(message));
				lastWriteTime = System.currentTimeMillis();
				writeToMessageQueue = false;
			}
	    
		}
	}
	
	
	private boolean createUser(Document command) {
		
		boolean isUserCurrent = true;

		if (command.containsKey("license_key")) {
			isUserCurrent = checkLicenceKey(command.getString("license_key"));
		}
		
		if (command.containsKey("twitter_auth_info")) {
			Document twitter_auth_info = (Document) command.get("twitter_auth_info");
			Twitter twitterStream = new TwitterFactory().getInstance();
			Configuration config = storm.sharon.util.Properties.getConfig();
			twitterStream.setOAuthConsumer(config.getString("RapidConsumerKey"), config.getString("RapidConsumerSecret"));
			
			twitterStream.setOAuthAccessToken(new AccessToken(twitter_auth_info.getString("twitter_consumerKey"),
					twitter_auth_info.getString("twitter_consumerSecret")));
			try {
				User user = twitterStream.verifyCredentials();
				if(user!=null && user.getScreenName().equals(command.getString("user_name"))){
					log.debug("User verified: "+user.getScreenName());
				} else {
					isUserCurrent=false;
				}
			} catch (TwitterException e) {
				log.error("Could not verify supplied credentials for "+command.getString("user_name"));
			}
		}
		
		if (isUserCurrent){		
			log.debug("User license key is valid, continuing");
			String user_name = command.getString("user_name");
			
			log.debug("Creating user: "+ user_name);
			
			
			if (!dbConn.containsDoc(DBInterface.USER_STORE, "user_name", user_name)) {
				log.debug("Adding user ["+ user_name +"] to database.");
				Document doc = new Document("user_name", user_name);
				// if the user has authenticated with twitter
				// save the twitter authentication keys
				
				if (command.containsKey("twitter_auth_info")) {
			  	   doc.append("twitter_auth_info", command.get("twitter_auth_info"));
				}
				dbConn.insertDoc(DBInterface.USER_STORE, doc);
			} else {
				log.warn("User ["+ user_name +"] already exists in the database");
				// if the user has authenticated with twitter
				// save the twitter authentication keys
				if (command.containsKey("twitter_auth_info")) {
					log.debug("Updating Twitter authentication keys");
					dbConn.updateDoc(DBInterface.USER_STORE, "user_name", user_name, "twitter_auth_info", command.get("twitter_auth_info"));
				}
			}	
			createUserMessageQueues(user_name);
		} else {
			log.debug("User license key is invalid, or user twitter credential is unauthorized: returning");
		}
			
		return isUserCurrent;
	}
	
	
	private boolean checkLicenceKey(String licenseKey) {
		if (dbConn.containsDoc(DBInterface.PRODUCT_KEY_STORE, "license_key", licenseKey)) {
			Document d = dbConn.getDoc(DBInterface.PRODUCT_KEY_STORE, "license_key", licenseKey);
			String expiryDate = d.getString("expiry_date");
			SimpleDateFormat dateF = new SimpleDateFormat("dd/MM/yyyy");
			
			Date expDate;

			try {
				expDate = dateF.parse(expiryDate);
			} catch (Exception e) {
				expDate = null;
				log.error("Failed to parse the date");
			}
		
			Date currDate = new Date();
			
			log.info("Current Date: " + dateF.format(currDate));
			log.info("Expiry Date: " + dateF.format(expDate));

		    if (currDate.after(expDate)) {
				 log.info("License has expired.");
				 return false;
			} else {
				 log.info("License is current.");
				 return true;
			}
			
			
		} else {
			log.debug("License key does not exist in the database");
			return false;	
		}
		
	}

	// Create the appropriate kafka message queues for the user
	private void createUserMessageQueues(String user_name) {
       
		ArrayList<String> messageQueues = new ArrayList<String>();
		messageQueues.add(MessageQueueInterface.TWEET_MESSAGE_QUEUE + "_" + user_name);
		messageQueues.add(MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE + "_" + user_name);
		messageQueues.add(MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE + "_" + user_name);
		messageQueues.add(MessageQueueInterface.TUPLE_MESSAGE_QUEUE + "_" + user_name);
		messageQueues.add(MessageQueueInterface.CUSTOM_QUERY_MESSAGE_QUEUE + "_" + user_name);
		messageQueues.add(MessageQueueInterface.CUSTOM_STREAM_MESSAGE_QUEUE + "_" + user_name);
		
        int numPartitions = 1;
        int replicationFactor = 1;
        Properties topicConfig = new Properties();

        for (String queueName: messageQueues) {
	        if (!AdminUtils.topicExists(zkClient, queueName)) {
	        	log.debug("Creating kafka message queue: " + queueName);
	        	AdminUtils.createTopic(zkClient, queueName, numPartitions, replicationFactor, topicConfig);
	        } else {
	        	log.debug("Kafka message queue: "+ queueName + " already exists");
	        }
        }
	
		
	}
	
	
	private void updateUserStore(Document doc){
		log.debug("Updating the topics to add user subscription for the topic:"  + doc.getString("user_name") + " " + doc);
		dbConn.updateDoc(DBInterface.USER_STORE, "user_name", doc.getString("user_name"), "tweet_queue_sub", (ArrayList<String>)doc.get("tweet_queue_sub"));
		dbConn.updateDoc(DBInterface.USER_STORE, "user_name", doc.getString("user_name"), "discussion_queue_sub", (ArrayList<String>)doc.get("discussion_queue_sub"));
		dbConn.updateDoc(DBInterface.USER_STORE, "user_name", doc.getString("user_name"), "community_queue_sub", (ArrayList<String>)doc.get("community_queue_sub"));
		dbConn.updateDoc(DBInterface.USER_STORE, "user_name", doc.getString("user_name"), "tuple_queue_sub", (ArrayList<String>)doc.get("tuple_queue_sub"));
	}
	
	
	
	private void removeFromUser(int topic_id) {
		Topic topic =  getTopicIdMap().get(topic_id);
		ArrayList<String> userList = topic.getSharedUsersAsList();
		
		for (String user_name: userList) {
			log.debug("User: " + user_name);
			if (userMap.containsKey(user_name)) {
				log.debug("Removing topic from user subscriptions: " + user_name);
				SharonUser u = userMap.get(user_name);
				u.removeFromAllQueues(topic.getTopicName());
				updateUserStore(u.toDocument());
			} else {
				log.debug("User does not exist in the list: " + user_name);
			}
		}

	}
	

	
	private Document addTopic(String topic_name, Integer topic_type, String topic_owner) {
		Integer topic_id;
		Boolean isCurrent;
		Topic t;
		topic_id =  getNextAvailable();
		if (topic_id != maxNumTopics) {
			isCurrent = true;
			t =  new Topic(topic_name, topic_id, isCurrent, topic_type, topic_owner);
			topicIdMap.put(topic_id, t);		
		    log.debug("Adding topic t: " + t.toDoc());
		    return t.toDoc();	
		} else {
			log.info("System has reached the maximum number of topics");
			return null;
		}
	}
	
	
	// check if topic exists
	private int getNextAvailable() {
		int nextTopicId = 0;
		if (topicIdMap.size() == 0) {
			nextTopicId = 0;
		}
		else {
			Set<Entry<Integer, Topic>> entries = topicIdMap.entrySet();	
			ArrayList<Integer> t_ids = new ArrayList<Integer>();
			for (Entry<Integer, Topic> e: entries) {
				t_ids.add(e.getKey());
			}
		
			Collections.sort(t_ids);
			for (Integer t:t_ids) {
				if (t == nextTopicId) {
					nextTopicId++;
				}
				else {
					break;
				}
			}
		}
		log.info("Using topic ID: " + nextTopicId);
		return nextTopicId;
	}


}
