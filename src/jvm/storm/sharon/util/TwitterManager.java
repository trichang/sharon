package storm.sharon.util;

import twitter4j.conf.ConfigurationBuilder;

public abstract class TwitterManager {
	
	protected TwitterAuth twitterAuth;
	private int id;
	ConfigurationBuilder _cb;
	
	TwitterManager(int id){
		
		this.id = id;
		twitterAuth = CredentialsManager.getTwitterCredentials(id);
		_cb = new ConfigurationBuilder();
		_cb.setOAuthConsumerKey(twitterAuth.getConsumerKey());
		_cb.setOAuthConsumerSecret(twitterAuth.getConsumerSecret());
		_cb.setOAuthAccessToken(twitterAuth.getAccessToken());
		_cb.setOAuthAccessTokenSecret(twitterAuth.getAccessTokenSecret());
		_cb.setJSONStoreEnabled(true);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}	


}
