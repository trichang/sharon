package storm.sharon.util;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterObjectFactory;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import backtype.storm.utils.Utils;
import twitter4j.Paging;
import twitter4j.Status;

public class TwitterQueryManager  extends TwitterManager {
   private Twitter _twitterConn;
   private Log log = LogFactory.getLog(TwitterQueryManager.class);
	
   public TwitterQueryManager(int id) {
	    super(id);
		_twitterConn = new TwitterFactory(_cb.build()).getInstance();
   }
	
  
   public List<Document> getUserTimeLine(String name) {
	   
	   
	   final Paging paging = new Paging();
	   long sinceId = 1; // This must be a positive integer - not 0
	   String user = null;
			
   	   
   	   int indx = name.indexOf('@');
   	   if (indx >= 1){
   		   user = name.substring(0, indx);
   		   sinceId = Long.parseLong(name.substring(indx + 1, name.length()));	    
   	   }
   	   else{
   		   user = name;
   	   }
   	   
      
		paging.setSinceId(sinceId);
		paging.count(200); // max statuses you can request for this call
	
		// TODO : To get the next 200 statuses, simply set paging.maxId to the id of the earliest status you just received and make the call again:
		//paging.maxId(earliestStatusId - 1);
		
		
		
		List<Status> statuses = null;
		List<Document> customStatuses = new ArrayList<Document>();
		try{
			statuses = _twitterConn.getUserTimeline(user, paging);
			for(Status s: statuses){
				Document customStatus = Document.parse(TwitterObjectFactory.getRawJSON(s));
				customStatuses.add(customStatus);
			}
		}
		catch(TwitterException e){
			log.warn("Failed to get user posts: " + e.getMessage());
		}
	   
		return customStatuses;
   }
   
   public Document getStatus(String id) {
	   Long a = Long.parseLong(id);
	   try {
		    log.debug("Retrieving TID: " + a);
			Status status = _twitterConn.showStatus(a);
			Document customStatus = Document.parse(TwitterObjectFactory.getRawJSON(status));
		    Utils.sleep(15*1000);
		    return customStatus;
		}
		catch(TwitterException e){
			log.warn("Failed to get user posts " + e.getMessage());
			return null;
		}
   }
   
   
	
}

