/**  
* Topic.java - class for represents a Topic.  
* @author  Shanika Karunasekera
* @version 1.0 
*/
package storm.sharon.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;


@SuppressWarnings("rawtypes")
public class Topic implements Comparable {
	
	private Log log = LogFactory.getLog(Topic.class);
	public static final int PUBLIC_TOPIC = 0;
	public static final int PRIVATE_TOPIC = 1;
	public static final int SHARED_TOPIC = 2;
	public static final String WINDOW_MENTION = "window_mention";
	public static final String WINDOW_REPLY = "window_reply";
	public static final String WINDOW_HASH = "window_hash";
	private String topicName;
	private Integer topicId;
	private boolean isCurrent;
	private Integer topicType; // 0 - public (default), 1 - private, 2 - shared
	private String topicOwner; // the person who creates the topic
	private HashSet<String> sharedUsers = new HashSet<String>(); // if the topic is shared the list of users 
	// This HasMap will contain the parameters for window based queries
	// index 0 - Window Length (minutes) index 1 - Window update frequency (minutes) (minimum 1) index - 2 threshold
	private HashMap<String, ArrayList<Integer>> windowParamMap = new HashMap<String, ArrayList<Integer>>(); 
	private ArrayList<String> keyHistory = null;
	private HashMap<String, Keyword> keyWords = new HashMap<String, Keyword>();
	private ArrayList<Document>	 queries = new ArrayList<Document>();

	public Topic(String topicName, Integer topicId) {
		this.topicName = topicName;
		this.topicId = topicId;
		this.isCurrent = true;
		this.topicType = PUBLIC_TOPIC;
		this.topicOwner = SharonUser.DEFAULT_SHARON_USER;
		this.sharedUsers.add(this.topicOwner);
		setDefaultWindowParms();
	}
	
	public Topic(String topicName, Integer topicId, boolean isCurrent) {
		this(topicName, topicId);
		this.isCurrent = isCurrent;
	}
	
	public Topic(String topicName, Integer topicId, boolean isCurrent, Integer topicType, String topicOwner) {
		this.topicName = topicName;
		this.topicId = topicId;
		this.isCurrent = isCurrent;
		this.topicType = topicType;
		this.topicOwner = topicOwner;
		this.sharedUsers.add(this.topicOwner);
		setDefaultWindowParms();
	}
	
	public Topic(String topicName, Integer topicId, boolean isCurrent, Integer topicType, String topicOwner, HashSet<String> sharedUsers) {
		this.topicName = topicName;
		this.topicId = topicId;
		this.isCurrent = isCurrent;
		this.topicType = topicType;
		this.topicOwner = topicOwner;
		this.sharedUsers = sharedUsers;
		setDefaultWindowParms();
	}
	
	@SuppressWarnings("unchecked")
	public Topic(Document doc) {
		this.topicName =  doc.getString("topic_name");
		this.topicId = doc.getInteger("topic_id");
		this.isCurrent = doc.getBoolean("isCurrent");
		this.topicType = doc.getInteger("topic_type");
		if(this.topicType==null){
			this.topicType=PUBLIC_TOPIC;
		}
		this.topicOwner = doc.getString("topic_owner");
 		if(this.topicOwner==null){
 			this.topicOwner=SharonUser.DEFAULT_SHARON_USER;
 		}
		ArrayList<String> shared = (ArrayList<String>) doc.get("shared_users");
		if(shared==null){
			shared = new ArrayList<String>();
		}
		this.sharedUsers = new HashSet(shared);
		
		if (doc.containsKey(WINDOW_MENTION)) {
			ArrayList<Integer> windowParms = (ArrayList<Integer>) doc.get(WINDOW_MENTION); 
			windowParamMap.put(WINDOW_MENTION, windowParms);
			
		} else {
			log.debug("Document does not contain window mention parameters; populate defaults");
			ArrayList<Integer> windowParms = new ArrayList<Integer>();
			windowParms.add(5); // Default window length for mentions 5
			windowParms.add(1); // Default update frequency for mentions 1
			windowParms.add(4); // Threshold for community detection 4 
			windowParamMap.put(WINDOW_MENTION, windowParms);
		}
		
		if (doc.containsKey(WINDOW_REPLY)) {
			log.debug("Document contains window reply parameters");
			ArrayList<Integer> windowParms = (ArrayList<Integer>) doc.get(WINDOW_REPLY); 
			windowParamMap.put(WINDOW_REPLY, windowParms);
			
		} else {
			log.debug("Document does not contain window reply parameters; populate defaults");
			ArrayList<Integer> windowParms = new ArrayList<Integer>();
			windowParms.add(10); // Default window length for mentions 10
			windowParms.add(1); // Default update frequency for mentions 1
			windowParms.add(2); // Threshold for community detection 2 
			windowParamMap.put(WINDOW_REPLY, windowParms);
		}
		
		if (doc.containsKey(WINDOW_HASH)) {
			log.debug("Document contains window hash parameters");
			ArrayList<Integer> windowParms = (ArrayList<Integer>) doc.get(WINDOW_HASH); 
			windowParamMap.put(WINDOW_HASH, windowParms);
			
		} else {
			log.debug("Document does not contain window hash parameters; populate defaults");
			ArrayList<Integer> windowParms = new ArrayList<Integer>();
			windowParms.add(3); // Default window length for mentions 3
			windowParms.add(1); // Default update frequency for mentions 1
			windowParms.add(5); // Threshold for community detection 5
			windowParamMap.put(WINDOW_HASH, windowParms);
		}
		
	}
	
	public String getTopicName() {
		return topicName;
	}
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	public Integer getTopicId() {
		return topicId;
	}
	public void setTopicId(Integer topicId) {
		this.topicId = topicId;
	}
	
	public boolean isCurrent() {
		return isCurrent;
	}

	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}
	
	public Integer getTopicType() {
		return topicType;
	}
	
	public void setTopicType(Integer topicType) {
		this.topicType = topicType;
	}
	
	public String getTopicOwner() {
		return topicOwner;
	}
	
	
	public void setTopicOwner(String topicOwner) {
		this.topicOwner = topicOwner;
	}
	
	
	public HashMap<String, Keyword> getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(HashMap<String, Keyword> keyWords) {
		this.keyWords = keyWords;
	}
	
	public ArrayList<String> getKeyHistory() {
		return keyHistory;
	}
	public void setKeyHistory(ArrayList<String> keyHistory) {
		this.keyHistory = keyHistory;
	}

	
	public ArrayList<Document> getQueries() {
		return queries;
	}
	
	
	public void addQuery(Document query) {
		queries.add(query);
	}
	

	public HashMap<String, ArrayList<Integer>> getWindowParamMap() {
		return windowParamMap;
	}

	public void setWindowParamMap(HashMap<String, ArrayList<Integer>> windowParamMap) {
		this.windowParamMap = windowParamMap;
	}

	@Override
	public int compareTo(Object o) {
		Topic t = (Topic) o;
		//return String.valueOf(this.getTopicId()).compareTo(String.valueOf(t.getTopicId()));
		return this.getTopicName().compareTo(t.getTopicName());
	}

	public void setQueries(ArrayList<Document> queries) {
		this.queries = queries;
	}
	
	public void addToSharedUser(String user) {
		this.sharedUsers.add(user);
	}
	
	public boolean isSharedUser(String user) {
		if (this.sharedUsers.contains(user)){
			return true;
		} else {
			return false;
		}
	}
	
	public HashSet<String> getSharedUsers() {
		return sharedUsers;
	}
	
	public ArrayList<String> getSharedUsersAsList() {
		  ArrayList<String> list = new ArrayList<String>(sharedUsers);
		  return list;
	}

	public int getWindowLengthSeconds(String queryId) {
		return windowParamMap.get(queryId).get(0)*60;
	}
	
	public void setWindowLengthMinutes(String queryId, Integer timeMinutes) {
		log.debug("length before: " + windowParamMap.get(queryId));
		windowParamMap.get(queryId).set(0, timeMinutes);
		log.debug("length after: " + windowParamMap.get(queryId));
	}

	public int getUpdateFrequencySeconds(String queryId) {
		return windowParamMap.get(queryId).get(1)*60;
	}
	
	public void setUpdateFrequencyMinutes(String queryId, Integer timeMinutes) {
		windowParamMap.get(queryId).set(1, timeMinutes);
	}


	public Integer getRollingCountThreshold(String query_id) {
		return windowParamMap.get(query_id).get(2);
	}
	
	public void setRollingCountThreshold(String queryId, Integer timeMinutes) {
		windowParamMap.get(queryId).set(2, timeMinutes);
	}

	public void setDefaultWindowParms() {
		
		log.debug("Setting default window parameters");
		HashMap<String, ArrayList<Integer>> windowParamMap = new HashMap<String, ArrayList<Integer>>();
		String query_id = Topic.WINDOW_MENTION;
		ArrayList<Integer> parms = new ArrayList<Integer>();
		parms.add(5);
		parms.add(1);
		parms.add(4);
		windowParamMap.put(query_id, parms);
		query_id = Topic.WINDOW_REPLY;
		parms = new ArrayList<Integer>();
		parms.add(10);
		parms.add(1);
		parms.add(2);
		windowParamMap.put(query_id, parms);
		query_id = Topic.WINDOW_HASH;
		parms = new ArrayList<Integer>();
		parms.add(3);
		parms.add(1);
		parms.add(5);
		windowParamMap.put(query_id, parms);
		setWindowParamMap(windowParamMap);	
	}
	
	public Document toDoc() {
		Document doc = new Document();
		
		doc.append("topic_name", this.topicName);
		doc.append("topic_id", this.topicId);
		doc.append("isCurrent", this.isCurrent);
		doc.append("topic_type", this.topicType);
		doc.append("topic_owner", this.topicOwner);
		doc.append("shared_users", this.getSharedUsersAsList());
		doc.append(WINDOW_MENTION, this.getWindowParamMap().get(WINDOW_MENTION));
		doc.append(WINDOW_REPLY, this.getWindowParamMap().get(WINDOW_REPLY));
		doc.append(WINDOW_HASH, this.getWindowParamMap().get(WINDOW_HASH));
		
		return doc;
	}

}
