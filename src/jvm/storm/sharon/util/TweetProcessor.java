package storm.sharon.util;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TweetProcessor {
	
	private static Log log = LogFactory.getLog(TweetProcessor.class);
	    
	public static ArrayList<String> tokenize(String text) {
		String[] tokens =  text.split(" ");
		ArrayList<String> validTokens = new ArrayList<String>();
		for (int i = 0; i < tokens.length; i++) {
			if (tokens[i].length() >= 3) {
				if ((tokens[i].charAt(0) == '#') ||(tokens[i].charAt(0) == '@')) {
					validTokens.add(tokens[i].substring(1, tokens[i].length()).toLowerCase());
				} else {
					validTokens.add(tokens[i].toLowerCase());
				}
			}
		}
		log.debug("Valid tokens: "+validTokens);
		return validTokens;
		
	}

}
