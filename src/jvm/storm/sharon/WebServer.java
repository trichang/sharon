package storm.sharon;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.NoSuchElementException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.BasicConfigurator;
import org.glassfish.grizzly.http.server.HttpServer;

import storm.sharon.web.ResultServer;
import storm.sharon.web.ResultService;
import storm.sharon.web.services.Control;
import storm.sharon.web.services.TweetQuery;
import storm.sharon.web.services.TweetStream;

/*
 * SharonServer starts WebSocket I/O for each streaming/query result.
 * Client programs connect to the WebSocket endpoints to receive result updates
 * and to send commands to change the stream/query or to send to backend.
 */

public class WebServer {
	private static Log log = LogFactory.getLog(WebServer.class);
	private static ResultServer[] servers;
	private static ResultService[] services;
	private static HttpServer webserver;
	private static int webport = 8000;
	private static int tweetStreamPort = 8001;
	private static int tweetQueryPort = 8002;
	private static int controlPort = 8003;

	public static void main(String[] args) {
		// configure the logger
		BasicConfigurator.configure();
		log.info("Starting Sharon Server");

		try {
			PropertiesConfiguration config = new PropertiesConfiguration(
					"sharon.properties");
			webport = config.getInt("interface.webport");
			tweetStreamPort = config.getInt("interface.tweetStreamPort");
			tweetQueryPort = config.getInt("interface.tweetQueryPort");
			controlPort = config.getInt("interface.controlPort");
		} catch (ConfigurationException e) {
			log.warn("could not read sharon.properties, using defaults");
		} catch (NoSuchElementException e){
			log.warn("could not read sharon.properties, using defaults");
		}

		//////////////////////////////////////////////////////////////
		// start the http server
		// Users of the system will point their browser to this server
		//////////////////////////////////////////////////////////////
		webserver = HttpServer.createSimpleServer("web", webport);
		try {
			webserver.start();
			log.info("web server started");
		} catch (Exception e) {
			log.fatal("web server failed to start ... check if port "+webport+" is available");
			System.exit(-1);
		}

		/////////////////////////////////////////////////////////
		// Start the websocket servers
		// Browsers will connect to these servers to get results.
		// Under certain circumstances, the backend will connect
		// to these servers to make data available.
		/////////////////////////////////////////////////////////
		
		// allocate array for all servers and services
		servers = new ResultServer[3];
		services = new ResultService[3];

		// startup the result services
		services[0] = new TweetStream();
		servers[0] = new ResultServer(services[0],tweetStreamPort);
		services[1] = new TweetQuery();
		servers[1] = new ResultServer(services[1],tweetQueryPort);
		services[2] = new Control();
		servers[2] = new ResultServer(services[2],controlPort);
		
		// open a web browser
		if (Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().browse(
						new URI("http://localhost:" + webport+"/index.html"));
			} catch (IOException e) {
				log.warn("there was an error starting the local web browser ... point your browser to http://localhost:"+webport+"/index.html");
			} catch (URISyntaxException e) {
				log.error("the URI specified is invalid ... point your browser to http://localhost:"+webport+"/index.html");
			} catch (UnsupportedOperationException e) {
				log.info("browser action is not supported ... point your browser to http://localhost:"+webport+"/index.html");
			}
		} else {
			log.info("desktop support is not detected ... point your browser to http://localhost:"+webport+"/index.html");
		}
		
		
		
		// keep running until ctrl-c
		Runtime.getRuntime().addShutdownHook(new Thread() {
			private Log log = LogFactory.getLog(WebServer.class);

			public void run() {
				try {
					Thread.sleep(200);
					log.info("shutting down ...");
					services[0].shutdown();
					services[1].shutdown();
					services[2].shutdown();
					servers[0].stop();
					servers[1].stop();
					servers[2].stop();

				} catch (InterruptedException e) {
					log.error("shutdown interrupted");
				}
			}
		});
		
		while(true) {
	        log.info("Sharon Server running.");

	        try 
	           { Thread.sleep(1000*60*60); } 
	        catch (InterruptedException ex) 
	           {
	        	
	           }
	    }
	}

}
