package storm.sharon;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame; 
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;

import java.awt.AWTException;
import java.awt.BorderLayout; 
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import storm.sharon.gui.core.MainPanel;
import storm.sharon.gui.core.RapidAPI;
import storm.sharon.gui.core.Subscriptions;
import storm.sharon.gui.plugins.Plugins;
import storm.sharon.gui.topics.LogPanel;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Rapid {  
	private static Logger log;
	private static Plugins plugins;
	private static Subscriptions subscriptions;
	
	private static TrayIcon trayIcon;

	private static LogPanel logpanel;
	
	public static void main(String[] args) {
		new Rapid(); 
	}  	
	
	private static List<BufferedImage> icons;

	public static List<BufferedImage> getIcons() {
		return icons;
	}
	
	public static String productName(){
		return "RAPID beta";
	}

	public static void ConfigureLogging(){
		//TODO: Disable log4j stuff
		
		// Configure logging programmatically 
		ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();
		builder.setStatusLevel(Level.ERROR);
		builder.setConfigurationName("BuilderTest");
		builder.add(builder.newFilter("ThresholdFilter", Filter.Result.ACCEPT, Filter.Result.NEUTRAL)
		    .addAttribute("level", Level.INFO));
		AppenderComponentBuilder appenderBuilder = builder.newAppender("Stdout", "CONSOLE").addAttribute("target",
		    ConsoleAppender.Target.SYSTEM_OUT);
		appenderBuilder.add(builder.newLayout("PatternLayout").
		    addAttribute("pattern", "%highlight{%-5level %c{1.} %msg%n%throwable}{STYLE=Logback}"));
		appenderBuilder.add(builder.newFilter("MarkerFilter", Filter.Result.DENY, Filter.Result.NEUTRAL).
		    addAttribute("marker", "FLOW"));
		builder.add(appenderBuilder);
		builder.add(builder.newLogger("org.apache.logging.log4j", Level.ERROR).
		    add(builder.newAppenderRef("Stdout")).addAttribute("additivity", false));
		builder.add(builder.newLogger("storm.sharon", Level.ERROR).
			    add(builder.newAppenderRef("Stdout")).addAttribute("additivity", false));
		builder.add(builder.newRootLogger(Level.ERROR).add(builder.newAppenderRef("Stdout")));
		LoggerContext ctx = Configurator.initialize(builder.build());
		ctx.updateLoggers();
	}
	
	public static void SystemTray(){
		
		if (java.awt.SystemTray.isSupported()) {
			
			java.awt.SystemTray tray = java.awt.SystemTray.getSystemTray();
			
			// Here are our listeners. One listens for the popup menu "Close" command and terminates the program.
			// This will be attached to the popup menu item "close"
			ActionListener exitListener = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			};
			
			// This message appears when an event has been detected.
			ActionListener actionListener = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					trayIcon.displayMessage("Double click", "You double clicked on the icon!",
						TrayIcon.MessageType.INFO);
				}
			};
					
			// Next we setup our popup menu. We then add one item to the menu called "close" to close our program.
			PopupMenu popup = new PopupMenu();
			MenuItem defaultItem = new MenuItem("Close");
			
			// Add our ActionListener called "exitlistener" to the popup menu item.
			defaultItem.addActionListener(exitListener);
			
			// Add the menu item finally to the popup menu.
			popup.add(defaultItem);

			// Now configure our tray icon and attach our actionListener to it to listen for actionPeformed events.
			trayIcon = new TrayIcon(icons.get(3), "RAPID GUI", popup);   
			trayIcon.setImageAutoSize(true);
			trayIcon.addActionListener(actionListener);

			// Lastly, add the icon to the tray variable we defined above. 
			// Since this can throw an error, lets trap the error and display a message if the add failed.
			try {
				tray.add(trayIcon);
			} catch (AWTException e) {
				log.error("Problem with adding icon to tray.");
			}

		} else {
			// We reach this error if the system simply won't let us access the tray or doesn't have one.
			log.warn("This system does not support a System Tray being accessed by Java");
		}
		
	}
	
	public Rapid() { 
		
		ConfigureLogging();
		
		log = LogManager.getLogger(Rapid.class.getName());
		
		log.info("Starting RAPID GUI");
		
		icons = new ArrayList<BufferedImage>();
		
		try {
			icons.add(ImageIO.read( ClassLoader.getSystemResource( "icon16.png" ) ));
			icons.add(ImageIO.read( ClassLoader.getSystemResource( "icon32.png" ) ));
			icons.add(ImageIO.read( ClassLoader.getSystemResource( "icon64.png" ) ));
			icons.add(ImageIO.read( ClassLoader.getSystemResource( "icon128.png" ) ));
		} catch (IOException e) {
			log.error("could not read icon resources");
		}
		
		
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			log.warn("Look and feel not set.");
		}
		
		
		SystemTray();
		
		boolean successful = RapidAPI.Authenticate(); // go through the Startup Protocol to get an authenticated session
		
		if (!successful) {
			log.debug("Exiting GUI");
			System.exit(-1);
		}
		
		// to monitor how many plugins are subscribing to topic-queue pairs
		setSubscriptions(new Subscriptions());
	
		// build the plugins
		setPlugins(new Plugins());
		
		// open the main window
		JFrame guiFrame = new JFrame();  
		guiFrame.setIconImages(icons);
		guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		guiFrame.addWindowListener(new WindowAdapter() {
			   public void windowClosing(WindowEvent evt) {
			     onExit();
			   }
			   });
		guiFrame.setTitle(productName()+": "+RapidAPI.getAuthenticationData().getRapid_userName()); 

		guiFrame.setSize(1440,900);  

		guiFrame.setLocationRelativeTo(null); 
		guiFrame.add(new MainPanel(), BorderLayout.CENTER);
		guiFrame.pack();
		guiFrame.setVisible(true);
		
	}  
	
	public void onExit() {
		  log.info("Exiting GUI");
		  System.exit(0);
	}

	public static Plugins getPlugins() {
		return plugins;
	}

	public static void setPlugins(Plugins plugins) {
		Rapid.plugins = plugins;
	}
	
	public static Subscriptions getSubscriptions() {
		return subscriptions;
	}

	public static void setSubscriptions(Subscriptions subscriptions) {
		Rapid.subscriptions = subscriptions;
	}

	public static LogPanel getLogpanel() {
		return logpanel;
	}

	public static void setLogpanel(LogPanel logpanel) {
		Rapid.logpanel = logpanel;
	}
}