/**
 * This bolt is responsible for receiving Tweets from the three tweet collection spouts and emitting them
 * with accurate topicIDs to be received by the bolts. The bolt will tokenize the Tweet text, and add the 
 * appropriate topic ID. 
 * @author Shanika Karunasekera
 *
 */


package storm.sharon.bolt;


import storm.sharon.util.TopicKeywordHandler;
import storm.sharon.util.TweetProcessor;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;




import java.util.Observable;
import java.util.Observer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;


@SuppressWarnings("serial")
public class TopicLabellerBolt extends BaseRichBolt {
	
	private OutputCollector _collector;
	private int numOfTasks;
    private int myId;
    //private HashMap<String, HashSet<Integer>> word_topic = new  HashMap<String, HashSet<Integer>>();
    private TopicKeywordHandler cmd_handler;
    private Log log = LogFactory.getLog(TopicLabellerBolt.class);
    private HashMap<String, HashSet<Integer>> word_topic = null;
    
    public TopicLabellerBolt(){
    	
    }

    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, OutputCollector collector) {
    	this.numOfTasks = context.getComponentTasks("directStream").size();
    	log.info("Number of tasks: " + numOfTasks);
    	_collector = collector;
        myId = context.getThisTaskIndex();
        log.info("Task ID: " + myId);
        cmd_handler = new TopicKeywordHandler();
    }

    @Override
    public void execute(Tuple tuple) {
    	
    	log.debug("TopicLabellerBolt " + myId + " received tuple");
    	log.debug("Tracking "  + cmd_handler.getTopicKeyword().size() + " topics");
    	
    	Fields f = tuple.getFields();
    	if (f.get(0).equals("command_response")) {
    		log.info("This is a topic/keyword change message");
    		String message = (String)tuple.getValueByField("command_response");
    		Document doc = Document.parse(message);
    		cmd_handler.process(doc);    
    		_collector.ack(tuple);
    	}
    	else {
	    	Integer trackID = (Integer)tuple.getValueByField("trackID");
	        Integer topicID = (Integer)tuple.getValueByField("topicID");
	    	String tweetJSON = (String)tuple.getValueByField("tweet");
	    	    	
	    	// If tweetJSON id not null do the following
	    	// 1. Create a document with contains the tweet, trackID
	    	// 2. Tokenize the Tweet text.
	    	// 3. If the topicID is -1 identify the topic/topics
	    	// 4. Append to the document the topicID and the tokens
	    	// 4. Emit the Document JSON, once for each topic
	    	if (tweetJSON != null) {
	    		Document doc = Document.parse(tweetJSON);
	    		doc.append("sharon_track_id", trackID);
	    		
	    		log.debug("TopicLabellerBolt received tuple: from trackID(" + trackID +  ") " + 
	    									 "topicID(" + topicID + ") " + doc.getString("id_str"));
	    		
	    		List<String> tokenList = TweetProcessor.tokenize(doc.getString("text"));;
	    		doc.append("sharon_tokens", tokenList);
	    		log.debug("TopicLabellerBolt text: (" + doc.getString("text") +  ") " + 
						 "tokens: " + tokenList);
	    	
	    		if (topicID == -1) {
	    			// This is from the tracking stream and therefore the topic ID must be determined 
	    	        word_topic = cmd_handler.getAllKeyWords();
	    	        
	    	        log.debug("All keywords KEYWORDS : " + word_topic);
		    		HashSet<Integer> topicList = new HashSet<Integer>();
		    		
		    		// Based on the tokens decide which topic/s this Tweet belongs to 
		    		if (word_topic != null ) {
		    			
		    			// first match on tokens, which matches keywords that have no white space
		    			for (String word: tokenList) {
		    			 
		    			  if (word_topic.containsKey(word)) {
		    				  
		    				  HashSet<Integer> topics = word_topic.get(word);
		    				  //log.debug("This is a keyword: " + word + "that belongs to " + topics.size() + " topics");
		    				  
		    				  for (Integer i: topics) {
		    					  if (!topicList.contains(i)) {
		    						  log.debug("Topic " + i + " was added to the topic list");
		    						  topicList.add(i);
		    						 
		    					  }
		    				  }
		    			  } else {
		    				  log.debug("This is not a keyword: " + word);
		    			  }
		    			}
		    		
		    			// Furthermore, check if any keyword appears in the tweet text exactly
						for (String word : word_topic.keySet()) {
							String text = doc.getString("text");
							Matcher m = Pattern.compile(Pattern.quote(word),
									Pattern.CASE_INSENSITIVE).matcher(text);
							while (m.find()) {
								// ignore matches where the preceding character
								// is a letter or digit
								int location = m.start();
								if (location > 0
										&& (Character.isLetter(text
												.charAt(location - 1)) || Character
												.isDigit(text
														.charAt(location - 1)))) {
									continue;
								}
								
								// ignore matches where the trailing character
								// is a letter or digit
								int end = m.end();
								if (end < text.length()
										&& (Character
												.isLetter(text.charAt(end)) || Character
												.isDigit(text.charAt(end)))) {
									continue;
								}

								// its a hit
								HashSet<Integer> topics = word_topic.get(word);
								for (Integer i : topics) {
									if (!topicList.contains(i)) {
										log.debug("Topic "
												+ i
												+ " was added to the topic list");
										topicList.add(i);
									}
								}
								break;
							}
						}
		    			
		    			for (Integer newTopicID: topicList) {
		    				//log.info("Tweet KEYWORD " + doc.getString("id_str") + " belong to topic: " + newTopicID);
		    				log.debug("Tweet KEYWORD: " + doc.toJson() + " belong to topic: " + newTopicID);
		    				Document newDoc = doc.append("sharon_topic_id", newTopicID);
		    				_collector.emit(tuple, new Values(newTopicID, trackID, newDoc.toJson()));
		    				_collector.ack(tuple);
		    			}
		    		}
		    		
		    		
		    		
	    		}
	    		else {
	    			// This is a Tweet from track ID 2 or 3 hence has a valid topic ID
	    			doc.append("sharon_topic_id", topicID);
	    			log.debug("Tweet " + doc.getString("id_str") + " belong to topic: " + topicID);
	    			_collector.emit(tuple, new Values(topicID, trackID, doc.toJson()));
	    			_collector.ack(tuple);
	    			
	    		}
		   
	    	}
	    	else {
	    		log.warn("Tweet JSON has a null value - ignoring");
	    	}
    	}
	      
    }

  
    
    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    	declarer.declare(new Fields("topicID", "trackID", "sharon_tweet"));
    }


}
