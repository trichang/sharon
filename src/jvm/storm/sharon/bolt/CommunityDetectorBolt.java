package storm.sharon.bolt;


import storm.sharon.algorithms.CommunityInterface;
import storm.sharon.algorithms.CommunityStrategyA;
import storm.sharon.algorithms.DiscussionInterface;
import storm.sharon.algorithms.DiscussionStrategyA;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

@SuppressWarnings("serial")
public class CommunityDetectorBolt extends BaseRichBolt implements Observer {
	
	OutputCollector _collector;
	private Log log = LogFactory.getLog(CommunityDetectorBolt.class);
	//private DiscussionCollectionObserver discussionCollector; 
	private CommunityInterface commIntf; 
	private Tuple tuple;
	private int myId;
	Integer topicID;
	
    public CommunityDetectorBolt(){
    	
    }

    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
        myId = context.getThisTaskIndex();
        int topic_id = myId;
    	commIntf = new CommunityStrategyA(this, topic_id);
    }

    @Override
    public void execute(Tuple tuple) {
    
    	this.tuple = tuple;
    	
    	
    	Fields f = tuple.getFields();
    	if (f.get(0).equals("sharon_command")) {
    		String message = (String)tuple.getValueByField("sharon_command");
    		log.info("This is a command: " + message);
    		Document doc = Document.parse(message);
    		if (doc.getString("command_name").equals("CLEAR_REPLIES") || doc.getString("command_name").equals("CLEAR_MENTIONS")) {
	    		if ( doc.getInteger("topic_id") == myId) {
	    			log.info("Received a reset message for topic ID: " + myId);
	    			if (doc.getString("command_name").equals("CLEAR_REPLIES")) {
	    				log.info("Received a reset reply for topic ID: " + myId);
	    				commIntf.clearReplyMap();
	    			}
	    			else if (doc.getString("command_name").equals("CLEAR_MENTIONS")) {
	    				log.info("Received a reset mention for topic ID: " + myId);
	    				commIntf.clearUserMap();
	    			}
	    		}
    		}
    		_collector.ack(tuple);
    	} else {
    	
	    	String s = (String)tuple.getValueByField("sharon_tweet");
	    	topicID = (Integer)tuple.getValueByField("topicID");
	   
	        Document d = Document.parse(s);
	               	
	        log.info("CommnityDetectorBolt  " + myId + " received tuple tweet = ");
	    	
	        log.debug("CommnityDetectorBolt  " + myId + " received tuple tweet = " + d);
	    		
	    	commIntf.process(d);
    	}
    	/*String message = doc.toJson();
		log.info("Sending message to topic discusssions: " + message);
		//Tuple tuple = null;
		_collector.emit(tuple, new Values(message));
		_collector.ack(tuple);
    */
    }

  
	
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("community", "topicID"));
	}

	@Override
	public void update(Observable arg0, Object arg) {
		Document doc = (Document)arg;
		String message = doc.toJson();
		log.debug("Sending message to topic communities: " + message);
		//Tuple tuple = null;
		_collector.emit(tuple, new Values(message, topicID));
		_collector.ack(tuple);
		
	}

}
