/**  
* CommandHandlerBolt.java - this bolt handles commands related to User and Topic management
* @author  Shanika Karunasekera
* @version 1.0 
*/


package storm.sharon.bolt;

import storm.sharon.util.TopicKeywordCommandHandler;
import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import org.I0Itec.zkclient.ZkClient;
import kafka.utils.ZKStringSerializer$; 
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;


@SuppressWarnings("serial")
public class CommandHandlerBolt extends BaseRichBolt {
	

	OutputCollector _collector;
	private Log log = LogFactory.getLog(CommandHandlerBolt.class);
    private TopicKeywordCommandHandler topic_cmd_handler;
  //  private UserCommandHandler user_cmd_handler;
    private ZkClient zkClient;
   
    
    public CommandHandlerBolt(){
    	
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
      Config conf = new Config();
      // Generate ticks every one minute
      int tickFrequencyInSeconds = 60;
      conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, tickFrequencyInSeconds);
      return conf;
    }

    
    

    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, OutputCollector collector) {
        _collector = collector;     
        int sessionTimeoutMs = 10000;
		int connectionTimeoutMs = 10000;
		zkClient = new ZkClient("localhost:2181", sessionTimeoutMs, connectionTimeoutMs,  ZKStringSerializer$.MODULE$);
        topic_cmd_handler = new TopicKeywordCommandHandler(_collector, zkClient);
       // user_cmd_handler = new UserCommandHandler(zkClient);
    }
    
    private static boolean isTickTuple(Tuple tuple) {
    	return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
    	    && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
    }


    @Override
    public void execute(Tuple tuple) {
    	
    	if (isTickTuple(tuple)) {
    		// To be implemented in the future
    		log.info("Received tick tuple, triggering emit of current window counts");
    		topic_cmd_handler.writeMessageQueue(tuple);
    	} else {
	    
	    	
	    	String command = (String) tuple.getValueByField("sharon_command");
	    	log.info("Received command: " +  command);
	    	
	    	Document doc = Document.parse(command);
	    
	    	
	    	// Process the command
	    	Document doc_resp = topic_cmd_handler.process(doc);;
	    	
	    	if (doc_resp != null) {
	    		String message = doc_resp.toJson();
	    		log.info("Sending message command_response : " + message);
	    		_collector.emit(tuple, new Values(message));
	    		_collector.ack(tuple);
	
	    	} else {
	    		_collector.ack(tuple);
	    	}
    	}
    }

	
    @Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("command_response"));
		declarer.declareStream("stream_spout", new Fields("trackWords"));
	}

}
