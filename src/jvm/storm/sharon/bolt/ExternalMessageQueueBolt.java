package storm.sharon.bolt;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.storm.shade.org.json.simple.JSONObject;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kafka.javaapi.producer.Producer;
import kafka.message.Message;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import storm.kafka.bolt.KafkaBolt;
/**
 * Created by tri on 25/04/16.
 */
public class ExternalMessageQueueBolt<T> extends KafkaBolt<T, String> {
    private static final Logger log = LoggerFactory.getLogger(ExternalMessageQueueBolt.class);

    private Producer<T, String> producer;
    private OutputCollector collector;
    private String queueName;
    private String fieldName;


    public ExternalMessageQueueBolt(String queueName) {
        this.queueName = queueName;
    }

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        Map configMap = (Map) stormConf.get(KAFKA_BROKER_PROPERTIES);
        Properties properties = new Properties();
        properties.putAll(configMap);
        ProducerConfig config = new ProducerConfig(properties);
        producer = new Producer<T, String>(config);
        this.collector = collector;
    }

    @Override
    public void execute(Tuple tuple) {

        T key = null;

        JSONObject obj = new JSONObject();
        List<String> fields = tuple.getFields().toList();
        for (String field : fields) {
            obj.put(field, tuple.getValueByField(field).toString());
        }

        String message = obj.toJSONString();
        try {
            log.info("Sending message to queue: " + queueName);
            log.debug("Sending message: " + message + "to queue: " + queueName);
            producer.send(new KeyedMessage<T, String>(queueName, null, message));
        } catch (Exception ex) {
            log.error("Could not send message with key '" + key + "' and value '" + message + "'", ex);
        } finally {
            collector.ack(tuple);
        }
    }
}
