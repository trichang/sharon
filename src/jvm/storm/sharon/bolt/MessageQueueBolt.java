


package storm.sharon.bolt;


import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kafka.javaapi.producer.Producer;
import kafka.message.Message;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import storm.kafka.bolt.KafkaBolt;
import storm.sharon.util.MessageQueueInterface;
import storm.sharon.util.Topic;
import storm.sharon.util.TopicKeywordHandler;
import storm.sharon.util.SharonUser;


public class MessageQueueBolt<K, V> extends KafkaBolt<K, V> {
	private static final Logger log = LoggerFactory.getLogger(MessageQueueBolt.class);
	
	private Producer<K, V> producer;
	private OutputCollector collector;
	private String queueName;
	private String fieldName;
	
	
	
	public MessageQueueBolt(String queueName, String fieldName) {
		this.queueName = queueName;
		this.fieldName = fieldName;
		
	}
	    
	@Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        Map configMap = (Map) stormConf.get(KAFKA_BROKER_PROPERTIES);
        Properties properties = new Properties();
        properties.putAll(configMap);
        ProducerConfig config = new ProducerConfig(properties);
        producer = new Producer<K, V>(config);
        this.collector = collector;
       
    }
	
	 @Override
	 public void execute(Tuple tuple) {
		 
		 K key = null;
		        
		 V message = (V)tuple.getValueByField(fieldName);
		 try {
		       log.info("Sending message to queue: " + queueName);
		       log.debug("Sending message: " + message + "to queue: " + queueName);
		       producer.send(new KeyedMessage<K, V>(queueName, null, message));
		  } catch (Exception ex) {
		       log.error("Could not send message with key '" + key + "' and value '" + message + "'", ex);
		  } finally {
		       collector.ack(tuple);
		  }
	    	
	   }

	
	

}
