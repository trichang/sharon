package storm.sharon.bolt;


import scala.Tuple2;
import scala.Tuple3;
import storm.sharon.util.QueryProcessor;
import storm.sharon.util.Topic;
import storm.sharon.util.TopicKeywordHandler;
import storm.sharon.util.TweetProcessor;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;




import java.util.Observable;
import java.util.Observer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

/**
 * @author Shanika Karunasekera
 *
 */

@SuppressWarnings("serial")
public class FilterBolt extends BaseRichBolt {
	
	private OutputCollector _collector;
	private int numOfTasks;
    private int myId;
    //private HashMap<String, HashSet<Integer>> word_topic = new  HashMap<String, HashSet<Integer>>();
    private TopicKeywordHandler cmd_handler;
    private Log log = LogFactory.getLog(FilterBolt.class);
    private HashMap<String, HashSet<Integer>> word_topic = null;
    
    public FilterBolt(){
    	
    }

    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, OutputCollector collector) {
    	this.numOfTasks = context.getComponentTasks("directStream").size();
    	log.info("Number of tasks: " + numOfTasks);
    	_collector = collector;
        myId = context.getThisTaskIndex();
        log.info("Task ID: " + myId);
        cmd_handler = new TopicKeywordHandler();
    }

    @Override
    public void execute(Tuple tuple) {
    	
    	log.debug("FilterBolt " + myId + " received tuple");
    	log.debug("Tracking "  + cmd_handler.getTopicKeyword().size() + " topics");
    	
    	Fields f = tuple.getFields();
    	if (f.get(0).equals("command_response")) {
    		log.info("This is a topic/keyword change message");
    		String message = (String)tuple.getValueByField("command_response");
    		Document doc = Document.parse(message);
    		cmd_handler.process(doc);    
    		_collector.ack(tuple);
    	}
    	else {
	    	Integer trackID = (Integer)tuple.getValueByField("trackID");
	        Integer topicID = (Integer)tuple.getValueByField("topicID");
	    	String tweetJSON = (String)tuple.getValueByField("sharon_tweet");
	    	
	    	log.info("TopicLabellerBolt received tuple: from trackID(" + trackID +  ") " + 
					 "topicID(" + topicID + ") " + tweetJSON);
	    	
	    	Topic t = cmd_handler.getTopic(topicID);
	    	
	    	if (t != null) {
	    		ArrayList<Document> queries = t.getQueries();
	    		
	    		for (Document query: queries) {
	    			String queryType = query.getString("QueryType");
	    			if (queryType.compareTo("SIMPLE_FILTER") == 0) {
	    				Document sharon_tweet = Document.parse(tweetJSON);
	    				Document filter_output = QueryProcessor.filterTweet(sharon_tweet, query);
	    				if (filter_output != null ) {
	    					String queryId = query.getString("QueryId");
	    					filter_output.append("topic_id", topicID);
	    					_collector.emit(tuple, new Values(queryId, filter_output.toJson()));
	    				}
	    			
	    			}
	    		}
	    	}
	    	
	    	_collector.ack(tuple);
	    	
    	}
    	
    	
	    
    }

  
    
    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    	declarer.declare(new Fields("queryId", "queryResult"));
    }


}
