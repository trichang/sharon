package storm.sharon.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import org.apache.storm.shade.org.json.simple.JSONObject;
import storm.sharon.util.Settings;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.*;

/**
 * Created by tri on 17/04/16.
 */
public class ClusterBolt  extends BaseRichBolt {
    private Socket socket;
    private PrintWriter out;
    private Settings setting;

    OutputCollector _collector;
    private String componentId;

    //@Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
        setting = Settings.getInstance();

        _collector = collector;
        componentId = "#"+context.getThisTaskId();

        try {
            socket = new Socket(setting.host, setting.port);
            out = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void doClustering(Tuple tuple, String seed, int edgeCount, List<String> list, HashMap<String, Set<String>> G) {
        int Gvol = edgeCount;

        double volS = 0, cutS = 0;
        double bestConductance = 1;
        HashSet<String> S = new HashSet<>();
        HashSet<String> bestSet = new HashSet<>();
        bestSet.add(list.get(0));

        for (String node : list) {
            volS += G.get(node).size();
            for (String v : G.get(node)) {
                if (S.contains(v)) cutS -= 1;
                else cutS += 1;
            }

            double currConductance = cutS/Math.min(volS, Gvol-volS);

            S.add(node);

            if (currConductance < bestConductance) {
                bestConductance = currConductance;
                bestSet = (HashSet<String>)S.clone();
            }
        }

//        org.json.simple.JSONObject obj = new org.json.simple.JSONObject();
//        obj.put("type", "CLUSTER");
//        obj.put("boltID", componentId);
//        obj.put("seed", seed);
//        obj.put("nodeList", bestSet);
//
//        out.println(obj.toJSONString());
//        out.flush();

        //TODO: emit type, boltID, seed, and nodeList
        _collector.emit(new Values(seed, bestSet));
        _collector.ack(tuple);
    }

    @Override
    public void execute(Tuple tuple) {
        String seed = tuple.getValueByField("seed").toString();
        int edgeCount = (int)tuple.getValueByField("edgeCount");
        List<String> nodeList = (List<String>)tuple.getValueByField("nodeList");
        HashMap<String, Set<String>> minigraph = (HashMap<String, Set<String>>)tuple.getValueByField("graph");

//        org.json.simple.JSONObject obj = new org.json.simple.JSONObject();
//        obj.put("type", "DEBUG");
//        obj.put("boltID", componentId);
//        obj.put("seed", seed);
//        obj.put("nodeList", nodeList);
//        obj.put("edgeCount", edgeCount);
//        obj.put("graph", minigraph);
//
//        out.println(obj.toJSONString());
//        out.flush();

        doClustering(tuple, seed, edgeCount, nodeList, minigraph);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("seed", "nodeList"));
    }
}
