package storm.sharon.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import storm.sharon.util.Settings;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by tri on 17/04/16.
 */
public class GraphBolt extends BaseRichBolt {
    private Socket socket;
    private PrintWriter out;
    private Settings setting;

    OutputCollector _collector;

    private int componentId;
    private double alpha;
    private double tolerance;
    private AtomicInteger edgeCount;
    private ConcurrentHashMap<String, Set<String>> graph;
    private Set<String> nodes;

    public GraphBolt (double alpha, double tolerance) {
        super();

        this.alpha = alpha;
        this.tolerance = tolerance;
    }

    //@Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");

        setting = Settings.getInstance();

        _collector = collector;
        componentId = context.getThisTaskIndex();//getThisTaskId();

        graph = new ConcurrentHashMap<String, Set<String>>();
        nodes = new HashSet<String>();
        nodes = Collections.synchronizedSet(nodes);
        edgeCount = new AtomicInteger(0);

        try {
            socket = new Socket(setting.host, setting.port);
            out = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void buildGraph(String src, String dest) {
        nodes.add(src); nodes.add(dest);

        if (graph.containsKey(src)) {
            if (graph.get(src).add(dest)) edgeCount.incrementAndGet();
        } else {
            Set<String> neighbor = new HashSet<>();
            neighbor = Collections.synchronizedSet(neighbor);
            if (neighbor.add(dest)) edgeCount.incrementAndGet();

            graph.put(src, neighbor);
        }

        if (graph.containsKey(dest)) {
            if (graph.get(dest).add(src)) edgeCount.incrementAndGet();
        } else {
            Set<String> neighbor = new HashSet<>();
            neighbor = Collections.synchronizedSet(neighbor);
            if (neighbor.add(src)) edgeCount.incrementAndGet();

            graph.put(dest, neighbor);
        }
    }

    public void doPPR(Tuple tuple, String seed) {
        if (!nodes.contains(seed)) return;
        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");

        Queue<String> Q = new LinkedList<>();
        HashMap<String, Double> residual = new HashMap<>();
        final HashMap<String, Double> rankScore = new HashMap<>();

        //seed.setResidual(1./seeds.length);
        residual.put(seed, 1.);
        Q.add(seed);

        while (!Q.isEmpty()) {
            String v = Q.remove();
            int lenGv = graph.get(v).size();

            if (!rankScore.containsKey(v)) rankScore.put(v, 0.);
            //v.setRankScore(v.getRankScore() + (1-alpha)*v.getResidual());
            rankScore.put(v, rankScore.get(v) + (1-alpha)*residual.get(v));
            //double mass = alpha*v.getResidual()/(2*lenGv);
            double mass = alpha*residual.get(v)/(2*lenGv);

            for (String u : graph.get(v)) {
                double gtol = graph.get(u).size()*tolerance;
                if (!residual.containsKey(u)) residual.put(u, 0.);
                if ( (residual.get(u) < gtol) && (residual.get(u)+mass >= gtol) ) {
                    Q.add(u);
                }

                //u.setResidual(u.getResidual() + mass);
                residual.put(u, residual.get(u) + mass);
            }

            //v.setResidual(mass * lenGv);
            residual.put(v, mass*lenGv);
            //if (v.getResidual() > lenGv*tolerance) Q.add(v);
            if (residual.get(v) > lenGv*tolerance) Q.add(v);
        }

        //normalized and minify graph
        ArrayList<String> nodeList = new ArrayList<>();
        HashMap<String, Set<String>> minigraph = new HashMap<String, Set<String>>();
        for (String n : nodes) {
            if (rankScore.containsKey(n)) {
                //n.setRankScore(n.getRankScore()/G.getNeighborCount(n));
                rankScore.put(n, rankScore.get(n) / graph.get(n).size());
                //if (n.getRankScore() > 0) nodeList.add(n);
                //if (rankScore.get(n.getId()) > 0)
                nodeList.add(n);

                minigraph.put(n, graph.get(n));
            }
        }

        //sort
        Collections.sort(nodeList, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                //if (o1.getRankScore() < o2.getRankScore()) return 1;
                if (rankScore.get(o1) < rankScore.get(o2)) return 1;
                else if (rankScore.get(o1) > rankScore.get(o2)) return -1;
                return 0;
            }
        });

//        String src= "" , dest = "", count="";
//        try {
//            String tupleJson = tuple.getValueByField("tuples").toString();
//            JSONObject edgeObj = (JSONObject) (new JSONParser().parse(tupleJson));
//            src = edgeObj.get("src_node").toString();
//            dest = edgeObj.get("dest_node").toString();
//            count = edgeObj.get("count").toString();
//        } catch (Exception ignored) {}

        _collector.emit(new Values(seed, edgeCount.get(), nodeList, minigraph));
        _collector.ack(tuple);
    }

    //@Override
    public void execute(Tuple tuple) {
        String src= "" , dest = "", query_id="", count="";
        try {
            String tupleJson = tuple.getValueByField("tuples").toString();
            JSONObject edgeObj = (JSONObject) (new JSONParser().parse(tupleJson));
            src = edgeObj.get("src_node").toString();
            dest = edgeObj.get("dest_node").toString();
            query_id = edgeObj.get("query_id").toString();
            count = edgeObj.get("count").toString();

//            JSONObject obj = new JSONObject();
//            obj.put("type", "EDGE");
//            obj.put("boltID", componentId);
//            obj.put("src", src);
//            obj.put("dest", dest);
//            obj.put("count", count);
//
//            out.println(obj.toJSONString());
//            out.flush();
        } catch (Exception ignored) {}

        buildGraph(src, dest);

        if (!count.equals("0")) {
            if (src.hashCode() % Settings.getInstance().numGraphBolts == componentId)
                doPPR(tuple, src);
            if (dest.hashCode() % Settings.getInstance().numGraphBolts == componentId)
                doPPR(tuple, dest);
        } else {
            if (graph.containsKey(src)) {
                Set<String> edges = graph.get(src);
                edges.remove(dest);
                edgeCount.decrementAndGet();

                if (edges.size() == 0) {
                    graph.remove(src);
                    nodes.remove(src);
                }
            }
            if (graph.containsKey(dest)) {
                Set<String> edges = graph.get(dest);
                edges.remove(src);
                edgeCount.decrementAndGet();

                if (edges.size() == 0) {
                    graph.remove(dest);
                    nodes.remove(dest);
                }
            }
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("seed", "edgeCount", "nodeList", "graph"));
    }
}
