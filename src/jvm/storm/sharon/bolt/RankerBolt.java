package storm.sharon.bolt;


import storm.sharon.algorithms.DiscussionInterface;
import storm.sharon.algorithms.DiscussionStrategyA;
import storm.sharon.algorithms.RankingInterface;
import storm.sharon.algorithms.RankingStrategyA;
import storm.sharon.util.TopicKeywordHandler;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

@SuppressWarnings("serial")
public class RankerBolt extends BaseRichBolt implements Observer  {
	
	OutputCollector _collector;
	private Log log = LogFactory.getLog(RankerBolt.class);
	private RankingInterface rankIntf; 
	
	private Tuple tuple;
	private int myId;
	
    public RankerBolt(){
    	
    }

    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
        myId = context.getThisTaskIndex();
    	log.info("RankerBolt id: "  + myId);
    	rankIntf = new RankingStrategyA(this, myId);
    }

    @Override
    public void execute(Tuple tuple) {
    
    	this.tuple = tuple;
    	
    	Fields f = tuple.getFields();
    	if (f.get(0).equals("command_response")) {
    		log.info("This is a topic/keyword change message");
    		String message = (String)tuple.getValueByField("command_response");
    		Document doc = Document.parse(message);
    		rankIntf.updateTK(doc);    		
    	}
    	else {
	    	String s = (String)tuple.getValueByField("sharon_tweet"); 
	    	Integer topicID = (Integer)tuple.getValueByField("topicID");
	        Document d = Document.parse(s);
	        log.debug("RankerBolt received tuple tweet = " + d);
	        log.debug("RankerBolt ID : " + myId + " received tuple: from topicID(" + topicID +  ") " + s);
	        //log.info("RankerBolt ID : " + myId + " received tuple: from topicID(" + topicID +  ") ");
	    	rankIntf.rank(d);
    	}
    	
    	_collector.ack(tuple);

    }
   

	@Override
	public void update(Observable o, Object arg) {
		Document doc = (Document)arg;
		String message = doc.toJson();
		log.info("Sending NEW_KEYWORDS message to CommandHandlerBolt: " + message);
		_collector.emit(tuple, new Values(message));
		_collector.ack(tuple);
		
	}
	
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("sharon_command"));
	}

}
