package storm.sharon.bolt;


import scala.Tuple2;
import scala.Tuple3;
import storm.sharon.util.Topic;
import storm.sharon.util.TopicKeywordHandler;
import storm.sharon.util.TweetProcessor;
import storm.sharon.util.window.RollingTupleCounterImpl_A;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;




import java.util.Observable;
import java.util.Observer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

/**
 * This spout is responsible for receiving Tweets from the three tweet collection spouts and emitting them
 * with accurate topicIDs to be received by the bolts. The bolt will tokenize the Tweet text, and add the 
 * appropriate topic ID. 
 * @author Shanika Karunasekera
 *
 */

@SuppressWarnings("serial")
public class MRH_TupleGeneratorBolt extends BaseRichBolt {
	
	private OutputCollector _collector;
	private int numOfTasks;
    private int myId;
    //private HashMap<String, HashSet<Integer>> word_topic = new  HashMap<String, HashSet<Integer>>();
    private TopicKeywordHandler cmd_handler;
    private Log log = LogFactory.getLog(MRH_TupleGeneratorBolt.class);
    private HashMap<String, HashSet<Integer>> word_topic = null;
    private Integer topicID;
    
    public MRH_TupleGeneratorBolt(){
    	
    }

    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, OutputCollector collector) {
    	this.numOfTasks = context.getComponentTasks("directStream").size();
    	log.info("Number of tasks: " + numOfTasks);
    	_collector = collector;
        myId = context.getThisTaskIndex();
        log.info("Task ID: " + myId);
        cmd_handler = new TopicKeywordHandler();
    }

    @Override
    public void execute(Tuple tuple) {
    	
	    Integer trackID = (Integer)tuple.getValueByField("trackID");
	    topicID = (Integer)tuple.getValueByField("topicID");
	    String  tweetJSON = (String)tuple.getValueByField("sharon_tweet");	
	    
	    Document doc = Document.parse(tweetJSON);
	   
	    log.info("TupleGeneratorBolt (" + myId + ") received tuple: from trackID(" + trackID +  ") " + 
				 "topicID(" + topicID + ") " + doc.getString("id_str"));
	    
	    String screen_name =  ((Document)doc.get("user")).getString("screen_name");
		
		List<Document> mentions = (List<Document>)((Document)doc.get("entities")).get("user_mentions");
		List<Document> hashtags = (List<Document>)((Document)doc.get("entities")).get("hashtags");
		
		String in_reply_to_screen_name =  doc.getString("in_reply_to_screen_name");
		
		
		if (mentions != null) {
			for (Document m: mentions) {
				String mention = m.getString("screen_name");
				log.debug("User: " + screen_name + " mentioned user: " + mention);
				Tuple3<String, String, String> key = new Tuple3<String, String, String>(String.valueOf(topicID), Topic.WINDOW_MENTION, mention);
				Tuple3<String, String, String> value = 
						new Tuple3<String, String, String>(String.valueOf(topicID), mention, screen_name);
				_collector.emit(tuple, new Values(key, value, topicID, Topic.WINDOW_MENTION));
			}
		}
		
		if (hashtags != null) {
			for (Document h: hashtags) {
				String hash = '#' + h.getString("text").toLowerCase();
				log.debug("User: " + screen_name + " mentioned hashtag: " +  hash);
				Tuple3<String, String, String> key = new Tuple3<String, String, String>(String.valueOf(topicID), Topic.WINDOW_HASH, hash);
				Tuple3<String, String, String> value = 
						new Tuple3<String, String, String>(String.valueOf(topicID), hash, screen_name);
				_collector.emit(tuple, new Values(key, value, topicID, Topic.WINDOW_HASH));
			}
		}
		
		
		if (in_reply_to_screen_name != null) {
			log.debug("User: " + screen_name + " replied to  user: " + in_reply_to_screen_name);
			Tuple3<String, String, String> key = new Tuple3<String, String, String>(String.valueOf(topicID), Topic.WINDOW_REPLY, in_reply_to_screen_name);
			Tuple3<String, String, String> value = 
					new Tuple3<String, String, String>(String.valueOf(topicID), in_reply_to_screen_name, screen_name);
			_collector.emit(tuple, new Values(key, value, topicID, Topic.WINDOW_REPLY));
		}
		_collector.ack(tuple);
		
    }
    
    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    	declarer.declare(new Fields("tupleKey", "tupleValue", "topicID", "queryID"));
    }


}
