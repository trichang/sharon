package storm.sharon.bolt;

import storm.sharon.util.DBInterface;
import storm.sharon.util.MongoDBImpl;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

@SuppressWarnings("serial")
public class TweetCollectBolt extends BaseRichBolt implements IRichBolt  {
		
	OutputCollector _collector;
  
    private DBInterface dbConn = null;
    private int myId;
    
    private Log log = LogFactory.getLog(TweetCollectBolt.class);
    
    public TweetCollectBolt(){
    	
    }

    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
        dbConn = new MongoDBImpl();
        myId = context.getThisTaskIndex();
    }

    @Override
    public void execute(Tuple tuple) {
    	
    	Integer topicID = (Integer)tuple.getValueByField("topicID");
    	Integer trackID = (Integer)tuple.getValueByField("trackID");
    	String tweetJSON = (String)tuple.getValueByField("sharon_tweet");
    	
    	log.debug("TwittCollectBolt ID : " + myId + " received tuple: from topicID(" + topicID +  ") " + tweetJSON);
    	log.info("TwittCollectBolt ID : " + myId + " received tuple: from topicID(" + topicID +  ") ");
    	
    	Document doc = Document.parse(tweetJSON);
        String id_str = doc.getString("id_str");
    	
    	// Adding the document to TWEET store if it does not exist already		
    	if (dbConn.containsDoc(DBInterface.TWEET_STORE + "_" + topicID, "id_str", id_str)) {
    		log.debug("Tweet already in database - skipping insertion");
    	}
    	else {
    		log.debug("Tweet not in database - adding");
    		String tweetStore = DBInterface.TWEET_STORE + "_" + topicID;
    		dbConn.insertDoc(tweetStore, doc);
    	}
    	
    	// Emit the a message with the tweet
    	_collector.emit(tuple, new Values(topicID, trackID, tweetJSON));
    	_collector.ack(tuple);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    	declarer.declare(true, new Fields("topicID", "trackID", "sharon_tweet"));
    }

	

}
