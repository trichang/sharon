package storm.sharon.bolt;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.bson.Document;

import scala.Tuple2;
import scala.Tuple3;
import storm.sharon.util.DBInterface;
import storm.sharon.util.MongoDBImpl;
import storm.sharon.util.Topic;
import storm.sharon.util.TopicKeywordHandler;
import storm.sharon.util.window.MRH_RollingTupleCounterImpl_A;
import storm.sharon.util.window.NthLastModifiedTimeTracker;
import storm.sharon.util.window.RollingTupleCounterImpl_A;
import storm.sharon.util.window.RollingTupleCounterInterface;
import storm.sharon.util.window.SlidingWindowCounter;
import storm.sharon.util.window.SlidingWindowObjectTracker;
//import storm.starter.tools.NthLastModifiedTimeTracker;
//import storm.starter.tools.SlidingWindowCounter;
//import storm.starter.util.TupleHelpers;
import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

/**
 *
 * 
 */
public class WindowedCommunityDetectorBolt extends BaseRichBolt {

	private static final Logger log = Logger.getLogger(WindowedCommunityDetectorBolt.class);
    private static final long serialVersionUID = 5537727428628598519L;
    public static final int EMIT_FREQUENCY_IN_SECONDS = 60;
    private OutputCollector collector;
    private TopicKeywordHandler cmd_handler;
    private RollingTupleCounterInterface rtc;
   
    
    private NthLastModifiedTimeTracker lastModifiedTracker;
    private int myId;
 

    public WindowedCommunityDetectorBolt() {
       
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
      //  lastModifiedTracker = new NthLastModifiedTimeTracker(deriveNumWindowChunksFrom(this.windowLengthInSeconds,
       //     this.emitFrequencyInSeconds));
        myId = context.getThisTaskIndex();
        log.info("Task ID: " + myId);
        cmd_handler = new TopicKeywordHandler();
        rtc = new MRH_RollingTupleCounterImpl_A(cmd_handler.getTopicIdMap(), collector);
    }

    @Override
    public void execute(Tuple tuple) {
    	Fields f = tuple.getFields();
        if (isTickTuple(tuple)) {
            log.debug("Received tick tuple, triggering emit of current window counts");
            rtc.emitCurrentWindowCounts();
        } else if (f.get(0).equals("command_response")) {
    		log.debug("This is a topic/keyword change message");
    		String message = (String)tuple.getValueByField("command_response");
    		Document doc = Document.parse(message);
    		cmd_handler.process(doc);
    		if (doc.containsKey("command_name")) {
    			if (doc.getString("command_name").equals("TOPIC_WINDOW_PARM_UPDATE")) {
    				log.debug("Window parameters were updated for topic: " + doc.getInteger("topic_id") + " query_id: " + doc.getString("query_id"));
    				log.debug("Initializing the rolling counter");
    				rtc.reInitializeTopic(doc.getInteger("topic_id"), doc.getString("query_id"));
    			}
    			if (doc.getString("command_name").equals("ADD_TOPIC")) {
    				log.debug("New topic got added: " + doc.getInteger("topic_id") + " intializing the rolling counters for mentions and replies");
    				rtc.initializeTopic(doc.getInteger("topic_id"), Topic.WINDOW_MENTION);
    				rtc.initializeTopic(doc.getInteger("topic_id"), Topic.WINDOW_REPLY);
    				rtc.initializeTopic(doc.getInteger("topic_id"), Topic.WINDOW_HASH);
    			}
    		}
    		collector.ack(tuple);
    	} else {
	        Integer topicID = (Integer)tuple.getValueByField("topicID");
	    	String queryID = (String)tuple.getValueByField("queryID");
	    	log.debug("Processing the next tuple received: topicID: " + topicID + " queryID: " + queryID);
        	rtc.countObjectAndAcknowledge(topicID, queryID, tuple);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tuples", "topicID"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Map<String, Object> conf = new HashMap<String, Object>();
        conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, EMIT_FREQUENCY_IN_SECONDS);
        return conf;
    }
    
    private static boolean isTickTuple(Tuple tuple) {
    	return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
    	    && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
    }

}



