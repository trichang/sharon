/**
 * This bolt is responsible for routing the messages to the appropriate Kakfka message queues
 * based on user subscriptions 
 * @author Shanika Karunasekera
 *
 */


package storm.sharon.bolt;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import storm.kafka.bolt.KafkaBolt;
import storm.sharon.util.SharonUser;
import storm.sharon.util.Topic;
import storm.sharon.util.TopicKeywordHandler;


public class UserMessageQueueBolt<K, V> extends KafkaBolt<K, V> {
	private static final Logger log = LoggerFactory.getLogger(UserMessageQueueBolt.class);
	
	private Producer<K, V> producer;
	private OutputCollector collector;
	private String queueName;
	private String fieldName;
	private TopicKeywordHandler cmd_handler;
	
	
	public UserMessageQueueBolt(String queueName, String fieldName) {
		this.queueName = queueName;
		this.fieldName = fieldName;
		
	}
	    
	@Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        Map configMap = (Map) stormConf.get(KAFKA_BROKER_PROPERTIES);
        Properties properties = new Properties();
        properties.putAll(configMap);
        ProducerConfig config = new ProducerConfig(properties);
        producer = new Producer<K, V>(config);
        this.collector = collector;
        cmd_handler = new TopicKeywordHandler();
       
    }
	
	 @Override
	    public void execute(Tuple tuple) {
		 
		 	Fields f = tuple.getFields();
		 	if (f.get(0).equals("command_response")) {
	    		log.debug("This is a topic/keyword change message");
	    		String message = (String)tuple.getValueByField("command_response");
	    		Document doc = Document.parse(message);
	    		cmd_handler.process(doc);    
	    		collector.ack(tuple);
	    	} else {
		        K key = null;
		        int topic_id = (int)tuple.getValueByField("topicID");
		        
		        log.debug("Received a message for queue: " + queueName + " topicID: " + topic_id);
		        ArrayList<String> queueList = getMessageQueueList(queueName, topic_id);
		        
		    	V message = (V)tuple.getValueByField(fieldName);
		        try {
		        	if (queueList != null) {
			        	for (String queue: queueList) {
			        		log.debug("Sending message to queue: " + queue);
			        		log.debug("Sending message: " + message + "to queue: " + queue);
			        		producer.send(new KeyedMessage<K, V>(queue, null, message));
			        	}
		        	}
		        } catch (Exception ex) {
		            log.error("Could not send message with key '" + key + "' and value '" + message + "'", ex);
		        } finally {
		            collector.ack(tuple);
		        }
	    	}
	   }

	private ArrayList<String> getMessageQueueList(String queueName, Integer topic_id) {
		
		ArrayList<String> messageQueueList = new ArrayList<String>();
		if (cmd_handler.getTopicIdMap().containsKey(topic_id)) {
			Topic topic = cmd_handler.getTopicIdMap().get(topic_id);
			
			Set<Entry<String, SharonUser>> mapValues = cmd_handler.getUserMap().entrySet();
			
			for (Entry<String, SharonUser> entry: mapValues) {
				String user = entry.getKey();
				if (cmd_handler.hasSubscribed(user, topic_id, queueName)) {
					log.debug("User " + user + " has subscribed to receive " + queueName + " for topic " + topic.getTopicName());
					String msq = queueName + "_" + user;
					log.debug("Adding message queue: " + msq  + " to the list of message queues");
					messageQueueList.add(msq);
				} else {
					log.debug("User " + user + " has NOT subscribed to receive " + queueName + " for topic " + topic.getTopicName());
				}
				
			}
			return messageQueueList;
		} else {
			log.info("Topic does not exist: " + topic_id + " " + queueName);
			return null;
		}
	}
	

}
