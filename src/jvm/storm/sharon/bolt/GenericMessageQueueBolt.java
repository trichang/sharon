package storm.sharon.bolt;

import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kafka.javaapi.producer.Producer;
import kafka.message.Message;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Tuple;
import storm.kafka.bolt.KafkaBolt;



public class GenericMessageQueueBolt<K, V> extends KafkaBolt<K, V> {
	private static final Logger log = LoggerFactory.getLogger(GenericMessageQueueBolt.class);
	private Producer<K, V> producer;
	private OutputCollector collector;
	
	
	public GenericMessageQueueBolt() {
		
	}
	    
	@Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        Map configMap = (Map) stormConf.get(KAFKA_BROKER_PROPERTIES);
        Properties properties = new Properties();
        properties.putAll(configMap);
        ProducerConfig config = new ProducerConfig(properties);
        producer = new Producer<K, V>(config);
        // This did not work - will investigate later
        //this.topic = (String) stormConf.get(TOPIC);
        this.collector = collector;
    }
	
	 @Override
	    public void execute(Tuple input) {
	        String topic = (String)input.getValueByField("queryId");
	    	V message = (V)input.getValueByField("queryResult");
	        try {
	        	log.info("Sending message to queue: " + topic);
	        	log.debug("Sending message: " + message + "to queue: " + topic);
	            producer.send(new KeyedMessage<K, V>(topic, null, message));
	        } catch (Exception ex) {
	            log.error("Could not send message with topic '" + topic + "' and value '" + message + "'", ex);
	        } finally {
	            collector.ack(input);
	        }
	   }
	

}
