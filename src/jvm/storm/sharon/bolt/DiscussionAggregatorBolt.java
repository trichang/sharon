package storm.sharon.bolt;


import storm.sharon.algorithms.DiscussionInterface;
import storm.sharon.algorithms.DiscussionStrategyA;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

@SuppressWarnings("serial")
public class DiscussionAggregatorBolt extends BaseRichBolt implements Observer  {
	
	OutputCollector _collector;
	private Log log = LogFactory.getLog(DiscussionAggregatorBolt.class);
	//private DiscussionCollectionObserver discussionCollector; 
	private DiscussionInterface disIntf; 
	private Tuple tuple;
	private int myId;
	private Integer topicID;
	
    public DiscussionAggregatorBolt(){
    	
    }

    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
        myId = context.getThisTaskIndex();
        int topic_id = myId;
    	disIntf = new DiscussionStrategyA(this, topic_id);
    }

    @Override
    public void execute(Tuple tuple) {
    
    	this.tuple = tuple;
    	
    	String s = (String)tuple.getValueByField("sharon_tweet");
    	topicID = (Integer)tuple.getValueByField("topicID");
   
        Document d = Document.parse(s);
               	
        log.debug("DiscussionAggregatorBolt  " + myId + " received tuple tweet = " + d);
        log.info("DiscussionAggregatorBolt  " + myId + " received tuple tweet got topic: " + topicID);
        
    	Document discDoc =  new Document("id_str", d.getString("id_str"))
    				.append("text", d.getString("text"))
    				.append("timestamp_ms",  d.getString("timestamp_ms"))
    				.append("in_reply_to_status_id_str", d.getString("in_reply_to_status_id_str"))
    				.append("screen_name", ((Document)d.get("user")).getString("screen_name"))
    				.append("in_reply_to_screen_name", d.getString("in_reply_to_screen_name"));
    		
    	log.debug("Discussion information: " + discDoc);
    	disIntf.add(discDoc);
    
    }

   

	@Override
	public void update(Observable o, Object arg) {
		Document doc = (Document)arg;
		String message = doc.toJson();
		log.debug("Sending message to topic discusssions: " + message);
		_collector.emit(tuple, new Values(message, topicID));
		_collector.ack(tuple);
		
	}
	
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("discussion", "topicID"));
	}

}
