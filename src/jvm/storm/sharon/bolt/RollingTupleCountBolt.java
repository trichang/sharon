package storm.sharon.bolt;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.bson.Document;

import scala.Tuple2;
import scala.Tuple3;
import storm.sharon.util.window.NthLastModifiedTimeTracker;
import storm.sharon.util.window.SlidingWindowCounter;
import storm.sharon.util.window.SlidingWindowObjectTracker;
//import storm.starter.tools.NthLastModifiedTimeTracker;
//import storm.starter.tools.SlidingWindowCounter;
//import storm.starter.util.TupleHelpers;
import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

/**
 *
 * 
 */
public class RollingTupleCountBolt extends BaseRichBolt {

    private static final long serialVersionUID = 5537727428628598519L;
    public static final int EMIT_FREQUENCY_IN_SECONDS = 60;
    private static final Logger log = Logger.getLogger(RollingTupleCountBolt.class);
    private static final int NUM_WINDOW_CHUNKS = 5;
    private static final int DEFAULT_SLIDING_WINDOW_IN_SECONDS = NUM_WINDOW_CHUNKS * 60;
    private static final int DEFAULT_EMIT_FREQUENCY_IN_SECONDS = DEFAULT_SLIDING_WINDOW_IN_SECONDS / NUM_WINDOW_CHUNKS;
    private static final String WINDOW_LENGTH_WARNING_TEMPLATE = "Actual window length is %d seconds when it should be %d seconds"
        + " (you can safely ignore this warning during the startup phase)";

   // private final SlidingWindowCounter<Object> counter;
    private final int windowLengthInSeconds;
    private final int emitFrequencyInSeconds;
    private OutputCollector collector;
    private NthLastModifiedTimeTracker lastModifiedTracker;
    private int myId;
    private int threshold = 5;
    SlidingWindowObjectTracker<Tuple2, Tuple3> objectTracker; 

    public RollingTupleCountBolt() {
        this(DEFAULT_SLIDING_WINDOW_IN_SECONDS, DEFAULT_EMIT_FREQUENCY_IN_SECONDS);
    }

    public RollingTupleCountBolt(int windowLengthInSeconds, int emitFrequencyInSeconds) {
    	
        this.windowLengthInSeconds = windowLengthInSeconds;
        this.emitFrequencyInSeconds = emitFrequencyInSeconds;
        objectTracker = 
    			new SlidingWindowObjectTracker<Tuple2, Tuple3>(deriveNumWindowChunksFrom(this.windowLengthInSeconds,
    		            this.emitFrequencyInSeconds));
    }

    private int deriveNumWindowChunksFrom(int windowLengthInSeconds, int windowUpdateFrequencyInSeconds) {
        return windowLengthInSeconds / windowUpdateFrequencyInSeconds;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        lastModifiedTracker = new NthLastModifiedTimeTracker(deriveNumWindowChunksFrom(this.windowLengthInSeconds,
            this.emitFrequencyInSeconds));
        myId = context.getThisTaskIndex();
        log.info("Task ID: " + myId);
    }

    @Override
    public void execute(Tuple tuple) {
        if (isTickTuple(tuple)) {
            log.debug("Received tick tuple, triggering emit of current window counts");
            emitCurrentWindowCounts();
        }
        else {
        	log.debug("Processing the next tuple received");
        	countObjAndAck(tuple);
        }
    }

    private void emitCurrentWindowCounts() {
        Map<Tuple3, Long> tupleCounts = objectTracker.advanceWindowReturnValues(threshold);
        int actualWindowLengthInSeconds = lastModifiedTracker.secondsSinceOldestModification();
        lastModifiedTracker.markAsModified();
        if (actualWindowLengthInSeconds != windowLengthInSeconds) {
            log.warn(String.format(WINDOW_LENGTH_WARNING_TEMPLATE, actualWindowLengthInSeconds, windowLengthInSeconds));
        }
        log.debug("Emitting Tuples: Periodic: " + tupleCounts);
        emit(tupleCounts, actualWindowLengthInSeconds);
    }

    private void emit(Map<Tuple3, Long> tupleCounts, int actualWindowLengthInSeconds) {
        if (tupleCounts != null) {
        	for (Entry<Tuple3, Long> entry : tupleCounts.entrySet()) {
        		Tuple3 tuple = entry.getKey();
        		Integer topic_id = Integer.parseInt((String)tuple._1());
        		String field_1 = (String)tuple._2();
        		String field_2 = (String)tuple._3();
        		int count = (int)((long)entry.getValue());
        		Document doc = new Document("count", count);
        		doc.append("actualWindowLength", actualWindowLengthInSeconds);
        		doc.append("topic_id", topic_id);
        		doc.append("field_1", field_1);
        		doc.append("field_2", field_2);
        		doc.append("time", System.currentTimeMillis());
        		log.debug("Emitting Tuple: " + doc.toJson());
        		collector.emit(new Values(doc.toJson(), topic_id));
        	}
        }
    }

    private void countObjAndAck(Tuple tuple) {
    	
    	Tuple2<String, String> tupleKey = (Tuple2<String, String>)tuple.getValueByField("tupleKey");
    	Tuple3<String, String, String> tupleValue = 
    			(Tuple3<String, String, String>)tuple.getValueByField("tupleValue");
 
    	
    	log.info("Bolt (" + myId + ") received tuple: key(" + tupleKey +  ") " + 
				 "value(" + tupleValue);
        
       
    	Map<Tuple3, Long> tupleCounts = objectTracker.addObjectReturnValues(tupleKey, tupleValue, threshold);
    	log.info("Emitting Tuples: Incremental: " + tupleCounts);
    	emit(tupleCounts, 0);
        collector.ack(tuple);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tuples", "topicID"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Map<String, Object> conf = new HashMap<String, Object>();
        conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, emitFrequencyInSeconds);
        return conf;
    }
    
    private static boolean isTickTuple(Tuple tuple) {
    	return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
    	    && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
    }

}



