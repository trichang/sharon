package storm.sharon.bolt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import backtype.storm.generated.GlobalStreamId;
import backtype.storm.grouping.CustomStreamGrouping;
import backtype.storm.task.WorkerTopologyContext;

public class TopicTrackGrouping implements CustomStreamGrouping, Serializable {
	
	private int numTasks = 0;
	private List<Integer> targetTasks;
	private Log log = LogFactory.getLog(TopicTrackGrouping.class);


	@Override
	public List<Integer> chooseTasks(int arg0, List<Object> values) {
		
		List<Integer> topicIds = new ArrayList<Integer>();
	    Integer topicId = (Integer) values.get(0);
	    Integer trackId = (Integer) values.get(1);
	    int A[] = new int[numTasks];
		int count = 0;
		if (trackId == 1) {
			for (Integer i: targetTasks) {
				A[count] = i;
				count++;
			}
	    
			if (topicId < numTasks) {
				topicIds.add(A[topicId]);
			} else {
				// If the number of bolts is less than the number of topic send it to the first bolt
				topicIds.add(A[0]);
			}
		}
	    
	
	    log.debug("TopicGrouping:  " + topicIds); 
	    
		return topicIds;
	}

	@Override
	public void prepare(WorkerTopologyContext arg0, GlobalStreamId arg1,
			List<Integer> targetTasks) {
		 
		this.targetTasks = targetTasks;
		this.numTasks = targetTasks.size();
		log.info("Number of tasks: " + numTasks);
		
	}
}
