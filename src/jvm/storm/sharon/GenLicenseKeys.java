package storm.sharon;


import org.bson.Document;

import storm.sharon.util.DBInterface;
import storm.sharon.util.MongoDBImpl;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GenLicenseKeys {

	
	public static void main(String[] args) {
		
		if (args.length < 3) {
			System.err.println("Usage: GenLiceseKeys <versionNumber> <userName> <expiryDate (dd/mm/yyyy)> [dbinfo]");
			System.err.println("example dbinfo = localhost:27017:sharonDB:sharon:sharonpassword");
			System.exit(-1);
		}
		
		String productKey = "RAPID-SHARON";
		String versionNumber = args[0];
		String userName = args[1];  // 
		String expiryDate = args[2]; // format dd/mm/yyyy
		Date expDate;
		SimpleDateFormat dateF = new SimpleDateFormat("dd/MM/yyyy");
		String dbinfo=null;
		if(args.length==4){
			dbinfo = args[3];
		} 
		
		try {
			expDate = dateF.parse(expiryDate);
		} catch (Exception e) {
			expDate = null;
			System.err.println("Failed to parse the date");
		}
	
		 Date currDate = new Date();
		
		 System.err.println("Current Date: " + dateF.format(currDate));
		 System.err.println("Expiry Date: " + dateF.format(expDate));

		 final String licenseKey = createLicenseKey(userName, productKey, versionNumber);
		 System.err.println("licenseKey = " + licenseKey);
		 System.err.println("Put the key into gui/files/LicenseKey.dat file before compiling.");
		 System.out.println(licenseKey);
		 
		 // Save the license key in the database
		 if(dbinfo!=null){
			 DBInterface dbConn = new MongoDBImpl(dbinfo);
			 Document doc = new Document("license_key", licenseKey);
			 doc.append("version_number", versionNumber);
			 doc.append("product_key", productKey);
			 doc.append("user_group", userName);
			 doc.append("expiry_date", dateF.format(expDate));
			 doc.append("generated_date", dateF.format(currDate));
			 dbConn.insertDoc(DBInterface.PRODUCT_KEY_STORE, doc);
			 dbConn.closeConn();
		 }
		 // Write the key to the LicenseKey.dat file 
		 /*
		 try {
			 FileWriter fw = new FileWriter(new File("gui/files/LicenseKey.dat"));
			 fw.write(licenseKey);
             fw.close();
		 } catch (Exception e) {
			 System.err.println("Failed to write to the LicenseKey.dat file.");
		 }*/
	}

	public static String createLicenseKey(String userName, String productKey, String versionNumber) {
		final String s = userName + "|" + productKey + "|" + versionNumber;
		final HashFunction hashFunction = Hashing.sha1();
		 //hashFunction.newHasher().putString(s, CharSet.ASCII_NUMERIC).hash();
		final HashCode hashCode = hashFunction.hashString(s, Charset.forName("US-ASCII"));
	    final String upper = hashCode.toString().toUpperCase();
	    return group(upper);
	}

	private static String group(String s) {
	     String result = "";
	     for (int i=0; i < s.length(); i++) {
	         if (i%6==0 && i > 0) {
	             result += '-';
	         }
	         result += s.charAt(i);
	     }
	     return result;
	 }
}
