package storm.sharon.spout;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import org.apache.storm.shade.org.json.simple.JSONObject;
import org.apache.storm.shade.org.json.simple.parser.JSONParser;
import org.apache.storm.shade.org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;

/**
 * Created by tri on 29/05/16.
 */
public class FileSpout extends BaseRichSpout {
    boolean _isDistributed;
    SpoutOutputCollector _collector;

    private ArrayList<Values> tupleList = new ArrayList<>();
    private Values[] tupleArray;
    private static volatile int counter = 0;

    public FileSpout() {
        this(true);
        tupleArray = new Values[tupleList.size()];
        tupleList.toArray(tupleArray);

        getGraph();
    }

    public FileSpout(boolean isDistributed) {
        this._isDistributed = isDistributed;
    }

    public void getGraph() {
        try {
            List<String> lines = Files.readAllLines(new File("/home/tri/work/workspace/data/3/testgraph.datmvn clean").toPath(), Charset.defaultCharset());
            for (String line : lines) {
                //"tuples":""tuples":"{ \"count\" : 1, \"actualWindowLength\" : 0, \"topic_id\" : 1, \"dest_node\" : \"#bpw16\", \"src_node\" : \"whereisandra\", \"time\" : { \"$numberLong\" : \"1464012075426\" }, \"query_id\" : \"window_hash\" }"}"}
                String nodes[] = line.split(" ");
                String json = String.format("{ \"count\" : 1, \"dest_node\" : \"%s\", \"src_node\" : \"%s\", \"query_id\" : \"window_hash\" }\"}", nodes[0], nodes[1]);

                Values value = new Values(new Object[] { "1", json });
                tupleList.add(value);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this._collector = collector;
    }

    public void close() {
    }

    public void nextTuple() {
        Utils.sleep(100L);
        if (counter < tupleArray.length) {
            System.out.println("spout sending: " + counter);
            this._collector.emit(tupleArray[counter]);
            counter++;
        }
    }

    public void ack(Object msgId) {
    }

    public void fail(Object msgId) {
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(new String[]{"topicID", "tuples"}));
    }

    public Map<String, Object> getComponentConfiguration() {
        if(!this._isDistributed) {
            HashMap ret = new HashMap();
            ret.put("topology.max.task.parallelism", Integer.valueOf(1));
            return ret;
        } else {
            return null;
        }
    }
}