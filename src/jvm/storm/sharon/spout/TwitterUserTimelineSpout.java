package storm.sharon.spout;


import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import storm.sharon.util.CredentialsManager;
import storm.sharon.util.FileMessageQueueImpl;
import storm.sharon.util.MessageQueueInterface;
import storm.sharon.util.TwitterQueryManager;
import backtype.storm.Config;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;




@SuppressWarnings("serial")
public class TwitterUserTimelineSpout extends BaseRichSpout {

	private SpoutOutputCollector _collector;
	private int myId;
	
	
	private TwitterQueryManager twitterQueryManager;
	
	boolean retrieve = true;
	private Log log = LogFactory.getLog(TwitterUserTimelineSpout.class);
    String zookeeper;
	
	public TwitterUserTimelineSpout(String zookeeper) {
		this.zookeeper = zookeeper;
	}
	

	@Override
	public void open(@SuppressWarnings("rawtypes") Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		
		myId = context.getThisTaskIndex();
		_collector = collector;
		log.info("Spout index: " + myId);
		twitterQueryManager = new TwitterQueryManager(myId);
	
		// Register to receive track change messages from the topic-keyword queue
		String topic = MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE;
		FetchUserMessageHandler m  = new FetchUserMessageHandler(zookeeper, topic, this);
		m.start();
		
		
	}
	
	
	public void getUserTimeline(String name, Integer topicID) {
		
		// Obtain the number of streams
    	int numStreams = CredentialsManager.getNumTwitterCredentials();
    	log.info("Number of streams: " + numStreams);
    				
    	// Use the first letter of the name to decide whether to handle the message 
    	int n = name.charAt(0);
    	if ( n%numStreams == myId ) {
    		
    		log.info("Handling user timeline message, spout ID: " + myId);
    	
	    	List<Document> statuses = twitterQueryManager.getUserTimeLine(name);
	    	     	
	     	log.debug("Topic ID: " + topicID);
			
			for (Document ret: statuses){
				Values v = new Values("1", 2, ret.toJson());
				log.info("TwitterUserTimelineSpout (tackID 2) emitting tuple: " + v);
			    _collector.emit(v);
			}
    	}
    	else {
    		log.debug("Not handling user timeline message, spout ID: " + myId);
    	}
		
	}

	@Override
	public void nextTuple() {	
	
	}

	@Override
	public void close() {
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		Config ret = new Config();
		return ret;
	}

	@Override
	public void ack(Object id) {
	}

	@Override
	public void fail(Object id) {
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("topicID", "trackID", "tweet"));
	}
	

}
