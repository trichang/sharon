package storm.sharon.spout;

import java.io.File;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import storm.sharon.gui.core.MessageQueueListener;
import storm.sharon.util.FileMessageQueueImpl;
import storm.sharon.util.MessageQueueInterface;
import storm.sharon.util.TwitterStreamManager;
import backtype.storm.Config;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

/**
 * This spout is responsible for collecting tweets from the streaming API based on keywords.
 * Currently only the keyword based tracking is implemented but location and user tracking can also be 
 * implemented in the future as required.
 * @author Shanika Karunasekera
 *
 */

@SuppressWarnings("serial")
public class TwitterStreamTrackerSpout extends BaseRichSpout {

	SpoutOutputCollector _collector;
	
	// Spout identification
	int myId;

	// Data structures for tacking words, locations and users
	ArrayList<String> keyWords;
	double[][] locations ;
	ArrayList<String> users;

	TwitterStreamManager twitterStreamManager;
	private ConsumerIterator<byte[], byte[]> it;

	private Log log = LogFactory.getLog(TwitterStreamTrackerSpout.class);
	
	int count = 0;
	boolean change = false;
	
	private String zookeeper;

	public TwitterStreamTrackerSpout(String zookeeper) {
		this.zookeeper = zookeeper;
	}
	
	
	@Override
	public void open(@SuppressWarnings("rawtypes") Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		
		 // Get the spout indetification
		 myId = context.getThisTaskIndex();
		 log.debug("Spout index: " + myId);
		
		 _collector = collector;
		 twitterStreamManager = new TwitterStreamManager(myId); 
		 
	
		 keyWords =  new ArrayList<String>();
		 users = new ArrayList<String>();
		 locations = null;
		 
		 openStream();
		 
		 // Register to receive track change messages from the topic-keyword queue
		 String topic = MessageQueueInterface.KEYWORD_TOPIC_MESSAGE_QUEUE;
		 TrackChangeMessageHandler m  = new TrackChangeMessageHandler(zookeeper, topic, this);
		 m.start();
	}
	
	private void openStream() {
		log.debug("Tracking " + keyWords.size() + "");
		for (String s: keyWords) {
			log.debug(s);
		}	
		for (String s: users) {
			log.debug(s);
		}
		// Open twitter stream
		twitterStreamManager.open(keyWords, users, locations);
	}
	
	public void resetKeywordsRestart(ArrayList<String> keywords) {
		log.debug("----------> Received a change keyword message");
    	log.debug("----------> Tracking " + keyWords.size() + "");
		this.keyWords = keywords;
    	for (String s: keyWords) {
			log.debug(s);
		}
    	
		log.debug("-----------> Changing twitter stream");
		twitterStreamManager.close();
		Utils.sleep(1000*60*1);
		log.debug("-----------> Reopening twitter stream");
	
		twitterStreamManager = new TwitterStreamManager(myId); 
		twitterStreamManager.open(keyWords, users, locations);
	}
  

	@Override
	public void nextTuple() {
		
		Document ret = twitterStreamManager.nextTweet();
		
		if (ret == null) {
			Utils.sleep(50);
		} else {
			// Emit tuple with a topic id of -1  because the spout is not aware of the topic.
			// This is be later determined by the SplitterBolt
			// Values v = new Values(-1, 1, ret.getRawStatus());
			Values v = new Values(-1, 1, ret.toJson());
			log.debug("TwitterStreamTrackerSpout (tackID 1) emitting tuple: " + v);
			_collector.emit(v);
		}
	}

	@Override
	public void close() {
		twitterStreamManager.close();
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		Config ret = new Config();
		return ret;
	}

	@Override
	public void ack(Object id) {
	}

	@Override
	public void fail(Object id) {
	}


	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// This spout will emit the topicID (which will always be -1, trackID (1) and the retrieved tweet
		declarer.declare(new Fields("topicID", "trackID", "tweet"));
	}

}
