package storm.sharon.spout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;


public class FetchTweetMessageHandler extends Thread {
	
	private ConsumerConnector consumer = null;
	private final String zookeeper;
	private String topic;
	private static Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = null;
	private Log log = LogFactory.getLog(FetchTweetMessageHandler.class);
	private TwitterTweetTrackerSpout tts;
	
	public FetchTweetMessageHandler(String zookeeper, String topic, TwitterTweetTrackerSpout tts) {
		this.topic = topic;
		this.zookeeper = zookeeper;
		this.tts = tts;
		
		log.debug("Initializing consumer connector");
		consumer = kafka.consumer.Consumer.createJavaConsumerConnector(createConsumerConfig());
		Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
		topicCountMap.put(topic, new Integer(1));
		consumerMap = consumer.createMessageStreams(topicCountMap);
		
	}
	
	
	/**
	 * Creates the consumer config.
	 *
	 * @return the consumer config
	 */
	private ConsumerConfig createConsumerConfig() {
		Properties props = new Properties();
		props.put("zookeeper.connect", zookeeper);
		props.put("group.id", "kafka-consumer-" + new Random().nextInt(100000));	
		props.put("zookeeper.session.timeout.ms", "40000");
		props.put("zookeeper.sync.time.ms", "20000");
		props.put("auto.commit.interval.ms", "1000");
		return new ConsumerConfig(props);

	}

	@Override
	public void run() {
		
		KafkaStream<byte[], byte[]> stream = consumerMap.get(topic).get(0);
		ConsumerIterator<byte[], byte[]> it = stream.iterator();
		log.debug("Reading topic: " + topic);
		
		while (it.hasNext()) {
			String receivedData = new String(it.next().message());
			Document d = Document.parse(receivedData);
			log.debug("Received message: " + d.toJson() + "  for topic " + topic);
			String command = d.getString("message_type");
			
			if ( (command.compareTo("FETCH_TWEET") == 0) ){
				Integer topicID = d.getInteger("topic_id");
				String tweetId = d.getString("tid");
				log.debug("Fetching tweet: " + tweetId);
				tts.fetchAndSendTweet(tweetId, topicID);
			}
		}
	}
	
}
