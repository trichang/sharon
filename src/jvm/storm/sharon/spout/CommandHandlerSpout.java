/**  
* CommandHandlerSpout.java - this spout listens to the Kafka command message queue to receive commands from the GUI
* @author  Shanika Karunasekera
* @version 1.0 
*/


package storm.sharon.spout;


import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import storm.sharon.util.MessageQueueInterface;
import backtype.storm.Config;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;


@SuppressWarnings("serial")
public class CommandHandlerSpout extends BaseRichSpout {

	private SpoutOutputCollector _collector;
	private int myId;
	private Log log = LogFactory.getLog(CommandHandlerSpout.class);
	private String zookeeper;

	public CommandHandlerSpout(String zookeeper) {
		this.zookeeper = zookeeper;
	}
	

	@Override
	public void open(@SuppressWarnings("rawtypes") Map conf,
			TopologyContext context, SpoutOutputCollector collector) {

		
		myId = context.getThisTaskIndex();
		_collector = collector;
		log.info("Spout index: " + myId);
		String topic = MessageQueueInterface.COMMAND_QUEUE;
		FetchCommandsMessageHandler m  = new FetchCommandsMessageHandler(zookeeper, topic, this);
		m.start();

	}
	
	
	public void routeMessage(String command) {
		
		Document d = Document.parse(command);
		Values v = new Values(command);
		
		if (d.getString("command_type").equalsIgnoreCase("TOPIC_MGMT") ||
				d.getString("command_type").equalsIgnoreCase("USER_MGMT"))
		{
			log.debug("Sending command: " + v + "to CommandHandlerBolt");
			_collector.emit("topic_user_stream", v);
		} else {
			// TBD Commands of type USER_QUERIES should be send to the appropriate bolt using a different stream
		}
	}

	@Override
	public void nextTuple() {	
	
	}

	@Override
	public void close() {
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		Config ret = new Config();
		return ret;
	}

	@Override
	public void ack(Object id) {
	}

	@Override
	public void fail(Object id) {
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		//declarer.declare(new Fields("sharon_command"));
		//declarer.declare(new Fields("sharon_command"));
		declarer.declareStream("topic_user_stream", new Fields("sharon_command"));
	}
}
