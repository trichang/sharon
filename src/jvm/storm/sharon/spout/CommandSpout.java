package storm.sharon.spout;



import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;

import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;


@SuppressWarnings("serial")
public class CommandSpout extends KafkaSpout {

	
	String fieldName;
	private Log log = LogFactory.getLog(CommandSpout.class);

	public CommandSpout(SpoutConfig kafkaConfig, String fieldName){
		super(kafkaConfig);
		this.fieldName = fieldName;
	}
	

	@Override
	public void nextTuple() {
		// TODO Auto-generated method stub
		super.nextTuple();
	}


	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields(fieldName));
	}
	
	

}
