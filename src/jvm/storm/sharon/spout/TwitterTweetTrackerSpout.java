package storm.sharon.spout;


import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;
import storm.sharon.util.CredentialsManager;
import storm.sharon.util.MessageQueueInterface;
import storm.sharon.util.TwitterQueryManager;
import backtype.storm.Config;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;




@SuppressWarnings("serial")
public class TwitterTweetTrackerSpout extends BaseRichSpout {

	private SpoutOutputCollector _collector;
	private int myId;
	private TwitterQueryManager twitterQueryManager;
	private Log log = LogFactory.getLog(TwitterTweetTrackerSpout.class);

	private boolean retrieve = true;
	private String zookeeper;

	public TwitterTweetTrackerSpout(String zookeeper) {
		this.zookeeper = zookeeper;
	}
	

	@Override
	public void open(@SuppressWarnings("rawtypes") Map conf,
			TopologyContext context, SpoutOutputCollector collector) {

		
		myId = context.getThisTaskIndex();
		_collector = collector;
		log.info("Spout index: " + myId);
		twitterQueryManager = new TwitterQueryManager(myId);
		// Register to receive track change messages from the topic-keyword queue
		String topic = MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE;
		FetchTweetMessageHandler m  = new FetchTweetMessageHandler(zookeeper, topic, this);
		m.start();

	}
	
	
	public void fetchAndSendTweet(String tweetId, Integer topicID) {
		
		// Obtain the number of streams
    	int numStreams = CredentialsManager.getNumTwitterCredentials();
    	
    	log.debug("Number of streams: " + numStreams);
    	log.debug("Received a fetch tweet meesage for tid: " + tweetId);
		
    	// Use the last three digits of the tweet to decide whether to handle the message 
		int n = Integer.parseInt(tweetId.substring(tweetId.length() - 3, tweetId.length() -1));
	    
		if ( n%numStreams == myId ) {
			
			log.debug("Handling tweet fetch message, spout ID, tweetId: " + myId + "," + tweetId);
				
			Document status = twitterQueryManager.getStatus(tweetId);
		
			if (status != null) {
				Values v = new Values(topicID, 3, status.toJson());
				log.debug("TwitterUserTimelineSpout (tackID 3) emitting tuple: " + v);
				_collector.emit(v);
			}
			else {
				log.warn("Failed to fetch tweet: "+ tweetId);  	
			}
		}
		else {
			
			log.debug("Not Handling tweet fetch message, spout ID: " + myId);
		}
	
	}

	@Override
	public void nextTuple() {	
	
	}

	@Override
	public void close() {
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		Config ret = new Config();
		return ret;
	}

	@Override
	public void ack(Object id) {
	}

	@Override
	public void fail(Object id) {
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("topicID", "trackID", "tweet"));
	}
}
