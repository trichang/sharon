package storm.sharon.cli;

import java.io.Console;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import org.bson.Document;

import storm.sharon.gui.core.RapidAPI;
import storm.sharon.util.MessageQueueInterface;
import storm.sharon.util.SharonUser;
import storm.sharon.util.Topic;

public class SimpleCLI implements Observer {

	HashMap<String, String> env;

	boolean tailing = false;
	int tailing_topic_id = -1;
	String tailing_queue_name = "";

	public SimpleCLI(){
		env = new HashMap<String, String>();
		env.put("kafkahostname", "sunrise.cis.unimelb.edu.au");
		env.put("zookeeperport", "2181");
		env.put("brokerport", "9092");
		env.put("dbhostname", "sunrise.cis.unimelb.edu.au");
		env.put("dbport", "27017");
		env.put("dbname", "sharonDB");
		env.put("dbuser", "sharon");
		env.put("dbpassword", "sharon123");
		env.put("username", SharonUser.DEFAULT_SHARON_USER);
		readcommands();
	}
	
	public void readcommands() {
		

		Console c = System.console();
		if (c == null) {
			System.err.println("No console.");
			System.exit(1);
		}

		CmdEnv(null);

		String command = c.readLine("rapid> ");
		while (command != null) {
			if (tailing) {
				tailing = false;
				continue;
			}
			String[] tokens = command.split(" ");
			if (tokens.length > 0) {
				String cmd = tokens[0];
				switch (cmd) {
				case "help":
					CmdHelp(tokens);
					break;
				case "env":
					CmdEnv(tokens);
					break;
				case "set":
					CmdSet(tokens);
					break;
				case "authenticate":
					CmdAuthenticate(tokens);
					break;
				case "listtopics":
					CmdListTopics(tokens);
					break;
				case "addkeyword":
					CmdAddKeyword(tokens);
					break;
				case "removekeyword":
					CmdRemoveKeyword(tokens);
					break;
				case "blacklist":
					CmdBlacklist(tokens);
					break;
				case "whitelist":
					CmdWhitelist(tokens);
					break;
				case "addtopic":
					CmdAddTopic(tokens);
					break;
				case "removetopic":
					CmdRemoveTopic(tokens);
					break;
				case "subscribe":
					CmdSubscribe(tokens);
					break;
				case "unsubscribe":
					CmdUnsubscribe(tokens);
					break;
				case "tail":
					CmdTail(tokens);
					break;
				case "blacklistnonc":
					CmdBlacklistNonc(tokens);
					break;
				case "addkeywordnonc":
					CmdAddKeywordNonc(tokens);
					break;
				}
			}
			command = c.readLine("rapid> ");
		}
	}

	private void CmdTail(String[] tokens) {
		tailing = true;
		tailing_topic_id = Integer.parseInt(tokens[1]);
	}

	private void CmdHelp(String[] tokens) {
		System.out.println("env");
		System.out.println("set <KEY> <VALUE>");
		System.out.println("authenticate");
		System.out.println("listtopics");
		System.out.println("addkeyword <TOPIC_ID> <KEYWORD>");
		System.out.println("removekeyword <TOPIC_ID> <KEYWORD>");
		System.out.println("blacklist <TOPIC_ID> <KEYWORD>");
		System.out.println("whitelist <TOPIC_ID> <KEYWORD>");
		System.out.println("blacklistnonc <TOPIC_ID> <KEYWORD>");
		System.out.println("addkeywordnonc <TOPIC_ID> <KEYWORD>");
		System.out.println("addtopic <TOPICNAME> <public|private>");
		System.out.println("removetopic <TOPIC_ID> ");
		System.out.println("subscribe <TOPIC_ID> <tweet|discussion|tuple|community>");
		System.out.println("unsubscribe <TOPIC_ID> <tweet|discussion|tuple|community>");
		System.out.println("tail <TOPIC_ID> <tweet|discussion|tuple|community>");
	}

	private void CmdUnsubscribe(String[] tokens) {
		Document doc = new Document("command_name", "UNSUBSCRIBE_TOPIC_QUEUE");
		String queue = "";
		switch (tokens[2]) {
		case "tweet":
			queue = MessageQueueInterface.TWEET_MESSAGE_QUEUE;
			break;
		case "discussion":
			queue = MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE;
			break;
		case "tuple":
			queue = MessageQueueInterface.TUPLE_MESSAGE_QUEUE;
			break;
		case "community":
			queue = MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE;
			break;
		}
		doc.append("command_type", "USER_MGMT");
		doc.append("user_name", RapidAPI.getAuthenticationData()
				.getRapidUserName());
		doc.append("topic_id", Integer.parseInt(tokens[1]));
		doc.append("queue_name", queue);
		RapidAPI.getProducer().sendMessage(doc);
	}
	
	private void CmdSubscribe(String[] tokens) {
		Document doc = new Document("command_name", "SUBSCRIBE_TOPIC_QUEUE");
		String queue = "";
		switch (tokens[2]) {
		case "tweet":
			queue = MessageQueueInterface.TWEET_MESSAGE_QUEUE;
			break;
		case "discussion":
			queue = MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE;
			break;
		case "tuple":
			queue = MessageQueueInterface.TUPLE_MESSAGE_QUEUE;
			break;
		case "community":
			queue = MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE;
			break;
		}
		doc.append("command_type", "USER_MGMT");
		doc.append("user_name", RapidAPI.getAuthenticationData()
				.getRapidUserName());
		doc.append("topic_id", Integer.parseInt(tokens[1]));
		doc.append("queue_name", queue);
		RapidAPI.getProducer().sendMessage(doc);
	}

	private void CmdRemoveTopic(String[] tokens) {
		ArrayList<Topic> topicList = RapidAPI.getUserTopicList();
		String tname = topicList.get(Integer.parseInt(tokens[2]))
				.getTopicName();
		Document doc = new Document("topic_name", tname);
		doc.append("topic_id", Integer.parseInt(tokens[2]));
		doc.append("command_name", "DELETE_TOPIC");
		doc.append("command_type", "TOPIC_MGMT");
		RapidAPI.getProducer().sendMessage(doc);
	}

	private void CmdAddTopic(String[] tokens) {
		Document doc = new Document("command_name", "ADD_TOPIC");
		doc.append("command_type", "TOPIC_MGMT");
		doc.append("topic_name", tokens[1]);
		doc.append("topic_type", tokens[2] == "public" ? 0 : 1);
		doc.append("topic_owner", RapidAPI.getAuthenticationData()
				.getRapidUserName());
		RapidAPI.getProducer().sendMessage(doc);
	}

	private void CmdAddKeywordNonc(String[] tokens) {
		Document doc = new Document("command_name",
				"ADD_NONCURRENT_KEYWORDS");
		doc.append("topic_id", Integer.parseInt(tokens[1]));
		doc.append("command_type", "TOPIC_MGMT");
		ArrayList<String> keywords = new ArrayList<String>();
		keywords.add(tokens[2]);
		doc.append("keywords", keywords);
		RapidAPI.getProducer().sendMessage(doc);
	}

	private void CmdBlacklistNonc(String[] tokens) {
		Document doc = new Document("command_name",
				"BLACKLIST_NONCURRENT_KEYWORDS");
		doc.append("topic_id", Integer.parseInt(tokens[1]));
		doc.append("command_type", "TOPIC_MGMT");
		ArrayList<String> keywords = new ArrayList<String>();
		keywords.add(tokens[2]);
		doc.append("keywords", keywords);
		RapidAPI.getProducer().sendMessage(doc);
	}

	private void CmdWhitelist(String[] tokens) {
		Document doc = new Document("command_name", "WHITELIST_KEYWORDS");
		doc.append("topic_id", Integer.parseInt(tokens[1]));
		doc.append("command_type", "TOPIC_MGMT");
		ArrayList<String> keywords = new ArrayList<String>();
		keywords.add(tokens[2]);
		doc.append("keywords", keywords);
		RapidAPI.getProducer().sendMessage(doc);
	}

	private void CmdBlacklist(String[] tokens) {
		Document doc = new Document("command_name", "BLACKLIST_KEYWORDS");
		doc.append("command_type", "TOPIC_MGMT");
		doc.append("topic_id", Integer.parseInt(tokens[1]));
		ArrayList<String> keywords = new ArrayList<String>();
		keywords.add(tokens[2]);
		doc.append("keywords", keywords);
		RapidAPI.getProducer().sendMessage(doc);
	}

	private void CmdRemoveKeyword(String[] tokens) {
		Document doc = new Document("command_name", "DELETE_KEYWORDS");
		doc.append("command_type", "TOPIC_MGMT");
		doc.append("topic_id", Integer.parseInt(tokens[1]));
		ArrayList<String> keywords = new ArrayList<String>();
		keywords.add(tokens[2]);
		doc.append("keywords", keywords);
		RapidAPI.getProducer().sendMessage(doc);
	}

	private void CmdAddKeyword(String[] tokens) {
		Document doc = new Document("command_name", "ADD_KEYWORDS");
		doc.append("command_type", "TOPIC_MGMT");
		doc.append("topic_id", Integer.parseInt(tokens[1]));
		ArrayList<String> keywords = new ArrayList<String>();
		keywords.add(tokens[2]);
		doc.append("keywords", keywords);
		RapidAPI.getProducer().sendMessage(doc);
	}

	private void CmdListTopics(String[] tokens) {
		ArrayList<Topic> topicList = RapidAPI.getUserTopicList();
		for (Topic topic : topicList) {
			System.out.println(topic.getTopicId() + ":"
					+ topic.getTopicName() + ":" + topic.getTopicOwner());
		}
	}

	private void CmdSet(String[] tokens) {
		env.put(tokens[1], tokens[2]);
	}

	private void CmdAuthenticate(String[] tokens) {
		ArrayList<String> data = new ArrayList<String>();
		data.add(env.get("kafkahostname"));
		data.add(env.get("zookeeperport"));
		data.add(env.get("brokerport"));
		data.add(env.get("dbhostname"));
		data.add(env.get("dbport"));
		data.add(env.get("dbname"));
		data.add(env.get("dbuser"));
		data.add(env.get("dbpassword"));
		data.add(env.get("username"));
		
		System.out.println("CLI is not supported at this time. See Aaron.");
		System.exit(-1);

		RapidAPI.getTweetReader().register(this);
		RapidAPI.getKeywordTopicReader().register(this);
		RapidAPI.getCommunityReader().register(this);
		RapidAPI.getTupleReader().register(this);
		// TODO we need the discussion reader
	}

	private void CmdEnv(String[] tokens) {
		for (String key : env.keySet()) {
			System.out.println(key + " = " + env.get(key));
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		Document d = (Document) arg;
		if (tailing) {
			if (d.getInteger("sharon_topic_id") != null
					&& d.getInteger("sharon_topic_id") == tailing_topic_id) {
				// TODO: select only specific queue as well, when queue
				// names are present
				System.out.println(d);
			}
		}
	}

}

