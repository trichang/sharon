package storm.sharon;


import org.apache.storm.shade.org.json.simple.JSONObject;
import org.apache.storm.shade.org.json.simple.parser.JSONParser;
import org.apache.storm.shade.org.json.simple.parser.ParseException;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerListener;
import org.graphstream.ui.view.ViewerPipe;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

/**
 * Created by tri on 24/05/16.
 */
public class CommunityGUI {
    private HashMap<String, HashSet<String>> communityList = new HashMap<>();
    private HashSet<String> graph_nodes = new HashSet<>();
    private HashMap<String, HashSet<String>> the_graph = new HashMap<>();
    private int distNum = 0, serNum = 0, mistakeCount = 0, correctCount = 0, wrongCount = 0;

    private static final String[] indexcolors = new String[]{
            "#000000", "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
            "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
            "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
            "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
            "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
            "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
            "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
            "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",

            "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
            "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
            "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
            "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
            "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
            "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
            "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
            "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379", "#012C58",

            "#7A7BFF", "#D68E01", "#353339", "#78AFA1", "#FEB2C6", "#75797C", "#837393", "#943A4D",
            "#B5F4FF", "#D2DCD5", "#9556BD", "#6A714A", "#001325", "#02525F", "#0AA3F7", "#E98176",
            "#DBD5DD", "#5EBCD1", "#3D4F44", "#7E6405", "#02684E", "#962B75", "#8D8546", "#9695C5",
            "#E773CE", "#D86A78", "#3E89BE", "#CA834E", "#518A87", "#5B113C", "#55813B", "#E704C4",
            "#00005F", "#A97399", "#4B8160", "#59738A", "#FF5DA7", "#F7C9BF", "#643127", "#513A01",
            "#6B94AA", "#51A058", "#A45B02", "#1D1702", "#E20027", "#E7AB63", "#4C6001", "#9C6966",
            "#64547B", "#97979E", "#006A66", "#391406", "#F4D749", "#0045D2", "#006C31", "#DDB6D0",
            "#7C6571", "#9FB2A4", "#00D891", "#15A08A", "#BC65E9", "#FFFFFE", "#C6DC99", "#203B3C",

            "#671190", "#6B3A64", "#F5E1FF", "#FFA0F2", "#CCAA35", "#374527", "#8BB400", "#797868",
            "#C6005A", "#3B000A", "#C86240", "#29607C", "#402334", "#7D5A44", "#CCB87C", "#B88183",
            "#AA5199", "#B5D6C3", "#A38469", "#9F94F0", "#A74571", "#B894A6", "#71BB8C", "#00B433",
            "#789EC9", "#6D80BA", "#953F00", "#5EFF03", "#E4FFFC", "#1BE177", "#BCB1E5", "#76912F",
            "#003109", "#0060CD", "#D20096", "#895563", "#29201D", "#5B3213", "#A76F42", "#89412E",
            "#1A3A2A", "#494B5A", "#A88C85", "#F4ABAA", "#A3F3AB", "#00C6C8", "#EA8B66", "#958A9F",
            "#BDC9D2", "#9FA064", "#BE4700", "#658188", "#83A485", "#453C23", "#47675D", "#3A3F00",
            "#061203", "#DFFB71", "#868E7E", "#98D058", "#6C8F7D", "#D7BFC2", "#3C3E6E", "#D83D66",

            "#2F5D9B", "#6C5E46", "#D25B88", "#5B656C", "#00B57F", "#545C46", "#866097", "#365D25",
            "#252F99", "#00CCFF", "#674E60", "#FC009C", "#92896B"
    };

    private String styleSheet="node {"+
            "fill-mode: dyn-plain;"+
            " fill-color: #999, #F53, #55F;"+
            " size: 20px;"+
            " stroke-mode: plain;"+
            " stroke-color: black;"+
            " stroke-width: 1px;"+
            "}"+
            "node.important {"+
            " fill-color: red;"+
            //" size: 30px;"+
            "}";

    public CommunityGUI() {
        super();

        System.setProperty("sun.java2d.directx", "True");
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
    }

    private void populateGraph() {
        System.out.println("\nReading Graph");
        try {
            final int[] lineCount = {0, 0};
            Files.lines(new File("/home/tri/work/workspace/data/3/testgraph.dat").toPath()).forEach((String line) -> {
//                if (lineCount[0] >= 7000) return; //6.1k looks good
                try {
                    lineCount[0]++;
                    //Ver 1.0
//                    JSONObject obj = (JSONObject) (new JSONParser().parse(line));
//                    JSONObject tuples = (JSONObject) (new JSONParser().parse(obj.get("tuples").toString()));
//
//                    String src = tuples.get("src_node").toString().trim().toLowerCase();
//                    String dest = tuples.get("dest_node").toString().trim().toLowerCase();
//                    String count = tuples.get("count").toString();

                    //Ver 2.0
                    String temp[] = line.split(" ");
                    String src = temp[0];
                    String dest = temp[1];
                    String count = "1";
                    if (count.equals("0")) {
                        //removeEdge(g, src, dest);
                        if (the_graph.containsKey(src)) {
                            the_graph.get(src).remove(dest);
                            if (the_graph.get(src).size() == 0) {
                                graph_nodes.remove(src);
                                the_graph.remove(src);
                            }
                        }
                        if (the_graph.containsKey(dest)) {
                            the_graph.get(dest).remove(src);
                            if (the_graph.get(dest).size() == 0) {
                                graph_nodes.remove(dest);
                                the_graph.remove(dest);
                            }
                        }
                    } else {
                        lineCount[1] += Integer.parseInt(count);
                        //addEdge(g, src, dest);
                        if (the_graph.containsKey(src)) {
                            the_graph.get(src).add(dest);
                        } else {
                            the_graph.put(src, new HashSet<>(Arrays.asList(new String[]{dest})));
                        }

                        if (the_graph.containsKey(dest)) {
                            the_graph.get(dest).add(src);
                        } else {
                            the_graph.put(dest, new HashSet<>(Arrays.asList(new String[]{src})));
                        }

                        graph_nodes.add(src);
                        graph_nodes.add(dest);
                    }

//                    if (lineCount[0]%1000 == 0) { //12 789
//                        System.out.println("read line " + lineCount[0]);
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            System.out.println("Total number of tweets: " + lineCount[1]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void drawGraph(Graph g) {
        for (String key : graph_nodes) {
            Node n = g.addNode(key.trim().toLowerCase());
//            n.addAttribute("ui.label", key);
        }

        for (String key : the_graph.keySet()) {
            for (String child : the_graph.get(key)) {
                try {
                    if (key.compareTo(child) > 0) {
                        g.addEdge(child + key, child, key);
                    } else {
                        g.addEdge(key + child, key, child);
                    }
                } catch (Exception ignored) {}
            }
        }
    }

    private void populateCluster() {
        System.out.println("\nReading Cluster");
        try {
            Files.lines(new File("/home/tri/work/workspace/data/3/cluster_stream2.log").toPath()).forEach((String line) -> {
                try {
                    line = line.replace(":[", ":\"[").replace("]", "]\"");
                    JSONObject obj = (JSONObject) (new JSONParser().parse(line));

                    String seed = obj.get("seed").toString().trim().toLowerCase();
                    String[] nodeList = obj.get("nodeList").toString().replace(" ",  "").replace("[", "").replace("]", "").toLowerCase().split(",");

                    if (!communityList.containsKey(seed)) {
                        communityList.put(seed, new HashSet<>());
                    }
                    HashSet<String> neighbors = communityList.get(seed);
                    neighbors.addAll(Arrays.asList(nodeList));
                    communityList.put(seed, neighbors);
                } catch (ParseException e) {
                    System.err.println(line);
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void coloringCluster (Graph g) {
        System.out.println("\nColoring Cluster");

        int counter = 0;
        int err = 0;
        for (String seed : communityList.keySet()) {
            for (String node : communityList.get(seed)) {
                try {
                    g.getNode(node.trim().toLowerCase()).addAttribute("ui.style", "fill-color: " + indexcolors[counter] + ";");
                } catch (Exception ex) {err++;}
            }

            counter++;
            counter %= indexcolors.length;
        }

        System.out.println("Err count: " + err);
    }

    public void start() {
        System.out.println("Graphic server started.");
        Graph g = display();

        //distributed
//        populateGraph();
//        drawGraph(g);
//        populateCluster();
//        coloringCluster(g);

        //serial
        populateGraph();
        drawGraph(g);
        doLocalClustering();
        coloringCluster(g);

//        exportGraph();
    }

    private void doLocalClustering() {
//        System.out.println(graph_nodes.size() + " " + communityList.size());
        for (String seed : graph_nodes) {
            PPR(0.99, 0.01, seed);
        }

        System.out.println("Total dist nodes identified: " + distNum);
        System.out.println("Total ser nodes identified: " + serNum);
        System.out.println("Total identified True: " + mistakeCount);
        System.out.println("Total correct seed: " + correctCount + " Total wrong seed: " + wrongCount);
    }
    private void PPR(double alpha, double tolerance, String seed) {
        HashMap<String, Double> x = new HashMap<>();
        HashMap<String, Double> r = new HashMap<>();
        Queue<String> Q = new LinkedList<>();

        r.put(seed, 1.);
        Q.add(seed);

        while (Q.size() > 0) {
            String v = Q.remove();

            if (!x.containsKey(v)) x.put(v, 0.);
            if (!r.containsKey(v)) r.put(v, 0.);
            x.put(v, x.get(v) + (1-alpha)*r.get(v));
            double mass = alpha * r.get(v) / (2 * the_graph.get(v).size());

            for (String u : the_graph.get(v)) {
                if (!r.containsKey(u)) r.put(u, 0.);
                double temp = the_graph.get(u).size()*tolerance;
//                System.out.printf("%f < %f and %f + %f >= %f\n", r.get(u), temp, r.get(u), mass, temp);
                if ((r.get(u) < temp) && ((r.get(u) + mass) >= temp)) {
                    Q.add(u);
                }
                r.put(u, r.get(u)+mass);
            }

            r.put(v, mass*the_graph.get(v).size());
            if (r.get(v) >= the_graph.get(v).size()*tolerance) Q.add(v);
        }

        ArrayList<String> sv = new ArrayList<>();
        for (String v : x.keySet()) {
//            if (seed.equals("#news")) System.out.println(">>" + v + the_graph.get(v).size());
            if (x.get(v) > 0) {
                x.put(v, x.get(v) / the_graph.get(v).size());
                sv.add(v);
            }
        }

        Collections.sort(sv, (o1, o2) -> {
            if (x.get(o1) < x.get(o2)) return 1;
            else if (x.get(o1) > x.get(o2)) return -1;
            return 0;
        });

//        if (seed.equals("#news")) {
//            System.out.println();
//            for (String y : sv) System.out.print(" " + y);
//            System.out.println();
//        }

        double Gvol = 0.;
        for (String key : the_graph.keySet()) {
            Gvol += the_graph.get(key).size();
        }
        Gvol = 2*Gvol;

        HashSet<String> S = new HashSet<>();
        double volS = 0., cutS = 0., bestcond = 1.;

        HashSet<String> bestset = new HashSet<>(Arrays.asList(new String[]{sv.get(0)}));
        for (String s : sv) {
            volS += the_graph.get(s).size();
            for (String v : the_graph.get(s)) {
                if (S.contains(v)) {
                    cutS -= 1;
                } else {
                    cutS += 1;
                }
            }
            double currcond = cutS/Math.min(volS, Gvol-volS);
            S.add(s);
//System.out.println(cutS + "   " + volS + "   " + Gvol);
            if (currcond < bestcond) {
                bestcond = currcond;
                bestset = (HashSet<String>) S.clone();
            }
        }

        communityList.put(seed, bestset);
        //compare result
//        try {
//            int distCount = communityList.get(seed).size();
//            int serCount = bestset.size();
//            bestset.retainAll(communityList.get(seed));
//            int intersectCount = bestset.size();
//
//            if (distCount != intersectCount || serCount != intersectCount) {
//                wrongCount++;
//                distNum += distCount;
//                serNum += serCount;
//                mistakeCount += intersectCount;
//
////                System.out.println("from File: " + seed + " " + (distCount-serCount));
//                System.out.println((serCount+distCount-intersectCount-intersectCount));
////                for (String item : communityList.get(seed)) {System.out.print(" " + item);}
////                System.out.println();
////                System.out.println("from PPR: " + seed);
////                for (String item : bestset) {System.out.print(" " + item);}
////                System.out.println();
//            } else {
//                correctCount++;
//                distNum += distCount;
//                serNum += serCount;
//                mistakeCount += intersectCount;
//            }
//        } catch (Exception ex) {
////            System.err.println("ERR " +  seed + " " + communityList.containsKey(seed));
//        }
    }

    private void exportGraph() {
        try {
            PrintWriter writer = new PrintWriter("testgraph.dat", "UTF-8");
            for (String key : the_graph.keySet()) {
                for (String value : the_graph.get(key)) {
                    writer.println(key + " " + value);
                }
            }
            writer.close();
        } catch (Exception ignored) { }
    }

    public Graph display() {
        final Graph graph = new SingleGraph("Community View");
        graph.addAttribute("ui.stylesheet", styleSheet);

//        Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
        Viewer viewer = graph.display();

        final ViewerPipe fromViewer = viewer.newViewerPipe();

        fromViewer.addSink(graph);

        new Thread() {
            volatile boolean isRunning = true;

            @Override
            public void run() {
                fromViewer.addViewerListener(new ViewerListener() {
                    public void viewClosed(String s) {
                        isRunning = false;
                    }

                    public void buttonPushed(String s) {
//                        if (communityList.get(s) != null)
//                            for (String i : communityList.get(s)) {
//                                graph.getNode(i).addAttribute("ui.color", 0.5);
//                            }
                    }

                    public void buttonReleased(String s) {
//                        if (communityList.get(s) != null)
//                            for (String i : communityList.get(s)) {
//                                graph.getNode(i).addAttribute("ui.color", 0);
//                            }
                    }
                });

                while (isRunning) {
                    fromViewer.pump();

                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }.start();

        return graph;
    }

    public static void main(String[] args) {
        CommunityGUI server = new CommunityGUI();
        server.start();
    }
}

