/**  
* RetrofitDatabase.java
* This program is used to retrofit the database with feature updates
* Read from a file, coll_value the collection, the fields and values to be retrofitted  
* Format of the file is as follows
* 
* CollectionName FieldName_1 FieldType Value_1 FieldName_2 FieldType Value_2 
* 
* Example of a coll_val file to retrofit table topicKeyCollection 
* topicKeyCollection topic_type int 0 topic_owner string SharonDefault shared_users stringList SharonDefault

* 
* @author  Shanika Karunasekera
* @version 1.0 
*/

package storm.sharon;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import storm.sharon.util.DBInterface;
import storm.sharon.util.MongoDBImpl;


public class RetrofitDatabase {
	
	private static Log log = LogFactory.getLog(RetrofitDatabase.class);	

	public static void main(String[] args) throws Exception {

		log.info("Initializing database.");
		DBInterface dbConn = new MongoDBImpl();
		String fileName = "coll_val"; // Read the file name coll_val
		
		
			
		try {
			FileReader fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ( (line = br.readLine()) != null) {
				log.debug("Read Line: " + line);
				StringTokenizer tk = new StringTokenizer(line, " ");
				String collectionName = tk.nextToken();
				while (tk.hasMoreTokens()) {
					String updateFieldName = tk.nextToken();
					String fieldType = tk.nextToken();
					String newString = tk.nextToken();
					if (fieldType.compareTo("int") == 0) {
						Integer newInt = Integer.parseInt(newString);
						dbConn.updateDocs(collectionName, updateFieldName, newInt);
					} else if (fieldType.compareTo("boolean") == 0) {
						Boolean newBool = Boolean.parseBoolean(newString);
						dbConn.updateDocs(collectionName, updateFieldName, newBool);
					} else if (fieldType.compareTo("string") == 0){
						dbConn.updateDocs(collectionName, updateFieldName, newString);
					} else if (fieldType.compareTo("stringList") == 0){
						ArrayList<String> list = new ArrayList<String>();
						list.add(newString);
						dbConn.updateDocs(collectionName, updateFieldName, list);
					}
				}
			}
			
			fr.close();
		}
		catch (Exception e) {
			log.error("Failed to read file: "+  fileName);
		}
				
	}
}
