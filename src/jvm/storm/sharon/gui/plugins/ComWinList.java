package storm.sharon.gui.plugins;

import storm.sharon.util.Topic;

import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.bson.Document;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
@SuppressWarnings("serial")
public class ComWinList extends DataPanel {
	private static Logger log = LogManager.getLogger(ComWinList.class.getName());
	protected	JList		listbox;
	protected	JScrollPane scrollPane;
	protected Vector<String>  dataMap;
	private String[] query_id = null;
	

	public ComWinList() {
		ConstructPanel();
	}

	public ComWinList(JFrame jframe, Topic topic, String longName, String shortName, String toolTip, String[] query_id) {
		super(jframe, topic, longName, shortName, toolTip);
		ConstructPanel();
		this.query_id = query_id;
	}
	
	protected void ConstructPanel(){
		
		jframe.add(this);
		
		dataMap = new Vector<String>();
		
		setLayout( new BorderLayout());
		
		listbox = new JList( dataMap );
		scrollPane = new JScrollPane();
		scrollPane.getViewport().add( listbox );
		add( scrollPane, BorderLayout.CENTER );

		
		// we are now ready to receive events
		RegisterTupleReader();

	}

	@SuppressWarnings("unchecked")
	@Override
	public void ReceiveStreamDocument(Document d) {
		String q = d.getString("query_id");
		for(String query:query_id){
			if(query.equals(q)){
				displayInfo(d);
				break;
			}
		}
	}
	
	private void displayInfo(Document d) {	
		if (!d.containsKey("command")) {
			int total_count = d.getInteger("count");
			String dest = d.getString("dest_node");
			String src = d.getString("src_node");
			String txt = "Destination: "+ dest + " Source: " + src + " Count: " + total_count;
			if(isLogging()){
				getRlog().logString(txt);
			}
			dataMap.add(0, txt);
			
			final Vector<String> ld = (Vector<String>) dataMap.clone();
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					listbox.setListData( ld );
					scrollPane.revalidate();
					scrollPane.repaint();
				}
			});
		} else {
			// This is a RESET command -ignore
		}
	}

	@Override
	public String LongName() {
		return longName;
	}

	@Override
	public String ShortName() {
		return shortName;
	}

	@Override
	public String Tooltip() {
		return toolTip;
	}

	@Override
	public void Clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public String[] getQuery_id() {
		return query_id;
	}
}
