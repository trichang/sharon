package storm.sharon.gui.plugins;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.JFrame;

import org.bson.Document;

import storm.sharon.Rapid;
import storm.sharon.gui.core.RapidAPI;
import storm.sharon.util.Topic;


public class Plugins implements Observer {
	private HashMap<Integer,Vector<PluginContainer>> containers;
	

	private HashMap<Integer,Vector<DataPanel>> plugins;
	
	
	public Plugins(){
		containers = new HashMap<Integer,Vector<PluginContainer>>();
		plugins = new HashMap<Integer,Vector<DataPanel>>();
		
		HashMap<Integer,Topic> topics = RapidAPI.getTopicKeywordHandler().getTopicIdMap();
		for(Integer topic_id: topics.keySet()){
			Topic topic = topics.get(topic_id);
			Vector<DataPanel> tplugins = new Vector<DataPanel>();
			addPlugins(tplugins,topic);
			for(DataPanel panel:tplugins){
				// if the database reports the user has subscribed to appropriate queues,
				// then this method will subscribe the plugin
				panel.SubscribeIfPossible(); 
			}
			plugins.put(topic.getTopicId(),tplugins);
		}
		
		RapidAPI.getKeywordTopicReader().register(this);
	}
	
	

	public Vector<DataPanel> getTopicPlugins(Integer topic_id){
		return plugins.get(topic_id);
	}

	@Override
	public void update(Observable o, Object arg) {
		Document d = (Document) arg;
		if (d.get("command_name") != null) {
			if (d.getString("command_name").equals("NEW_TOPIC")) {
				int topic_id = d.getInteger("topic_id");
				Topic topic = RapidAPI.getTopicKeywordHandler().getTopicIdMap().get(topic_id);
				Vector<DataPanel> tplugins = new Vector<DataPanel>();
				addPlugins(tplugins,topic);
				for(DataPanel panel:tplugins){
					// if the database reports the user has subscribed to appropriate queues,
					// then this method will subscribe the plugin
					panel.SubscribeIfPossible(); 
				}
				plugins.put(topic_id,tplugins);
			}
			if (d.getString("command_name").equals("DELETE_TOPIC")) {
				int topic_id = d.getInteger("topic_id");
				Vector<DataPanel> panels = plugins.get(topic_id);
				for(DataPanel panel: panels){
					panel.Destroy(); // destroying the panel will unsubscribe it
				}
				plugins.remove(topic_id);
			}

		}
	}
	
	/*
	 * Register the available plugins with the system.
	 */
	private void addPlugins(Vector<DataPanel> tplugins,Topic topic){
		
		Vector<PluginContainer> containerList = new Vector<PluginContainer>();
		containers.put(topic.getTopicId(),containerList);
		
		// 0 Tweets container
		containerList.add(new PluginContainer("Tweets"));
		tplugins.add(new TweetList(newPluginJFrame(),topic));  
		tplugins.add(new WordCloud(newPluginJFrame(),topic)); 
		containerList.get(0).getPlugins().add(tplugins.get(0));
		containerList.get(0).getPlugins().add(tplugins.get(1));
		
		// 1 Community Time Windowed Lists container
		containerList.add(new PluginContainer("Community Windowed Lists"));
		tplugins.add(new ComWinList(newPluginJFrame(),topic, "Community Windowed Mention List", "CWMentionList", "A list of highly mentioned users, windowed over a time period.", new String[]{Topic.WINDOW_MENTION} ));
		tplugins.add(new ComWinList(newPluginJFrame(),topic, "Community Windowed Reply List", "CWReplyList", "A list of highly replied to users, windowed over a time period.", new String[]{Topic.WINDOW_REPLY} ));
		tplugins.add(new ComWinList(newPluginJFrame(),topic, "Community Windowed Hashtag List", "CWHashtagList", "A list of highly mentioned hashtags, windowed over a time period.", new String[]{Topic.WINDOW_HASH} ));
		tplugins.add(new ComWinList(newPluginJFrame(),topic, "Community Windowed List Combined", "CWList", "A list of highly mentioned users, hashtags and replied to users, windowed over a time period.", new String[]{Topic.WINDOW_MENTION,Topic.WINDOW_REPLY,Topic.WINDOW_HASH}));
		containerList.get(1).getPlugins().add(tplugins.get(2));
		containerList.get(1).getPlugins().add(tplugins.get(3));
		containerList.get(1).getPlugins().add(tplugins.get(4));
		containerList.get(1).getPlugins().add(tplugins.get(5));
		
		// 2 Community Time Windowed Graphs container
		containerList.add(new PluginContainer("Community Windowed Graphs"));
		tplugins.add(new ComWinGraph(newPluginJFrame(),topic, "Community Windowed Mention Graph", "CWMentionGraph", "Shows a graph of who mentions who a lot, over a time window.", new String[]{Topic.WINDOW_MENTION}));
		tplugins.add(new ComWinGraph(newPluginJFrame(),topic, "Community Windowed Reply Graph", "CWReplyGraph", "Shows a graph of highly replied users, over a time window.", new String[]{Topic.WINDOW_REPLY}));
		tplugins.add(new ComWinGraph(newPluginJFrame(),topic, "Community Windowed Hashtag Graph", "CWHashtagGraph", "Shows a graph of highly mentioned hashtags, over a time window.", new String[]{Topic.WINDOW_HASH}));
		tplugins.add(new ComWinGraph(newPluginJFrame(),topic, "Community Windowed Graph Combined", "CWGraph", "Shows a graph of highly mentioned users, hashtags and replied to users, over a time window.", new String[]{Topic.WINDOW_MENTION,Topic.WINDOW_REPLY,Topic.WINDOW_HASH}));
		containerList.get(2).getPlugins().add(tplugins.get(6));
		containerList.get(2).getPlugins().add(tplugins.get(7));
		containerList.get(2).getPlugins().add(tplugins.get(8));
		containerList.get(2).getPlugins().add(tplugins.get(9));
		
		// 3 Discussions container
		containerList.add(new PluginContainer("Discussions"));
		tplugins.add(new Discussions(newPluginJFrame(),topic));
		containerList.get(3).getPlugins().add(tplugins.get(10));
		
		// 4 Custom Streams container
		containerList.add(new PluginContainer("Custom Streams"));
		
		// 5 Custom Streams container
		containerList.add(new PluginContainer("Custom Queries"));
				
		
		//tplugins.add(new ComMentionsList(newPluginJFrame(),topic));
		//tplugins.add(new ComReplyList(newPluginJFrame(),topic));
		//tplugins.add(new ComMentionsGraph(newPluginJFrame(),topic));
		//tplugins.add(new ComReplyGraph(newPluginJFrame(),topic));
		
		
	}
	
	private JFrame newPluginJFrame(){
		JFrame jframe = new JFrame();
		jframe.setSize(1280,720);  
		jframe.setIconImages(Rapid.getIcons());
		jframe.setLocationRelativeTo(null); 
		return jframe;
	}
	
	public HashMap<Integer, Vector<PluginContainer>> getContainers() {
		return containers;
	}
}
