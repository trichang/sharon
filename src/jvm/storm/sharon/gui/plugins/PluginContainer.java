package storm.sharon.gui.plugins;

import java.util.HashMap;
import java.util.Vector;


public class PluginContainer {

	String name;
	
	Vector<DataPanel> plugins;
	
	public PluginContainer(String name) {
		this.name = name;
		plugins = new Vector<DataPanel>();
	}
	
	public Vector<DataPanel> getPlugins() {
		return plugins;
	}

	public String getName() {
		return name;
	}

}
