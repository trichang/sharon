package storm.sharon.gui.plugins;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.bson.Document;

import storm.sharon.gui.communities.MentionGraphGenerator;
import storm.sharon.util.Topic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("serial")
public class ComMentionsGraph extends DataPanel implements ActionListener {
	private static Logger log = LogManager.getLogger(ComMentionsGraph.class.getName());
	protected	JButton		clearButton;
	protected	JButton		resetButton;
	MentionGraphGenerator mentionGraphGenerator;
	protected   JLabel      Prompter;
	
	
	public ComMentionsGraph() {
		ConstructPanel();
	}

	public ComMentionsGraph(JFrame jframe, Topic topic) {
		super(jframe, topic);
		ConstructPanel();
	}
	
	private void ConstructPanel(){
		
		jframe.setLayout(new BorderLayout());
		
		
		
		
		mentionGraphGenerator = new MentionGraphGenerator();
		mentionGraphGenerator.displayGraph(jframe);
		
		// Panel at bottom of display for topic list selector and buttons
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout( new BorderLayout() );
		jframe.add(dataPanel, BorderLayout.SOUTH );

	    // Buttons	   
		clearButton = new JButton( "Clear" );
		clearButton.addActionListener( this );
		resetButton = new JButton( "Reset" );
		resetButton.addActionListener( this );
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(0,2));
		buttonPanel.add(clearButton, BorderLayout.CENTER);
		buttonPanel.add(resetButton, BorderLayout.EAST);
		dataPanel.add( buttonPanel, BorderLayout.WEST);
		
		RegisterCommunityReader();
	}

	@Override
	public void ReceiveStreamDocument(Document d) {
		
		mentionGraphGenerator.addToCollection(d);
	}

	@Override
	public String LongName() {
		return "Community Mentions Graph";
	}

	@Override
	public String ShortName() {
		return "CMentionGraph";
	}

	@Override
	public String Tooltip() {
		return "Shows a graph of who mentions who a lot.";
	}

	@Override
	public void Clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
