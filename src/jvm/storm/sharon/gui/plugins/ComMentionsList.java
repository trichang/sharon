package storm.sharon.gui.plugins;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;

import org.bson.Document;

import storm.sharon.gui.core.SharonTreeNode;
import storm.sharon.gui.tweets.TreeViewUtil;
import storm.sharon.util.Topic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("serial")
public class ComMentionsList extends DataPanel implements ActionListener {
	private static Logger log = LogManager.getLogger(ComMentionsList.class.getName());
	private HashMap<Integer,DefaultTreeModel> treeModels;
	private HashMap<Integer,JTree> jTrees;
	private HashMap<Integer,SharonTreeNode> roots;
	protected HashMap<Integer,JScrollPane> scrollPanes;
	protected	JButton		clearButton;
	protected	JButton		resetButton;
	private static boolean KeepUpdating = true;
	protected   JButton     UpdateControlButton;
	protected   JLabel      Prompter;
	
	public ComMentionsList() {
		ConstructPanel();
	}

	public ComMentionsList(JFrame jframe, Topic topic) {
		super(jframe, topic);
		ConstructPanel();
	}
	
	private void ConstructPanel(){

		jframe.add(this);
		
		setLayout( new BorderLayout());

		// Panel at bottom of display for topic list selector and buttons
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout( new BorderLayout() );
		add(dataPanel, BorderLayout.SOUTH );

	    // Buttons	   
		clearButton = new JButton( "Clear" );
		clearButton.addActionListener( this );
		resetButton = new JButton( "Reset" );
		resetButton.addActionListener( this );
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(0,2));
		buttonPanel.add(clearButton, BorderLayout.CENTER);
		buttonPanel.add(resetButton, BorderLayout.EAST);
		dataPanel.add( buttonPanel, BorderLayout.WEST);
		
		
		UpdateControlButton = new JButton("Toggle Updating");
		UpdateControlButton.addActionListener(this);
		
		Prompter = new JLabel("Currently updating");
		dataPanel.add(UpdateControlButton, BorderLayout.EAST);
		dataPanel.add(Prompter, BorderLayout.CENTER);
		

		roots = new HashMap<Integer,SharonTreeNode>();
		scrollPanes = new HashMap<Integer,JScrollPane>();
		jTrees = new HashMap<Integer,JTree>();
		treeModels = new HashMap<Integer,DefaultTreeModel>();
		
		

		int topic_id = topic.getTopicId();
		String topicName = topic_id + ":" + topic.getTopicName() + " (0)";
		SharonTreeNode topicNode = new SharonTreeNode(topicName);

		DefaultTreeModel treeModel = new DefaultTreeModel(topicNode);
		JTree jTree = new JTree(treeModel);
		JScrollPane scrollPane = new JScrollPane(jTree);

		add(scrollPane, BorderLayout.CENTER);

		roots.put(topic_id,topicNode);
		scrollPanes.put(topic_id,scrollPane);
		treeModels.put(topic_id,treeModel);
		jTrees.put(topic_id,jTree);
			
			
		

		Prompter.setText("Mention Panel is updating.");
		RegisterCommunityReader();
	}

	@Override
	public void ReceiveStreamDocument(Document d) {
		
		int topic_id=topic.getTopicId();
	
		if(roots!=null ){
			SharonTreeNode root = roots.get(topic_id);
			JTree jTree = jTrees.get(topic_id);
			DefaultTreeModel treeModel = treeModels.get(topic_id);
			String username = d.getString("user_name");
			long total_mentions = d.getInteger("total_mentions");

			if (root.getAllLeafNodes().size() > TreeViewUtil.MAX_CHILD_NODES) {

				for (int i = 0; i < root.getChildCount(); i++) {
					if (!root.getChildAt(i).isLeaf()) {
						((SharonTreeNode) root.getChildAt(i)).removeOldestLeafNode();
					}
				}
			}
			


			if (total_mentions > 0) {

				// Try to limit the maximal children a node can have.

				/*
				 * if (topicNode.getChildCount() >
				 * TreeViewUtil.MAX_CHILD_NODES) {
				 * topicNode.remove(TreeViewUtil.MAX_CHILD_NODES); }
				 */

				List<Document> mentioning_list = (List<Document>) d.get("mentioning_list");
				SharonTreeNode userNode = new SharonTreeNode(username + " (" + mentioning_list.size() + ")");
				userNode.setReplyCount(mentioning_list.size());
				String TimeAndMentionCountNodeName = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss")
						.format(new java.util.Date()) + " Mentions: " + mentioning_list.size();
				SharonTreeNode TimeAndMentionCountNode = new SharonTreeNode(TimeAndMentionCountNodeName, d,
						mentioning_list.size());

				// Check whether this node's user already exists.
				// If found, insert it in. If not, build a new user
				// node.
				boolean found = false;

				if (root != null) {

					for (int i = 0; i < root.getChildCount(); i++) {

						SharonTreeNode tempChildNode = (SharonTreeNode) root.getChildAt(i);
						String userNodeName = userNode.toString().substring(0,
								userNode.toString().indexOf("(") - 1);
						String exsitingUserNodeName = tempChildNode.toString().substring(0,
								tempChildNode.toString().indexOf("(") - 1);

						if (exsitingUserNodeName.equals(userNodeName)) {
							found = true;

							/*
							 * // Try to limit the maximal children a
							 * node // can have. if
							 * (tempChildNode.getChildCount() >
							 * TreeViewUtil.MAX_CHILD_NODES) {
							 * tempChildNode.remove(TreeViewUtil.
							 * MAX_CHILD_NODES); }
							 */

							tempChildNode.insert(TimeAndMentionCountNode, 0);
							tempChildNode.setReplyCount(tempChildNode.getReplyCount() + mentioning_list.size());
							if (true) {
								tempChildNode.setUserObject(
										exsitingUserNodeName + " (" + tempChildNode.getReplyCount() + ")");
							}

							// Why?
							try {
								TreeViewUtil.ReloadSpecificNode(tempChildNode, jTree, treeModel, KeepUpdating,
										false);
							} catch (ArrayIndexOutOfBoundsException e) {

							}
							break;
						}
					}
				}
				if (found == false) {
				    root.insert(userNode, 0);
					userNode.insert(TimeAndMentionCountNode, 0);
					// Reload but in sorting reversely, so the last
					// parameter is true.
					TreeViewUtil.ReloadSpecificNode(root, jTree, treeModel, KeepUpdating, true);
				}

				root.setReplyCount(root.getReplyCount() + mentioning_list.size());
				String topicNodeName = root.toString();
				String updatedNodeName = topicNodeName.substring(0, topicNodeName.indexOf("(") - 1) + " ("
						+ root.getReplyCount() + ")";

				root.setUserObject(updatedNodeName);
				if (KeepUpdating) {
					treeModel.nodeChanged(root);
				}

			}
		}
	}

	@Override
	public String LongName() {
		return "Community Mentions List";
	}

	@Override
	public String ShortName() {
		return "CMentionsFolder";
	}

	@Override
	public String Tooltip() {
		return "View the highly mentioned users in a folder view";
	}

	@Override
	public void Clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int topic_id = topic.getTopicId();
		if (e.getSource() == UpdateControlButton) {

			if (KeepUpdating == false) {
				TreeViewUtil.ReloadButKeepTheExpansionState(jTrees.get(topic_id), treeModels.get(topic_id));
				Prompter.setText("Mention Panel is updating.");
			} else {
				Prompter.setText("Updating has stopped! Reclick to restart updating.");
			}

			KeepUpdating = !KeepUpdating;

		} 
		
	}

}
