package storm.sharon.gui.plugins;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.bson.Document;

import storm.sharon.gui.communities.ReplyGraphGenerator;
import storm.sharon.util.Topic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("serial")
public class ComReplyGraph extends DataPanel implements ActionListener {
	private static Logger log = LogManager.getLogger(ComMentionsGraph.class.getName());
	protected	JButton		clearButton;
	protected	JButton		resetButton;
	ReplyGraphGenerator replyGraphGenerator;
	protected   JLabel      Prompter;
	
	
	public ComReplyGraph() {
		ConstructPanel();
	}

	public ComReplyGraph(JFrame jframe, Topic topic) {
		super(jframe, topic);
		ConstructPanel();
	}
	
	private void ConstructPanel(){
		
		jframe.setLayout(new BorderLayout());
		
		
		
		
		replyGraphGenerator = new ReplyGraphGenerator();
		replyGraphGenerator.displayGraph(jframe);
		
		// Panel at bottom of display for topic list selector and buttons
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout( new BorderLayout() );
		jframe.add(dataPanel, BorderLayout.SOUTH );

	    // Buttons	   
		clearButton = new JButton( "Clear" );
		clearButton.addActionListener( this );
		resetButton = new JButton( "Reset" );
		resetButton.addActionListener( this );
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(0,2));
		buttonPanel.add(clearButton, BorderLayout.CENTER);
		buttonPanel.add(resetButton, BorderLayout.EAST);
		dataPanel.add( buttonPanel, BorderLayout.WEST);
		
		RegisterCommunityReader();
	}

	@Override
	public void ReceiveStreamDocument(Document d) {
		
		replyGraphGenerator.addToCollection(d);
	}

	@Override
	public String LongName() {
		return "Community Replies Graph";
	}

	@Override
	public String ShortName() {
		return "CReplyGraph";
	}

	@Override
	public String Tooltip() {
		return "Shows a graph of who replies to who a lot.";
	}

	@Override
	public void Clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
