package storm.sharon.gui.plugins;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;

import storm.sharon.Rapid;
import storm.sharon.gui.core.DocumentStream;
import storm.sharon.gui.core.RapidAPI;

import storm.sharon.gui.core.Stream;

import storm.sharon.gui.core.RapidLog;

import storm.sharon.gui.core.Subscriptions;
import storm.sharon.gui.core.TopicStream;
import storm.sharon.util.MessageQueueInterface;
import storm.sharon.util.Topic;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;
import org.bson.Document;

/*
 * The DataPanel is the base class for all plugins that display data.
 */
@SuppressWarnings("serial")
public abstract class DataPanel extends JPanel implements  DocumentStream {
	private static Logger log=LogManager.getLogger(DataPanel.class.getName());;
	protected JFrame jframe;
	protected Topic topic;
	protected String longName;
	protected String shortName;
	protected String toolTip;
	
	private Boolean subscribed=false;
	private Boolean logging=false;
	
	private Boolean registeredForTweets = false;
	private Boolean registeredForDiscussions = false;
	private Boolean registeredForCommunities = false;
	private Boolean registeredForTopics = false;
	private Boolean registeredForTuples = false;
	

	private HashMap<String,Stream> streams;

	private RapidLog rlog;

	
	public DataPanel(){
		streams = new HashMap<String,Stream>();
		Standalone();
	}
	
	public DataPanel(JFrame jframe,Topic topic){
		streams = new HashMap<String,Stream>();
		this.jframe = jframe;
		this.topic = topic;
		this.jframe.setVisible(false);
		this.jframe.setTitle(LongName()+": "+topic.getTopicName());
		this.jframe.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
	}
	
	public DataPanel(JFrame jframe,Topic topic, String longName, String shortName, String toolTip){
		streams = new HashMap<String,Stream>();
		this.longName = longName;
		this.shortName = shortName;
		this.toolTip = toolTip;
		this.jframe = jframe;
		this.topic = topic;
		this.jframe.setVisible(false);
		this.jframe.setTitle(LongName()+": "+topic.getTopicName());
		this.jframe.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
	}
	
	public String toString(){
		return LongName();
	}
	
	
	public void Open(){
		Subscribe();
		jframe.setVisible(true);
	}
	
	public void Destroy(){
		Unsubscribe();
		jframe.setVisible(false);
		jframe.dispose();
	}
	
	private void Standalone(){
		
		Rapid.ConfigureLogging();
		
		log = LogManager.getLogger(DataPanel.class.getName());
		
		log.info("Starting "+LongName()+" in standalone mode.");
		
		log.fatal("Standalone is not supported in this verions yet, please contact Aaron.");
		System.exit(-1);
		
		log.debug("Authenticated with TestUser");
		jframe = new JFrame();
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.setSize(1024,576);  
		jframe.setVisible(true);
		ArrayList<Topic> topicList = RapidAPI.getUserTopicList();
		
		for(Topic t: topicList){
			if(t.getTopicOwner().equals(RapidAPI.getAuthenticationData().getRapidUserName())){
				topic=t;
				log.info("Setting topic "+topic.getTopicName()+" for user "+RapidAPI.getAuthenticationData().getRapidUserName());
				break;
			}
		}
		
		if(topic==null){
			log.fatal("There is no topic in the database for \"TestUser\". Use Rapid GUI to add a topic for \"TestUser\" and try again.");
			System.exit(-1);
		}
		
	}
	
	private final void SubscribeQueue(String queue){
		streams.put(queue,new TopicStream(this,queue,topic.getTopicId()));
		streams.get(queue).subscribe();
		
	}
	
	private final void UnsubscribeQueue(String queue){
		streams.get(queue).unsubscribe();
		streams.remove(queue);
	}
	
	/*
	 * Call this method when creating the panel for the first time and you want 
	 * to set the existing subscription to true or false based on what is in the database.
	 */
	public final void SubscribeIfPossible(){
		if(subscribed) return;
		String u = RapidAPI.getAuthenticationData().getRapidUserName();
		if(registeredForTweets && !RapidAPI.getTopicKeywordHandler().hasSubscribed(u,topic.getTopicId(),MessageQueueInterface.TWEET_MESSAGE_QUEUE)) return;
		if(registeredForDiscussions && !RapidAPI.getTopicKeywordHandler().hasSubscribed(u,topic.getTopicId(),MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE)) return;
		if(registeredForCommunities && !RapidAPI.getTopicKeywordHandler().hasSubscribed(u,topic.getTopicId(),MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE)) return;
		if(registeredForTopics && !RapidAPI.getTopicKeywordHandler().hasSubscribed(u,topic.getTopicId(),MessageQueueInterface.KEYWORD_TOPIC_MESSAGE_QUEUE)) return;
		if(registeredForTuples && !RapidAPI.getTopicKeywordHandler().hasSubscribed(u,topic.getTopicId(),MessageQueueInterface.TUPLE_MESSAGE_QUEUE)) return;
		Subscribe();
	}
	
	/*
	 * Call this method when you want to subscribe to the relevant queues for the panel.
	 */
	public final void Subscribe(){
		if(subscribed) return;
		if(registeredForTweets) SubscribeQueue(MessageQueueInterface.TWEET_MESSAGE_QUEUE);
		if(registeredForDiscussions) SubscribeQueue(MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE);
		if(registeredForCommunities) SubscribeQueue(MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE);
		if(registeredForTopics) SubscribeQueue(MessageQueueInterface.KEYWORD_TOPIC_MESSAGE_QUEUE);
		if(registeredForTuples) SubscribeQueue(MessageQueueInterface.TUPLE_MESSAGE_QUEUE);
		subscribed = true;
	}
	
	public final void Unsubscribe(){
		if(!subscribed) return;
		if(registeredForTweets) UnsubscribeQueue(MessageQueueInterface.TWEET_MESSAGE_QUEUE);
		if(registeredForDiscussions) UnsubscribeQueue(MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE);
		if(registeredForCommunities) UnsubscribeQueue(MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE);
		if(registeredForTopics) UnsubscribeQueue(MessageQueueInterface.KEYWORD_TOPIC_MESSAGE_QUEUE);
		if(registeredForTuples) UnsubscribeQueue(MessageQueueInterface.TUPLE_MESSAGE_QUEUE);
		subscribed = false;
	}
	
	/*
	 * Start logging information
	 */
	public final void StartLogging(RapidLog rlog){
		this.rlog = rlog;
		logging=true;
	}
	
	/*
	 * Stop logging information
	 */
	public final void StopLogging(){
		logging=false;
		if(rlog!=null)
			rlog.close();
		rlog=null;
	}
	
	public final Boolean isSubscribed() {
		return subscribed;
	}

	public final void setSubscribed(Boolean subscribed) {
		this.subscribed = subscribed;
	}

	public final Boolean isLogging() {
		return logging;
	}

	public void setLogging(Boolean logging) {
		this.logging = logging;
	}

	
	
	public Topic getTopic() {
		return topic;
	}

	/*
	 * Stream registrations, register with whatever streams are required.
	 */
	public final void RegisterKeywordTopicReader() {
		registeredForTopics = true;
	}
	
	public final void RegisterTweetReader() {
		registeredForTweets = true;
	}
	
	public final void RegisterCommunityReader() {
		registeredForCommunities = true;
	}

	public final void RegisterTupleReader() {
		registeredForTuples = true;
	}
	
	public final void RegisterDiscussionReader() {
		registeredForDiscussions = true;
	}
	
	/*
	 * Receive stream documents.
	 */
	public abstract void ReceiveStreamDocument(Document d);
	
	/*
	 * Return a long name for the plugin, suitable for display in the title frame.
	 * E.g. "Geographic Tweet Locations"
	 */
	public abstract String LongName();
	
	/*
	 * Return a short name for the plugin, suitable for display in a button.
	 * E.g. "Geo"
	 * Do not put spaces or punctuation in the short name.
	 * The short name may also be used for filenames.
	 */
	public abstract String ShortName();
	
	/*
	 * Return a short informative sentence describing the plugin.
	 * E.g. "Displays tweets on a map." 
	 */
	public abstract String Tooltip();

	/*
	 * Clear all accumulated data.
	 */
	public abstract void Clear();
	
	public String[] getQuery_id(){
		return null;
	}

	public RapidLog getRlog() {
		return rlog;
	}
	
	
	
}
