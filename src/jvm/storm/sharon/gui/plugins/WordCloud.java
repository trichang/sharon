package storm.sharon.gui.plugins;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.mcavallo.opencloud.Cloud;
import org.mcavallo.opencloud.Tag;

import storm.sharon.util.Stemmer;
import storm.sharon.util.StopWords;
import storm.sharon.util.Topic;

@SuppressWarnings("serial")
public class WordCloud extends DataPanel implements ActionListener {

	private static Logger log;
	
	private ArrayList<Cloud> clouds;
	private ArrayList<String> allTokens = new ArrayList<String>();
	private CharSequence cs = "http";
	private	JButton		refreshButton;
	
	private boolean refresh = false;
	private long logcounter = 0;
	// standalone constructor
	public WordCloud(){
		super();
		ConstructPanel(); 
	}
	
	// plugin constructor
	public WordCloud(JFrame jframe,Topic topic){
		super(jframe,topic); 
		ConstructPanel();
	}
	
	private void ConstructPanel(){
		log = LogManager.getLogger(WordCloud.class.getName());
		// add our panel to the supplied jframe 
		jframe.add(this);
		
		// we want to add a button to our panel
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout( new BorderLayout() );
		jframe.add(dataPanel, BorderLayout.SOUTH );
		refreshButton = new JButton("Refresh Cloud");
		dataPanel.add(refreshButton,BorderLayout.EAST);
		refreshButton.addActionListener( this );
		refreshButton.setToolTipText("Force an update to the word cloud.");
		
		logo();
		
		displayData(false);
		
		// finally register to receive the tweets
		RegisterTweetReader();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void ReceiveStreamDocument(Document d) {
		ArrayList<String> tokens = (ArrayList<String>)d.get("sharon_tokens");
		// Limit allTokens count in 10k
		if (allTokens.size() >= 10000) {
			
			allTokens = new ArrayList<String>(allTokens.subList(1000, 10000));
		}
		if(tokens!=null){
			for (int i = 0; i < tokens.size(); i++) {
				if (((String) tokens.get(i)).contains(cs) != true) {
					allTokens.add(((String) tokens.get(i)).replaceAll("[^a-zA-Z ]", ""));
				}
			}
		}
		boolean show = (allTokens.size()  > 100) | refresh;
		displayData(show);
		
	}
	
	private void displayData(final boolean show){
		if(isLogging()){
			logcounter++;
			if(logcounter>100){
				getRlog().logPanel(this);
				logcounter=0;
			}
		}
		SwingUtilities.invokeLater(new Runnable() {
		    
			@SuppressWarnings("rawtypes")
			public void run() {
		
				clouds = new ArrayList<Cloud>();
				clouds.add(new Cloud());
				Random rd = new Random();
				int tagCount = 0;
				
				removeAll();
				
				if (show == false){
					logo();
				} else{
					refresh=true;
					allTokens = StopWords.delete(allTokens);
					HashMap<String, Integer> tokenMap = Stemmer.stemming(allTokens);
					
					//Option 1: Create a arraylist for clouds
					Iterator iter = tokenMap.entrySet().iterator();
					while(iter.hasNext()){
						Map.Entry entry = (Map.Entry)iter.next();
						String word = (String)entry.getKey();
						int freq = (Integer)entry.getValue();
						for(int j = 0; j < freq; j++){
							if (clouds.get(clouds.size() - 1).size() >= 150){
								clouds.add(new Cloud());
							}
							clouds.get(clouds.size() - 1).addTag(word);
						}
					}
					
					// The following will limit the memory consumption
					if(clouds.size() >= 10){
						clouds = new ArrayList<Cloud>(clouds.subList(2, 9));
					}
					
					for(int i = 0; i < clouds.size(); i++){
						tagCount += clouds.get(i).size();
						for(Tag tag: clouds.get(i).tags()){
							JLabel label = new JLabel(tag.getName());
							label.setOpaque(false);
							label.setFont(label.getFont().deriveFont((float)Math.pow(tag.getWeight() * 1000,  1.0/2)));
							
							float r = rd.nextFloat() / 2f;
							float g = rd.nextFloat() / 2f;
							float b = (float) (rd.nextFloat() / 2f + 0.5);
							Color randomColor = new Color(r, g, b);
							label.setForeground(randomColor);
							add(label);
						}
					}
					
				
				}
				revalidate();
				repaint();
				
		    }
		});
	
	}
	
	private void logo(){
		
		try {
			removeAll();
			URL url;
			url = new URL("http://www.unimelb.edu.au/alumni/alumni_in_the_news/files/unimelb_logo_100.jpg");
			BufferedImage image = ImageIO.read(url);
			if(image!=null){
				Image newImage = image.getScaledInstance(75, 75,
						java.awt.Image.SCALE_SMOOTH);
				ImageIcon newIcon = new ImageIcon(newImage);
				JLabel imageLabel = new JLabel(newIcon);
				imageLabel.setOpaque(true);
				add(imageLabel);
			}
			JLabel label = new JLabel("Waiting for at least a few tokens to generate word cloud...");
			add(label);
		} catch (MalformedURLException e) {
			log.error(e);
		} catch (IOException e){
			log.error(e);
		}
		revalidate();
		repaint();
	}

	@Override
	public String LongName() {
		return "Tweet Word Cloud";
	}

	@Override
	public String ShortName() {
		return "WordCloud";
	}

	@Override
	public String Tooltip() {
		return "Shows a Word Cloud for the tweet stream.";
	}

	@Override
	public void Clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == refreshButton){
			refresh=true;
			displayData(true);
		}
		
	}
	
	/*
	 * For testing purposes, each plugin can be an application on its own.
	 */
	public static void main(String[] args) {
		new WordCloud();
	}

}
