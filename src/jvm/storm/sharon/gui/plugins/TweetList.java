package storm.sharon.gui.plugins;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;

import storm.sharon.gui.core.SharonTreeNode;
import storm.sharon.gui.tweets.TreeViewUtil;
import storm.sharon.util.Topic;

@SuppressWarnings("serial")
public class TweetList extends DataPanel implements ActionListener {
	private static Logger log = LogManager.getLogger(TweetList.class.getName());
	private HashMap<Integer, DefaultTreeModel> treeModels;
	private HashMap<Integer, JTree> jTrees;

	private HashMap<Integer,SharonTreeNode> roots;
	
	// private Vector<SharonTreeNode> topicNodes;
	private HashMap<Integer,SharonTreeNode> currentDateNodes;
	private HashMap<Integer,SharonTreeNode> currentHourNodes;
	private HashMap<Integer,SharonTreeNode> currentMinsNodes;

	protected HashMap<Integer,JScrollPane> scrollPanes;

	private static boolean KeepUpdating = true;	
	protected	JButton		toggleButton;
	private   JLabel Prompter;
	private JPanel toolbar;
	
	public TweetList(){
		super();
		
	
		ConstructPanel();
	}
	
	public TweetList(JFrame jframe,Topic topic){
		super(jframe,topic);
		ConstructPanel();
	}
	
	private void ConstructPanel(){
		
		jframe.add(this);
		
		setLayout(new BorderLayout());
		
		toolbar = new JPanel();
		toolbar.setLayout(new BorderLayout());
		
		add(toolbar,BorderLayout.SOUTH);
		
		toggleButton = new JButton("Toggle Updating" );
		toolbar.add( toggleButton, BorderLayout.EAST);
		toggleButton.addActionListener( this );
		

		
		// Initialize backbone nodes
		roots = new HashMap<Integer,SharonTreeNode>();
	
		
		// topicNodes = new Vector<SharonTreeNode>();
		currentDateNodes = new HashMap<Integer,SharonTreeNode>();
		currentHourNodes = new HashMap<Integer,SharonTreeNode>();
		currentMinsNodes = new HashMap<Integer,SharonTreeNode>();
		scrollPanes = new HashMap<Integer,JScrollPane>();

		jTrees = new HashMap<Integer,JTree>();
		treeModels = new HashMap<Integer,DefaultTreeModel>();

		int topic_id = topic.getTopicId();
		String topicName = topic_id + ":" + topic.getTopicName();
		SharonTreeNode topicNode = new SharonTreeNode(topicName);

		DefaultTreeModel treeModel = new DefaultTreeModel(topicNode);
		JTree jTree = new JTree(treeModel);
		//jTree.setRootVisible(false);
		jTree.addMouseListener(TreeViewUtil.Add_And_Return_MouseAdapter());

		JScrollPane scrollPane = new JScrollPane(jTree);

	
		add(scrollPane, BorderLayout.CENTER);
		

		roots.put(topic_id,topicNode);
		
		currentDateNodes.put(topic_id,new SharonTreeNode(""));
		currentHourNodes.put(topic_id,new SharonTreeNode(""));
		currentMinsNodes.put(topic_id,new SharonTreeNode(""));

		scrollPanes.put(topic_id,scrollPane);
		treeModels.put(topic_id,treeModel);
		jTrees.put(topic_id,jTree);
	

		Prompter = new JLabel("");
		toolbar.add( Prompter, BorderLayout.CENTER);
		Prompter.setText("Tweet stream is being received. Click Toggle Updating to pause.");

		
		RegisterTweetReader();
	}
	
	@Override
	public void ReceiveStreamDocument(Document d) {
		if (d.getInteger("sharon_topic_id") != null && roots != null) {
			final Document doc = new Document(d);
			if(isLogging()){
				getRlog().logString(TreeViewUtil.DeleteSharonValue(d).toJson());
			}
			SwingUtilities.invokeLater(new Runnable() {
			    
				public void run() {
					int topic_id = doc.getInteger("sharon_topic_id");
		
					SharonTreeNode root = roots.get(topic_id);
					JTree jTree = jTrees.get(topic_id);
					DefaultTreeModel treeModel = treeModels.get(topic_id);
		
					String dateStr = TreeViewUtil.getDate(doc);
					SharonTreeNode newDateNode = new SharonTreeNode(dateStr);
		
					String hour = TreeViewUtil.getHour(doc);
					String hourStr = hour + ":00";
					SharonTreeNode newHourNode = new SharonTreeNode(hourStr);
		
					String minute = TreeViewUtil.getMins(doc);
					String minuteStr = hour + ":" + minute;
					SharonTreeNode newMinsNode = new SharonTreeNode(minuteStr);
		
					// This is the leaf node, which possesses the document.
					String textStr = TreeViewUtil.getText(doc);
					SharonTreeNode newTextNode = new SharonTreeNode(textStr, doc);
		
					if (root != null) {
						if (root.getAllLeafNodes().size() > TreeViewUtil.MAX_CHILD_NODES) {
		
							for (int i = 0; i < root.getChildCount(); i++) {
								if (!root.getChildAt(i).isLeaf()) {
									((SharonTreeNode) root.getChildAt(i)).removeOldestLeafNode();
								}
							}
						}
					}
		
					if (currentDateNodes.get(topic_id).toString().equals("")) {
						// If currently there is no date node, the node just
						// received is the first node.
						TreeViewUtil.InsertManyNodes(newTextNode, newMinsNode, newHourNode, newDateNode, root);
		
						// Initialize the current date node, hour node and
						// minute node.
						currentDateNodes.put(topic_id,newDateNode);
						currentHourNodes.put(topic_id,newHourNode);
						currentMinsNodes.put(topic_id,newMinsNode);
		
					} else {
						// If this newly received node is not the first node, we
						// need to judge whether it is
						// an out-of-order node (its date is earlier than
						// current date or its hour is earlier
						// than current hour, or its minute is earlier than
						// current minute) If so, there are
						// methods to deal with these nodes.
		
						// This node's date is not the same as current date.
						if (!(newDateNode.toString()).equals(currentDateNodes.get(topic_id).toString())) {
		
							// This node's date is not earlier than current
							// date.
							if (newDateNode.toString().compareTo(currentDateNodes.get(topic_id).toString()) > 0) {
		
								// Build a new date node and insert it into the
								// topic node.
								TreeViewUtil.InsertManyNodes(newTextNode, newMinsNode, newHourNode, newDateNode, root);
								// Maybe it is not necessary to reload this node
								// to keep the sequence,
								// but in case of unknown bugs.
								TreeViewUtil.ReloadSpecificNode(root, jTree, treeModel, KeepUpdating, false);
		
								currentDateNodes.put(topic_id,newDateNode);
								currentHourNodes.put(topic_id,newHourNode);
								currentMinsNodes.put(topic_id,newMinsNode);
		
							} else {
								// This node's date is out-of-order.
								// Build a new date node and insert it into the
								// topic node.
								// Reload it (the reload method includes a sort
								// method).
								TreeViewUtil.DealWithOutOfOrders(root, newDateNode, newHourNode, newMinsNode,newTextNode, KeepUpdating);
								TreeViewUtil.ReloadSpecificNode(root, jTree, treeModel, KeepUpdating, false);
							}
		
						} else if (!newHourNode.toString().equals(currentHourNodes.get(topic_id).toString())) {
		
							// This block deals with sequential or out-of-order
							// hour nodes.
							if (newHourNode.toString().compareTo(currentHourNodes.get(topic_id).toString()) > 0) {
		
								TreeViewUtil.InsertManyNodes(newTextNode, newMinsNode, newHourNode,currentDateNodes.get(topic_id));
								TreeViewUtil.ReloadSpecificNode(currentDateNodes.get(topic_id), jTree, treeModel,KeepUpdating, false);
		
								currentHourNodes.put(topic_id,newHourNode);
								currentMinsNodes.put(topic_id,newMinsNode);
		
							} else { // They are out-of-order hour nodes.
		
								TreeViewUtil.DealWithOutOfOrders(currentDateNodes.get(topic_id), newHourNode,newMinsNode, newTextNode, KeepUpdating);
								TreeViewUtil.ReloadSpecificNode(currentDateNodes.get(topic_id), jTree, treeModel,KeepUpdating, false);
							}
		
						} else if (!newMinsNode.toString().equals(currentMinsNodes.get(topic_id).toString())) {
		
							// This block deals with sequential or out-of-order
							// minute nodes.
							if (newMinsNode.toString().compareTo(currentMinsNodes.get(topic_id).toString()) > 0) {
		
								TreeViewUtil.InsertManyNodes(newTextNode, newMinsNode, currentHourNodes.get(topic_id));
								TreeViewUtil.ReloadSpecificNode(currentHourNodes.get(topic_id), jTree, treeModel,KeepUpdating, false);
		
								currentMinsNodes.put(topic_id,newMinsNode);
		
							} else { // They are out-of-order minute nodes.
		
								TreeViewUtil.DealWithOutOfOrders(currentHourNodes.get(topic_id), newMinsNode,newTextNode, KeepUpdating);
								TreeViewUtil.ReloadSpecificNode(currentHourNodes.get(topic_id), jTree, treeModel,KeepUpdating, false);
							}
		
						} else {
		
							// This text node has the current date, current
							// hour, and current minute.
							// So just insert it into the current minute node.
							currentMinsNodes.get(topic_id).insert(newTextNode, 0);
							TreeViewUtil.ReloadSpecificNode(currentMinsNodes.get(topic_id), jTree, treeModel,KeepUpdating, false);
		
						}
					}
					
					TreeViewUtil.ReloadSpecificNode(root, jTree, treeModel,KeepUpdating, false);
				}
			});

		}

	}

	@Override
	public String LongName() {
		return "Tweet List";
	}

	@Override
	public String ShortName() {
		return "TweetList";
	}

	@Override
	public String Tooltip() {
		return "Tweets are displayed in a folder view.";
	}

	@Override
	public void Clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int current_topic_id = topic.getTopicId();
		if (e.getSource() == toggleButton) {
			if (KeepUpdating == false) {
				TreeViewUtil.ReloadButKeepTheExpansionState(jTrees.get(current_topic_id),treeModels.get(current_topic_id));
				Prompter.setText("Tweet stream is being received. Click Toggle Updating to pause.");
			} else {
				Prompter.setText("Tweet stream is paused. Click Toggle Updating to resume.");
			}

			KeepUpdating = !KeepUpdating;
		} 
	}

}
