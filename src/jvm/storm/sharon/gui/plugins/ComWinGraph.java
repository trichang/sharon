package storm.sharon.gui.plugins;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.bson.Document;
import org.graphstream.ui.swingViewer.View;

import storm.sharon.gui.communities.MentionGraphGenerator;
import storm.sharon.gui.communities.TupleGraphGenerator;
import storm.sharon.gui.core.RapidLog;
import storm.sharon.util.Topic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("serial")
public class ComWinGraph extends DataPanel implements ActionListener {
	private static Logger log = LogManager.getLogger(ComWinGraph.class.getName());
	protected	JButton		clearButton;
	protected	JButton		resetButton;
	protected TupleGraphGenerator graphGenerator;
	protected   JLabel      Prompter;
	private String[] query_id = null;;
	private int logcounter=0;
	
	public ComWinGraph() {
		ConstructPanel();
	}

	public ComWinGraph(JFrame jframe, Topic topic, String longName, String shortName, String toolTip, String[] query_id) {
		super(jframe, topic, longName, shortName, toolTip);
		ConstructPanel();
		this.query_id = query_id;
	}
	
	protected void ConstructPanel(){
		
		jframe.setLayout(new BorderLayout());
	
		graphGenerator = new TupleGraphGenerator();
		graphGenerator.displayGraph(jframe);
		
		// Panel at bottom of display for topic list selector and buttons
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout( new BorderLayout() );
		jframe.add(dataPanel, BorderLayout.SOUTH );

	    
		RegisterTupleReader();
	}

	@Override
	public void ReceiveStreamDocument(Document d) {
		String q = d.getString("query_id");
		for(String query:query_id){
			if(query.equals(q)){
				final Document doc = new Document(d);
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						graphGenerator.addToCollection(doc);
					}
				});
				if(isLogging()){
					logcounter++;
					if(logcounter>500){
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								View v = graphGenerator.getView();
								
								getRlog().logPanel(v);
								
							}
						});
						logcounter=0;
					}
				}
				break;
			}
		}
	}


	@Override
	public String LongName() {
		return longName;
	}

	@Override
	public String ShortName() {
		return shortName;
	}

	@Override
	public String Tooltip() {
		return toolTip;
	}

	@Override
	public void Clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String[] getQuery_id() {
		return query_id;
	}

	
}
