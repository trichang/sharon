package storm.sharon.gui.plugins;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;

import org.bson.Document;

import storm.sharon.gui.core.SharonTreeNode;
import storm.sharon.gui.tweets.TreeViewUtil;
import storm.sharon.util.Topic;

public class Discussions extends DataPanel implements ActionListener {

	private static boolean KeepUpdating = true;	
	private	JButton		toggleButton;
	private   JLabel Prompter;
	private JPanel toolbar;
	
	private JScrollPane scrollPane;
	private JTree jTree;
	
	private DefaultTreeModel treeModel;
	private HashMap<String,SharonTreeNode> discussionMap;
	
	SharonTreeNode root;
	
	public Discussions() {
		ConstructPanel();
	}

	public Discussions(JFrame jframe, Topic topic) {
		super(jframe,topic);
		ConstructPanel();
	}

	private void ConstructPanel(){
		jframe.add(this);
		
		setLayout(new BorderLayout());
		
		toolbar = new JPanel();
		toolbar.setLayout(new BorderLayout());
		
		add(toolbar,BorderLayout.SOUTH);
		
		toggleButton = new JButton("Toggle Updating" );
		toolbar.add( toggleButton, BorderLayout.EAST);
		toggleButton.addActionListener( this );
		
		root = new SharonTreeNode("Discussions");
		treeModel = new DefaultTreeModel(root);
		discussionMap = new HashMap<String,SharonTreeNode>();
		jTree = new JTree(treeModel);
		jTree.addMouseListener(TreeViewUtil.Add_And_Return_MouseAdapter());
		scrollPane = new JScrollPane(jTree);
		
		add(scrollPane, BorderLayout.CENTER);
		
		Prompter = new JLabel("");
		toolbar.add( Prompter, BorderLayout.CENTER);
		Prompter.setText("Discussion stream is being received. Click Toggle Updating to pause.");

		RegisterDiscussionReader();
	}
	
	@Override
	public void ReceiveStreamDocument(Document d) {
		if(d.get("message_type").equals("FETCH_TWEET"))return;
		String discussion_name = (String) d.get("discussion_name");
		ArrayList<String> children = (ArrayList<String>) d.get("children");
		String in_reply_to_status_id_str = (String) d.get("in_reply_to_status_id_str");
		String id_str = (String) d.get("id_str");
		if(isLogging()){
			getRlog().logString(((Document)d.get("data")).toJson());
		}
		if(!discussionMap.containsKey(discussion_name)){
			// this is the first time we've seen this discussion
			SharonTreeNode discussionNode;
			if(discussionMap.containsKey(id_str)){
				// we already know about the node, so update it
				discussionNode = discussionMap.get(id_str);
				discussionNode.setUserObject(DataString(d));
			} else {
				// create a discussion root 
				discussionNode = new SharonTreeNode(DataString(d),(Document)d.get("data"));
				discussionMap.put(id_str, discussionNode);
			}
			root.insert(discussionNode,0);
			discussionMap.put(discussion_name, discussionNode);
			ReloadTree(root);
		} else {
			// the discussion already exists
			
			// first get&update or create the discussion node
			SharonTreeNode discussionNode;
			if(discussionMap.containsKey(id_str)){
				// we already know about the node, so update it
				discussionNode = discussionMap.get(id_str);
				discussionNode.setUserObject(DataString(d));
				// Since the update will never change the in_reply_to_status_id_str
				// we have nothing more to do.
				ReloadTree(discussionNode);
				return;
			} else {
				// create a new discussion node 
				discussionNode = new SharonTreeNode(DataString(d),(Document)d.get("data"));
				discussionMap.put(id_str, discussionNode);
			}
			
			// now get the discussion root
			SharonTreeNode discussionRoot = discussionMap.get(discussion_name);
			
			if(in_reply_to_status_id_str==null){
				// this node is (becomes) the root of this discussion
				if(id_str.equals(discussionRoot.getDocument().getString("id_str"))){
					// we are just updating the root's data so nothing more to do
					ReloadTree(discussionRoot);
				} else {
					// we have a new node that is becoming the root
					discussionNode.add(discussionRoot);
					root.insert(discussionNode, 0);
					discussionMap.put(discussion_name, discussionNode);
					ReloadTree(root);
				}
			} else {
				// this node is just some other node in the tree
				SharonTreeNode replyToNode; 
				if(discussionMap.containsKey(in_reply_to_status_id_str)){
					// the node to attach to is already present
					replyToNode = discussionMap.get(in_reply_to_status_id_str);
					replyToNode.add(discussionNode);
					ReloadTree(replyToNode);
				} else {
					// the node to attach to is not present, so this forms a new root
					discussionNode.add(discussionRoot);
					root.insert(discussionNode, 0);
					discussionMap.put(discussion_name, discussionNode);
					ReloadTree(root);
				}
			}
		}
		
	}
	
	public void ReloadTree(SharonTreeNode node){
		TreeViewUtil.ReloadSpecificNode(node,jTree,treeModel,KeepUpdating,false);
	}
	
	public static String DataString(Document d){
		Document data = (Document) d.get("data");
		String discussion_name = d.getString("discussion_name");
		String id_str =data.getString("id_str");
		String text = data.getString("text");
		String timestamp_ms = "[UNKNOWN TIME]";
		String in_reply_to_status_id_str = "[]";
		String screen_name = "[UNKNOWN SCREEN NAME]";
		String in_reply_to_screen_name = "[UNKNOWN IN REPLY TO SCREEN NAME]";
		
		timestamp_ms = data.getString("timestamp_ms");
		if(timestamp_ms!=null){
			String sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new java.util.Date(Long.parseLong(timestamp_ms)));
			timestamp_ms = sdf;
		}
		in_reply_to_status_id_str = data.getString("in_reply_to_status_id_str");
		screen_name = data.getString("screen_name");
		in_reply_to_screen_name = data.getString("in_reply_to_screen_name");
		
		return discussion_name+" "+timestamp_ms+" "+id_str+" "+screen_name+" in reply to "+in_reply_to_screen_name+" ("+in_reply_to_status_id_str+"): "+text;
	}

	@Override
	public String LongName() {
		return "Discussion Trees";
	}

	@Override
	public String ShortName() {
		return "Discussions";
	}

	@Override
	public String Tooltip() {
		return "Shows discussion trees.";
	}

	@Override
	public void Clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == toggleButton) {
			if (KeepUpdating == false) {
				TreeViewUtil.ReloadButKeepTheExpansionState(jTree,treeModel);
				Prompter.setText("Discussion stream is being received. Click Toggle Updating to pause.");
			} else {
				Prompter.setText("Discussion stream is paused. Click Toggle Updating to resume.");
			}

			KeepUpdating = !KeepUpdating;
		} 
	}

}
