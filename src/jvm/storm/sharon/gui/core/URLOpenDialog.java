package storm.sharon.gui.core;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.text.DefaultEditorKit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import storm.sharon.Rapid;

public class URLOpenDialog extends JDialog implements ActionListener {
	private static Logger log = LogManager.getLogger(URLOpenDialog.class.getName());
	private JButton okButton;
	private JTextField textField;
	private JLabel textLabel;
	private ClusterDataAuthenticationPanel callingPanel;
    public URLOpenDialog (ClusterDataAuthenticationPanel jp, String title, boolean b, String text) {
    	super((JFrame) null, title, b);
    	callingPanel = jp;
    	
    	setIconImages(Rapid.getIcons());
    	setSize(600,600);
    	
    	textField = new JTextField(text);
		
    	/*
    	 * popup menu
    	 */
    	JPopupMenu popup = new JPopupMenu();
	    JMenuItem item = new JMenuItem(new DefaultEditorKit.CutAction());
	    item.setText("Cut");
	    popup.add(item);
	    item = new JMenuItem(new DefaultEditorKit.CopyAction());
	    item.setText("Copy");
	    popup.add(item);
	    item = new JMenuItem(new DefaultEditorKit.PasteAction());
	    item.setText("Paste");
	    popup.add(item);
	    
	    /*
	     * text field settings
	     */
	    textField.setComponentPopupMenu(popup);
	    textField.setEditable(false);
	    textField.setBorder(null);
	    textField.setBackground(Color.WHITE);
	    textField.addActionListener(this);
	    textField.setFont(new Font("Sans Serif", Font.PLAIN, 20));
	    textField.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
	    /*
	     * ok button
	     */
    	okButton = new JButton("OK");
        okButton.addActionListener(this);
        
        /*
         * help text
         */
        textLabel = new JLabel("We could not automatically open your browser. Please go to the following URL in your browser and authorize RAPID:");
        
        setLayout(new GridLayout(0,1));
        JPanel bp = new JPanel();
        bp.setLayout(new BoxLayout(bp,BoxLayout.PAGE_AXIS));
       
        bp.add(Box.createRigidArea(new Dimension(20,20)));
        
        JPanel jp1 = new JPanel(new BorderLayout());
        jp1.add(textLabel,BorderLayout.WEST);
        jp1.setAlignmentY(LEFT_ALIGNMENT);
        bp.add(jp1);
        
        bp.add(Box.createRigidArea(new Dimension(20,20)));
        
        JPanel jp4 = new JPanel(new BorderLayout());
        jp4.add(Box.createRigidArea(new Dimension(10,10)),BorderLayout.WEST);
        JPanel tp = new JPanel();
        tp.add(textField);
        tp.setBorder(BorderFactory.createLoweredBevelBorder());
        jp4.add(tp, BorderLayout.CENTER);
        jp4.add(Box.createRigidArea(new Dimension(20,20)),BorderLayout.EAST);
        jp4.setAlignmentY(CENTER_ALIGNMENT);
        bp.add(jp4);
        
        bp.add(Box.createRigidArea(new Dimension(20,20)));
        
        JPanel jp2 = new JPanel();
        jp2.add(okButton);
        jp2.setAlignmentY(RIGHT_ALIGNMENT);
        bp.add(jp2);
        
        add(bp);
        
        pack();
        setLocationRelativeTo(callingPanel);
        setResizable(false);
        setVisible(true);
     }

     public void actionPerformed(ActionEvent e) {        
    	 if (e.getSource() == okButton) {
    		 dispose();
    	 }  else {
    		 log.error("Invalid condition");
    	 }
     }

}
