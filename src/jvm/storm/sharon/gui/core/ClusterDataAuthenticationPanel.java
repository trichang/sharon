package storm.sharon.gui.core;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.Border;
import javax.swing.text.DefaultEditorKit;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import storm.sharon.Rapid;
import storm.sharon.util.SharonUser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



//import org.bson.Document;


@SuppressWarnings("serial")
public class ClusterDataAuthenticationPanel extends JPanel implements ActionListener, ItemListener {
	private static Logger log = LogManager.getLogger(ClusterDataAuthenticationPanel.class.getName());
	private JButton continueButton;
	private JButton getPINButton;
	private JButton authenticatePINButton;
	
	private JComboBox rapid_userName;

	private JTextField twitterPin;
	private String twitterURL;
	private AccessToken accessToken = null;
	private Twitter twitterStream;
	private String selectedUserName;

	private JCheckBox agreementCheckBox;
	
	
	private StartupProtocol startupProtocol;

	/*
	 * Use all default values for the panel.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ClusterDataAuthenticationPanel(StartupProtocol startupProtocol){
		
		this.startupProtocol = startupProtocol;
		
		/*
		 * Selection box initialization
		 */
		rapid_userName = new JComboBox();
		for(String name:startupProtocol.getValidNames()){
			rapid_userName.addItem(name);
		}
		rapid_userName.addItem(SharonUser.DEFAULT_SHARON_USER);
		rapid_userName.addActionListener(this);
		rapid_userName.setSelectedIndex(0);
		if(startupProtocol.getValidNames().size()>0){
			selectedUserName = startupProtocol.getValidNames().get(0);
		} else {
			selectedUserName = SharonUser.DEFAULT_SHARON_USER;
		}
		
		
		/*
		 * Twitter pin field initialization
		 */
		twitterPin = new JTextField(20);
		JPopupMenu popup = new JPopupMenu();
	    JMenuItem item = new JMenuItem(new DefaultEditorKit.CutAction());
	    item.setText("Cut");
	    popup.add(item);
	    item = new JMenuItem(new DefaultEditorKit.CopyAction());
	    item.setText("Copy");
	    popup.add(item);
	    item = new JMenuItem(new DefaultEditorKit.PasteAction());
	    item.setText("Paste");
	    popup.add(item);
	    twitterPin.setComponentPopupMenu(popup);
	    twitterPin.setFont(new Font("Sans Serif", Font.PLAIN, 20));
	    
		OpenDataAuthenticationPanel();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void OpenDataAuthenticationPanel() {
		setLayout( new GridLayout(0,2));
		
		JPanel step1Panel = new JPanel();
		step1Panel.setLayout(new BoxLayout(step1Panel,BoxLayout.PAGE_AXIS));
		Border step1PanelBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"Authorization");
		step1Panel.setBorder(step1PanelBorder);
		
		
		getPINButton = new JButton("Twitter Login...");
		getPINButton.addActionListener(this);
		getPINButton.setToolTipText("Opens a browser window with an authorization URL.");
		
		String info="<p>You may link multiple Twitter accounts to your RAPID account.";
		info+=" For each Twitter account you will need to authorize RAPID to use it.";
		info+=" Click on Twitter Login to open a browser window for authorization.";
		info+=" If you later revoke access to a Twitter account then it will be unlinked from your RAPID account.</p>";
		JLabel label = new JLabel("<html>"+info+"</html>");
		step1Panel.add(label);
		step1Panel.add(Box.createRigidArea(new Dimension(0,10)));
		
		step1Panel.add(getPINButton);

		JPanel pinPanel = new JPanel();
		pinPanel.setLayout(new BoxLayout(pinPanel,BoxLayout.PAGE_AXIS));
		Border pinPanelBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"Twitter Pin");
		pinPanel.setBorder(pinPanelBorder);
		pinPanel.add(Box.createRigidArea(new Dimension(0,10)));
		pinPanel.add(twitterPin);
		pinPanel.add(Box.createRigidArea(new Dimension(0,10)));
		twitterPin.setMaximumSize( new Dimension((int)twitterPin.getMaximumSize().getWidth(),(int)twitterPin.getPreferredSize().getHeight()) );
		
		authenticatePINButton = new JButton("Authenticate PIN");
		authenticatePINButton.addActionListener(this);
		authenticatePINButton.setToolTipText("Links your Twitter account to your RAPID account.");
		
		pinPanel.add(authenticatePINButton);
		pinPanel.add(Box.createRigidArea(new Dimension(0,10)));
		step1Panel.add(Box.createRigidArea(new Dimension(0,10)));
		pinPanel.setAlignmentX(LEFT_ALIGNMENT);
		step1Panel.add(pinPanel);
		
		
		try {
			BufferedImage img = ImageIO.read(getClass().getResourceAsStream("/glossy.png"));
			JPanel imgp= new JPanel(new BorderLayout());
			Border imgpPanelBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"The Realtime Analysis Platform for Interactive Datamining");
			imgp.setBorder(imgpPanelBorder);			
			imgp.add(new JLabel(new ImageIcon(img)),BorderLayout.CENTER);
			imgp.setAlignmentX(LEFT_ALIGNMENT);
			step1Panel.add(imgp);
		} catch (IOException e1) {
			log.error("could not read image");
		}
		
		JPanel step2Panel = new JPanel();
		step2Panel.setLayout(new BorderLayout());
		Border step2PanelBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"Select a credential");
		step2Panel.setBorder(step2PanelBorder);
		
		info="<p>Select your RAPID account that you wish to use.";
		info+=" SharonDefault can be used if you do not have a linked Twitter account.</p>";
		JLabel label2 = new JLabel("<html>"+info+"</html>");
		label2.setAlignmentX(LEFT_ALIGNMENT);
		
		
		step2Panel.add(Box.createRigidArea(new Dimension(0,10)));
		JPanel wrapper = new JPanel(new BorderLayout());
		wrapper.add(label2,BorderLayout.NORTH);
		rapid_userName.setMaximumSize( new Dimension((int)rapid_userName.getMaximumSize().getWidth(),(int)rapid_userName.getPreferredSize().getHeight()) );
		wrapper.add(rapid_userName,BorderLayout.SOUTH);
		
		wrapper.setAlignmentX(LEFT_ALIGNMENT);
		step2Panel.add(wrapper, BorderLayout.NORTH);
		
		InputStream is = getClass().getResourceAsStream("/SoftwareLicense.dat");
		String licenseString="";
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String line;
			while ((line = br.readLine()) != null) {
				licenseString += " "+line;
			}
		} catch (Exception e) {
			licenseString="<html>No license details supplied.</html>";
		}
		
		JTextPane license = new JTextPane();
		license.setContentType("text/html"); 
		license.setText(licenseString);
		license.setEditable(false);
		
		JScrollPane sp = new JScrollPane(license);
		
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		step2Panel.add(sp,BorderLayout.CENTER);
		
		JPanel ab = new JPanel(new BorderLayout());
		agreementCheckBox = new JCheckBox("Agree with license terms and conditions.");
		agreementCheckBox.setSelected(false);
		agreementCheckBox.addItemListener(this);
		ab.add(agreementCheckBox,BorderLayout.NORTH);
		continueButton = new JButton("Continue...");
		continueButton.setAlignmentX(LEFT_ALIGNMENT);
		ab.add(continueButton,BorderLayout.SOUTH);
		continueButton.setEnabled(false);
		continueButton.addActionListener(this);
		continueButton.setToolTipText("Login to the RAPID system.");
		step2Panel.add(ab,BorderLayout.SOUTH);
		
	
		add(step1Panel);
		add(step2Panel);
		
		continueButton.setText("Continue as "+selectedUserName+"...");
		
		
	}
	
	@Override
	public void itemStateChanged(ItemEvent e) {
		Object source = e.getItemSelectable();
		if( source == agreementCheckBox){
			switch(e.getStateChange()){
			case ItemEvent.DESELECTED:
				continueButton.setEnabled(false);
				break;
			case ItemEvent.SELECTED:
				continueButton.setEnabled(true);
				break;
			}
		} 
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent event) {
		 if( event.getSource() == authenticatePINButton ){
			try {
				twitterStream = new TwitterFactory().getInstance();
				twitterStream.setOAuthConsumer(startupProtocol.getRequestToken().getToken(), startupProtocol.getRequestToken().getTokenSecret());
				if (twitterPin.getText().length() > 0) {
					log.debug("using request token: "+startupProtocol.getRequestToken().toString());
					accessToken = twitterStream.getOAuthAccessToken(startupProtocol.getRequestToken(), twitterPin.getText());
			    } else {
			    	log.debug("proceeding without request token");
			        accessToken = twitterStream.getOAuthAccessToken();
				}
				if(accessToken!=null){
					if(!startupProtocol.getValidNames().contains(accessToken.getScreenName())){
						startupProtocol.getValidNames().add(0, accessToken.getScreenName());
						startupProtocol.getValidKeys().add(0,accessToken.getToken());
						startupProtocol.getValidSecrets().add(0,accessToken.getTokenSecret());
						rapid_userName.removeAllItems();
						for(String name:startupProtocol.getValidNames()){
							rapid_userName.addItem(name);
						}
						rapid_userName.addItem(SharonUser.DEFAULT_SHARON_USER);
					} else {
						JOptionPane.showMessageDialog(this, "A valid credential for Twitter user "+accessToken.getScreenName()+" is already linked to RAPID.");
					}
					rapid_userName.setSelectedIndex(0);
					selectedUserName = startupProtocol.getValidNames().get(0); // just to make sure :-)
					continueButton.setText("Continue as "+selectedUserName+"...");
				} else {
					log.error("Something wrong with Twitter AccessToken");
				}
			} catch (Exception e) {
				log.warn("Failed to get access token from Twitter.");
				JOptionPane.showMessageDialog(this, "Failed to get access token from Twitter. Did you enter the correct PIN?");
			}
			
		} else if( event.getSource() == continueButton ){
			startupProtocol.startUserSession(selectedUserName);
	
		} else if ( event.getSource() == getPINButton ) {

			getTwitterURL();
			if (twitterURL != null) {
				if(!openWebpage(twitterURL)){
					log.warn("Could not automatically open a webpage for you.");
					JDialog urlDialog = new URLOpenDialog(this, Rapid.productName()+": Authorization URL", true, twitterURL);
				    urlDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE); 
				    
				}
			} else {
				log.error("Failed to receive an authentication URL from the server." );
			}
		} 
		else if ( event.getSource() == rapid_userName ) {
			selectedUserName = (String) rapid_userName.getSelectedItem();
			if(continueButton!=null) continueButton.setText("Continue as "+selectedUserName+"...");
		} else {
			log.error("Unknown ActionEvent received by DataAuthenticationPanel: "+event.getActionCommand());
		}
	}

	private void getTwitterURL () 
	{
		startupProtocol.serverGetAuthorizationURL();
		twitterURL = startupProtocol.getAuthenticationURL();
	}
	
	public static boolean openWebpage  (URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	    	try {
				desktop.browse(uri);
				return true;
			} catch (IOException e) {
				log.error("Browser should be supported but we got an exception: "+e);
				return false;
			}
	    } else {
	    	return false;
	    }
	}

	public static boolean openWebpage (String s) {
		
		try {
			URL url = new URL(s);
			return openWebpage(url.toURI());
		} catch (URISyntaxException | MalformedURLException e) {
			log.error("The URI syntax was not valid: "+e);
			return false;
		}
	}
 }
	