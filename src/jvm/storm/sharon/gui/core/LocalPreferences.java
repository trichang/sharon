package storm.sharon.gui.core;

import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/*
 * Only one of these objects is needed.
 */

public class LocalPreferences {
	private static Logger log = LogManager.getLogger(LocalPreferences.class.getName());
	public static final String LOCAL_PREFS_FILE = ".RAPID";
	public static PropertiesConfiguration config;
	public static boolean cansave = true;
	
	public LocalPreferences() {
		
	}
	
	public static void load(){
		String filePath = FilenameUtils.concat(System.getProperty("user.home"), LOCAL_PREFS_FILE);
		try {
			config = new PropertiesConfiguration(filePath);
			log.info("Using local preferences found at "+filePath);
		} catch (ConfigurationException e) {
			// file not available
			log.warn("Local preferences file not found, will create a new one at "+filePath);
			config = new PropertiesConfiguration();
			save();
		}
	}
	
	private static void save(){
		if(!cansave) return;
		String filePath = FilenameUtils.concat(System.getProperty("user.home"), LOCAL_PREFS_FILE);
		try {
			config.save(filePath);
		} catch (ConfigurationException e) {
			log.error("Failed to create local preferences file at "+filePath);
			cansave=false;
		}
	}
	
	public static String getLicenseKey(){
		if(config.containsKey("licensekey")){
			return config.getString("licensekey");
		} else {
			return null;
		}
	}
	
	public static void setLicenseKey(String key){
		config.clearProperty("licensekey");
		config.addProperty("licensekey", key);
		save();
	}
	
	/*
	 * screennames,tokens,keys are an array of authorized twitter accounts
	 * the order of each array needs to be preserved
	 */
	public static ArrayList<String> getTwitterScreenNames(){
		if(config.containsKey("twitterscreennames")){
			ArrayList<String> tsn = new ArrayList<String>();
			String names = config.getString("twitterscreennames");
			log.debug("Twitter Screen Names: " + names);
			StringTokenizer st = new StringTokenizer(names, ";");
			while (st.hasMoreTokens()){
				tsn.add(st.nextToken());
			}
			return tsn;
		} else {
			return new ArrayList<String>();
		}
	}
	
	public static void setTwitterScreenNames(ArrayList<String> names){
		if(names.size()>0){
			String joined = "";
			for(int i=0;i<names.size();i++){
				joined=joined+names.get(i);
				if(i<names.size()-1){
					joined=joined+";";
				}
			}
			config.clearProperty("twitterscreennames");
			config.addProperty("twitterscreennames", joined);
		} else {
			config.clearProperty("twitterscreennames");
		}
		save();
	}
	
	public static ArrayList<String> getTwitterSecrets(){
		if(config.containsKey("twittersecrets")){
			ArrayList<String> tt = new ArrayList<String>();
			String names = config.getString("twittersecrets");
			StringTokenizer st = new StringTokenizer(names, ";");
			while (st.hasMoreTokens()){
				tt.add(st.nextToken());
			}
			return tt;
		} else {
			return new ArrayList<String>();
		}
	}
	
	public static void setTwitterSecrets(ArrayList<String> secrets){
		if(secrets.size()>0){
			String joined = "";
			for(int i=0;i<secrets.size();i++){
				joined=joined+secrets.get(i);
				if(i<secrets.size()-1){
					joined=joined+";";
				}
			}
			config.clearProperty("twittersecrets");
			config.addProperty("twittersecrets", joined);
			
		} else {
			config.clearProperty("twittersecrets");
		}
		save();
	}
	
	public static ArrayList<String> getTwitterKeys(){
		if(config.containsKey("twitterkeys")){
			ArrayList<String> tt = new ArrayList<String>();
			String names = config.getString("twitterkeys");
			StringTokenizer st = new StringTokenizer(names, ";");
			while (st.hasMoreTokens()){
				tt.add(st.nextToken());
			}
			return tt;
		} else {
			return new ArrayList<String>();
		}
	}
	
	public static void setTwitterKeys(ArrayList<String> keys){
		if(keys.size()>0){
			String joined = "";
			for(int i=0;i<keys.size();i++){
				joined=joined+keys.get(i);
				if(i<keys.size()-1){
					joined=joined+";";
				}
			}
			config.clearProperty("twitterkeys");
			config.addProperty("twitterkeys", joined);
			
		} else {
			config.clearProperty("twitterkeys");
		}
		save();
	}
}
