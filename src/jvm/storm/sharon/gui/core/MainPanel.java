package storm.sharon.gui.core;


import javax.swing.BoxLayout;
import javax.swing.JTabbedPane;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JComponent;

import net.miginfocom.swing.MigLayout;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import storm.sharon.gui.topics.KeyListPanel;
import storm.sharon.gui.topics.PastKeyListPanel;
import storm.sharon.gui.topics.PluginsPanel;
import storm.sharon.gui.topics.StreamsPanel;
import storm.sharon.gui.topics.TopicListPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
 
@SuppressWarnings("serial")
public class MainPanel extends JPanel {
	private static Logger log = LogManager.getLogger(MainPanel.class.getName());
	
	public MainPanel() {

    	super();

    	setLayout(new BorderLayout());
    	
    	JTabbedPane tabbedPane = new JTabbedPane();
        
        // Topics and Keywords Tab
        JComponent topicKeywordPanel = makeTopicKeywordPanel();
        tabbedPane.addTab("Control", null, topicKeywordPanel, "Control the system");
        
        JPanel configPanel = new ConfigPanel();
        tabbedPane.addTab("Configuration",null,configPanel,"Configure the system");
        
        JPanel aboutPanel = new AboutPanel();
        tabbedPane.addTab("About", null,aboutPanel,"About the system");
        
     
    
        //Add the tabbed pane to this panel.
        add(tabbedPane,BorderLayout.CENTER);
         
        //The following line enables to use scrolling tabs.
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        //tabbedPane.getTabComponentAt(3).enable(false);
    }
  
    // Topics and Keywords Tab
    protected JComponent makeTopicKeywordPanel() {
        JPanel panel = new JPanel(false);
        panel.setLayout( new MigLayout("fill") );
        
        
        
        PluginsPanel panel4 = new PluginsPanel();
        //StreamsPanel panel5 = new StreamsPanel();
        PastKeyListPanel panel3 = new PastKeyListPanel();
        KeyListPanel panel2 = new KeyListPanel();
        TopicListPanel panel1 = new TopicListPanel(panel2, panel3, panel4);
        
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Current Keywords",null,panel2,"A list of keywords currently being tracked.");
        tabbedPane.addTab("Past Keywords",null,panel3,"A list of past keywords, not currently tracked.");
        
        JPanel StreamsPluginsPanel = new JPanel();
        //StreamsPluginsPanel.setLayout(new BoxLayout(StreamsPluginsPanel,BoxLayout.PAGE_AXIS));
        //StreamsPluginsPanel.setLayout(new BorderLayout());
        //panel5.setMaximumSize(new Dimension(panel5.getMaximumSize().width,panel5.getPreferredSize().height));
       // StreamsPluginsPanel.add(panel5, BorderLayout.NORTH);
        //StreamsPluginsPanel.add(panel4, BorderLayout.CENTER);
        
        panel.add(panel1, "cell 0 0,grow,width 50%!");
        panel.add(panel4, "cell 1 0 1 2,grow,width 50%!");
        panel.add(tabbedPane, "cell 0 1,grow,width 50%!");
        //panel.add(panel3);
        
        return panel;
    }

    
    /** Returns an ImageIcon, or null if the path was invalid. */
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = MainPanel.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            log.error("Couldn't find file: " + path);
            return null;
        }
    }
}