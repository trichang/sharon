package storm.sharon.gui.core;


import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/*
 * RapidLog files have extension .rlg
 * Filename has format basename-counter.rlg
 * counter starts at 1 and increments for each successive log file
 */
public class RapidLog {
	private static Logger log = LogManager.getLogger(RapidLog.class.getName());
	private File file;
	private FileOutputStream fos=null;
	private String basename;
	private File folder;
	private long counter;
	private long byteCount;
	private long maxSize;
	private boolean active;
	private enum LogType {string,image};
	private LogType lastType = LogType.string;
	
	public RapidLog(String folder, String basename,long maxSize) {
		this.folder = new File(folder);
		if(!this.folder.isDirectory()){
			try{
				this.folder.mkdir();
			} catch (SecurityException e) {
				log.error("Could not create the directory specified for logging: "+folder);
				log.info("Stopping RAPID log for "+basename);
				active=false;
				return;
			}
		}
		this.basename = basename;
		this.maxSize = maxSize;
		determineCounter();
		byteCount=maxSize+1;
		active=true;
		//rollLog(LogType.string);
	}
	
	private void determineCounter(){
		long max = 0;
		for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            // ignore directories
	        } else {
	            String name = fileEntry.getName();
	            String ext = FilenameUtils.getExtension(name);
	            if(ext.equalsIgnoreCase("rlg") || ext.equalsIgnoreCase("jpg")){
		            String tbasename = FilenameUtils.getBaseName(name);
		            String[] parts = tbasename.split("-");
		            if(parts!=null && parts[0].equalsIgnoreCase(basename) && parts.length>1){
		            	try{
		            		long counter = Long.parseLong(parts[1]);
		            		if(counter>max){
		            			max=counter;
		            		}
		            	} catch (NumberFormatException e){
		            		// ignore other files
		            	}
		            } else {
		            	// ignore other files
		            }
	            }
	        }
	    }
		counter=max;
	}
	
	private void rollLog(LogType lt){
		if(fos!=null){
			try{
				fos.close();
			} catch (IOException e){
				log.error("Exception while closing log file "+file.getPath()+" :"+e);
			}
		}
		counter++;
		String extension = lt==LogType.string ? "rlg":"jpg";
		String filename = basename+"-"+String.format("%08d", counter)+FilenameUtils.EXTENSION_SEPARATOR+extension;
		file = new File(FilenameUtils.concat(folder.getAbsolutePath(), filename));
		try {
			fos = new FileOutputStream(file);
			byteCount=0;
		} catch (FileNotFoundException e) {
			log.error("Exception opening RAPID logfile: "+file.getPath());
			log.info("Stopping RAPID log for "+basename);
			active=false;
		}
		lastType = lt;
	}
	
	public void close(){
		if(fos!=null){
			try{
				fos.close();
			} catch (IOException e){
				log.error("Exception while closing log file "+file.getPath()+" :"+e);
			}
		}
		active=false;
	}
	
	public void logString(String msg){
		if(!active) return;
		byte[] msgb = msg.getBytes();
		if(lastType == LogType.image || byteCount+msgb.length>maxSize){
			rollLog(LogType.string);
		}
		try {
			fos.write(msgb);
			fos.write("\n".getBytes());
			byteCount+=msgb.length;
		} catch (IOException e) {
			log.error("Could not write to RAPID logfile: "+file.getPath());
			log.info("Stopping RAPID log for "+basename);
			active=false;
		}
	}
	
	@SuppressWarnings("unused")
	private static void layoutComponent(Component c) {
        synchronized (c.getTreeLock()) {
            c.doLayout();
            if (c instanceof Container)
                for (Component child : ((Container) c).getComponents())
                    layoutComponent(child);
        }
    }
	
	public void logPanel(JPanel panel){
		BufferedImage bufImage=null;
		if(panel.getHeight()<=0 || panel.getWidth()<=0) {
			panel.setSize(new Dimension(1280,720));
			bufImage = new BufferedImage(panel.getWidth(), panel.getHeight(),BufferedImage.TYPE_INT_RGB);
			Graphics2D g = bufImage.createGraphics();
			panel.paint(g);
			g.dispose();
		} else {
			bufImage = new BufferedImage(panel.getWidth(), panel.getHeight(),BufferedImage.TYPE_INT_RGB);
		    panel.paint(bufImage.createGraphics());
		}
		
       
	   try{
	    	rollLog(LogType.image);
	        ImageIO.write(bufImage, "jpeg", file);
	   }catch(Exception ex){
		   log.error("Failed to write image: "+file.getPath());
		   log.info("Stopping RAPID log for "+basename);
		   active=false;
	   }
	}

	public boolean isActive() {
		return active;
	}

}
