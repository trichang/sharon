package storm.sharon.gui.core;


import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;

import org.bson.Document;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import storm.sharon.util.MessageQueueInterface;

public class MessageQueueReader extends Thread {
	private static Logger log = LogManager.getLogger(MessageQueueReader.class.getName());
	protected String topic;
	protected Observable guiSubject;
	protected Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap;
	public boolean ready=false;
	class myObserver extends Observable {
		
		@Override
		public synchronized void addObserver(Observer o) {
			super.addObserver(o);
		}

		public void setData(Document d) {
			setChanged();
			notifyObservers(d);
		}
		
	}
	
	public MessageQueueReader(String topic) {
		this.topic = topic;
		switch(topic){
		case MessageQueueInterface.KEYWORD_TOPIC_MESSAGE_QUEUE: // the keyword-topic is also where we receive commands
			consumerMap = CommandQueueListener.getConsumerMap(); 
			break;
		default:
			consumerMap = MessageQueueListener.getConsumerMap();
		}
		guiSubject = new myObserver();
	}
	
	public Observable getGuiSubject() {
		return guiSubject;
	}

	public void register(Observer obs) {
		guiSubject.addObserver(obs);	
	}
	
	@Override
	public void run() {
		KafkaStream<byte[], byte[]> stream = consumerMap.get(topic).get(0);
		ConsumerIterator<byte[], byte[]> it = stream.iterator();
		log.debug("reading Kafka topic: " + topic);
		ready=true;
		while (it.hasNext()) {
			String receivedData = new String(it.next().message());
			Document d = Document.parse(receivedData);
			d.append("rapid_queue_name", topic);
			((myObserver) guiSubject).setData(d);	
		}
		log.warn("terminating Kafka topic: "+topic);
	}
}
