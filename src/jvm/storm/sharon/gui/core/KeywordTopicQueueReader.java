package storm.sharon.gui.core;


import java.util.ArrayList;

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import storm.sharon.util.TopicKeywordHandler;

import org.bson.Document;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class KeywordTopicQueueReader extends MessageQueueReader  {
	private static Logger log = LogManager.getLogger(KeywordTopicQueueReader.class.getName());
	
	public KeywordTopicQueueReader(String topic) {
		super(topic);
	}
	
	
	@Override
	public void run() {
		KafkaStream<byte[], byte[]> stream = consumerMap.get(topic).get(0);
		ConsumerIterator<byte[], byte[]> it = stream.iterator();
		log.debug("subscribed to queue: "+topic);
		ready=true;
		ArrayList<Document> buffer = new ArrayList<Document>();
		while (it.hasNext()) { 
			String receivedData = new String(it.next().message());
			Document d = Document.parse(receivedData);
			log.debug("received document: " + d);
			String command = d.getString("command_name"); 
			
			if ( command.compareTo("CHANGE_TRACK") != 0 ) {
				
				// if it a new user session then we definitely accept it
				if (command.equals("NEW_USER_SESSION") && !RapidAPI.isCompleted()) {	
					if (d.getString("user_name").equals(RapidAPI.getAuthenticationData().getRapidUserName())) {
						log.debug("This is a session acknowledgment for the user: " + d.getString("user_name"));
						//RapidAPI.getTopicKeywordHandler().process(d);
						RapidAPI.getTopicKeywordHandler(d);
						// pass it through, to populate topics and user information
					} else {
						continue; // ignore NEW_USER_SESSION for other users
					}
					
				}
				
				d.append("rapid_queue_name", topic);
				if(RapidAPI.isCompleted()) {
					RapidAPI.getTopicKeywordHandler().process(d);
				} else {
					buffer.add(d); // TODO: flush this buffer when completed, _after_ the NEW_USER_SESSION
				}
				((myObserver) guiSubject).setData(d);
				
			}	
				
			
		}
	}	
}
