package storm.sharon.gui.core;

import org.bson.Document;

public class QueryTopicStream extends TopicStream {

	String query_id;
	

	public QueryTopicStream(DocumentStream target,String queue, int topic_id, String query_id) {
		super(target,queue, topic_id);
		this.query_id = query_id;
	}

	@Override
	public boolean filter(Document d){
		if(!super.filter(d)) return false;
		if(d.getString("query_id")!=null && d.getString("query_id").equals(query_id)) return true;
		return false;
	}
	
	public String getQuery_id() {
		return query_id;
	}

}
