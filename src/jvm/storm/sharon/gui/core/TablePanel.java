package storm.sharon.gui.core;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTable;

@SuppressWarnings("serial")
public class TablePanel extends JPanel {
	public TablePanel(JTable table) {
		super(new BorderLayout());
		add(table, BorderLayout.CENTER);
		add(table.getTableHeader(), BorderLayout.NORTH);
	}
}