package storm.sharon.gui.core;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextPane;
import javax.swing.text.DefaultEditorKit;


@SuppressWarnings("serial")
public class AboutPanel extends JPanel{
	AboutPanel(){
		super();

		setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		
		String info="<h1>RAPID: <em>Real-time Analysis Platform for Interactive Datamining</em></h1>"+
		"<p>Thankyou for evaluating our software. We look forward to hearing from you.</p>"+
		"<p>Contact Associate Professor Shanika Karunasekera karus@unimelb.edu.au for questions and feedback.</p>"+
		"<p>Visit the <i>Big Systems Research Group</i> at <a href=\"http://people.eng.unimelb.edu.au/aharwood/BigSystems\">http://people.eng.unimelb.edu.au/aharwood/BigSystems</a> for information concerning our research.</p>";
		
		JPopupMenu popup = new JPopupMenu();
	    JMenuItem item = new JMenuItem(new DefaultEditorKit.CutAction());
	    item.setText("Cut");
	    popup.add(item);
	    item = new JMenuItem(new DefaultEditorKit.CopyAction());
	    item.setText("Copy");
	    popup.add(item);
	    item = new JMenuItem(new DefaultEditorKit.PasteAction());
	    item.setText("Paste");
	    popup.add(item);
		
		JTextPane infop = new JTextPane();
		infop.setContentType("text/html"); 
		infop.setText("<html>"+info+"</html>");
		infop.setEditable(false);
		infop.setComponentPopupMenu(popup);
		add(infop);
		
		add(Box.createVerticalGlue());
    	
	}
}
