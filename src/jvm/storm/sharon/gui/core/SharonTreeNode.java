package storm.sharon.gui.core;

import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import javax.swing.tree.DefaultMutableTreeNode;

import org.bson.Document;

@SuppressWarnings("serial")
public class SharonTreeNode extends DefaultMutableTreeNode {

	private Document d;
	private int ReplyCount = 0;
	private String storedTime;

	public SharonTreeNode(String nodeName) {
		super(nodeName);
		this.d = null;
		this.storedTime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new java.util.Date());
	}

	public SharonTreeNode(String nodeName, Document d) {
		super(nodeName);
		this.setDocument(d);
		this.storedTime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new java.util.Date());
	}

	public SharonTreeNode(String nodeName, Document d, int replyCount) {
		super(nodeName);
		this.setDocument(d);
		this.setReplyCount(replyCount);
		this.storedTime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new java.util.Date());
	}

	public Set<SharonTreeNode> getAllLeafNodes() {
		Set<SharonTreeNode> leafNodes = new HashSet<SharonTreeNode>();
		if (this.isLeaf()) {
			leafNodes.add(this);
		} else {
			for (int i = 0; i < this.getChildCount(); i++) {
				leafNodes.addAll(((SharonTreeNode) this.getChildAt(i)).getAllLeafNodes());
			}
		}
		return leafNodes;
	}

	public void removeOldestLeafNode() {
		SharonTreeNode oldestLeafNode = new SharonTreeNode("");
		String oldestTime = oldestLeafNode.storedTime;

		for (SharonTreeNode tempNode : this.getAllLeafNodes()) {
			String tempTime = tempNode.storedTime;
			if (tempTime.compareTo(oldestTime) < 0) {
				oldestLeafNode = tempNode;
				oldestTime = oldestLeafNode.storedTime;
			}
		}

		if (oldestLeafNode.getParent() != null) {
			((SharonTreeNode) oldestLeafNode.getParent()).remove(oldestLeafNode);
			oldestLeafNode = null;
		}
	}

	public Document getDocument() {
		return d;
	}

	public void setDocument(Document d) {
		this.d = d;
	}

	public int getReplyCount() {
		return ReplyCount;
	}

	public void setReplyCount(int replyCount) {
		ReplyCount = replyCount;
	}

}
