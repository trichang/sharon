package storm.sharon.gui.core;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import javax.swing.JDialog;
import javax.swing.JFrame;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;

import storm.sharon.Rapid;
import storm.sharon.util.SharonUser;
import storm.sharon.util.TwitterAuth;
import twitter4j.auth.RequestToken;
/*
 * 
 * 
    Client starts up and enters “License Validation Phase”:
        Client checks local storage for license key
        If no license key, Client checks built-in license key
        (A) If no license key, Client asks user to enter license
        Client does not continue until license key is obtained
        Client sends license key to Server
        Server responds with valid or invalid
        If key is invalid, Client goes back to step (A) above
    Client enters “Authentication Phase”:
        Client checks local storage for existing Twitter KEY/SECRET/username credentials
        Client validates all credentials found in local storage with the server, invalid credentials are dropped
        (B) Client allows user to add more credentials if desired
        	Client requests authorization URL from server
        	User logs in to twitter and gets PIN, and authorizes a new credential
        	Credential saved locally
        Client only proceeds when a credential has been selected by user 
    Client continues to start up the system, sending new session message.
    	If server responds with invalid license key or credential then exit with error 

 * 
 */
public class StartupProtocol implements Observer {
	private  Logger log = LogManager.getLogger(StartupProtocol.class.getName());;
	private  SecureRandom random = new SecureRandom();
	private  String uniquesession;
	private  String authenticationURL;
	private  RequestToken requestToken;
	
	private enum ProtocolStates {
		Processing,
		WaitingForLicenseValidation,
		WaitingForCredentialValidation,
		WaitingForURL,
		WaitingForNewUserSession
	};
	private  ProtocolStates protocolState;
	
	private  boolean validLicenseKey;
	private  boolean validCredential;
	private  boolean sessionAuthenticated;
	
	private String tryLicense=null;
	
	private  ArrayList<String> validNames;
	private  ArrayList<String> validSecrets;
	private  ArrayList<String> validKeys;
	private int selected=-1;
	
	private  GuiAuthenticationData guiAuthenticationData;
	private JDialog guiFrame1;
	
	private boolean gotMyMessage = false;
	
	public StartupProtocol() {
		
		
	}
	
	public boolean initiate(){
		// our current state, used for asynchronous message processing
		protocolState = ProtocolStates.Processing;
		
		// We initialize a gui authentication object
		guiAuthenticationData = new GuiAuthenticationData();
		RapidAPI.setAuthenticationData(guiAuthenticationData);
		
		// Read the host list from the file
		InputStream is = getClass().getResourceAsStream("/HostList.dat");
		ArrayList<String> hostList = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String line;
			while ((line = br.readLine()) != null) {
				hostList.add(line);
			}
		} catch (Exception e) {
			log.fatal("Failed to obtain IP addresses for the backend.");
			System.exit(-1);
		}
		if(hostList.size()<2){
			log.fatal("Failed to obtain IP addresses for the backend.");
			System.exit(-1);
		}
		int numHosts = hostList.size();
		Random rn = new Random();
		int indx = rn.nextInt(numHosts -1) + 1;
		
		guiAuthenticationData.setKf_hostName(hostList.get(indx));
		
		// Port information is default set in the new authentication data object, so no need to set it here
		
		// Initialize the producer and keyword-topic queues, which we need for the startup protocol
		String brokerList;
		String zookeeper;
		zookeeper = guiAuthenticationData.getKfHostName() + ":" + guiAuthenticationData.getZport();
		brokerList = guiAuthenticationData.getKfHostName() + ":" + guiAuthenticationData.getBports();
		ArrayList<String> topics = new ArrayList<String>();
		topics.add("keyword-topic");	
		CommandQueueListener.Init(zookeeper, topics);
		RapidAPI.setKeywordTopicReader(new KeywordTopicQueueReader("keyword-topic"));
		RapidAPI.getKeywordTopicReader().start();
		RapidAPI.setProducer(new MessageQueueProducer(brokerList, "keyword-topic"));
		
		// a unique string to communicate with server
		// this avoids broadcasting sensitive information to others listening on keyword-topic queue
		// TODO: restrict access to the command queue: only the guiAuthenticationData.getSessionid()command handler should be able to listen to command queue!!
		uniquesession = new BigInteger(130, random).toString(32);
		
		guiAuthenticationData.setSessionid(uniquesession);
		
		// we have to process responses from server, asynchronously
		RapidAPI.getKeywordTopicReader().register(this);
				
		// keep sending ourselves a test message until we get it
		// this ensures that our consumer is _really_ ready
		gotMyMessage = false;
		while(!gotMyMessage){
			Document doc = new Document("sessionid",uniquesession);
			doc.append("command_name","gotmymessage");
			RapidAPI.getProducer().sendMessage(doc);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				log.error("Sleep was interrupted.");
			}
			
		}
		
		
		RapidAPI.setProducer(new MessageQueueProducer(brokerList, "commands"));
		
		
		// load local preferences
		LocalPreferences.load();
		
		/*
		 * License key validation phase
		 */
		String licenseKey = LocalPreferences.getLicenseKey();
		if(licenseKey==null){
			is = getClass().getResourceAsStream("/LicenseKey.dat");
			
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				String line;
				while ((line = br.readLine()) != null) {
					licenseKey = line;
					log.info("Selecting embedded license key: "  + licenseKey);
				}
			} catch (Exception e) {
				log.error("Could not obtain any license key.");
				log.info("License key will need to be manually entered.");
				licenseKey = null;
			}
		} else {
			log.info("Selecting license key found from local preferences: "+licenseKey);
		}
		validLicenseKey = false;
		int tries=0;
		while(!validLicenseKey && tries<3){
			if(licenseKey==null){
				// ask user to enter license key
				LicenseKeyDialog lkd = new LicenseKeyDialog(Rapid.productName()+": Enter License Key", true, this);
				licenseKey = getTryLicense();
			}
			if(licenseKey!=null) {
				if(!serverValidateLicenseKey(licenseKey)){
					licenseKey=null;
				}
			} else {
				// user clicked cancel when asked to enter license key
				return false; // system will exit
			}
			tries++;
		}
		if(!validLicenseKey) {
			// TODO: put dialog box to let user know where to get a new key
			log.error("Invalid License Key");
			return false; // system will exit.
		}
		
		// we now have a valid license key
		guiAuthenticationData.setLicense_key(licenseKey);
		LocalPreferences.setLicenseKey(licenseKey);
		log.info("Using license key: "+licenseKey);
		
		/*
		 * Authentication phase
		 */
		ArrayList<String> twitternames = LocalPreferences.getTwitterScreenNames();
		ArrayList<String> twittersecrets = LocalPreferences.getTwitterSecrets();
		ArrayList<String> twitterkeys = LocalPreferences.getTwitterKeys();
		validNames = new ArrayList<String>();
		validSecrets = new ArrayList<String>();
		validKeys = new ArrayList<String>();
		for(int i=0;i<twitternames.size();i++){
			validCredential=false;
			if(serverValidateCredential(twitternames.get(i),twittersecrets.get(i),twitterkeys.get(i))){
				// this is a possible credential to add to the list the user can select from
				validNames.add(twitternames.get(i));
				validSecrets.add(twittersecrets.get(i));
				validKeys.add(twitterkeys.get(i));
			} else {
				// this one is no longer valid, ignore it
				log.warn("Dropping invalid credential from local preferences: "+twitternames.get(i));
			}
		}
		LocalPreferences.setTwitterScreenNames(validNames);
		LocalPreferences.setTwitterKeys(validKeys);
		LocalPreferences.setTwitterSecrets(validSecrets);

		// open a panel for user to authorize more twitter accounts if desired
		// and to select a twitter account to continue with
		selected = -2; //if it stays this way, it means the user clicked the go-away box
		guiFrame1 = new JDialog((JFrame)null, Rapid.productName()+": Startup", true);
		guiFrame1.setIconImages(Rapid.getIcons());
		guiFrame1.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE); 
		guiFrame1.add(new ClusterDataAuthenticationPanel(this));
		guiFrame1.setSize(1280,720);  
		guiFrame1.setResizable(false);
		//guiFrame1.pack();
		guiFrame1.setLocationRelativeTo(null);
		guiFrame1.setVisible(true);
		
		
		// save changes made by the panel
		LocalPreferences.setTwitterScreenNames(validNames);
		LocalPreferences.setTwitterKeys(validKeys);
		LocalPreferences.setTwitterSecrets(validSecrets);
		
		// find the selected authentication details
		if(selected==-2){
			// user clicked go away
			return false; // application will quit
		} else if(selected!=-1){
			TwitterAuth twitterAuth = new TwitterAuth("","",validKeys.get(selected),validSecrets.get(selected));
			guiAuthenticationData.setTwitterAuth(twitterAuth);
			guiAuthenticationData.setRapid_userName(validNames.get(selected));
		} else {
			// default user
			guiAuthenticationData.setRapid_userName(SharonUser.DEFAULT_SHARON_USER);
		}
		
		// send a new user session message
		// Send a message to the server to register user session
		log.info("Starting a new user session for "+guiAuthenticationData.getRapid_userName());
		serverNewUserSession();
		
		if (sessionAuthenticated) {
			return true;
		} else {
			log.error("License has expried.");
			return false;
		}
		
	}
	
	public void startUserSession(String name){
		selected = -1;
		for(int i=0;i<validNames.size();i++){
			if(validNames.get(i).equals(name)){
				selected=i;
				break;
			}
		}
		guiFrame1.dispose();
	}
	
	private boolean serverNewUserSession(){
		Document doc = new Document("command_name", "NEW_USER_SESSION");
		doc.append("command_type", "USER_MGMT");
		doc.append("sessionid",uniquesession);
		doc.append("user_name",  guiAuthenticationData.getRapidUserName());
		if (guiAuthenticationData.getTwitterAuth() != null ) {
			doc.append("twitter_auth_info", guiAuthenticationData.getTwitterAuth().toDoc());
		}
		doc.append("license_key", guiAuthenticationData.getLicense_key());
		protocolState = ProtocolStates.WaitingForNewUserSession;	
		sessionAuthenticated=false;
		RapidAPI.getProducer().sendMessage(doc);
		while (protocolState == ProtocolStates.WaitingForNewUserSession) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				log.error("Sleep was interrupted.");
			}
		}
		return sessionAuthenticated;
	}
	
	private boolean serverValidateLicenseKey(String licenseKey){
		Document d = new Document("sessionid",uniquesession);
		d.append("license_key",licenseKey);
		d.append("command_name","VALIDATE_LICENSE_KEY");
		d.append("command_type", "USER_MGMT");
		protocolState = ProtocolStates.WaitingForLicenseValidation;
		RapidAPI.getProducer().sendMessage(d);
		while(protocolState == ProtocolStates.WaitingForLicenseValidation){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				log.error("Sleep was interrupted.");
			}
		}
		return validLicenseKey;
	}
	
	private boolean serverValidateCredential(String name,String secret,String key){
		Document d = new Document("sessionid",uniquesession);
		d.append("command_name", "VALIDATE_USER_CREDENTIAL");
		d.append("command_type", "USER_MGMT");
		d.append("user_name", name);
		Document t = new Document();
		t.append("twitter_consumerKey", key);
		t.append("twitter_consumerSecret", secret);
		d.append("twitter_auth_info",t);
		protocolState = ProtocolStates.WaitingForCredentialValidation;
		RapidAPI.getProducer().sendMessage(d);
		while(protocolState == ProtocolStates.WaitingForCredentialValidation){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				log.error("Sleep was interrupted.");
			}
		}
		return validCredential;
	}
	
	public void serverGetAuthorizationURL(){
		Document d = new Document("command_name","OBTAIN_AUTH_URL");
		d.append("sessionid",uniquesession);
		d.append("command_type", "USER_MGMT");
		protocolState = ProtocolStates.WaitingForURL;
		authenticationURL="";
		RapidAPI.getProducer().sendMessage(d);
		while(protocolState == ProtocolStates.WaitingForURL){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				log.error("Sleep was interrupted.");
			}
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		Document d = (Document)arg;
		
		if(d.containsKey("sessionid")){
			String sessionid = d.getString("sessionid");
			if(!sessionid.equals(uniquesession)) {
				return; // ignore messages for other sessions
			}
			if(d.containsKey("command_name") && d.getString("command_name").equals("gotmymessage")){
				gotMyMessage=true;
				return;
			}
			switch(protocolState){
			case Processing:
				log.error("Received a session message from server but not waiting for any message: "+d.toJson());
				break; // ignore if we are not waiting for something
			case WaitingForLicenseValidation:
				if(d.containsKey("valid_license_key")){
					validLicenseKey = d.getBoolean("valid_license_key");
					if(!validLicenseKey){
						log.warn("Invalid license key: "+d.getString("license_error"));
					}
					protocolState = ProtocolStates.Processing;
				} else {
					log.error("Waiting for license validation response but got something else from server: "+d.toJson());
				}
				break;
			case WaitingForCredentialValidation:
				if(d.containsKey("valid_credential")){
					validCredential = d.getBoolean("valid_credential");
					protocolState = ProtocolStates.Processing;
				} else {
					log.error("Waiting for credential validation response but got something else from server: "+d.toJson());
				}
				break;
			case WaitingForURL:
				if(d.containsKey("url")){
					authenticationURL = d.getString("url");
					requestToken = new RequestToken(d.getString("request_token"),d.getString("request_token_secret"));
					protocolState = ProtocolStates.Processing;
				} else {
					log.error("Waiting for authorization URL response but got something else from server: "+d.toJson());
				}
				break;
			case WaitingForNewUserSession:
				if(d.containsKey("authenticated")){
					sessionAuthenticated = d.getBoolean("authenticated");
					protocolState = ProtocolStates.Processing;
				} else {
					log.error("Waiting for session authentication response but got something else from server: "+d.toJson());
				}
				break;
			default:
				break;
			}
		}
	}

	public  GuiAuthenticationData getGuiAuthenticationData() {
		return guiAuthenticationData;
	}

	public  String getAuthenticationURL() {
		return authenticationURL;
	}

	public  RequestToken getRequestToken() {
		return requestToken;
	}

	public ArrayList<String> getValidNames() {
		return validNames;
	}

	public void setValidNames(ArrayList<String> validNames) {
		this.validNames = validNames;
	}

	public ArrayList<String> getValidSecrets() {
		return validSecrets;
	}

	public void setValidSecrets(ArrayList<String> validSecrets) {
		this.validSecrets = validSecrets;
	}

	public ArrayList<String> getValidKeys() {
		return validKeys;
	}

	public void setValidKeys(ArrayList<String> validKeys) {
		this.validKeys = validKeys;
	}

	public String getTryLicense() {
		return tryLicense;
	}

	public void setTryLicense(String tryLicense) {
		this.tryLicense = tryLicense;
	}
	
	

}
