package storm.sharon.gui.core;

import java.util.Properties;

import org.bson.Document;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class MessageQueueProducer {
	private static Logger log = LogManager.getLogger(MessageQueueProducer.class.getName());
	private Producer<String, String> producer;

	private String topic;
	public boolean ready=false;
	
	public MessageQueueProducer(String brokerList, String topic) {
		this.topic = topic;
		Properties props = new Properties();
        //props.put("metadata.broker.list", "localhost:9092");
		
		props.put("metadata.broker.list", brokerList);
        props.put("serializer.class", "kafka.serializer.StringEncoder");
        props.put("request.required.acks", "1");
        ProducerConfig config = new ProducerConfig(props);

        //Define producer object, its a java generic and takes 2 params; first
        //type of partition key, second type of the message                                                                                       
        producer = new Producer<String, String>(config);     
        ready=true;
	}

	
	public void sendMessage(Document doc) {
		if(RapidAPI.getAuthenticationData()!=null) {
			doc.append("authenticated_user", RapidAPI.getAuthenticationData().getRapidUserName());
		}
		String msg = doc.toJson();
		
		KeyedMessage<String, String> data = new KeyedMessage<String, String>(topic, null, msg);
		try {
			producer.send(data);
		} catch (Exception e){
			log.fatal("Could not connect to the system at ["+RapidAPI.getAuthenticationData().getKfHostName()+"]: "+e);
			System.exit(-1);
		}
	}
}
