package storm.sharon.gui.core;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.text.DefaultEditorKit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import storm.sharon.Rapid;

@SuppressWarnings("serial")
public class LicenseKeyDialog extends JDialog implements ActionListener {
	private static Logger log = LogManager.getLogger(LicenseKeyDialog.class.getName());
	private JButton okButton;
	private JButton cancelButton;
	private JTextField textField;
	private JLabel textLabel;
	private ClusterDataAuthenticationPanel callingPanel;
	private StartupProtocol startupProtocol;
    public LicenseKeyDialog ( String title, boolean b, StartupProtocol startupProtocol ) {
    	super((JFrame) null, title, b);
    	this.startupProtocol = startupProtocol;
    	setIconImages(Rapid.getIcons());
    	setSize(600,600);
    	
    	textField = new JTextField(40);
    	textField.setFont(new Font("Sans Serif", Font.PLAIN, 20));
		JPopupMenu popup = new JPopupMenu();
	    JMenuItem item = new JMenuItem(new DefaultEditorKit.CutAction());
	    item.setText("Cut");
	    popup.add(item);
	    item = new JMenuItem(new DefaultEditorKit.CopyAction());
	    item.setText("Copy");
	    popup.add(item);
	    item = new JMenuItem(new DefaultEditorKit.PasteAction());
	    item.setText("Paste");
	    popup.add(item);
	    textField.setComponentPopupMenu(popup);
	    textField.addActionListener(this);
    	okButton = new JButton("OK");
        okButton.addActionListener(this);
        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        textLabel = new JLabel("Enter License Key:");
        setLayout( new GridLayout(0, 1));
        JPanel bPanel1 = new JPanel();
		bPanel1.setLayout(new GridLayout(0, 1));
		JTextField blank_1 = new JTextField(10);
		blank_1.setEditable(false);
		blank_1.setBorder(null);
		bPanel1.add(blank_1);
		add(bPanel1);
        JPanel jp1 = new JPanel(new BorderLayout());
        jp1.add(textLabel, BorderLayout.WEST);
        jp1.add(textField, BorderLayout.EAST);
        add(jp1);
        JPanel bPanel2 = new JPanel();
      	bPanel1.setLayout(new GridLayout(0, 1));
      	JTextField blank_2 = new JTextField(10);
      	blank_2.setEditable(false);
      	blank_2.setBorder(null);
      	bPanel2.add(blank_2);
      	add(bPanel2);
        JPanel jp2 = new JPanel(new GridLayout(0,5));
        jp2.add(okButton);
        jp2.add(cancelButton);
        add(jp2);
        pack();
        setLocationRelativeTo(callingPanel);
        setVisible(true);
     }

     public void actionPerformed(ActionEvent e) {        
    	 if (e.getSource() == okButton) {
    		 startupProtocol.setTryLicense(textField.getText());
    		 dispose();
    	 } else if (e.getSource() == cancelButton) {
    		 //callingPanel.setNewLicenseKey(null);
    		 startupProtocol.setTryLicense(null);
    		 dispose();
    	 }  else {
    		 log.error("Invalid condition");
    	 }
     }

}
