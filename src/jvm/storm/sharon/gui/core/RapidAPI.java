package storm.sharon.gui.core;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.bson.Document;
import storm.sharon.util.Topic;
import storm.sharon.util.TopicKeywordHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/*
 * Provides the suite of methods for accessing the RAPID backend.
 */
public class RapidAPI {
	private static Logger log = LogManager.getLogger(RapidAPI.class.getName());
	private static MessageQueueProducer producer;
	private static KeywordTopicQueueReader keywordTopicReader = null;
	private static MessageQueueReader tweetReader = null;
	private static MessageQueueReader communityReader = null;
	private static MessageQueueReader tupleReader = null;
	private static MessageQueueReader discussionReader = null;
	private static GuiAuthenticationData authenticationData = null;
	private static boolean completed = false;

	public static void setProducer(MessageQueueProducer producer) {
		RapidAPI.producer = producer;
	}

	public static void setKeywordTopicReader(KeywordTopicQueueReader keywordTopicReader) {
		RapidAPI.keywordTopicReader = keywordTopicReader;
	}

	public static TopicKeywordHandler getTopicKeywordHandler() {
		return TopicKeywordHandler.getInstance();
	}
	
	public static TopicKeywordHandler getTopicKeywordHandler(Document doc) {
		return TopicKeywordHandler.getInstance(doc);
	}

	public static MessageQueueProducer getProducer() {
		return producer;
	}
	
	public static MessageQueueReader getKeywordTopicReader() {
		return keywordTopicReader;
	}
	
	public static MessageQueueReader getTweetReader() {
		return tweetReader;
	}
	
	public static MessageQueueReader getCommunityReader() {
		return communityReader;
	}

	public static MessageQueueReader getTupleReader() {
		return tupleReader;
	}
	
	public static MessageQueueReader getDiscussionReader() {
		return discussionReader;
	}
	
	@SuppressWarnings("unchecked")
	@Deprecated
	public static ArrayList<Topic> getUserTopicList() {
		ArrayList<Topic> topicList = new ArrayList<Topic>();
		
		Document doc = getTopicKeywordHandler().getTopics();
		List<Document> topics = (List<Document>) doc.get("topics");
		
		for (Document topic: topics) {
			
			Topic t = new Topic(topic);
			// Display the topic only if it is a public topic OR if it is private and user is in the shared list
			String user = getAuthenticationData().getRapid_userName();
			
			if (t.getTopicType() == Topic.PUBLIC_TOPIC) {
				topicList.add(t);
			} else {
				if (t.isSharedUser(user)) {
					// The user can view the topic because it is shared
					topicList.add(t);
				} else {
					// User is not permitted to view the topic 
					
				}
					
			}
		}
		// List the topic in alphabetical order
		Collections.sort(topicList);
		
		return topicList;
	}
	
	/*
	 * This function uses StartupProtocol to handle all of the authentication and user session things.
	 */
	public static boolean Authenticate() {
		
		StartupProtocol startupProtocol = new StartupProtocol();
		completed=false;
		if(startupProtocol.initiate()){
			initializeDataQueues();
			completed=true;
			return true;
		} else {
			return false;
		}
	}
	
	public static GuiAuthenticationData getAuthenticationData() {
		return authenticationData;
	}
	
	private static void initializeDataQueues() {
		String user = authenticationData.getRapid_userName();
		String zookeeper = authenticationData.getKfHostName() + ":" + authenticationData.getZport();
		
		ArrayList<String> topics = new ArrayList<String>();
		topics.add("tweets_" + user );
		topics.add("tuples_" + user);
		topics.add("discussions_" + user);
		topics.add("graph_stream");
		topics.add("cluster_stream");
		topics.add("node_stream");
		
		MessageQueueListener.Init(zookeeper, topics);
		
		tweetReader = new MessageQueueReader("tweets_" + user);
		tweetReader.start();
		
		tupleReader = new MessageQueueReader("tuples_" + user);
		tupleReader.start();
		
		discussionReader = new MessageQueueReader("discussions_"+user);
		discussionReader.start();
		
		while(!(tweetReader.ready && tupleReader.ready && discussionReader.ready)){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				log.error("Sleep was interrupted.");
			}
		}
	}
	
	public static boolean isCompleted() {
		return completed;
	}

	public static void setCompleted(boolean completed) {
		RapidAPI.completed = completed;
	}
	

	public static void setAuthenticationData(GuiAuthenticationData authenticationData) {
		RapidAPI.authenticationData = authenticationData;
	}

}
