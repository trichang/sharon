package storm.sharon.gui.core;


import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;

import storm.sharon.Rapid;
import storm.sharon.gui.plugins.DataPanel;
import storm.sharon.util.Topic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
@SuppressWarnings("serial")
public class PluginConfig extends JDialog implements ActionListener {
	private static Logger log = LogManager.getLogger(PluginConfig.class.getName()); 
	private DataPanel plugin;
	private Vector<Integer> window_length;
	private Vector<Integer> update_frequency;
	private Vector<Integer> threshold;
	private Vector<JFormattedTextField> window_length_field;
	private Vector<JFormattedTextField> update_frequency_field;
	private Vector<JFormattedTextField> threshold_field;
	NumberFormat amountFormat;
	JButton setButton;
	JButton cancelButton;
	public PluginConfig(DataPanel plugin) {
		setName("Configuration");
		setIconImages(Rapid.getIcons());
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		
		window_length=new Vector<Integer>();
		update_frequency=new Vector<Integer>();
		threshold=new Vector<Integer>();
		window_length_field=new Vector<JFormattedTextField> ();
		update_frequency_field=new Vector<JFormattedTextField> ();
		threshold_field=new Vector<JFormattedTextField> ();
		
		this.plugin = plugin;
		int i=0;
		
		for(String query:plugin.getQuery_id()){
			
			// returned time values are in seconds...
			window_length.addElement(plugin.getTopic().getWindowLengthSeconds(query)/60);
			update_frequency.addElement(plugin.getTopic().getUpdateFrequencySeconds(query)/60);
			
			threshold.addElement(plugin.getTopic().getRollingCountThreshold(query));
			
			setLayout( new GridLayout(0, 2));
			
			amountFormat = NumberFormat.getNumberInstance();
			window_length_field.addElement(new JFormattedTextField(amountFormat));
			window_length_field.get(i).setText(""+window_length.get(i));
			update_frequency_field.addElement(new JFormattedTextField(amountFormat));
			update_frequency_field.get(i).setText(""+update_frequency.get(i));
			threshold_field.addElement(new JFormattedTextField(amountFormat));
			threshold_field.get(i).setText(""+threshold.get(i));
			
			add(new JLabel(query+" Window Length (minutes)"));
			add(window_length_field.get(i));
			add(new JLabel(query+" Update Frequency (minutes)"));
			add(update_frequency_field.get(i));
			add(new JLabel(query+" Threshold (count)"));
			add(threshold_field.get(i));
			i++;
		}
		
		setButton = new JButton("Set");
		add(setButton);
		cancelButton = new JButton("Cancel");
		add(cancelButton);
		
		setButton.addActionListener(this);
		cancelButton.addActionListener(this);
		pack();
		setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==cancelButton){
			dispose();
		} else {
			int i=0;
			for(String query:plugin.getQuery_id()){
				Document d = new Document();
				d.append("command_name","TOPIC_WINDOW_PARM_UPDATE");
				d.append("command_type", "USER_MGMT");
				d.append("topic_id",plugin.getTopic().getTopicId());
				d.append("query_id",query);
				int wl;
				try{
					wl = Integer.parseInt(window_length_field.get(i).getText());
				} catch (NumberFormatException ex) {
					wl = window_length.get(i);
				}
				d.append("window_length", wl);
				int uf;
				try{
					uf = Integer.parseInt(update_frequency_field.get(i).getText());
				} catch (NumberFormatException ex) {
					uf = update_frequency.get(i);
				}
				d.append("update_frequency", uf);
				int th;
				try{
					th = Integer.parseInt(threshold_field.get(i).getText());
				} catch (NumberFormatException ex) {
					th = threshold.get(i);
				}
				d.append("threshold", th);
				RapidAPI.getProducer().sendMessage(d);
				i++;
			}
			dispose();
		}
		
	}

}
