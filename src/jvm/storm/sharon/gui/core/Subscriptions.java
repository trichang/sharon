package storm.sharon.gui.core;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.Vector;

import org.bson.Document;

import storm.sharon.util.Topic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Subscriptions implements Observer {
	
	private HashMap<Integer,Set<String>> topicQueueMap;
	private HashMap<String,Integer> subscribers;
	private Logger log = LogManager.getLogger(Subscriptions.class.getName());
	
	
	public Subscriptions(){
		subscribers = new HashMap<String,Integer>();
		topicQueueMap = new HashMap<Integer,Set<String>>();
		RapidAPI.getKeywordTopicReader().register(this);
		log.info("Subscription monitoring ready");
	}
	
	synchronized public void Subscribe(String queue,int topic_id){
		incrementSubscribers(queue,topic_id);
		String u = RapidAPI.getAuthenticationData().getRapidUserName();
		addQueueToTopic(queue,topic_id);
		if(RapidAPI.getTopicKeywordHandler().hasSubscribed(u, topic_id, queue)) return;
		Document doc = new Document("command_name", "SUBSCRIBE_TOPIC_QUEUE");
		doc.append("command_type", "USER_MGMT");
		doc.append("user_name", RapidAPI.getAuthenticationData().getRapidUserName());
		doc.append("topic_id",topic_id);
		doc.append("queue_name", queue);
		RapidAPI.getProducer().sendMessage(doc);	
	}
	
	synchronized public void Unsubscribe(String queue,int topic_id){
		if(decrementSubscribers(queue,topic_id)==0){
			removeQueueFromTopic(queue,topic_id);
			Document doc = new Document("command_name", "UNSUBSCRIBE_TOPIC_QUEUE");
			doc.append("command_type", "USER_MGMT");
			doc.append("user_name", RapidAPI.getAuthenticationData().getRapidUserName());
			doc.append("topic_id",topic_id);
			doc.append("queue_name", queue);
			RapidAPI.getProducer().sendMessage(doc);
		}
	}
	
	private void addQueueToTopic(String queue, int topic_id){
		if(!topicQueueMap.containsKey(topic_id)){
			topicQueueMap.put(topic_id, new HashSet<String>());
		}
		topicQueueMap.get(topic_id).add(queue);
	}
	
	private void removeQueueFromTopic(String queue, int topic_id){
		topicQueueMap.get(topic_id).remove(queue);
	}
	
	private int incrementSubscribers(String queue,int topic_id){
		String key = queue+":"+topic_id;
		if(!subscribers.containsKey(key)){
			subscribers.put(key,1);
		} else {
			subscribers.put(key,subscribers.get(key)+1);
		}
		return subscribers.get(key);
	}
	
	private int decrementSubscribers(String queue,int topic_id){
		String key = queue+":"+topic_id;
		if(!subscribers.containsKey(key)){
			log.error("Key "+key+" not found in subscribers map");
		} else {
			int s = subscribers.get(key);
			if(s>0){
				subscribers.put(key,s-1);
			} else {
				log.error("No current subscribers for "+key);
			}
		}
		return subscribers.get(key);
	}

	@Override
	public void update(Observable arg0, Object arg) {
		Document d = (Document) arg;
		if (d.get("command_name") != null) {
			if (d.getString("command_name").equals("NEW_TOPIC")) {
				int topic_id = d.getInteger("topic_id");
				Topic topic = RapidAPI.getTopicKeywordHandler().getTopicIdMap().get(topic_id);
				
				// actually nothing to do since Plugins will call relevant subscribe functions in this case
			}
			if (d.getString("command_name").equals("DELETE_TOPIC")) {
				int topic_id = d.getInteger("topic_id");
				Topic topic = RapidAPI.getTopicKeywordHandler().getTopicIdMap().get(topic_id);
						
				// actually nothing to do since Plugins will call relevant subscribe functions in this case
				
			}

		}
		
	}

	public HashMap<Integer, Set<String>> getTopicQueueMap() {
		return topicQueueMap;
	}
	
	
}
