package storm.sharon.gui.core;

import org.bson.Document;

import storm.sharon.Rapid;

public class TopicStream extends Stream {

	int topic_id;
	
	public TopicStream(DocumentStream target,String queue, int topic_id) {
		super(target,queue);
		this.topic_id = topic_id;
	}

	@Override
	public boolean filter(Document d){
		if(!super.filter(d)) return false;
		if (d.getInteger("sharon_topic_id") !=null && d.getInteger("sharon_topic_id")!= topic_id) return false; // ignore other topics
		if (d.getInteger("topic_id") !=null && d.getInteger("topic_id")!= topic_id) return false; // ignore other topics
		return true;
	}
	
	public int getTopic_id() {
		return topic_id;
	}
	
	@Override
	public void subscribe(){
		Rapid.getSubscriptions().Subscribe(getQueue(), getTopic_id());
	}
	
	@Override
	public void unsubscribe(){
		Rapid.getSubscriptions().Unsubscribe(getQueue(), getTopic_id());
	}
}
