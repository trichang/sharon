package storm.sharon.gui.core;


import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import storm.sharon.Rapid;
import storm.sharon.gui.topics.LogPanel;

@SuppressWarnings("serial")
public class ConfigPanel extends JPanel {
	ConfigPanel(){
		super();

		setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		
    	JTabbedPane tabbedPane = new JTabbedPane();
    	
    	 LogPanel logPanel = new LogPanel();
    	 Rapid.setLogpanel(logPanel); // provide a hook for other parts of the system to access log configuration
         tabbedPane.addTab("Logging", null, logPanel, "Log settings");
        
         add(tabbedPane);
        
	}
}
