package storm.sharon.gui.core;

import org.bson.Document;

public interface DocumentStream {
	void ReceiveStreamDocument(Document d);
}
