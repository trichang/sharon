/**  
* GuiAuthenticatonData.java - class for represents a Topic.  
* @author  Shanika Karunasekera
* @version 1.0 
*/

package storm.sharon.gui.core;

import java.util.ArrayList;

import storm.sharon.util.TwitterAuth;

public class GuiAuthenticationData {

	
	private String kf_hostName;
	private String zport;
	private String bports;
	private String rapid_userName;
	private String license_key = null;
	private TwitterAuth twitterAuth = null;
	private String sessionid;

	public GuiAuthenticationData() {
		this.zport = "2181";
		this.bports = "9092";
	}
	
	public GuiAuthenticationData(String rapid_userName, String db_hostName, String kf_hostName) {
		this.kf_hostName = kf_hostName;
		this.zport = "2181";
		this.bports = "9092";
		this.rapid_userName = rapid_userName;
	}
	
	
	public String getKfHostName() {
		return kf_hostName;
	}
	
	public String getZport() {
		return zport;
	}
	
	public String getBports() {
		return bports;
	}
	
	
	@Deprecated
	public String getRapidUserName() {
		return rapid_userName;
	}
	
	public String getLicense_key() {
		return license_key;
	}

	public void setLicense_key(String license_key) {
		this.license_key = license_key;
	}

	public TwitterAuth getTwitterAuth() {
		return twitterAuth;
	}


	public void setTwitterAuth(TwitterAuth twitterAuth) {
		this.twitterAuth = twitterAuth;
	}


	public String getKf_hostName() {
		return kf_hostName;
	}


	public void setKf_hostName(String kf_hostName) {
		this.kf_hostName = kf_hostName;
	}


	public String getRapid_userName() {
		return rapid_userName;
	}


	public void setRapid_userName(String rapid_userName) {
		this.rapid_userName = rapid_userName;
	}


	public void setZport(String zport) {
		this.zport = zport;
	}


	public void setBports(String bports) {
		this.bports = bports;
	}


	public String getSessionid() {
		return sessionid;
	}


	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	
	
	
}
