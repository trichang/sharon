package storm.sharon.gui.core;

import java.util.Observable;
import java.util.Observer;

import storm.sharon.Rapid;
import storm.sharon.util.MessageQueueInterface;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;

public abstract class Stream implements Observer {
	private Logger log = LogManager.getLogger(Subscriptions.class.getName());
	private String queue;
	
	protected DocumentStream target;
	
	public Stream(DocumentStream target,String queue) {
		this.queue=queue;
		this.target = target;
		Register();
	}

	private void Register(){
		switch(queue){
		case MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE:
			RapidAPI.getCommunityReader().register(this);
			break;
		case MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE:
			RapidAPI.getDiscussionReader().register(this);
			break;
		case MessageQueueInterface.TUPLE_MESSAGE_QUEUE:
			RapidAPI.getTupleReader().register(this);
			break;
		case MessageQueueInterface.TWEET_MESSAGE_QUEUE:
			RapidAPI.getTweetReader().register(this);
			break;
		case MessageQueueInterface.KEYWORD_TOPIC_MESSAGE_QUEUE:
			RapidAPI.getKeywordTopicReader().register(this);
			break;
		default:
			log.error("Unknown queue: "+queue);
		}
	}
	
	@Override
	public void update(Observable o, Object arg) {
		Document d = (Document)arg;
		if(filter(d)) target.ReceiveStreamDocument(d);
	}
	
	public void subscribe(){
		// we never just subscribe to a queue
	}
	
	public void unsubscribe(){
		// nothing to unsubscribe from
	}
	
	public boolean filter(Document d){
		return true; // include the document
	}
	
	public String getQueue() {
		return queue;
	}
	
}

