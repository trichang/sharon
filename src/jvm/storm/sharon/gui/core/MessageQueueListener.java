package storm.sharon.gui.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import kafka.consumer.ConsumerConfig;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MessageQueueListener {
	private static Logger log = LogManager.getLogger(MessageQueueListener.class.getName());
	private static ConsumerConnector consumer = null;
	private static String zookeeper;
	private static Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = null;
	
	public static void Init(String zoo, ArrayList<String> topics){
		zookeeper = zoo;
		if (consumer == null) {
			log.debug("Initializing consumer connector to "+zoo);
			consumer = kafka.consumer.Consumer.createJavaConsumerConnector(createConsumerConfig());
			Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
			for (String topic: topics) {
				topicCountMap.put(topic, new Integer(1));
			}
			consumerMap = consumer.createMessageStreams(topicCountMap);
		}
	}
	
	
	public static Map<String, List<KafkaStream<byte[], byte[]>>> getConsumerMap() {
		return consumerMap;
	}
	
	/**
	 * Creates the consumer config.
	 *
	 * @return the consumer config
	 */
	private static ConsumerConfig createConsumerConfig() {
		Properties props = new Properties();
		props.put("zookeeper.connect", zookeeper);
		props.put("group.id", "kafka-consumer-" + new Random().nextInt(100000));	
		props.put("zookeeper.session.timeout.ms", "40000");
		props.put("zookeeper.sync.time.ms", "20000");
		props.put("auto.commit.interval.ms", "1000");
		//props.put("consumer.timeout.ms", 100);
		return new ConsumerConfig(props);
	}
}
