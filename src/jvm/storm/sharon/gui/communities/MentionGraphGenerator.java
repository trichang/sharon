package storm.sharon.gui.communities;
import java.util.List;


import org.bson.Document;
import org.graphstream.algorithm.generator.Generator;


public class MentionGraphGenerator extends GraphGenerator
      implements Generator {
 
   
 
   public MentionGraphGenerator() {
	   super();
   }
 
  
  
   @Override
   public boolean nextEvents() {
	   
	   Integer total_mentions = doc.getInteger("total_mentions");
	   Integer total_replies = doc.getInteger("total_replies");
	   
	   
	   if (total_mentions != 0) {
		   String dest_node = doc.getString("user_name");
			Integer dest_node_size = 0;
			   
		   dest_node_size = total_mentions;
		   if (dest_node_size > 30) {
			   dest_node_size = 30;
		   }
		   if (graph.getNode(dest_node) == null) {
			   sendNodeAdded(sourceId, dest_node);
		   }
		   sendNodeAttributeAdded(sourceId, dest_node, "ui.label", dest_node);
		   sendNodeAttributeAdded(sourceId, dest_node, "ui.size", dest_node_size);
		   sendNodeAttributeAdded(sourceId, dest_node, "ui.fill-color", 0);
		   
		   
		   List<Document> mentioning_list = (List<Document>) doc.get("mentioning_list");
		   
		   if (mentioning_list != null) {
			   for (Document d: mentioning_list) {
				   String user = d.getString("mentioning_user_name");
				   if (graph.getNode(user) == null) {
					   sendNodeAdded(sourceId,  user);
					   if(graph.getEdge(user+dest_node)==null){
						   sendEdgeAdded(sourceId, user + dest_node, user, dest_node,  true);
					   }
				   } else {
					   if(graph.getEdge(user+dest_node)==null){
						   sendEdgeAdded(sourceId, user + dest_node, user, dest_node,  true);
					   }
				   }
			   }
		   }
	   }
	   
	   
	   return false;
   }
	
}