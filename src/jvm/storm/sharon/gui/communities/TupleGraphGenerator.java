package storm.sharon.gui.communities;

import java.awt.BorderLayout;
import java.util.HashMap;

import javax.swing.JFrame;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.graphstream.algorithm.generator.Generator;
import org.graphstream.algorithm.util.FibonacciHeap.Node;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.SourceBase;
import org.graphstream.ui.swingViewer.View;
import org.graphstream.ui.swingViewer.Viewer;

import scala.Tuple2;
import storm.sharon.gui.plugins.ComMentionsGraph;

public class TupleGraphGenerator extends SourceBase implements Generator {
	private static Logger log = LogManager.getLogger(TupleGraphGenerator.class.getName());
	private Document doc = null;
	private Graph graph = null;
	private Viewer viewer = null;
	private View view = null;
	private HashMap<String, Integer> destMap = new HashMap<String, Integer>();
	private HashMap<String, Integer> sourceMap = new HashMap<String, Integer>();
	private HashMap<Tuple2<String, String>, Integer> sourceDestMap = new HashMap<Tuple2<String, String>, Integer>();

	public TupleGraphGenerator() {
		this.graph = new SingleGraph("Tuple Graph");
		
		this.graph.addAttribute("ui.stylesheet",
				" node {size-mode: dyn-size; size: 10px; shape: box;  fill-mode: dyn-plain; fill-color: green, blue; stroke-mode: plain; stroke-color: yellow; }");
		addSink(graph);
	}
	
	public TupleGraphGenerator(Integer topic_id) {
		this.graph = new SingleGraph("Tuple Graph");
		
		this.graph.addAttribute("ui.stylesheet",
				" node {size-mode: dyn-size; size: 10px;shape: box; fill-color: green; stroke-mode: plain; stroke-color: yellow; }");
		addSink(graph);
	}

	public void addToCollection(Document d) {
		doc = d;
		nextEvents();
	}

	public void displayGraph(JFrame frame) {
		viewer = new Viewer(graph,Viewer.ThreadingModel.GRAPH_IN_SWING_THREAD);
		viewer.enableAutoLayout();
		view = viewer.addDefaultView(false);
		
		
		frame.add(view,BorderLayout.CENTER);
	}

	public void purgeGraph() {
		if (viewer != null) {
			viewer.close();
		}
	}

	public void begin() {

	}

	public void end() {

	}

	@Override
	public boolean nextEvents() {
		
		if (!doc.containsKey("command")) {

			Integer count = doc.getInteger("count");
			String dest_node = doc.getString("dest_node");
			String source_node = doc.getString("src_node");
			String edge = source_node + '@' + dest_node;
			
			if (count == 0) {
				if (graph.getEdge(edge) != null) {
					sendEdgeRemoved(sourceId, edge);
				}
				
				if ( graph.getNode(dest_node) != null) {
					if ((graph.getNode(dest_node).getInDegree() + graph.getNode(dest_node).getOutDegree()) == 0) {
						sendNodeRemoved(sourceId, dest_node);
					}
				}
				
				if ( graph.getNode(source_node) != null) {
					if ((graph.getNode(source_node).getInDegree() + graph.getNode(source_node).getOutDegree()) == 0) {
						sendNodeRemoved(sourceId, source_node);
					}
				}
				
			} else {
				if (graph.getNode(dest_node) == null) {
					sendNodeAdded(sourceId, dest_node);
				}
				if (graph.getNode(source_node) == null) {
					sendNodeAdded(sourceId, source_node);
				}
				sendNodeAttributeAdded(sourceId, source_node, "ui.label", source_node);
				
				if (graph.getEdge(edge) == null) {
					sendEdgeAdded(sourceId, edge, source_node, dest_node, true);
				}
				sendEdgeAttributeAdded(sourceId, edge, "ui.size", count);
				
				sendNodeAttributeAdded(sourceId, dest_node, "ui.label", dest_node);
				sendNodeAttributeAdded(sourceId, dest_node, "ui.size", graph.getNode(dest_node).getInDegree() + 10);
			
				if (dest_node.charAt(0) == '#') {
					log.trace("Rendering a hashtag node");
					sendNodeAttributeAdded(sourceId, dest_node, "ui.color", 1);
					sendNodeAttributeAdded(sourceId, dest_node, "ui.shape", "box");
				} else {
					log.trace("Rendering a non-hashtag node");
					sendNodeAttributeAdded(sourceId, dest_node, "ui.color", 0);
				}
				
			}
		} else {
			graph.clear();
			this.graph.addAttribute("ui.stylesheet",
					" node {size-mode: dyn-size; size: 10px; shape: box;  fill-mode: dyn-plain; fill-color: green, blue; stroke-mode: plain; stroke-color: yellow; }");
		}
		


		return false;
	}

	public Viewer getViewer() {
		return viewer;
	}

	public View getView() {
		return view;
	}

	
}