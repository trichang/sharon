package storm.sharon.gui.communities;

import java.awt.BorderLayout;



import javax.swing.JFrame;



import org.bson.Document;
import org.graphstream.algorithm.generator.Generator;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.SourceBase;
import org.graphstream.ui.swingViewer.View;
import org.graphstream.ui.swingViewer.Viewer;


public abstract class GraphGenerator extends SourceBase implements Generator {

	int currentIndex = 0;
	int edgeId = 0;
	Document doc = null;

	protected Graph graph = null;
	protected Viewer viewer = null;
	protected View view = null;

	public GraphGenerator() {
		graph = new SingleGraph("Community Graph");
		graph.addAttribute("ui.stylesheet",
				" node {size-mode: dyn-size; size: 10px;shape: box; fill-color: green; stroke-mode: plain; stroke-color: yellow; }");
		addSink(graph);
	}

	public void addToCollection(Document d) {
		
		doc = d;
		nextEvents();
		
	}

	public void displayGraph(JFrame frame) {

	
		viewer = new Viewer(graph,Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
		viewer.enableAutoLayout();
		view = viewer.addDefaultView(false);
		
		frame.add(view,BorderLayout.CENTER);
		
		//org.graphstream.ui.swingViewer.View view = v.addDefaultView(false);
		//frame.add(view);
		
		//v.setCloseFramePolicy(CloseFramePolicy.HIDE_ONLY);
		
	}

	public void clearGraph(JFrame frame) {
		purgeGraph();
		System.gc();
	}

	public void purgeGraph() {
		if (viewer != null) {
			viewer.close();
		}
	}

	public void begin() {

	}

	public void end() {

	}

	public Viewer getViewer() {
		return viewer;
	}

	public View getView() {
		return view;
	}

	
}