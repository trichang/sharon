package storm.sharon.gui.communities;
import java.util.List;
import java.util.Scanner;

import javax.swing.text.View;

import org.bson.Document;
import org.graphstream.algorithm.generator.Generator;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.SourceBase;







import org.graphstream.ui.swingViewer.Viewer;
import org.graphstream.ui.swingViewer.Viewer.CloseFramePolicy;

import storm.sharon.algorithms.CommunityCollection;
import storm.sharon.algorithms.User;

public class ReplyGraphGenerator extends GraphGenerator
      implements Generator {
 
   
 
   public ReplyGraphGenerator() {
	   super();
   }
 
  
  
   @Override
   public boolean nextEvents() {
	   
	   Integer total_mentions = doc.getInteger("total_mentions");
	   Integer total_replies = doc.getInteger("total_replies");
	   
	   
	   if (total_replies != 0) {
		   
		   String dest_node = doc.getString("user_name");
			  // Integer dest_node_size = doc.getInteger("total_mentions");
			Integer dest_node_size = 0;
			
			
		   dest_node_size = total_replies;
		   if (dest_node_size > 20) {
			   dest_node_size = 20;
		   }
		   if (graph.getNode(dest_node) == null) {
			   sendNodeAdded(sourceId, dest_node);
		   }
		   sendNodeAttributeAdded(sourceId, dest_node, "ui.label", dest_node);
		   sendNodeAttributeAdded(sourceId, dest_node, "ui.size", dest_node_size + 10);
		   sendNodeAttributeAdded(sourceId, dest_node, "ui.fill-color", 1);
		   
		   
		   List<Document> replying_list = (List<Document>) doc.get("replying_list");
		   
		   if (replying_list != null) {
			   for (Document d: replying_list) {
				   String user = d.getString("replying_user_name");
				   if (graph.getNode(user) == null) {
					   sendNodeAdded(sourceId,  user);
					   if(graph.getEdge(user+dest_node)==null){
						   sendEdgeAdded(sourceId, user + dest_node, user, dest_node,  true);
					   }
				   } else {
					   if(graph.getEdge(user+dest_node)==null){
						   sendEdgeAdded(sourceId, user + dest_node, user, dest_node,  true);
					   }
				   }
			   }
		   }
	   }
	  
	   
	   return false;
   }
	
	
 
		
		
}