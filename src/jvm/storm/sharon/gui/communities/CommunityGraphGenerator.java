package storm.sharon.gui.communities;

import org.bson.Document;
import org.graphstream.algorithm.generator.Generator;

import java.util.List;

/**
 * Created by tri on 28/04/16.
 */
public class CommunityGraphGenerator extends GraphGenerator
        implements Generator {

    public CommunityGraphGenerator() {
        super();
    }

    @Override
    public boolean nextEvents() {
        return false;
    }
}
