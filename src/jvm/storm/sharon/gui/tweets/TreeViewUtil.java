package storm.sharon.gui.tweets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;

import org.bson.Document;

import storm.sharon.gui.core.SharonTreeNode;

public class TreeViewUtil {
	
	public static final int MAX_CHILD_NODES = 1000;

	// The insertion sequence is:
	// textNode->minsNode->hourNode->dateNode->topicNode
	public static void InsertManyNodes(SharonTreeNode textNode, SharonTreeNode minsNode, SharonTreeNode hourNode,
			SharonTreeNode dateNode, SharonTreeNode topicNode) {
		InsertManyNodes(textNode, minsNode, hourNode, dateNode);
		topicNode.insert(dateNode, 0);
	}

	// The insertion sequence is: textNode->minsNode->hourNode
	public static void InsertManyNodes(SharonTreeNode textNode, SharonTreeNode minsNode, SharonTreeNode hourNode) {
		minsNode.insert(textNode, 0);
		hourNode.insert(minsNode, 0);
	}

	// The insertion sequence is: textNode->minsNode->hourNode->dateNode
	public static void InsertManyNodes(SharonTreeNode textNode, SharonTreeNode minsNode, SharonTreeNode hourNode,
			SharonTreeNode dateNode) {
		InsertManyNodes(textNode, minsNode, hourNode);
		dateNode.insert(hourNode, 0);
	}

	public static void DealWithOutOfOrders(SharonTreeNode topicRoot, SharonTreeNode dateNode, SharonTreeNode hourNode,
			SharonTreeNode minuteNode, SharonTreeNode textNode, boolean KeepUpdating) {
		boolean canFindRightFatherDateNode = false;

		if (topicRoot != null) {
			// If you can find the right father node, just put it in.
			for (int i = 0; i < topicRoot.getChildCount(); i++) {
				if (topicRoot.getChildAt(i).toString().equals(dateNode.toString())) {
					// Try to find the right hour node.
					DealWithOutOfOrders((SharonTreeNode) (topicRoot.getChildAt(i)), hourNode, minuteNode, textNode,
							KeepUpdating);
					canFindRightFatherDateNode = true;
					break;
				}
			}

			// Otherwise, build an appropriate fater node (or the topic node)
			// and sort the father node.
			if (canFindRightFatherDateNode == false) {

				InsertManyNodes(textNode, minuteNode, hourNode, dateNode, topicRoot);
				if (KeepUpdating) {
					TreeViewUtil.sortTree(topicRoot);
				}
			}
		}

	}

	public static void DealWithOutOfOrders(SharonTreeNode dateRoot, SharonTreeNode hourNode, SharonTreeNode minuteNode,
			SharonTreeNode textNode, boolean KeepUpdating) {

		boolean canFindRightFatherHourNode = false;

		if (dateRoot != null) {
			for (int i = 0; i < dateRoot.getChildCount(); i++) {
				if (dateRoot.getChildAt(i).toString().equals(hourNode.toString())) {
					// Try to find the right minute node.
					DealWithOutOfOrders((SharonTreeNode) (dateRoot.getChildAt(i)), minuteNode, textNode, KeepUpdating);
					canFindRightFatherHourNode = true;
					break;
				}
			}

			if (canFindRightFatherHourNode == false) {
				InsertManyNodes(textNode, minuteNode, hourNode, dateRoot);
				if (KeepUpdating) {
					TreeViewUtil.sortTree(dateRoot);
				}
			}
		}
	}

	public static void DealWithOutOfOrders(SharonTreeNode hourRoot, SharonTreeNode minuteNode, SharonTreeNode textNode,
			boolean KeepUpdating) {

		boolean canFindRightFatherMinuteNode = false;

		if (hourRoot != null) {
			for (int i = 0; i < hourRoot.getChildCount(); i++) {
				if (hourRoot.getChildAt(i).toString().equals(minuteNode.toString())) {
					((SharonTreeNode) hourRoot.getChildAt(i)).insert(textNode, 0);
					canFindRightFatherMinuteNode = true;
					break;
				}
			}

			if (canFindRightFatherMinuteNode == false) {
				InsertManyNodes(textNode, minuteNode, hourRoot);
				if (KeepUpdating) {
					TreeViewUtil.sortTree(hourRoot);
				}
			}
		}
	}

	public static void ReloadSpecificNode(SharonTreeNode node, JTree jTree, DefaultTreeModel treeModel,
			boolean KeepUpdating, boolean Reverse) {
		if (node != null && KeepUpdating) {
			String expansionState = getExpansionState(jTree);
			if (Reverse == false) {
				TreeViewUtil.sortTree(node);
			} else {
				TreeViewUtil.sortTreeReverse(node);
			}
			treeModel.reload(node);
			setExpansionState(expansionState, jTree);
		}
	}

	public static void ReloadNodeButNotItsChildren(SharonTreeNode node, JTree jTree, DefaultTreeModel treeModel,
			boolean KeepUpdating, boolean Reverse) {
		if (node != null && KeepUpdating) {
			String expansionState = getExpansionState(jTree);
			if (Reverse == false) {
				TreeViewUtil.TreeNodeSelectionSort(node);
			} else {
				TreeViewUtil.SelectionSortReverse(node);
			}
			treeModel.reload(node);
			setExpansionState(expansionState, jTree);
		}
	}

	public static void ReloadButKeepTheExpansionState(JTree jTree, DefaultTreeModel treeModel) {
		if (treeModel != null) {
			String expansionState = getExpansionState(jTree);
			treeModel.reload();
			setExpansionState(expansionState, jTree);
		}
	}

	public static String getExpansionState(JTree jTree) {

		StringBuilder sb = new StringBuilder("");

		if (jTree != null) {

			for (int i = 0; i < jTree.getRowCount(); i++) {
				TreePath tp = jTree.getPathForRow(i);
				if (jTree.isExpanded(i)) {
					sb.append(tp.toString());
					sb.append(",");
				}
			}

		}
		return sb.toString();
	}

	public static void setExpansionState(String s, JTree jTree) {

		if (jTree != null) {
			for (int i = 0; i < jTree.getRowCount(); i++) {
				TreePath tp = jTree.getPathForRow(i);
				if (s.contains(tp.toString())) {
					jTree.expandRow(i);
				}
			}
		}
	}

	// ************************************************************************************
	// The following is learned from StackOverFlow
	public static void sortTree(SharonTreeNode root) {
		if (root != null) {
			Enumeration e = root.depthFirstEnumeration();
			while (e.hasMoreElements()) {
				SharonTreeNode node = (SharonTreeNode) e.nextElement();
				if (!node.isLeaf()) {
					TreeNodeSelectionSort(node);
				}
			}
		}
	}

	public static Comparator<SharonTreeNode> tnc = new Comparator<SharonTreeNode>() {
		@Override
		public int compare(SharonTreeNode a, SharonTreeNode b) {
			// Sort the parent and child nodes separately:
			if (a.isLeaf() && !b.isLeaf()) {
				return 1;
			} else if (!a.isLeaf() && b.isLeaf()) {
				return -1;
			} else {
				String sa = a.getUserObject().toString();
				String sb = b.getUserObject().toString();
				return sa.compareToIgnoreCase(sb);
			}
		}
	};

	// descending order
	public static void TreeNodeSelectionSort(SharonTreeNode parent) {
		if (parent != null) {
			int n = parent.getChildCount();
			for (int i = 0; i < n - 1; i++) {
				int max = i;
				for (int j = i + 1; j < n; j++) {
					if (tnc.compare((SharonTreeNode) parent.getChildAt(max),
							(SharonTreeNode) parent.getChildAt(j)) < 0) {
						max = j;
					}
				}
				if (i != max) {
					MutableTreeNode a = (MutableTreeNode) parent.getChildAt(i);
					MutableTreeNode b = (MutableTreeNode) parent.getChildAt(max);
					parent.insert(b, i);
					parent.insert(a, max);
				}
			}
		}
	}

	public static void sortTreeReverse(SharonTreeNode root) {
		// TODO Auto-generated method stub
		if (root != null) {
			Enumeration e = root.depthFirstEnumeration();
			while (e.hasMoreElements()) {
				SharonTreeNode node = (SharonTreeNode) e.nextElement();
				if (!node.isLeaf()) {
					SelectionSortReverse(node);
				}
			}
		}
	}

	// ascending order
	public static void SelectionSortReverse(SharonTreeNode parent) {
		if (parent != null) {
			int n = parent.getChildCount();
			for (int i = 0; i < n - 1; i++) {
				int min = i;
				for (int j = i + 1; j < n; j++) {
					if (tnc.compare((SharonTreeNode) parent.getChildAt(min),
							(SharonTreeNode) parent.getChildAt(j)) > 0) {
						min = j;
					}
				}
				if (i != min) {
					MutableTreeNode a = (MutableTreeNode) parent.getChildAt(i);
					MutableTreeNode b = (MutableTreeNode) parent.getChildAt(min);
					parent.insert(b, i);
					parent.insert(a, min);
				}
			}
		}
	}
	// ************************************************************************************

	public static MouseAdapter Add_And_Return_MouseAdapter() {

		MouseAdapter ma = new MouseAdapter() {

			private void myPopupEvent(MouseEvent e) {
				int x = e.getX();
				int y = e.getY();
				JTree tree = (JTree) e.getSource();
				TreePath path = tree.getPathForLocation(x, y);
				if (path == null)
					return;

				tree.setSelectionPath(path);

				// I got this node.
				final Object obj = (Object) path.getLastPathComponent();

				// The menu
				String exportRawTweets = "Export as raw tweets";
				String exportTuplesTweets = "Export as tuples";
				JPopupMenu popup = new JPopupMenu();
				JMenuItem exportRaw = new JMenuItem(exportRawTweets);
				JMenuItem exportTuples = new JMenuItem(exportTuplesTweets);

				exportRaw.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						ExportTweets(obj, 'R');// 'R' means "Raw Tweets"
					}
				});

				exportTuples.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						ExportTweets(obj, 'T');// 'T' means "Tuples Tweets"
					}
				});

				popup.add(exportRaw);
				popup.add(exportTuples);
				popup.show(tree, x, y);
			}

			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger())
					myPopupEvent(e);
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger())
					myPopupEvent(e);
			}

		};

		return ma;
	}

	private static void ExportTweets(Object obj, char option) {

		JFrame parentFrame = new JFrame();
		String DeskTopPath = System.getProperty("user.home") + "/Desktop";

		String currentTimeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new java.util.Date());
		String fileName = "";

		if (option == 'R') {
			fileName = currentTimeStamp + "_RawTweets.txt";
		} else if (option == 'T') {
			fileName = currentTimeStamp + "_Tuples.txt";
		}

		File fileToSave = new File(DeskTopPath + "/" + fileName);

		JFileChooser fileChooser = new JFileChooser(fileToSave);
		fileChooser.setDialogTitle("Specify a directory to save");
		FileFilter filter = new FileNameExtensionFilter("Text File", "txt");
		fileChooser.setFileFilter(filter);
		fileChooser.setSelectedFile(fileToSave);

		int userSelection = fileChooser.showSaveDialog(parentFrame);

		if (userSelection == JFileChooser.APPROVE_OPTION) {

			fileToSave = new File(fileChooser.getCurrentDirectory().toString() + "/" + fileName);

			WriteIntoFile(obj, fileToSave, option);

			// Later, I will add a label in the middle bottom of the panel to
			// replace these "println"s.
			System.out.println("Save as file: " + fileToSave);
			parentFrame.setVisible(false);
		}

	}

	public static void WriteIntoFile(Object obj, File fileToSave, char option) {

		PrintWriter writer = null;
		SharonTreeNode node = (SharonTreeNode) obj;

		if (node.isLeaf()) {
			// Leaf text node
			Document d = ((SharonTreeNode) obj).getDocument();

			try {
				writer = new PrintWriter(fileToSave, "UTF-8");

				// Write into file here.
				if (option == 'R') {
					writer.println(DeleteSharonValue(d).toJson());
				} else if (option == 'T') {
					writer.println(ExtractTupleStrFromDocument(d));
				}

				writer.close();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

		} else if (!node.isLeaf()) {
			// If this node is not a leaf node, we should export all its leaf
			// nodes.

			try {
				writer = new PrintWriter(fileToSave, "UTF-8");

				Set<SharonTreeNode> leafNodes = node.getAllLeafNodes();

				for (SharonTreeNode leafNode : leafNodes) {

					Document tempDoc = ((SharonTreeNode) leafNode).getDocument();

					if (option == 'R') {
						writer.println(DeleteSharonValue(tempDoc).toJson());
					} else if (option == 'T') {
						writer.println(ExtractTupleStrFromDocument(tempDoc));
					}

				}

				writer.close();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

		} else {
			System.out.println("Not a valid tree node!");
		}

	}

	private static String ExtractTupleStrFromDocument(Document d) {
		// TIMESTAMP TWEETID USERID @SCREENNAME
		String str = d.getString("timestamp_ms") + " " + d.getLong("id") + " "
				+ ((Document) d.get("user")).getString("id_str") + " @"
				+ ((Document) d.get("user")).getString("screen_name");

		Document entities = ((Document) d.get("entities"));
		// [@MENTION]
		if (entities.get("user_mentions") != null) {
			// Solve the problem of multiple mentions
			List<Document> mentions = (List<Document>) entities.get("user_mentions");
			for (Document doc : mentions) {
				str += " @" + doc.getString("screen_name");
			}
		}
		// [#HASHTAG]
		if (entities.get("hashtags") != null) {
			// Solve the problem of multiple hashtags
			List<Document> hashtags = (List<Document>) entities.get("hashtags");
			for (Document doc : hashtags) {
				str += " #" + doc.getString("text");
			}
		}

		return str;
	}

	public static Document DeleteSharonValue(Document d) {
		// Delete some 'sharon' values
		if (d.get("sharon_track_id") != null) {
			d.remove("sharon_track_id");
		}
		if (d.get("sharon_tokens") != null) {
			d.remove("sharon_tokens");
		}
		if (d.get("sharon_topic_id") != null) {
			d.remove("sharon_topic_id");
		}
		return d;
	}

	public static String getText(Document d) {

		String tweet = d.getString("text");
		String id = d.getString("id_str");
		String user = ((Document) d.get("user")).getString("screen_name");

		String tweet_string = id + ": " + user + ": " + tweet;

		return tweet_string;
	}

	private static String MonthTransformer(String str) {

		String monthNo = "";

		if (str.equals("Jan")) {
			monthNo = "01";
		} else if (str.equals("Feb")) {
			monthNo = "02";
		} else if (str.equals("Mar")) {
			monthNo = "03";
		} else if (str.equals("Apr")) {
			monthNo = "04";
		} else if (str.equals("May")) {
			monthNo = "05";
		} else if (str.equals("Jun")) {
			monthNo = "06";
		} else if (str.equals("Jul")) {
			monthNo = "07";
		} else if (str.equals("Aug")) {
			monthNo = "08";
		} else if (str.equals("Sep")) {
			monthNo = "09";
		} else if (str.equals("Oct")) {
			monthNo = "10";
		} else if (str.equals("Nov")) {
			monthNo = "11";
		} else if (str.equals("Dec")) {
			monthNo = "12";
		}

		return monthNo;
	}

	public static String getDate(Document d) {

		String originalStr = d.getString("created_at");
		String month = MonthTransformer(originalStr.substring(4, 7));

		String date = originalStr.substring(8, 10);
		String year = originalStr.substring(originalStr.length() - 4, originalStr.length());

		return year + "/" + month + "/" + date;
	}

	public static String getHour(Document d) {

		String originalStr = d.getString("created_at");
		return originalStr.substring(11, 13);
	}

	public static String getMins(Document d) {

		String originalStr = d.getString("created_at");
		return originalStr.substring(14, 16);
	}
}
