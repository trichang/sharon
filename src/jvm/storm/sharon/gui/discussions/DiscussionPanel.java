package storm.sharon.gui.discussions;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DiscussionPanel {
	
}
/*
@SuppressWarnings("serial")
public class DiscussionPanel extends SelectableListPanel implements Observer {
	private static Logger log = LogManager.getLogger(DiscussionPanel.class.getName());
	
	
	private HashMap<Integer,DefaultTreeModel> treeModels;
	private HashMap<Integer,JTree> jTrees;
	private HashMap<Integer,SharonTreeNode> roots;

	private HashMap<Integer,JScrollPane> scrollPanes;

	

	private static boolean KeepUpdating = true;

	public DiscussionPanel() {
		super();
		


		topicListBox.setVisible(true);
		
		toggleButton.setText("Toggle Updating");



		roots = new HashMap<Integer,SharonTreeNode>();
		scrollPanes = new HashMap<Integer,JScrollPane>();
		jTrees = new HashMap<Integer,JTree>();
		treeModels = new HashMap<Integer,DefaultTreeModel>();

	
		
		ArrayList<Topic> topics = RapidAPI.getUserTopicList();
		for (Topic topic : topics) {
			int topic_id = topic.getTopicId();
			String topicName = topic_id + ":" + topic.getTopicName();
			
			SharonTreeNode topicNode = new SharonTreeNode(topicName);

			DefaultTreeModel treeModel = new DefaultTreeModel(topicNode);
			JTree jTree = new JTree(treeModel);
		
			jTree.addMouseListener(TreeViewUtil.Add_And_Return_MouseAdapter());

			JScrollPane scrollPane = new JScrollPane(jTree);

			if (topic_id == current_topic_id) {
				
				add(scrollPane, BorderLayout.CENTER);
			} else {
				
			}

			roots.put(topic.getTopicId(),topicNode);
			scrollPanes.put(topic.getTopicId(),scrollPane);
			treeModels.put(topic.getTopicId(),treeModel);
			jTrees.put(topic.getTopicId(),jTree);
		}
		Prompter.setText("Click Toggle Updating to pause.");
		
		try{
			model.setSelectedItem(topicList.get(current_topic_id));
		} catch (ArrayIndexOutOfBoundsException ae){
			log.error("ArrayIndexOutOfBoundsException: " + ae.getMessage());
		}
		// We are now ready to receive events
		RapidAPI.getTweetReader().register(this);
		RapidAPI.getKeywordTopicReader().register(this);

	}

	public void update(Observable o, final Object arg) {
		Document d = (Document) arg;

		if (d.get("command_name") != null) {

			if (d.getString("command_name").equals("NEW_TOPIC")) {
				final Document doc = d;
				
				int topic_id = doc.getInteger("topic_id");
				String topicName = topic_id + ":"+ doc.getString("topic_name");
				SharonTreeNode topicNode = new SharonTreeNode(topicName);

				DefaultTreeModel treeModel = new DefaultTreeModel(topicNode);
				JTree jTree = new JTree(treeModel);
				jTree.addMouseListener(TreeViewUtil.Add_And_Return_MouseAdapter());

				JScrollPane scrollPane = new JScrollPane(jTree);
				add(scrollPane, BorderLayout.CENTER);
				scrollPane.setVisible(false);

				roots.put(topic_id,topicNode);

				scrollPanes.put(topic_id,scrollPane);
				treeModels.put(topic_id,treeModel);
				jTrees.put(topic_id,jTree);

				addTopic(d.getInteger("topic_id"), d.getString("topic_name"));
			}
			if (d.getString("command_name").equals("DELETE_TOPIC")) {
				int topic_id = d.getInteger("topic_id");
				if(topic_id==current_topic_id){
					scrollPanes.get(topic_id).setVisible(false);
					current_topic_id=-1;
				}
				roots.remove(topic_id);
				scrollPanes.remove(topic_id);
				treeModels.remove(topic_id);
				jTrees.remove(topic_id);
				deleteTopic(d.getInteger("topic_id"), d.getString("topic_name"));
				scrollPanes.get(current_topic_id).setVisible(true);
			}

		} else if (d.getInteger("sharon_topic_id") != null && roots != null ) {

			int topic_id = d.getInteger("sharon_topic_id");
			

			SharonTreeNode root = roots.get(topic_id);
			JTree jTree = jTrees.get(topic_id);
			DefaultTreeModel treeModel = treeModels.get(topic_id);

			String text = TreeViewUtil.getText(d);
			String dateStr = TreeViewUtil.getDate(d);
			String hour = TreeViewUtil.getHour(d);
			String minute = TreeViewUtil.getMins(d);
			String minuteStr = hour + ":" + minute;

			if (root != null) {
				if (root.getAllLeafNodes().size() > TreeViewUtil.MAX_CHILD_NODES / 10) {

					for (int i = 0; i < root.getChildCount(); i++) {
						if (!root.getChildAt(i).isLeaf()) {
							((SharonTreeNode) root.getChildAt(i)).removeOldestLeafNode();
						}
					}
				}
			} else {
				log.error("DiscussionPanel could not find topic.");
			}

			int temp_topic_id = d.getInteger("sharon_topic_id");
			// SharonTreeNode topicNode = topicNodes.get(temp_topic_id);
			SharonTreeNode repliedTopicNode = roots.get(temp_topic_id);

			SharonTreeNode newTextNode = new SharonTreeNode("Reply:0 " + dateStr + " " + minuteStr + " " + text,d);

			if (d.getString("in_reply_to_status_id_str") != null) {

				String reply_id = d.getString("in_reply_to_status_id_str");

				boolean foundInRepliedNodes = false;
				Enumeration<SharonTreeNode> e = repliedTopicNode.depthFirstEnumeration();

				while (e.hasMoreElements()) {

					SharonTreeNode tempNode = (SharonTreeNode) e.nextElement();

					if (tempNode.getDocument() != null) {

						String tempStrID = tempNode.getDocument().getString("id_str");

						if (tempStrID.equals(reply_id)) {

							String updatedParentName = "Reply:" + (tempNode.getReplyCount() + 1) + ""
									+ tempNode.toString().substring(tempNode.toString().indexOf(" "),
											tempNode.toString().length());
							tempNode.setUserObject(updatedParentName);
							tempNode.setReplyCount(tempNode.getReplyCount() + 1);
							tempNode.insert(newTextNode, 0);

							// Update tempNode's parents
							SharonTreeNode parent = (SharonTreeNode) tempNode.getParent();
							while (!parent.toString().equals(repliedTopicNode.toString())) {
								String updatedName = "Reply:" + (parent.getReplyCount() + 1) + ""
										+ parent.toString().substring(parent.toString().indexOf(" "),
												parent.toString().length());
								parent.setUserObject(updatedName);
								parent.setReplyCount(parent.getReplyCount() + 1);
								parent = (SharonTreeNode) parent.getParent();
							}

							TreeViewUtil.ReloadSpecificNode(repliedTopicNode, jTree, treeModel, KeepUpdating,false);

							foundInRepliedNodes = true;
							break;
						}
					}

				}

				if (!foundInRepliedNodes) {

					
					SharonTreeNode tempTopicNode = (SharonTreeNode) TreeViewPanel.getOutRoots().get(temp_topic_id);

					for (SharonTreeNode tempNode : tempTopicNode.getAllLeafNodes()) {

						if (tempNode.getDocument() != null) {
							String tempStrID = tempNode.getDocument().getString("id_str");
						
							if (tempStrID.equals(reply_id)) {
								
								String tempNodeName = tempNode.toString();
								String updatedNodeName = "Reply:" + (tempNode.getReplyCount() + 1) + " "
										+ tempNodeName;
								SharonTreeNode replyCountUpdated = new SharonTreeNode(updatedNodeName,
										tempNode.getDocument(), tempNode.getReplyCount() + 1);
										

								// treeModel.removeNodeFromParent(tempNode);
								replyCountUpdated.insert(newTextNode, 0);
								roots.get(temp_topic_id).insert(replyCountUpdated, 0);
								
								TreeViewUtil.ReloadSpecificNode(repliedTopicNode, jTree, treeModel,KeepUpdating, false);
								break;
							}
						}
					}
				}

			} 

		}		
	}

	public void actionPerformed(final ActionEvent e) {
		String topic_name = ((Topic)topicListBox.getSelectedItem()).getTopicName();
		Integer topic_id = ((Topic)topicListBox.getSelectedItem()).getTopicId();
		if (e.getSource() == toggleButton) {
			// I have changed displayButton to start/stop updating button.
			if (KeepUpdating == false) {

				TreeViewUtil.ReloadButKeepTheExpansionState(jTrees.get(current_topic_id),treeModels.get(current_topic_id));
				Prompter.setText("Click Toggle Updating to pause.");
			} else {
				Prompter.setText("Updating is paused. Click Toggle Updating to resume.");
			}

			KeepUpdating = !KeepUpdating;
		} else {
			// ComboBox Control
			if (topic_id != current_topic_id) {

				scrollPanes.get(current_topic_id).setVisible(false);
				current_topic_id = topic_id;
				add(scrollPanes.get(current_topic_id), BorderLayout.CENTER);
				scrollPanes.get(current_topic_id).setVisible(true);
				// listData = dataMap.get(current_topic_id);
				// listbox.setListData(listData);
				revalidate();
				repaint();

			}
		}
	}

}
*/