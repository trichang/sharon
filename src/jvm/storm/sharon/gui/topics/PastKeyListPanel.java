package storm.sharon.gui.topics;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextAttribute;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.bson.Document;






import storm.sharon.gui.core.RapidAPI;
import storm.sharon.gui.core.WrapLayout;
import storm.sharon.util.Keyword;
import storm.sharon.util.TopicKeywordHandler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("serial")
public class PastKeyListPanel extends JPanel implements Observer, ActionListener, ListSelectionListener {
	private static Logger log = LogManager.getLogger(PastKeyListPanel.class.getName()); 
    private Integer current_topic_id = 0;
    
    protected	JList		listbox;
	protected	Vector		listData;
	protected	JButton		addButton;
	protected	JButton		titleButton;
	protected	JButton		removeButton;
	protected	JButton		whitelistButton;
	protected	JButton		blacklistButton;
	//protected	JTextField	dataField;
	protected	JScrollPane scrollPane;

    
    
	@SuppressWarnings("unchecked")
	public PastKeyListPanel () {
		//super();	
		displayPanel();

		listbox.setCellRenderer(new DefaultListCellRenderer() {
			@Override
            public Component getListCellRendererComponent(JList list, Object value, int index,
                      boolean isSelected, boolean cellHasFocus) {
				Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                 
                	 if (value instanceof Keyword) {
                         Keyword keyword = (Keyword) value;
                         String text = keyword.getKeyword();
                         if (keyword.getIsBlacklisted() != null){
	                         if (keyword.getIsBlacklisted()) {
	                             //setBackground(Color.BLUE);
	                        	 
	                        	 Font font = new Font("Verdana", Font.PLAIN, 16);
	                        	 Map  attributes = font.getAttributes();
	                        	 attributes.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON);
	                        	 Font newFont = new Font(attributes);
	                        	 setFont(newFont);
	                        	 setForeground(Color.BLACK);
	                        	 
	                         } else {
	                        	 Font font = new Font("Verdana", Font.PLAIN, 16);
	                       	     setFont(font);
	                       	  	setForeground(Color.BLACK);
	                       	  
	                         }
	                         setText(text);
	                         if (isSelected) {
	                              setBackground(getBackground().darker());
	                         }
                         }
                         
                    } 
                 return c;
                 
            }
			 
		});
		
		RapidAPI.getKeywordTopicReader().register(this);
	}
	
	
	
	@SuppressWarnings("unchecked")
	public void displayKeyList(Integer topic_id) {
		this.current_topic_id = topic_id;
		listData.removeAllElements();
		Document doc = RapidAPI.getTopicKeywordHandler().getKeywords(topic_id);
		//System.out.println("PastKeyListPanel Current Topic: " + current_topic_id + " Keywords: " + doc);
		List<Document> keywords = (List<Document>) doc.get("keywords");
		if (keywords != null) {
			for (Document keyword: keywords) {
				Boolean isCurrent = keyword.getBoolean("isCurrent");
				if (!isCurrent) {
					Keyword k = new Keyword(keyword);
					//Keyword k = new Keyword(keyword.getString("keyword"),
						//	keyword.getInteger("type"), keyword.getBoolean("isCurrent"));
					listData.addElement(k);
					//listData.addElement(keyword.getString("keyword") + " " 
					//		+ keyword.getInteger("type"));
				}
			}
			
			Collections.sort(listData);
		}
		listbox.setListData( listData );
		scrollPane.revalidate();
		scrollPane.repaint();
	}

	// Handler for button presses
	public void actionPerformed( ActionEvent event )
	{
		if( event.getSource() == addButton ) 
		{
			log.trace("Add button pressed");
			// Get the current selection
			int selection = listbox.getSelectedIndex();
			if( selection >= 0 )
			{   
				Keyword s = (Keyword) listData.get(selection);
				String keyword = s.getKeyword();
				log.info("Adding non-current keyword: " + keyword);
				Document doc = new Document("command_name", "ADD_NONCURRENT_KEYWORDS");
				doc.append("topic_id",  current_topic_id);
				doc.append("command_type", "TOPIC_MGMT");
				ArrayList<String> keywords = new ArrayList<String>();
				keywords.add(keyword);
				doc.append("keywords", keywords);
				RapidAPI.getProducer().sendMessage(doc);
							
			}
		}
				
		if( event.getSource() == blacklistButton ) 
		{
			log.trace("Blacklist button pressed");
			// Get the current selection
			int selection = listbox.getSelectedIndex();
			if( selection >= 0 )
			{   
				Keyword s = (Keyword) listData.get(selection);
				String keyword = s.getKeyword();
				log.info("Blacklisting non-current keyword: " + keyword);
				Document doc = new Document("command_name", "BLACKLIST_NONCURRENT_KEYWORDS");
				doc.append("topic_id",  current_topic_id);
				doc.append("command_type", "TOPIC_MGMT");
				ArrayList<String> keywords = new ArrayList<String>();
				keywords.add(keyword);
				doc.append("keywords", keywords);
				RapidAPI.getProducer().sendMessage(doc);
							
			}
		}
		
		if( event.getSource() == whitelistButton )
		{
			// Get the current selection
			log.trace("Whitelist button pressed");
			int selection = listbox.getSelectedIndex();
			if( selection >= 0 )
			{   
				Keyword s = (Keyword) listData.get(selection);
				String keyword = s.getKeyword();
				log.info("Whitelisting keyword: " + keyword);
				Document doc = new Document("command_name", "WHITELIST_KEYWORDS");
				doc.append("topic_id",  current_topic_id);
				doc.append("command_type", "TOPIC_MGMT");
				ArrayList<String> keywords = new ArrayList<String>();
				keywords.add(keyword);
				doc.append("keywords", keywords);
				RapidAPI.getProducer().sendMessage(doc);
				
			}
		}
	}
	
	public void valueChanged( ListSelectionEvent event ) {
		// See if this is a listbox selection and the
		// event stream has settled
		/*System.out.println("KeyListPanel valueChanaged event ");  
		if( event.getSource() == listbox
							&& !event.getValueIsAdjusting() ) {
			// Get the current selection and place it in the
			// edit field
			//String stringValue = (String)listbox.getSelectedValue();
			Keyword s = (Keyword) listbox.getSelectedValue();
			System.out.println("KeyListPanel displaying keywords ");  
			if (s != null) {
				String keyword = s.getKeyword();
				if( keyword != null )
					dataField.setText( keyword );
			}
		}*/
	}


	@Override
	public void update(Observable o, Object arg) {
		
		// No action is required because the update method of the TopicList panel will
		// update the keylist
		
	}
	
	
	public  void displayPanel() {
		/*
		 * The main panel (this) contains a list box, with a control panel south.
		 */
		Border lineBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"Past keywords");
		setBorder(lineBorder);
		setToolTipText("Past Keywords are not currently tracked, blacklisted keywords will never be tracked.");
		setLayout( new BorderLayout());
		
		
		// Create the data model for this example
		listData = new Vector();
		//listData.add("Test Topic");

		// Create a new listbox control
		listbox = new JList( listData );
		listbox.addListSelectionListener( this );

		// Add the listbox to a scrolling pane
		scrollPane = new JScrollPane();
		scrollPane.getViewport().add( listbox );
		add( scrollPane, BorderLayout.CENTER );
		
		/*
		 * The control panel contains the past keyword list controls.
		 */
				
		JPanel controlPanel = new JPanel();
		Border lineBorder2 = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"Past keyword controls");
		controlPanel.setBorder(lineBorder2);
		controlPanel.setLayout( new GridLayout(0,3));
		add( controlPanel, BorderLayout.SOUTH );

		// Create some function buttons
		addButton = new JButton( "Make Current" );
		controlPanel.add( addButton);
		addButton.addActionListener( this );
		addButton.setToolTipText("Move the selected keyword to the Current Keywords list.");

		blacklistButton = new JButton( "Blacklist" );
		controlPanel.add( blacklistButton);
		blacklistButton.addActionListener( this );
		blacklistButton.setToolTipText("Blacklist the selected keyword.");
		
		whitelistButton = new JButton( "Whitelist" );
		controlPanel.add( whitelistButton);
	    whitelistButton.addActionListener( this );
	    whitelistButton.setToolTipText("Unblacklist the selected keyword.");
			
	
	}
	
}
