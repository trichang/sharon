package storm.sharon.gui.topics;

import java.awt.GridLayout;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import storm.sharon.util.Topic;


//import org.bson.Document;


@SuppressWarnings("serial")
public class TopicConfigurationPanel extends JPanel {
	
	public TopicConfigurationPanel(Topic t) {
	
		 super(new GridLayout(1, 1));
	
		 JTabbedPane tabbedPane = new JTabbedPane();
	    
		 
	     JComponent topicKeywordPanel = new CustomTrackPanel(t);
	     tabbedPane.addTab("Track Criteria", null, topicKeywordPanel,"Unimplemented");
	     
	     JComponent tweetPanel = new CustomQueryPanel(t);
	     tabbedPane.addTab("Custom Queries", null, tweetPanel,"Unimplemented");
	     
	     add(tabbedPane);
         
	     //The following line enables to use scrolling tabs.
	     tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
	}

 }
	