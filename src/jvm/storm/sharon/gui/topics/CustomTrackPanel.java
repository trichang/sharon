package storm.sharon.gui.topics;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.bson.Document;

import storm.sharon.Rapid;
import storm.sharon.util.Keyword;
import storm.sharon.util.Query;
import storm.sharon.util.Topic;
import storm.sharon.util.TopicKeywordHandler;


public class CustomTrackPanel extends JPanel implements Observer, ActionListener, ListSelectionListener {
	
    private Integer current_topic_id = 0;
    private String dbInfo;
    
    protected	JList		listbox;
	protected	Vector		listData;
	protected	JButton		addButton;
	protected	JButton		removeButton;
	protected	JButton		blacklistButton;
	protected	JTextField	dataField;
	protected	JScrollPane scrollPane;

    
    
	@SuppressWarnings({ "unchecked", "serial" })
	public CustomTrackPanel (Topic t) {
		//super();	
		displayPanel();
		//this.dbInfo = dbInfo;
		//GuiApp1.getKeywordTopicReader().register(this);
		

		listbox.setCellRenderer(new DefaultListCellRenderer() {
			@SuppressWarnings("rawtypes")
			@Override
            public Component getListCellRendererComponent(JList list, Object value, int index,
                      boolean isSelected, boolean cellHasFocus) {
                 Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                 if (value instanceof Query) {
                      Query q = (Query) value;
                      setText(q.getQueryName());
                      if (q.isCurrent()) {
                          //setBackground(Color.BLUE);
                    	  setForeground(Color.GREEN);
                      } else {
                          // setBackground(Color.RED);
                    	  setForeground(Color.RED);
                      } 
                 
                      if (isSelected) {
                           setBackground(getBackground().darker());
                      }
                 } else {
                      setText("whodat?");
                 }
                 return c;
            }
			 
		});
	}
	
	
	
	public void displayKeyList(Integer topic_id) {
/*		this.current_topic_id = topic_id;
		listData.removeAllElements();
		Document doc = TopicKeywordHandler.getInstance(dbInfo).getKeywords(topic_id);
		System.out.println("Current Topic: " + current_topic_id + " Keywords: " + doc);
		List<Document> keywords = (List<Document>) doc.get("keywords");
		if (keywords != null) {
			for (Document keyword: keywords) {
				Boolean isCurrent = keyword.getBoolean("isCurrent");
				if (isCurrent) {
					Keyword k = new Keyword(keyword);
					//Keyword k = new Keyword(keyword.getString("keyword"),
						//	keyword.getInteger("type"), keyword.getBoolean("isCurrent"));
					listData.addElement(k);
					//listData.addElement(keyword.getString("keyword") + " " 
					//		+ keyword.getInteger("type"));
				}
			}
			
			Collections.sort(listData);
		}
		listbox.setListData( listData );
		scrollPane.revalidate();
		scrollPane.repaint();*/
	}

	// Handler for button presses
	public void actionPerformed( ActionEvent event )
	{
		if( event.getSource() == addButton ) 
		{
			System.out.println("Add button got pressed");
			// Get the text field value
			String stringValue = dataField.getText();
			dataField.setText( "" );
			Query q = new Query(stringValue, 0);
			listData.add(q);
			listbox.setListData( listData );
			scrollPane.revalidate();
			scrollPane.repaint();
		}
								
		if( event.getSource() == removeButton )
		{
			// Get the current selection
			int selection = listbox.getSelectedIndex();
			if( selection >= 0 )
			{   
				Query s = (Query) listData.get(selection);
				if ( s.isCurrent()) {
					s.setCurrent(false);
				} else {
					s.setCurrent(true);
				}
			}
			listbox.setListData( listData );
			scrollPane.revalidate();
			scrollPane.repaint();
		}
	
	}
	
	public void valueChanged( ListSelectionEvent event ) {
		// See if this is a listbox selection and the
		// event stream has settled
		if( event.getSource() == listbox
							&& !event.getValueIsAdjusting() ) {
			// Get the current selection and place it in the
			// edit field
			//String stringValue = (String)listbox.getSelectedValue();
			Query q = (Query) listbox.getSelectedValue();
			if (q != null) {
				String queryString = q.getQueryName();
				if( queryString != null )
					dataField.setText( queryString );
			}
		}
	}


	@Override
	public void update(Observable o, Object arg) {
		
		// No action is required because the update method of the TopicList panel will
		// update the keylist
		//System.out.println("BBBBB Update method of KeyListPanel: Current Topic ID: " 
		//										+ current_topic_id);
		//displayKeyList(current_topic_id);
	}
	
	
	public  void displayPanel() {
		
		setLayout( new BorderLayout());
		// Create the data model for this example
		listData = new Vector();
		//listData.add("Test Topic");

		// Create a new listbox control
		listbox = new JList( listData );
		listbox.addListSelectionListener( this );

		// Add the listbox to a scrolling pane
		scrollPane = new JScrollPane();
		scrollPane.getViewport().add( listbox );
		add( scrollPane, BorderLayout.CENTER );
				
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout( new BorderLayout() );
		add( dataPanel, BorderLayout.SOUTH );

		// Create some function buttons
		addButton = new JButton( "Add" );
		dataPanel.add( addButton, BorderLayout.WEST );
		addButton.addActionListener( this );

		removeButton = new JButton( "Activate/Deactivate" );
		//dataPanel.add( removeButton, BorderLayout.EAST );
		removeButton.addActionListener( this );
		
		
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		//buttonPanel.add(blacklistButton, BorderLayout.WEST);
		buttonPanel.add(removeButton, BorderLayout.EAST);
		dataPanel.add( buttonPanel, BorderLayout.EAST);
		
		

		// Create a text field for data entry and display
		dataField = new JTextField();
		dataField.setText(null);
		dataPanel.add( dataField, BorderLayout.CENTER );
	}
	
}
