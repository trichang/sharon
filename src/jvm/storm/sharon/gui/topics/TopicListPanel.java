package storm.sharon.gui.topics;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.bson.Document;

import storm.sharon.Rapid;
import storm.sharon.gui.core.RapidAPI;
import storm.sharon.util.Topic;
import storm.sharon.util.TopicKeywordHandler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@SuppressWarnings("serial")
public class TopicListPanel extends JPanel implements Observer, ActionListener, ListSelectionListener{
	private static Logger log = LogManager.getLogger(TopicListPanel.class.getName());
	private KeyListPanel keyListPanel = null;
	private PastKeyListPanel pastKeyListPanel = null;
	private PluginsPanel pluginsPanel = null;
	
	private int currentTopicId = 0;
	@SuppressWarnings("rawtypes")
	private	JList		listbox;
	private	Vector<Topic>		listData;
	private	JButton		addButton;
	private JButton		deleteButton;
	private	JButton		startStopButton;
	private	JButton		configureButton;
	private	JTextField	dataField;
	private	JScrollPane scrollPane;
	private Topic	selectedTopic;
	private boolean complete = true;
	private Integer topicType = 0;
	
	
	public JPanel getKeyListPanel() {
		return keyListPanel;
	}


	public JPanel getPastKeyListPanel() {
		return pastKeyListPanel;
	}
	
	public void setTopicType(Integer t) {
		this.topicType = t;
	}

	@SuppressWarnings("unchecked")
	public TopicListPanel (KeyListPanel keyListPanel, PastKeyListPanel pastKeyListPanel, PluginsPanel pluginsPanel) {
		displayPanel();
		
		this.keyListPanel = keyListPanel;
		this.pastKeyListPanel = pastKeyListPanel;
		this.pluginsPanel = pluginsPanel;
		
		displayTopics(-1);
		
		
		listbox.setCellRenderer(new DefaultListCellRenderer() {
			@SuppressWarnings("rawtypes")
			@Override
            public Component getListCellRendererComponent(JList list, Object value, int index,
                      boolean isSelected, boolean cellHasFocus) {
                 Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                 
                 if (value instanceof Topic) {
                      Topic topic = (Topic) value;
                      String text = topic.getTopicName();
                      if(topic.getTopicType()==Topic.PUBLIC_TOPIC){
                    	  text+=" [public,";
                      } else if(topic.getTopicType()==Topic.PRIVATE_TOPIC){
                    	  text+=" [private,";
                      } else if(topic.getTopicType()==Topic.SHARED_TOPIC){
                    	  text+=" [shared,";
                      }
                      text+=topic.getTopicOwner();
                      if (topic.isCurrent()) {
                    	  setForeground(new Color(0, 0, 0));
                    	  Font font = new Font("Verdana", Font.PLAIN, 16);
                    	  setFont(font);
                    	  text+="]";
                      } else {
                    	  Font font = new Font("Verdana", Font.ITALIC, 14);
                    	  setFont(font);
                    	  setForeground(new Color(50, 50, 50));
                    	  text+=",not tracking]";
                      }
                      setText(text);
                      if (isSelected) {
                           setBackground(getBackground().darker());
                      }
                 } 
                 return c;
            } 
		});
		
		RapidAPI.getKeywordTopicReader().register(this);
	}
	
	public void setComplete(boolean comp) {
		complete = comp;
	}
	
	public boolean getComplte() {
		return complete;
	}
	
	@SuppressWarnings("unchecked")
	private void displayTopics(int topicId) {
		listData.removeAllElements();
		selectedTopic = null;
		ArrayList<Topic> topicList = RapidAPI.getUserTopicList();
		int currentTopicIndex = -1;
		int i = 0;
		for (Topic t: topicList) {
			if (t.getTopicId() == topicId) {
				currentTopicIndex = i;
			}
			listData.addElement(t);
			i++;
		}
		if (currentTopicIndex == -1) {
			currentTopicIndex = 0;
		}
		listbox.setListData( listData );
		listbox.setSelectedIndex(currentTopicIndex);
	
	}
	
	public void valueChanged( ListSelectionEvent event ){
		// See if this is a listbox selection and the
		// event stream has settled
		if( event.getSource() == listbox) {
			// Get the current selection and place it in the
			// edit field
			if (listbox.getSelectedValue() != null) {
				selectedTopic = (Topic) listbox.getSelectedValue();
				currentTopicId = selectedTopic.getTopicId();
			} else {
				selectedTopic = null;
			}
	
			if( selectedTopic != null ) {
				keyListPanel.displayKeyList(currentTopicId);	
				pastKeyListPanel.displayKeyList(currentTopicId);
				pluginsPanel.displayPluginList(currentTopicId);
				
			}
		}
	}
	
	// Handler for button presses
	public void actionPerformed( ActionEvent event )
	{
		if( event.getSource() == addButton ){
			// Get the text field value
			String stringValue = dataField.getText();
			log.info("User adds topic: "+stringValue);
			dataField.setText( "" );
						
			
			if( stringValue != null &&  !stringValue.equals(""))
			{
				// Display a dialog box for topic type
				String[] values = {"Public", "Private"};
				Object selected = JOptionPane.showInputDialog(null, "Topic Type", "Selection", JOptionPane.DEFAULT_OPTION, null, values, "private");
				log.debug("Topic type: " + topicType);
				
				String selectedString;
				if ( selected != null ){//null if the user cancels. 
				    selectedString = selected.toString();
				} else {
				   log.info("User cancelled topic add operation.");
				   return;
				}
				
				if ( selectedString.equalsIgnoreCase("Private")) {
					topicType = 1;
				} else if ( selectedString.equalsIgnoreCase("Public")) {
					topicType = 0;
				}
				
				Document doc = new Document("command_name", "ADD_TOPIC");
				doc.append("command_type", "TOPIC_MGMT");
				doc.append("topic_name",  stringValue);
				doc.append("topic_type", topicType);
				doc.append("topic_owner", RapidAPI.getAuthenticationData().getRapid_userName());
				RapidAPI.getProducer().sendMessage(doc);
			}
		} else if( event.getSource() == startStopButton ) {
			// Get the current selection
			int selection = listbox.getSelectedIndex();			
			
			if( selection >= 0 )
			{   
				Topic t = listData.get(selection);
				String topicName = t.getTopicName();
				Integer topicId = t.getTopicId();
				Boolean isCurrent = t.isCurrent();
				
				Document doc = new Document("topic_name", topicName);
				doc.append("topic_id",  topicId);
				if (isCurrent) {
					doc.append("command_name", "STOP_TOPIC");
				} else {
					doc.append("command_name", "START_TOPIC");
				}
				doc.append("command_type", "TOPIC_MGMT");
				
				RapidAPI.getProducer().sendMessage(doc);
					
			}

		} else if( event.getSource() == deleteButton ) {
			// Get the current selection
			int selection = listbox.getSelectedIndex();			
			
			if( selection >= 0 )
			{   
				Topic t = listData.get(selection);
				String topicName = t.getTopicName();
				Integer topicId = t.getTopicId();
				
				
				Document doc = new Document("topic_name", topicName);
				doc.append("topic_id",  topicId);
				doc.append("command_name", "DELETE_TOPIC");
				doc.append("command_type", "TOPIC_MGMT");
				RapidAPI.getProducer().sendMessage(doc);
					
			}
		} else if( event.getSource() == configureButton ) {

			JFrame guiFrame1 = new JFrame();  
			Topic topic = RapidAPI.getTopicKeywordHandler().getTopic(currentTopicId);

			guiFrame1.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); 
			String title = Rapid.productName()+": Topic Configuration - " + topic.getTopicName();
			guiFrame1.setIconImages(Rapid.getIcons());
			guiFrame1.setTitle(title);
			guiFrame1.setSize(1000,500);  
			
			guiFrame1.setLocationRelativeTo(null); 
			
			guiFrame1.add(new TopicConfigurationPanel(topic), BorderLayout.CENTER);
			
			guiFrame1.setVisible(true);
		}
	}


	@Override
	public void update(Observable arg, Object arg1) {
		//Document doc = (Document)arg1;
		displayTopics(currentTopicId);
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public  void displayPanel() {
		
		/*
		 * The main panel (this) contains a list box, with a control panel south.
		 */
		Border lineBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"Available topics");
		setBorder(lineBorder);
		setLayout( new BorderLayout());
		setToolTipText("Shows a list of available topics and topic controls.");
		
		// Create the data model for this example
		listData = new Vector<Topic>();

		// Create a new listbox control
		listbox = new JList( listData );
		listbox.addListSelectionListener( this );

		// Add the listbox to a scrolling pane
		scrollPane = new JScrollPane();
		scrollPane.getViewport().add( listbox );
		add( scrollPane, BorderLayout.CENTER );
		
		/*
		 * The control panel contains the topic controls.
		 */
		
		JPanel controlPanel = new JPanel();
		Border lineBorder2 = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"Topic controls");
		controlPanel.setBorder(lineBorder2);
		controlPanel.setLayout( new BorderLayout() );
		add( controlPanel, BorderLayout.SOUTH );

		// Create add topic button and text field panel
		JPanel topicAddPanel = new JPanel();
		topicAddPanel.setLayout(new BorderLayout());
		addButton = new JButton( "Add Topic..." );
		addButton.addActionListener( this );
		addButton.setToolTipText("Add a new topic to be tracked.");
		topicAddPanel.add( addButton, BorderLayout.LINE_START);
		
		// Create a text field for data entry and display
		dataField = new JTextField();
		dataField.setText(null);
		dataField.setToolTipText("Type the name of a topic to add.");
		dataField.setName("Topic name");
		topicAddPanel.add( dataField, BorderLayout.CENTER);


		controlPanel.add(topicAddPanel,BorderLayout.PAGE_START);
		
		
		configureButton = new JButton( "Configure Topic..." );
		configureButton.addActionListener( this );
		configureButton.setEnabled(false);

		startStopButton = new JButton( "Toggle Tracking" );

		startStopButton.addActionListener( this );
		startStopButton.setToolTipText("Toggle selected topic between tracking and not tracking.");

		deleteButton = new JButton( "Delete Topic" );

		deleteButton.addActionListener( this );
		deleteButton.setToolTipText("Delete selected topic - WARNING: instant and cannot be undone.");
		
		JPanel buttonPanel = new JPanel();

		buttonPanel.setLayout(new GridLayout(0,3));
		buttonPanel.add(configureButton);
		buttonPanel.add(startStopButton);
		buttonPanel.add(deleteButton);
		controlPanel.add( buttonPanel, BorderLayout.CENTER);

	}

}
