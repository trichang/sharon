package storm.sharon.gui.topics;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import storm.sharon.util.Topic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("rawtypes")
public class TopicComboBoxRenderer implements ListCellRenderer {
	private static Logger log = LogManager.getLogger(TopicComboBoxRenderer.class.getName());
	protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {

		String theText = null;

		JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected,cellHasFocus);

		if (value instanceof Topic) {
			Topic t = (Topic) value;
			theText = t.getTopicName();
		}

		renderer.setText(theText);

		return renderer;
	}
}
