package storm.sharon.gui.topics;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.bson.Document;

import storm.sharon.Rapid;
import storm.sharon.util.Keyword;
import storm.sharon.util.Query;
import storm.sharon.util.Topic;
import storm.sharon.util.TopicKeywordHandler;


public class CustomQueryPanel extends JPanel implements Observer, ActionListener, ListSelectionListener {

    protected	JList		listbox;
	protected	Vector		listData;
	protected	JButton		newButton;
	protected	JButton		startStopButton;
	protected	JButton		subscribeButton;
	protected	JScrollPane scrollPane;
    private Topic topic;
    
    
	@SuppressWarnings({ "unchecked", "serial" })
	public CustomQueryPanel (Topic t) {
	
		topic = t;
		displayPanel();

		listbox.setCellRenderer(new DefaultListCellRenderer() {
			
			@SuppressWarnings("rawtypes")
			@Override
            public Component getListCellRendererComponent(JList list, Object value, int index,
                      boolean isSelected, boolean cellHasFocus) {
                 Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                 if (value instanceof Document) {
                      Document q = (Document) value;
                      setText(q.toJson());
                      if (q.getBoolean("isActive") == true) {
                          //setBackground(Color.BLUE);
                    	  setForeground(Color.GREEN);
                      } else {
                          // setBackground(Color.RED);
                    	  setForeground(Color.RED);
                      } 
                 
                      if (isSelected) {
                           setBackground(getBackground().darker());
                      }
                 } else {
                      setText("whodat?");
                 }
                 return c;
            }
			 
		});
	}
	
	
	
	public void displayQueryList() {
		int topic_id = topic.getTopicId();
		listData.removeAllElements();
		ArrayList<Document> queries = topic.getQueries();
		
		if (queries != null) {
			for (Document query: queries) {
					listData.addElement(query);
			}
			
			//Collections.sort(listData);
		}
		listbox.setListData( listData );
		scrollPane.revalidate();
		scrollPane.repaint();
	}

	// Handler for button presses
	public void actionPerformed( ActionEvent event )
	{
		/*if( event.getSource() == addButton ) 
		{
			System.out.println("Add button got pressed");
			// Get the text field value
			String stringValue = dataField.getText();
			dataField.setText( "" );
			Query q = new Query(stringValue, 0);
			listData.add(q);
			listbox.setListData( listData );
			scrollPane.revalidate();
			scrollPane.repaint();
		}
								
		if( event.getSource() == removeButton )
		{
			// Get the current selection
			int selection = listbox.getSelectedIndex();
			if( selection >= 0 )
			{   
				Query s = (Query) listData.get(selection);
				if ( s.isCurrent()) {
					s.setCurrent(false);
				} else {
					s.setCurrent(true);
				}
			}
			listbox.setListData( listData );
			scrollPane.revalidate();
			scrollPane.repaint();
		}*/
	
	}
	
	public void valueChanged( ListSelectionEvent event ) {
		// See if this is a listbox selection and the
		// event stream has settled
		if( event.getSource() == listbox
							&& !event.getValueIsAdjusting() ) {
			// Get the current selection and place it in the
			// edit field
			//String stringValue = (String)listbox.getSelectedValue();
			/*Query q = (Query) listbox.getSelectedValue();
			if (q != null) {
				String queryString = q.getQueryName();
				if( queryString != null )
					dataField.setText( queryString );
			}*/
		}
	}


	@Override
	public void update(Observable o, Object arg) {
		
		// No action is required because the update method of the TopicList panel will
		// update the keylist
		//System.out.println("BBBBB Update method of KeyListPanel: Current Topic ID: " 
		//										+ current_topic_id);
		displayQueryList();
	}
	
	
	public  void displayPanel() {
		
		setLayout( new BorderLayout());
		listData = new Vector();

		// Create a new listbox control
		listbox = new JList( listData );
		listbox.addListSelectionListener( this );

		// Add the listbox to a scrolling pane
		scrollPane = new JScrollPane();
		scrollPane.getViewport().add( listbox );
		add( scrollPane, BorderLayout.CENTER );
				
		JPanel dataPanel = new JPanel();
		dataPanel.setLayout( new GridLayout(1,10) );
		add( dataPanel, BorderLayout.SOUTH );

		// Create some function buttons
		newButton = new JButton( "New Query" );
		newButton.addActionListener( this );

		startStopButton = new JButton( "Start/Stop" );
		startStopButton.addActionListener( this );
		
		subscribeButton = new JButton( "Subscribe" );
		subscribeButton.addActionListener( this );
		
		
		dataPanel.add(newButton);
		dataPanel.add(startStopButton);
		dataPanel.add(subscribeButton);
		

	
	}
	
}
