package storm.sharon.gui.topics;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import storm.sharon.Rapid;
import storm.sharon.gui.core.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("serial")
public class StreamsPanel extends JPanel implements Observer, ActionListener, ListSelectionListener, TableModelListener{
	private static Logger log = LogManager.getLogger(StreamsPanel.class.getName());
	
	protected	JScrollPane scrollPane;
	int current_topic_id = -1;
	private JTable table;
	JButton configureButton;
	private AbstractTableModel pluginTableModel;
	private Set<String> streams;
	
	public StreamsPanel() {
		
		pluginTableModel = newAbstractTableModel(new Object[][] {{"X",new Boolean(false),new Boolean(false)}});
		
		displayPanel();
	}
	
	private AbstractTableModel newAbstractTableModel(final Object[][] r){
		return new AbstractTableModel() {
			String[] columnNames = {"Stream",
		            "Subscribe",
		            "Log"};
			public Object[][] rowData = r;
			public String getColumnName(int col) {
		        return columnNames[col].toString();
		    }
		    public int getRowCount() { return rowData.length; }
		    public int getColumnCount() { return columnNames.length; }
		    public Object getValueAt(int row, int col) {
		        return rowData[row][col];
		    }
		    public boolean isCellEditable(int row, int col)
		        { if(col>0) return true; else return false; }
		    public void setValueAt(Object value, int row, int col) {
		        rowData[row][col] = value;
		        fireTableCellUpdated(row, col);
		    }
		    @SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int column){
		    	if(column>0){
		    		return Boolean.class;
		    	} else {
		    		return String.class;
		    	}
		    }
		};
	}
	
	@SuppressWarnings("unchecked")
	public void displayStreamList(Integer topic_id) {
		this.current_topic_id = topic_id;
		streams = Rapid.getSubscriptions().getTopicQueueMap().get(topic_id);
		if(streams==null) {
			log.error("streams is null");
			return;
		}
		Object[][] rows = new Object[streams.size()][3];
		int i=0;
		for(String stream:streams){
			rows[i][0] = stream;
			rows[i][1] = true;
			rows[i][2] = false;
			i++;
		}
		scrollPane.getViewport().remove(table);
		pluginTableModel = newAbstractTableModel(rows);
		table = new JTable(pluginTableModel);
		table.addMouseListener(new MouseAdapter() {
			  public void mouseClicked(MouseEvent e) {
			    if (e.getClickCount() == 2) {
			      JTable target = (JTable)e.getSource();
			      int row = target.getSelectedRow();
			      int column = target.getSelectedColumn();
			      if(column==0){
			    	  log.debug("double-clicked on stream "+table.getModel().getValueAt(row, 0));
			    	
			      }
			    }
			  }
			});
		table.getModel().addTableModelListener(this);
		TableColumn column = null;
		for (i = 0; i < 3; i++) {
		    column = table.getColumnModel().getColumn(i);
		    if (i == 0) {
		        column.setPreferredWidth(200); //third column is bigger
		    } else {
		        column.setPreferredWidth(20);
		    }
		}
		
		scrollPane.getViewport().add(table);
		
		scrollPane.revalidate();
		scrollPane.repaint();
	}

	
	@Override
	public void tableChanged(TableModelEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public  void displayPanel() {
		
		Border lineBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"Streams");
		setBorder(lineBorder);
		setToolTipText("List of available streams.");
		setLayout( new BorderLayout());

		

		table = new JTable(pluginTableModel);
		
	
		
		// Add the listbox to a scrolling pane
		scrollPane = new JScrollPane();
		scrollPane.getViewport().add( table );
		add( scrollPane, BorderLayout.CENTER );
		
		/*
		 * The control panel contains the keyword controls.
		 */
		JPanel controlPanel = new JPanel();
		Border lineBorder2 = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"Stream controls");
		controlPanel.setBorder(lineBorder2);
		controlPanel.setLayout( new GridLayout(0,1) );
		add( controlPanel, BorderLayout.SOUTH );

		configureButton = new JButton( "Configure..." );
		controlPanel.add(configureButton);
		configureButton.addActionListener( this );
		configureButton.setToolTipText("Configure the selected stream.");
	}

}
