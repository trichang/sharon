package storm.sharon.gui.topics;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.bson.Document;

import storm.sharon.gui.core.RapidAPI;
import storm.sharon.util.Keyword;
import storm.sharon.util.TopicKeywordHandler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("serial")
public class KeyListPanel extends JPanel implements Observer, ActionListener, ListSelectionListener {
	private static Logger log = LogManager.getLogger(KeyListPanel.class.getName()); 
    private Integer current_topic_id = 0;
   
    
    @SuppressWarnings("rawtypes")
	protected	JList		listbox;
	protected	Vector<Keyword>		listData;
	protected	JButton		addButton;
	protected	JButton		titleButton;
	protected	JButton		removeButton;
	protected	JButton		blacklistButton;
	protected	JTextField	dataField;
	protected	JScrollPane scrollPane;

    
    
	@SuppressWarnings({ "unchecked" })
	public KeyListPanel () {
		//super();	
		displayPanel();

		listbox.setCellRenderer(new DefaultListCellRenderer() {
			@SuppressWarnings("rawtypes")
			@Override
            public Component getListCellRendererComponent(JList list, Object value, int index,
                      boolean isSelected, boolean cellHasFocus) {
                 Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                 if (value instanceof Keyword) {
                      Keyword keyword = (Keyword) value;
                      setText(keyword.getKeyword());
                      if (keyword.getType() == 0) {
                          //setBackground(Color.BLUE);
                    	  Font font = new Font("Verdana", Font.BOLD, 16);
                    	  setFont(font);
                    	  setForeground(new Color(0,0,0));
                      } else if (keyword.getType() == 1) {
                          // setBackground(Color.RED);
                    	  Font font = new Font("Verdana", Font.ITALIC, 15);
                    	  setFont(font);
                    	  setForeground(Color.BLACK);
                      } else {
                    	  Font font = new Font("Verdana", Font.PLAIN, 14);
                    	  setFont(font);
                    	  setForeground(Color.BLACK);
                      }
                    	  
                      if (isSelected) {
                           setBackground(getBackground().darker());
                      }
                 } 
                 return c;
            }
			 
		});
		
		RapidAPI.getKeywordTopicReader().register(this);
	}
	
	
	
	@SuppressWarnings("unchecked")
	public void displayKeyList(Integer topic_id) {
		this.current_topic_id = topic_id;
		listData.removeAllElements();
		Document doc = RapidAPI.getTopicKeywordHandler().getKeywords(topic_id);
		//System.out.println("KeyListPanel Current Topic: " + current_topic_id + " Keywords: " + doc);
		List<Document> keywords = (List<Document>) doc.get("keywords");
		if (keywords != null) {
			for (Document keyword: keywords) {
				Boolean isCurrent = keyword.getBoolean("isCurrent");
				if (isCurrent) {
					Keyword k = new Keyword(keyword);
					//Keyword k = new Keyword(keyword.getString("keyword"),
						//	keyword.getInteger("type"), keyword.getBoolean("isCurrent"));
					listData.addElement(k);
					//listData.addElement(keyword.getString("keyword") + " " 
					//		+ keyword.getInteger("type"));
				}
			}
			
			Collections.sort(listData);
		}
		listbox.setListData( listData );
		scrollPane.revalidate();
		scrollPane.repaint();
	}

	// Handler for button presses
	public void actionPerformed( ActionEvent event )
	{
		if( event.getSource() == addButton ) 
		{
			
			// Get the text field value
			String stringValue = dataField.getText();
			dataField.setText( "" );

			// Add this item to the list and refresh
			if( stringValue != null )
			{
				Document doc = new Document("command_name", "ADD_KEYWORDS");
				doc.append("command_type", "TOPIC_MGMT");
				doc.append("topic_id",  current_topic_id);		
				ArrayList<String> keywords = new ArrayList<String>();
				keywords.add(stringValue);
				doc.append("keywords", keywords);
				RapidAPI.getProducer().sendMessage(doc);
			}
		}
								
		if( event.getSource() == removeButton )
		{
			// Get the current selection
			int selection = listbox.getSelectedIndex();
			if( selection >= 0 )
			{   
				Keyword s = listData.get(selection);
				String keyword = s.getKeyword();
				log.info("Removing keyword: " + keyword);
				Document doc = new Document("command_name", "DELETE_KEYWORDS");
				doc.append("command_type", "TOPIC_MGMT");
				doc.append("topic_id",  current_topic_id);
				ArrayList<String> keywords = new ArrayList<String>();
				keywords.add(keyword);
				doc.append("keywords", keywords);
				RapidAPI.getProducer().sendMessage(doc);
				
			}
		}
		
		if( event.getSource() == blacklistButton )
		{
			// Get the current selection
			int selection = listbox.getSelectedIndex();
			if( selection >= 0 )
			{   
				Keyword s = listData.get(selection);
				String keyword = s.getKeyword();
				log.info("Blacklisting keyword: " + keyword);
				Document doc = new Document("command_name", "BLACKLIST_KEYWORDS");
				doc.append("command_type", "TOPIC_MGMT");
				doc.append("topic_id",  current_topic_id);
				ArrayList<String> keywords = new ArrayList<String>();
				keywords.add(keyword);
				doc.append("keywords", keywords);
				RapidAPI.getProducer().sendMessage(doc);
				
			}
		}
	}
	
	public void valueChanged( ListSelectionEvent event ) {
		// See if this is a listbox selection and the
		// event stream has settled
		
		if( event.getSource() == listbox
							&& !event.getValueIsAdjusting() ) {
			// Get the current selection and place it in the
			// edit field
			//String stringValue = (String)listbox.getSelectedValue();
			Keyword s = (Keyword) listbox.getSelectedValue();
			//System.out.println("KeyListPanel displaying keywords ");  
			if (s != null) {
				String keyword = s.getKeyword();
				if( keyword != null )
					dataField.setText( keyword );
			}
		}
	}


	@Override
	public void update(Observable o, Object arg) {
		
		// No action is required because the update method of the TopicList panel will
		// update the keylist
		//System.out.println("BBBBB Update method of KeyListPanel: Current Topic ID: " 
		//										+ current_topic_id);
		//displayKeyList(current_topic_id);
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public  void displayPanel() {
		
		Border lineBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"Current keywords");
		setBorder(lineBorder);
		setToolTipText("Current Keywords are being tracked, bold keywords are user added, italic keywords are system added.");
		setLayout( new BorderLayout());

		
		// Create the data model for this example
		listData = new Vector<Keyword>();
		//listData.add("Test Topic");

		// Create a new listbox control
		listbox = new JList( listData );
		listbox.addListSelectionListener( this );

		// Add the listbox to a scrolling pane
		scrollPane = new JScrollPane();
		scrollPane.getViewport().add( listbox );
		add( scrollPane, BorderLayout.CENTER );
		
		/*
		 * The control panel contains the keyword controls.
		 */
		
		
		JPanel controlPanel = new JPanel();
		Border lineBorder2 = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"Keyword controls");
		controlPanel.setBorder(lineBorder2);
		controlPanel.setLayout( new BorderLayout() );
		add( controlPanel, BorderLayout.SOUTH );

		// Create add keyword button and text field panel
		JPanel keywordAddPanel = new JPanel();
		keywordAddPanel.setLayout(new BorderLayout());
		addButton = new JButton( "Add Keyword" );
		addButton.addActionListener( this );
		addButton.setToolTipText("Add a keyword to the selected topic.");
		keywordAddPanel.add( addButton, BorderLayout.LINE_START );

		// Create a text field for data entry and display
		dataField = new JTextField();
		dataField.setText(null);
		dataField.setToolTipText("Type the name of a keyword to add.");
		keywordAddPanel.add( dataField, BorderLayout.CENTER );
		
		controlPanel.add(keywordAddPanel,BorderLayout.PAGE_START);	
		
		removeButton = new JButton( "Remove Keyword" );
		removeButton.addActionListener( this );
		removeButton.setToolTipText("Move the selected keyword to the Past Keywords list.");
		
		blacklistButton = new JButton( "Blacklist" );
		blacklistButton.addActionListener( this );
		blacklistButton.setToolTipText("Prevent the keyword from being tracked.");
		
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(0,2));
		buttonPanel.add(blacklistButton);
		buttonPanel.add(removeButton);
		controlPanel.add( buttonPanel, BorderLayout.CENTER);

	}
	
}
