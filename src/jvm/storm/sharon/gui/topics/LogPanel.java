package storm.sharon.gui.topics;


import java.awt.Component;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Observable;
import java.util.Observer;


import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
@SuppressWarnings("serial")
public class LogPanel extends JPanel implements Observer, ActionListener, PropertyChangeListener {
	private static Logger log = LogManager.getLogger(LogPanel.class.getName()); 

	
	private JFileChooser chooser;
	private String choosertitle = "Select directory to store log files";
	
	private JButton selectDirectoryButton;
	private JTextField directoryName;
	private File directory;
	
	private JFormattedTextField maxsizeField;
	private long maxsize = 100;
	
	public LogPanel(){
		
		setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		
		
		
		JPanel directoryPanel = new JPanel();
		//Border directoryPanelBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"Log directory");
		//directoryPanel.setBorder(directoryPanelBorder);
		directoryPanel.setLayout(new GridLayout(0, 2));
		selectDirectoryButton = new JButton( "Select Log Directory..." );
		selectDirectoryButton.setToolTipText("Select a directory to store log files.");
		selectDirectoryButton.addActionListener(this);
		directoryPanel.add( selectDirectoryButton );
		directoryName = new JTextField();
		chooser = new JFileChooser();
		directoryName.setText(chooser.getCurrentDirectory().toString());
		directoryName.setToolTipText("Type the name of the directory to store log files.");
		directoryPanel.add( directoryName );
		
		
		
		JLabel maxsizeLabel = new JLabel("Maximum log file size (MB): ");
		maxsizeLabel.setToolTipText("Specify the size that a log file may reach at most, in megabytes.");
		directoryPanel.add(maxsizeLabel);
		maxsizeField=new JFormattedTextField();
		maxsizeField.setValue(new Long(maxsize));
		maxsizeField.addPropertyChangeListener("value",this);
		maxsizeField.setToolTipText("Type an integer as the maximum size in megabytes. Leave empty for no maximum size.");
		directoryPanel.add(maxsizeField);
		
		//directoryPanel.add(maxsizePanel,BorderLayout.SOUTH);
		
		directoryPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		directoryPanel.setMaximumSize( directoryPanel.getPreferredSize() );
		
	
		add(directoryPanel);
		
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == selectDirectoryButton){
			chooser = new JFileChooser();
			chooser.setCurrentDirectory(new java.io.File("."));
			chooser.setDialogTitle(choosertitle);
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			chooser.setAcceptAllFileFilterUsed(false);
			if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				directory=chooser.getSelectedFile();
				directoryName.setText(directory.toString());
			} else {
				log.warn("No selection.");
			}
		} 
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void propertyChange(PropertyChangeEvent e) {
		Object source = e.getSource();
		if(source == maxsizeField){
			maxsize = ((Number)maxsizeField.getValue()).intValue();
		}
	}

	public String getDirectoryName() {
		return directoryName.getText();
	}

	// returns max size in bytes
	public long getMaxsize() {
		long maxsize;
		try{
			maxsize = Long.parseLong(maxsizeField.getText());
		} catch (NumberFormatException e){
			log.error("Maximum size specified in Configuration Log Panel is not a number.");
			log.info("Using 100MB as the maximum log file size.");
			maxsize = 100;
		}
		return maxsize*1024*1024;
	}

	
}
