package storm.sharon.gui.topics;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;






import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import storm.sharon.Rapid;
import storm.sharon.gui.core.PluginConfig;
import storm.sharon.gui.core.TablePanel;
import storm.sharon.gui.plugins.ComWinGraph;
import storm.sharon.gui.plugins.ComWinList;
import storm.sharon.gui.core.RapidLog;
import storm.sharon.gui.plugins.DataPanel;
import storm.sharon.gui.plugins.PluginContainer;
import storm.sharon.gui.plugins.Plugins;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("serial")
public class PluginsPanel extends JPanel implements Observer, ActionListener, ListSelectionListener, TableModelListener {
	private static Logger log = LogManager.getLogger(PluginsPanel.class.getName());
	JButton viewButton;
	
	
	
	private Vector<PluginContainer> containers;
	
	protected	JScrollPane scrollPane;
	
	int current_topic_id = -1;
	
	private JPopupMenu popup;
	private DataPanel selected;
	private int selectedRow;
	private JTable selectedTable;
	private Vector<JTable> tables;
	JMenuItem viewMenuItem;
	JMenuItem configMenuItem;
	@SuppressWarnings("unchecked")
	public PluginsPanel(){

		containers = new Vector<PluginContainer>();
		tables = new Vector<JTable>();
		
		popup = new JPopupMenu();
		viewMenuItem = new JMenuItem("View...");
		viewMenuItem.addActionListener(this);
		popup.add(viewMenuItem);
		configMenuItem = new JMenuItem("Configure...");
		configMenuItem.addActionListener(this);
		popup.add(configMenuItem);
		
		displayPanel();
		
		
	}
	
	private AbstractTableModel newAbstractTableModel(final Object[][] r, final String name){
		return new AbstractTableModel() {
			String[] columnNames = {name,
		            "Subscribe",
		            "Log"};
			public Object[][] rowData = r;
			public String getColumnName(int col) {
		        return columnNames[col].toString();
		    }
		    public int getRowCount() { return rowData.length; }
		    public int getColumnCount() { return columnNames.length; }
		    public Object getValueAt(int row, int col) {
		        return rowData[row][col];
		    }
		    public boolean isCellEditable(int row, int col)
		        { if(col>0) return true; else return false; }
		    public void setValueAt(Object value, int row, int col) {
		        rowData[row][col] = value;
		        fireTableCellUpdated(row, col);
		    }
		    @SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int column){
		    	if(column>0){
		    		return Boolean.class;
		    	} else {
		    		return DataPanel.class;
		    	}
		    }
		};
	}
	
	@SuppressWarnings("unchecked")
	public void displayPluginList(Integer topic_id) {
		this.current_topic_id = topic_id;
		containers = Rapid.getPlugins().getContainers().get(topic_id);
		scrollPane.getViewport().removeAll();
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
		for(PluginContainer container:containers){
			Vector<DataPanel> plugins = container.getPlugins();
			
			Object[][] rows = new Object[plugins.size()][3];
			for(int i=0;i<plugins.size();i++){
				rows[i][0] = plugins.get(i);
				rows[i][1] = plugins.get(i).isSubscribed();
				rows[i][2] = plugins.get(i).isLogging();
			}
			
			AbstractTableModel pluginTableModel = newAbstractTableModel(rows,container.getName());
			JTable table = new JTable(pluginTableModel);
			TablePanel tablepanel = new TablePanel(table);
			tables.add(table);
			//table.setComponentPopupMenu(popup);
			table.addMouseListener(new MouseAdapter() {
				  
				  @Override
				  public void mouseClicked(MouseEvent e) {
					JTable target = (JTable)e.getSource();
				    int row = target.getSelectedRow();
				    int column = target.getSelectedColumn();
				    if (e.getClickCount() == 2) {
				     
				      if(column==0){
				    	 DataPanel plugin = (DataPanel)target.getModel().getValueAt(row, column);
				    	 target.getModel().setValueAt(true, row, 1);
						 plugin.Open();
				      }
				    }
				  }
				  
				  @Override
				  public void mousePressed(MouseEvent e){
					  maybeShowPopup(e);
				
				  }
				  
				  @Override
				  public void mouseReleased(MouseEvent e){
					  maybeShowPopup(e);
				  }
				  
				  private void maybeShowPopup(MouseEvent e){
					  JTable target = (JTable)e.getSource();
					  Point point = e.getPoint();
					  int currentRow = target.rowAtPoint(point);
					  int currentColumn = target.columnAtPoint(point);
					  int column = target.getSelectedColumn();
					  int row = target.getSelectedRow();
					  if(e.isPopupTrigger() && currentColumn==0){
						  selected = (DataPanel)target.getModel().getValueAt(currentRow, 0);
						  selectedRow = currentRow;
						  selectedTable = target;
						  popup.show(e.getComponent(), e.getX(), e.getY());
					  }
				  }
				});
			
			table.getModel().addTableModelListener(this);
			TableColumn column = null;
			for (int i = 0; i < 3; i++) {
			    column = table.getColumnModel().getColumn(i);
			    if (i == 0) {
			        column.setPreferredWidth(200); //third column is bigger
			    } else {
			        column.setPreferredWidth(20);
			    }
			}
			panel.add(tablepanel);
			if(plugins.size()==0){
				panel.add(new JLabel("empty"));
			} else {
				
			}
		}
		scrollPane.getViewport().add(panel);
		scrollPane.revalidate();
		scrollPane.repaint();
	}

	
	@Override
	public void valueChanged(ListSelectionEvent e) {
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==viewMenuItem){
			selectedTable.getModel().setValueAt(true, selectedRow, 1);
			selected.Open();
		} else if(e.getSource()==configMenuItem){
			if(selected.getQuery_id()!=null){
				PluginConfig pc = new PluginConfig(selected);
				pc.setLocationRelativeTo(selectedTable);
			} else {
				JFrame msgframe = new JFrame();
				
				msgframe.setIconImages(Rapid.getIcons());
				msgframe.setLocationRelativeTo(selectedTable);
				JOptionPane.showMessageDialog(msgframe,
					    "Sorry, configuration is not yet available for "+selected.LongName()+".",
					    "Cannot configure",
					    JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public  void displayPanel() {
		
		Border lineBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.lightGray),"Data views and analysis");
		setBorder(lineBorder);
		setToolTipText("List of available plugins for data viewing and analysis.");
		setLayout( new BorderLayout());

		

		//table = new JTable(pluginTableModel);
		JPanel panel = new JPanel();
		
		
		// Add the listbox to a scrolling pane
		scrollPane = new JScrollPane();
		scrollPane.getViewport().add( panel);
		add( scrollPane, BorderLayout.NORTH );
		


	}

	@Override
	public void tableChanged(TableModelEvent e) {
		
		int row = e.getFirstRow();
        int column = e.getColumn();
        TableModel model = (TableModel)e.getSource();
        String columnName = model.getColumnName(column);
        Object data = model.getValueAt(row, column);
        if(column==1){
        	// subscribe
        	DataPanel plugin = (DataPanel)model.getValueAt(row, 0);
        	Boolean sub = (Boolean)data;
        	if(sub){
        		plugin.Subscribe();
        	} else {
        		plugin.Unsubscribe();
        	}
        } else if(column==2){
        	// log
        	DataPanel plugin = (DataPanel)model.getValueAt(row, 0);
        	Boolean startlog = (Boolean)data;
        	if(startlog){
        		LogPanel lp = Rapid.getLogpanel();
        		plugin.StartLogging(new RapidLog(FilenameUtils.concat(lp.getDirectoryName(),plugin.getTopic().getTopicName()),plugin.ShortName(),lp.getMaxsize()));
        	} else {
        		plugin.StopLogging();
        	}
        }
        
		
	}
	
}
