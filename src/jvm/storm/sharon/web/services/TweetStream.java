package storm.sharon.web.services;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import storm.sharon.web.ResultService;


/*
 * This ResultService streams a sample of all tweets entering the DB to the client.
 * It accepts a command from the client to change the sample fraction.
 */
@ServerEndpoint(value = "/resultservice")
public class TweetStream extends ResultService  {
	private static Log log = LogFactory.getLog(TweetStream.class);
	
	public TweetStream (){
		log.info("initialized");
	}
	

	public void shutdown(){
		super.shutdown();
		log.info("signalled to terminate");
	}
	

	@OnOpen
	public void onOpen(Session session) {
		super.onOpen(session);
		log.info("new client connection from "+session.getUserPrincipal());
	}

	@OnClose
	public void onClose(Session session, CloseReason closeReason) {
		super.onClose(session, closeReason);
		log.info("client disconnected from "+session.getUserPrincipal());
	}

	@Override
	@OnMessage
	public String onMessage(String data, Session sesssion) {
		return data;
		// TODO Auto-generated method stub
		
	}

}
