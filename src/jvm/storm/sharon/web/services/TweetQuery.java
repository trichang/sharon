package storm.sharon.web.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mongodb.client.FindIterable;

import storm.sharon.util.MongoDBImpl;
import storm.sharon.web.ResultService;


/*
 * Query interface to return a set of tweets matching a query.
 */
@ServerEndpoint(value = "/resultservice")
public class TweetQuery extends ResultService {
	private static Log log = LogFactory.getLog(TweetQuery.class);

	private MongoDBImpl db;


	public TweetQuery() {
		db = new MongoDBImpl();
		log.info("initialized");
	}

	
	public void shutdown() {
		super.shutdown();
		log.info("signalled to terminate");
	}

	@OnOpen
	public void onOpen(Session session) {
		super.onOpen(session);
		log.debug("new client connection from " + session.getId());
	}

	
	@OnClose
	public void onClose(Session session, CloseReason closeReason) {
		super.onClose(session,closeReason);
		log.debug("client disconnected from " + session.getId());
	}

	@Override
	@OnMessage
	public String onMessage(String data, Session session) {
		Map<String, Object> map = new Gson().fromJson(data, new TypeToken<HashMap<String, Object>>() {}.getType());
		String cmd=(String)(map.get("cmd"));
		if(cmd.equals("query")){
			@SuppressWarnings("unchecked")
			Map<String, Object> parameters = (Map<String, Object>)map.get("parameters");
			
			// must match
			List<Document> id_list = generateId(parameters);
			List<Document> screenname_list = generateScreenname(parameters);
			List<Document> id_inreplyto_list = generateIdInreplyto(parameters);
			List<Document> screenname_inreplyto_list = generateScreennameInreplyto(parameters);
			List<Document> id_retweeted_list = generateIdRetweeted(parameters);
			List<Document> screenname_retweeted_list = generateScreennameRetweeted(parameters);
			List<Document> statusescount_atleast = generateStatusescountAtleast(parameters);
			List<Document> statusescount_atmost = generateStatusescountAtmost(parameters);
			List<Document> followerscount_atleast = generateFollowerscountAtleast(parameters);
			List<Document> followerscount_atmost = generateFollowerscountAtmost(parameters);
			List<Document> friendscount_atleast = generateFriendscountAtleast(parameters);
			List<Document> friendscount_atmost = generateFriendscountAtmost(parameters);
			List<Document> favouritescount_atleast = generateFavouritescountAtleast(parameters);
			List<Document> favouritescount_atmost = generateFavouritescountAtmost(parameters);
			List<Document> retweetedcount_atleast = generateRetweetedcountAtleast(parameters);
			List<Document> rewteetedcount_atmost = generateRetweetedcountAtmost(parameters);
			List<Document> language_list = generateLanguage(parameters);
			List<Document> hashtags_list = generateHashtags(parameters);
			List<Document> mentions_list = generateMentions(parameters);
			
			// exclude
			List<Document> exclude_id_list = generateExcludeId(parameters);
			List<Document> exclude_screenname_list = generateExcludeScreenname(parameters);
			List<Document> exclude_id_inreplyto_list = generateExcludeIdInreplyto(parameters);
			List<Document> exclude_screenname_inreplyto_list = generateExcludeScreennameInreplyto(parameters);
			List<Document> exclude_id_retweeted_list = generateExcludeIdRetweeted(parameters);
			List<Document> exclude_screenname_retweeted_list = generateExcludeScreennameRetweeted(parameters);
			List<Document> exclude_statusescount_atleast = generateExcludeStatusescountAtleast(parameters);
			List<Document> exclude_statusescount_atmost = generateExcludeStatusescountAtmost(parameters);
			List<Document> exclude_followerscount_atleast = generateExcludeFollowerscountAtleast(parameters);
			List<Document> exclude_followerscount_atmost = generateExcludeFollowerscountAtmost(parameters);
			List<Document> exclude_friendscount_atleast = generateExcludeFriendscountAtleast(parameters);
			List<Document> exclude_friendscount_atmost = generateExcludeFriendscountAtmost(parameters);
			List<Document> exclude_favouritescount_atleast = generateExcludeFavouritescountAtleast(parameters);
			List<Document> exclude_favouritescount_atmost = generateExcludeFavouritescountAtmost(parameters);
			List<Document> exclude_retweetedcount_atleast = generateExcludeRetweetedcountAtleast(parameters);
			List<Document> exclude_rewteetedcount_atmost = generateExcludeRetweetedcountAtmost(parameters);
			List<Document> exclude_language_list = generateExcludeLanguage(parameters);
			List<Document> exclude_hashtags_list = generateExcludeHashtags(parameters);
			List<Document> exclude_mentions_list = generateExcludeMentions(parameters);
			
			
			List<Document> disjunct_list = new ArrayList<Document>();
			
			// must match
			if(!id_list.isEmpty()) disjunct_list.add(new Document("$or",id_list));
			if(!screenname_list.isEmpty()) disjunct_list.add(new Document("$or",screenname_list));
			if(!id_inreplyto_list.isEmpty()) disjunct_list.add(new Document("$or",id_inreplyto_list));
			if(!screenname_inreplyto_list.isEmpty()) disjunct_list.add(new Document("$or",screenname_inreplyto_list));
			if(!id_retweeted_list.isEmpty()) disjunct_list.add(new Document("$or",id_retweeted_list));
			if(!screenname_retweeted_list.isEmpty()) disjunct_list.add(new Document("$or",screenname_retweeted_list));
			if(!statusescount_atleast.isEmpty()) disjunct_list.add((Document)statusescount_atleast.get(0));
			if(!statusescount_atmost.isEmpty()) disjunct_list.add((Document)statusescount_atmost.get(0));
			if(!followerscount_atleast.isEmpty()) disjunct_list.add((Document)followerscount_atleast.get(0));
			if(!followerscount_atmost.isEmpty()) disjunct_list.add((Document)followerscount_atmost.get(0));
			if(!friendscount_atleast.isEmpty()) disjunct_list.add((Document)friendscount_atleast.get(0));
			if(!friendscount_atmost.isEmpty()) disjunct_list.add((Document)friendscount_atmost.get(0));
			if(!favouritescount_atleast.isEmpty()) disjunct_list.add((Document)favouritescount_atleast.get(0));
			if(!favouritescount_atmost.isEmpty()) disjunct_list.add((Document)favouritescount_atmost.get(0));
			if(!retweetedcount_atleast.isEmpty()) disjunct_list.add((Document)retweetedcount_atleast.get(0));
			if(!rewteetedcount_atmost.isEmpty()) disjunct_list.add((Document)rewteetedcount_atmost.get(0));
			if(!language_list.isEmpty()) disjunct_list.add(new Document("$or",language_list));
			if(!hashtags_list.isEmpty()) disjunct_list.add(new Document("$or",hashtags_list));
			if(!mentions_list.isEmpty()) disjunct_list.add(new Document("$or",mentions_list));
			
			//exclude
			if(!exclude_id_list.isEmpty()) disjunct_list.add(new Document("$nor",exclude_id_list));
			if(!exclude_screenname_list.isEmpty()) disjunct_list.add(new Document("$nor",exclude_screenname_list));
			if(!exclude_id_inreplyto_list.isEmpty()) disjunct_list.add(new Document("$nor",exclude_id_inreplyto_list));
			if(!exclude_screenname_inreplyto_list.isEmpty()) disjunct_list.add(new Document("$nor",exclude_screenname_inreplyto_list));
			if(!exclude_id_retweeted_list.isEmpty()) disjunct_list.add(new Document("$nor",exclude_id_retweeted_list));
			if(!exclude_screenname_retweeted_list.isEmpty()) disjunct_list.add(new Document("$nor",exclude_screenname_retweeted_list));
			if(!exclude_statusescount_atleast.isEmpty()) disjunct_list.add((Document)exclude_statusescount_atleast.get(0));
			if(!exclude_statusescount_atmost.isEmpty()) disjunct_list.add(((Document)exclude_statusescount_atmost.get(0)));
			if(!exclude_followerscount_atleast.isEmpty()) disjunct_list.add((Document)exclude_followerscount_atleast.get(0));
			if(!exclude_followerscount_atmost.isEmpty()) disjunct_list.add((Document)exclude_followerscount_atmost.get(0));
			if(!exclude_friendscount_atleast.isEmpty()) disjunct_list.add((Document)exclude_friendscount_atleast.get(0));
			if(!exclude_friendscount_atmost.isEmpty()) disjunct_list.add((Document)exclude_friendscount_atmost.get(0));
			if(!exclude_favouritescount_atleast.isEmpty()) disjunct_list.add((Document)exclude_favouritescount_atleast.get(0));
			if(!exclude_favouritescount_atmost.isEmpty()) disjunct_list.add((Document)exclude_favouritescount_atmost.get(0));
			if(!exclude_retweetedcount_atleast.isEmpty()) disjunct_list.add((Document)exclude_retweetedcount_atleast.get(0));
			if(!exclude_rewteetedcount_atmost.isEmpty()) disjunct_list.add((Document)exclude_rewteetedcount_atmost.get(0));
			if(!exclude_language_list.isEmpty()) disjunct_list.add(new Document("$nor",exclude_language_list));
			if(!exclude_hashtags_list.isEmpty()) disjunct_list.add(new Document("$nor",exclude_hashtags_list));
			if(!exclude_mentions_list.isEmpty()) disjunct_list.add(new Document("$nor",exclude_mentions_list));
			
			if(!disjunct_list.isEmpty()){
				Document query = new Document();
				query.append("$and",disjunct_list);
				
				FindIterable<Document> docs = db.getDocs(MongoDBImpl.TWEET_STORE, query);
				for(Document doc: docs){
					String json = new Gson().toJson(doc);
					try {
						session.getBasicRemote().sendText("{\"status\":"+json+"}");
					} catch (IOException e) {
						log.error("could not send data to client");
					}
				}
			}
			return "{\"cmd\":\"closing\"}";
		}
		log.warn("unknown cmd received: "+map.get("cmd"));
		return "{\"cmd\":\"closing\"}";
	}
	
	///////////////////
	// query commands//
	///////////////////
	
	// must match
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateId(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("id_str"))return id_list;
		if(parameter.get("id_str")==null) return id_list;
		String[] ids = (((String)(parameter).get("id_str"))).split("[^0-9]");
		for(String id: ids){
			long nid=Long.parseLong(id);
			id_list.add(new Document("id",nid));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateScreenname(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("screenname"))return id_list;
		if(parameter.get("screenname")==null) return id_list;
		String[] ids = (((String)(parameter).get("screenname"))).split("[,| |@]+");
		for(String id: ids){
			id_list.add(new Document("user.screen_name",id));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateIdInreplyto(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("id_str_inreplyto"))return id_list;
		if(parameter.get("id_str_inreplyto")==null) return id_list;
		String[] ids = (((String)(parameter).get("id_str_inreplyto"))).split("[^0-9]");
		for(String id: ids){
			long nid=Long.parseLong(id);
			id_list.add(new Document("in_reply_to_user_id",nid));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateScreennameInreplyto(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("screenname_inreplyto"))return id_list;
		if(parameter.get("screenname_inreplyto")==null) return id_list;
		String[] ids = (((String)(parameter).get("screenname_inreplyto"))).split("[,| |@]+");
		for(String id: ids){
			id_list.add(new Document("in_reply_to_screen_name",id));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateIdRetweeted(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("id_str_retweeted"))return id_list;
		if(parameter.get("id_str_retweeted")==null) return id_list;
		String[] ids = (((String)(parameter).get("id_str_retweeted"))).split("[^0-9]");
		for(String id: ids){
			long nid=Long.parseLong(id);
			id_list.add(new Document("retweeted_status.user.id_str",nid));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateScreennameRetweeted(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("screenname_retweeted"))return id_list;
		if(parameter.get("screenname_retweeted")==null) return id_list;
		String[] ids = (((String)(parameter).get("screenname_retweeted"))).split("[,| |@]+");
		for(String id: ids){
			id_list.add(new Document("retweeted_status.user.screen_name",id));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateStatusescountAtleast(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("statusescount_atleast"))return id_list;
		if(parameter.get("statusescount_atleast")==null) return id_list;
		String count = (((String)(parameter).get("statusescount_atleast")));
		id_list.add(new Document("user.statuses_count",new Document("$gte",Long.parseLong(count))));
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateStatusescountAtmost(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("statusescount_atmost"))return id_list;
		if(parameter.get("statusescount_atmost")==null) return id_list;
		String count = (((String)(parameter).get("statusescount_atmost")));
		id_list.add(new Document("user.statuses_count",new Document("$lte",Long.parseLong(count))));
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateFollowerscountAtleast(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("followerscount_atleast"))return id_list;
		if(parameter.get("followerscount_atleast")==null) return id_list;
		String count = (((String)(parameter).get("followerscount_atleast")));
		id_list.add(new Document("user.followers_count",new Document("$gte",Long.parseLong(count))));
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateFollowerscountAtmost(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("followerscount_atmost"))return id_list;
		if(parameter.get("followerscount_atmost")==null) return id_list;
		String count = (((String)(parameter).get("followerscount_atmost")));
		id_list.add(new Document("user.followers_count",new Document("$lte",Long.parseLong(count))));
		return id_list;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateFriendscountAtleast(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("friendscount_atleast"))return id_list;
		if(parameter.get("friendscount_atleast")==null) return id_list;
		String count = (((String)(parameter).get("friendscount_atleast")));
		id_list.add(new Document("user.friends_count",new Document("$gte",Long.parseLong(count))));
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateFriendscountAtmost(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("friendscount_atmost"))return id_list;
		if(parameter.get("friendscount_atmost")==null) return id_list;
		String count = (((String)(parameter).get("friendscount_atmost")));
		id_list.add(new Document("user.friends_count",new Document("$lte",Long.parseLong(count))));
		return id_list;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateFavouritescountAtleast(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("favouritescount_atleast"))return id_list;
		if(parameter.get("favouritescount_atleast")==null) return id_list;
		String count = (((String)(parameter).get("favouritescount_atleast")));
		id_list.add(new Document("user.favourites_count",new Document("$gte",Long.parseLong(count))));
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateFavouritescountAtmost(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("favouritescount_atmost"))return id_list;
		if(parameter.get("favouritescount_atmost")==null) return id_list;
		String count = (((String)(parameter).get("favouritescount_atmost")));
		id_list.add(new Document("user.favourites_count",new Document("$lte",Long.parseLong(count))));
		return id_list;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateRetweetedcountAtleast(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("retweetedcount_atleast"))return id_list;
		if(parameter.get("retweetedcount_atleast")==null) return id_list;
		String count = (((String)(parameter).get("retweetedcount_atleast")));
		id_list.add(new Document("retweet_count",new Document("$gte",Long.parseLong(count))));
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateRetweetedcountAtmost(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("retweetedcount_atmost"))return id_list;
		if(parameter.get("retweetedcount_atmost")==null) return id_list;
		String count = (((String)(parameter).get("retweetedcount_atmost")));
		id_list.add(new Document("retweet_count",new Document("$lte",Long.parseLong(count))));
		return id_list;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateLanguage(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("language"))return id_list;
		if(parameter.get("language")==null) return id_list;
		String[] ids = (((String)(parameter).get("language"))).split("[,| ]+");
		for(String id: ids){
			id_list.add(new Document("lang",id));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateHashtags(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("hashtags"))return id_list;
		if(parameter.get("hashtags")==null) return id_list;
		String[] ids = (((String)(parameter).get("hashtags"))).split("[,| |#]+");
		for(String id: ids){
			id_list.add(new Document("entities.hashtags.text",id));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateMentions(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("mentions"))return id_list;
		if(parameter.get("mentions")==null) return id_list;
		String[] ids = (((String)(parameter).get("mentions"))).split("[,| |@]+");
		for(String id: ids){
			id_list.add(new Document("entities.user_mentions.screen_name",id));
		}
		return id_list;
	}
	
	/////////////
	// exclude //
	/////////////
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeId(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_id_str"))return id_list;
		if(parameter.get("exclude_id_str")==null) return id_list;
		String[] ids = (((String)(parameter).get("exclude_id_str"))).split("[^0-9]");
		for(String id: ids){
			long nid=Long.parseLong(id);
			id_list.add(new Document("id",nid));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeScreenname(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_screenname"))return id_list;
		if(parameter.get("exclude_screenname")==null) return id_list;
		String[] ids = (((String)(parameter).get("exclude_screenname"))).split("[,| |@]+");
		for(String id: ids){
			id_list.add(new Document("user.screen_name",id));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeIdInreplyto(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_id_str_inreplyto"))return id_list;
		if(parameter.get("exclude_id_str_inreplyto")==null) return id_list;
		String[] ids = (((String)(parameter).get("exclude_id_str_inreplyto"))).split("[^0-9]");
		for(String id: ids){
			long nid=Long.parseLong(id);
			id_list.add(new Document("in_reply_to_user_id",nid));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeScreennameInreplyto(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_screenname_inreplyto"))return id_list;
		if(parameter.get("exclude_screenname_inreplyto")==null) return id_list;
		String[] ids = (((String)(parameter).get("exclude_screenname_inreplyto"))).split("[,| |@]+");
		for(String id: ids){
			id_list.add(new Document("in_reply_to_screen_name",id));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeIdRetweeted(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_id_str_retweeted"))return id_list;
		if(parameter.get("exclude_id_str_retweeted")==null) return id_list;
		String[] ids = (((String)(parameter).get("exclude_id_str_retweeted"))).split("[^0-9]");
		for(String id: ids){
			long nid=Long.parseLong(id);
			id_list.add(new Document("retweeted_status.user.id_str",nid));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeScreennameRetweeted(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_screenname_retweeted"))return id_list;
		if(parameter.get("exclude_screenname_retweeted")==null) return id_list;
		String[] ids = (((String)(parameter).get("exclude_screenname_retweeted"))).split("[,| |@]+");
		for(String id: ids){
			id_list.add(new Document("retweeted_status.user.screen_name",id));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeStatusescountAtleast(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_statusescount_atleast"))return id_list;
		if(parameter.get("exclude_statusescount_atleast")==null) return id_list;
		String count = (((String)(parameter).get("exclude_statusescount_atleast")));
		id_list.add(new Document("user.statuses_count",new Document("$lt",Long.parseLong(count))));
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeStatusescountAtmost(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_statusescount_atmost"))return id_list;
		if(parameter.get("exclude_statusescount_atmost")==null) return id_list;
		String count = (((String)(parameter).get("exclude_statusescount_atmost")));
		id_list.add(new Document("user.statuses_count",new Document("$gt",Long.parseLong(count))));
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeFollowerscountAtleast(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_followerscount_atleast"))return id_list;
		if(parameter.get("exclude_followerscount_atleast")==null) return id_list;
		String count = (((String)(parameter).get("exclude_followerscount_atleast")));
		id_list.add(new Document("user.followers_count",new Document("$lt",Long.parseLong(count))));
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeFollowerscountAtmost(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_followerscount_atmost"))return id_list;
		if(parameter.get("exclude_followerscount_atmost")==null) return id_list;
		String count = (((String)(parameter).get("exclude_followerscount_atmost")));
		id_list.add(new Document("user.followers_count",new Document("$gt",Long.parseLong(count))));
		return id_list;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeFriendscountAtleast(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_friendscount_atleast"))return id_list;
		if(parameter.get("exclude_friendscount_atleast")==null) return id_list;
		String count = (((String)(parameter).get("exclude_friendscount_atleast")));
		id_list.add(new Document("user.friends_count",new Document("$lt",Long.parseLong(count))));
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeFriendscountAtmost(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_friendscount_atmost"))return id_list;
		if(parameter.get("exclude_friendscount_atmost")==null) return id_list;
		String count = (((String)(parameter).get("exclude_friendscount_atmost")));
		id_list.add(new Document("user.friends_count",new Document("$gt",Long.parseLong(count))));
		return id_list;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeFavouritescountAtleast(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_favouritescount_atleast"))return id_list;
		if(parameter.get("exclude_favouritescount_atleast")==null) return id_list;
		String count = (((String)(parameter).get("exclude_favouritescount_atleast")));
		id_list.add(new Document("user.favourites_count",new Document("$lt",Long.parseLong(count))));
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeFavouritescountAtmost(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_favouritescount_atmost"))return id_list;
		if(parameter.get("exclude_favouritescount_atmost")==null) return id_list;
		String count = (((String)(parameter).get("exclude_favouritescount_atmost")));
		id_list.add(new Document("user.favourites_count",new Document("$gt",Long.parseLong(count))));
		return id_list;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeRetweetedcountAtleast(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_retweetedcount_atleast"))return id_list;
		if(parameter.get("exclude_retweetedcount_atleast")==null) return id_list;
		String count = (((String)(parameter).get("exclude_retweetedcount_atleast")));
		id_list.add(new Document("retweet_count",new Document("$lt",Long.parseLong(count))));
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeRetweetedcountAtmost(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_retweetedcount_atmost"))return id_list;
		if(parameter.get("exclude_retweetedcount_atmost")==null) return id_list;
		String count = (((String)(parameter).get("exclude_retweetedcount_atmost")));
		id_list.add(new Document("retweet_count",new Document("$gt",Long.parseLong(count))));
		return id_list;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeLanguage(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_language"))return id_list;
		if(parameter.get("exclude_language")==null) return id_list;
		String[] ids = (((String)(parameter).get("exclude_language"))).split("[,| ]+");
		for(String id: ids){
			id_list.add(new Document("lang",id));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeHashtags(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_hashtags"))return id_list;
		if(parameter.get("exclude_hashtags")==null) return id_list;
		String[] ids = (((String)(parameter).get("exclude_hashtags"))).split("[,| |#]+");
		for(String id: ids){
			id_list.add(new Document("entities.hashtags.text",id));
		}
		return id_list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> generateExcludeMentions(Map<String,Object> parameter){
		List id_list = new ArrayList<Document>();
		if(!parameter.containsKey("exclude_mentions"))return id_list;
		if(parameter.get("exclude_mentions")==null) return id_list;
		String[] ids = (((String)(parameter).get("exclude_mentions"))).split("[,| |@]+");
		for(String id: ids){
			id_list.add(new Document("entities.user_mentions.screen_name",id));
		}
		return id_list;
	}
}
