package storm.sharon.web.services;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import storm.sharon.util.CredentialsManager;
import storm.sharon.util.MongoDBImpl;
import storm.sharon.util.TwitterAuth;
import storm.sharon.web.ResultService;

@ServerEndpoint(value = "/resultservice")
public class Control extends ResultService {

	private static Log log = LogFactory.getLog(Control.class);

	private MongoDBImpl db;


	public Control() {
		db = new MongoDBImpl();
		log.info("initialized");
	}

	
	public void shutdown() {
		super.shutdown();
		log.info("signalled to terminate");
	}

	@OnOpen
	public void onOpen(Session session) {
		super.onOpen(session);
		log.debug("new client connection from " + session.getId());
	}

	
	@OnClose
	public void onClose(Session session, CloseReason closeReason) {
		super.onClose(session,closeReason);
		log.debug("client disconnected from " + session.getId());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@OnMessage
	public String onMessage(String data, Session session) {
		Map<String, Object> map = new Gson().fromJson(data, new TypeToken<HashMap<String, Object>>() {}.getType());
		String cmd=(String)(map.get("cmd"));
		if(cmd.equals("getcredentials")){
			return getCredentials();
		} else if(cmd.equals("putcredential")){
			return putCredential((Map<String,Object>) map.get("credential"));
		} else if(cmd.equalsIgnoreCase("deletecredential")){
			return deleteCredential((String) map.get("filename"));
		}
		log.warn("unknown cmd received: "+cmd);
		return "{\"ret\":\"fail\",\"reason\":\"unknown command\"}";
	}
	
	public String getCredentials(){
		ArrayList<String> files;
		files=CredentialsManager.getCredentialList();
		List<Document> credlist = new ArrayList<Document>();
		if(files.size()>0){
			for(String file: files){
				TwitterAuth cred = CredentialsManager.getTwitterCredentials(file);
				Document creddoc = new Document();
				creddoc.append("filename", file);
				creddoc.append("consumerKey",cred.getConsumerKey());
				creddoc.append("consumerSecret",cred.getConsumerSecret());
				creddoc.append("accessToken",cred.getAccessToken());
				creddoc.append("accessTokenSecret",cred.getAccessTokenSecret());
				credlist.add(creddoc);
			}
		} else {
			// returns an empty list
		}
		Document creds = new Document();
		creds.append("credentials",credlist);
		creds.append("ret", "success");
		String json = new Gson().toJson(creds);
		return json;
	}
	
	public String putCredential(Map<String,Object> cred){
		String filename = (String) cred.get("filename");
		String consumerKey = (String) cred.get("consumerKey");
		String consumerSecret = (String) cred.get("consumerSecret");
		String accessToken = (String) cred.get("accessToken");
		String accessTokenSecret = (String) cred.get("accessTokenSecret");
		TwitterAuth tcred = new TwitterAuth(consumerKey,consumerSecret,accessToken,accessTokenSecret);
		if(CredentialsManager.putTwitterCredentials(filename, tcred)){
			return "{\"ret\":\"success\"}";
		} else {
			return "{\"ret\":\"fail\",\"reason\":\"could not write credential to file\"}";
		}
		
	}
	
	public String deleteCredential(String filename){
		CredentialsManager.deleteTwitterCredential(filename);
		return "{\"ret\":\"success\"}";
	}
}
