package storm.sharon.web;



import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.tyrus.server.Server;


/*
 * ResultsServers are started by SharonServer.
 * There is one ResultServer for each type of result. A result is handled
 * by a ResultService.
 */
public class ResultServer {
	private static Log log = LogFactory.getLog(ResultServer.class);
	public Server server;
	
	public ResultServer(ResultService resultService,int port){
		server = new Server("localhost", port, "/websocket",null, resultService.getClass());
		
        try {
            server.start();
        } catch (Exception e) {
        	log.fatal("could not start websocket server on port "+port);
            throw new RuntimeException(e);
        } 
	}
	
	public void stop(){
		
		server.stop();
	}
}
