package storm.sharon.web;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class ResultService extends Thread {
	private static Log log = LogFactory.getLog(ResultService.class);
	private static final Set<Session> sessions = Collections
			.synchronizedSet(new HashSet<Session>());

	public ResultService() {

	}

	@OnOpen
	public void onOpen(Session session) {
		sessions.add(session);
	}

	@OnClose
	public void onClose(Session session, CloseReason closeReason) {
		sessions.remove(session);
	}

	@OnMessage
	public abstract String onMessage(String data, Session session);

	public void shutdown() {
		for (Session s : sessions) {
			if (s.isOpen()) {
				try {
					s.close();
				} catch (IOException ex) {
					log.warn("session did not close properly");
				}
			}
		}
	}

}
