package storm.sharon.algorithms;

import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import storm.sharon.util.DBInterface;
import storm.sharon.util.MongoDBImpl;

public class CommunityCollection extends Observable {
	
	private Log log = LogFactory.getLog(CommunityCollection.class);
	private HashMap<String, User>  communityMap;
	private DBInterface dbConn = null;
	private final String COMMUNITY_COLLECTION_NAME;
	
	public CommunityCollection(Observer o, int topic_id) {
		super();
		this.communityMap = new HashMap<String, User>();
		if (o != null) {
			addObserver(o);
		}
		dbConn = new MongoDBImpl();
		this.COMMUNITY_COLLECTION_NAME = DBInterface.COMMUNITY_STORE + "_" + String.valueOf(topic_id);
	}
	
	public CommunityCollection(String databaseInfo, Observer o, int topic_id) {
		super();
		this.communityMap = new HashMap<String, User>();
		if (o != null) {
			addObserver(o);
		}
		dbConn = new MongoDBImpl(databaseInfo);
		this.COMMUNITY_COLLECTION_NAME = DBInterface.COMMUNITY_STORE + "_" + String.valueOf(topic_id);
	}
	
	public HashMap<String, User> getCommunityMap() {
		return communityMap;
	}
	
	public void setDiscussionMapId(HashMap<String, User> communityMap) {
		this.communityMap = communityMap;
	}
		
	public void addToCommunity(Document doc, Boolean persist) {

		log.info("COMMUNITY: Adding document to the community collection: " + doc.getString("user_name"));

		addToCommunity(doc);
		// Update the database
		Document doc_ret = dbConn.getDoc(COMMUNITY_COLLECTION_NAME, "user_name", 
									doc.getString("user_name"));
		if (doc_ret != null) {
			// Delete the existing document
			log.info("Deleting existing document from community collection: " + doc.getString("user_name"));
			dbConn.deleteDoc(COMMUNITY_COLLECTION_NAME, "user_name", 
													doc.getString("user_name"));
		}
	
		dbConn.insertDoc(COMMUNITY_COLLECTION_NAME, doc);
		//doc.append("command_name", "ADD_USER");
		setChanged();
		notifyObservers(doc);
	}
	
	public void addToCommunity(Document doc) {
		User u = new User(doc);
		communityMap.put(doc.getString("user_name"), u);
		
	}

}
