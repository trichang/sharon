package storm.sharon.algorithms;

import org.bson.Document;

public interface RankingInterface {
	
	 public void rank(Document doc);
	 public void updateTK(Document doc);

}
