package storm.sharon.algorithms;

import org.bson.Document;

public interface CommunityInterface {
	
	 public void  process(Document doc);
	 public void  clearUserMap();
	 public void  clearReplyMap();

}
