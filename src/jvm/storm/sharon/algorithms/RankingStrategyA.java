package storm.sharon.algorithms;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Observer;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import storm.sharon.util.Keyword;
import storm.sharon.util.TopicKeywordHandler;

public class RankingStrategyA implements RankingInterface {

	private static final int KEY_THRESHOLD = 10;
	private Log log = LogFactory.getLog(RankingStrategyA.class);
	
	private RankedCollection rankColl = null;
	// This keeps the current keywords for the topic
	private HashSet<String> keywords = null; 
	
	// This maintains the updates to the keywords and topics
	private TopicKeywordHandler tk_handler;

	private HashMap<String, Integer> n_keywords;
	private ArrayList<String> trackWords;
	private int numNewKeywords = 0;
	private int topic_id = 0;
	private HashMap<String, Keyword> keymap;
	private int min_words;
	
	public RankingStrategyA(Observer observer, int topic_id) {
		
		rankColl = new RankedCollection();
		rankColl.addObserver(observer);  
		tk_handler = new TopicKeywordHandler();	
		this.topic_id = topic_id;
		reset();
	}
	
	private void reset() {
		keywords = new HashSet<String>();
		if (tk_handler.getTopicKeyword().containsKey(topic_id)) {
			keymap = tk_handler.getTopicKeyword().get(topic_id);
			n_keywords = new HashMap<String, Integer>();
			trackWords = new ArrayList<String>();
			numNewKeywords = 0;
			if (keymap != null) {
				Set<Entry<String, Keyword>> c_keywords_es = keymap.entrySet();
			
				for (Entry entry: c_keywords_es) {
					if (((Keyword)entry.getValue()).getIsCurrent()){
						String key = ((String)entry.getKey());
						keywords.add(key.toLowerCase());
						if ((((Keyword)entry.getValue()).getType() == 0)){
							n_keywords.put(key.toLowerCase(), KEY_THRESHOLD + 1);
							trackWords.add(key);
							numNewKeywords++;
						}
					
					}
					
				}
			}
		} else {
			log.info("Topic does not exist");
		}
		
		if (keywords.size() > 6 ) {
			min_words = keywords.size() - 2;
		} else if (keywords.size() >= 3 ) {
			min_words = 2;
		} else {
			min_words = 1;
		}
	}

	@Override
	public void rank(Document doc) {
		
		log.info("RANK: Topic ID: " + topic_id + "Keywords" + keywords);
		
		// Get the hashtags in the tweet
		List<Document> hashtags = (List<Document>)((Document)doc.get("entities")).get("hashtags");
		log.info("RANK: Topic ID: " + topic_id + "Hashtags"  + hashtags);
	
		
		int x = newKeywords(hashtags);
		numNewKeywords = numNewKeywords + x;
			
		log.info("RANK: New Words: " + numNewKeywords);
		
	    if ( (numNewKeywords) >  keywords.size()) {
		
			
			log.info("RANK: Generating new keyword list");
			
			
			ArrayList<String> topKeyWordsAll = new ArrayList<String>(trackWords);
			
		
			Document doc1 = new Document("command_name", "NEW_KEYWORDS");
			doc1.append("command_type", "TOPIC_MGMT");
			doc1.append("keywords", topKeyWordsAll);
			doc1.append("topic_id", topic_id);
			
			// Reset all words
			numNewKeywords = 0;
			n_keywords = new HashMap<String, Integer>();
			trackWords = new ArrayList<String>();
			rankColl.add(doc1);
		} 
	}


	private int newKeywords(List<Document> tags) {
	
		ArrayList<String> hash_keywords = new ArrayList<String>();
		for (Document h: tags) {
			String word = h.getString("text").toLowerCase();
			if (keywords != null) {
				if (keywords.contains(word)) {
					hash_keywords.add(word);
				}
			}
		}
		
		int x = hash_keywords.size();
		int n = 0;
		
		if ( x >=  min_words ) {
		
			log.info ("RANK: Tweet contains significant hastags: " + hash_keywords);
			for (Document h: tags) {
				String word = h.getString("text").toLowerCase();
				int count = 0;
				//if (keywords.size() <= 1 || x >= 2) {
				//if ( x >=  Math.max(keywords.size() -1, 2)  ) {
			
			
			
				// If the is only one keyword increment the count if the hashtag occurs in the tweet
				if (n_keywords.containsKey(word)) {
					count = n_keywords.get(word) + 1;		
				}
				else {
					count = 1;
				}
				n_keywords.put(word, count);
			
			
				log.info("RANK: " + word + " " + count);
			
				if (count == KEY_THRESHOLD) {
					log.info("RANK: New keyword included for tracking: " +  word);
					trackWords.add(word);
					n++;
				}
			}
			
		}
		return n;
	}

	

	@Override
	public void updateTK(Document doc) {
		Document d = tk_handler.process(doc);
		if (d != null) {
			// If this a Topic Management message and the command is a keyword change message
			// reset the keyword list
			if (d.containsKey("topic_id")) {
				if (doc.getInteger("topic_id") == this.topic_id) {
					String command = doc.getString("command_name");
					if ( command.equals("ADD_KEYWORDS") ||
							command.equals("NEW_KEYWORDS") ||
							command.equals("DELETE_KEYWORDS")) {
						log.info("RANK Resetting ranking for topic: " + topic_id);
						reset();
					}
						
				}
			}
		}
					
	}
	

}
