package storm.sharon.algorithms;

import java.util.HashMap;
import java.util.Observable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import storm.sharon.util.DBInterface;
import storm.sharon.util.MongoDBImpl;

public class DiscussionCollection extends Observable {
	
	private Log log = LogFactory.getLog(DiscussionCollection.class);
	private HashMap<String, Document>  discussionMapId;
	private DBInterface dbConn = null;
	private final String DISCUSSOIN_COLLECTION_NAME;
	private int topic_id;
	
	public DiscussionCollection(int topic_id) {
		super();
		this.topic_id = topic_id;
		this.discussionMapId = new HashMap<String, Document>();
		dbConn = new MongoDBImpl();
		this.DISCUSSOIN_COLLECTION_NAME = DBInterface.DISCUSSION_STORE + "_" + String.valueOf(topic_id);
	}
	
	public HashMap<String, Document> getDiscussionMapId() {
		return discussionMapId;
	}
	
	public void setDiscussionMapId(HashMap<String, Document> discussionMapId) {
		this.discussionMapId = discussionMapId;
	}
	
	
	
	public void addToDiscussion(Document doc) {

		log.info("Adding document to the dicussion collection: " + doc.getString("id_str"));
		discussionMapId.put(doc.getString("id_str"), doc);
		// Update the database
		Document doc_ret = dbConn.getDoc(DISCUSSOIN_COLLECTION_NAME, "id_str", 
									doc.getString("id_str"));
		if (doc_ret != null) {
			// Delete the existing document
			log.info("Deleting existing document to the dicussion collection: " + doc.getString("id_str"));
			dbConn.deleteDoc(DISCUSSOIN_COLLECTION_NAME, "id_str", 
													doc.getString("id_str"));
		}
		dbConn.insertDoc(DISCUSSOIN_COLLECTION_NAME, doc);
		doc.append("topic_id", topic_id);
		doc.append("message_type", "DISCUSSION");
		setChanged();
		notifyObservers(doc);
	}

}
