package storm.sharon.algorithms;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Map.Entry;
import java.util.Observer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import storm.sharon.util.FileMessageQueueImpl;
import storm.sharon.util.MessageQueueInterface;


public class DiscussionStrategyA extends Observable implements DiscussionInterface {

	public static final int DECENDENT_THRESHOLD = 1;
	private HashMap<String, Node<Document>>  tweetMapId = new HashMap<String, Node<Document>>();
	private Log log = LogFactory.getLog(DiscussionStrategyA.class);
	private DiscussionCollection discColl = null;
	private MessageQueueInterface messageQueue = null;
	private int topic_id;
	 
	public DiscussionStrategyA(Observer discObserver, int topic_id) {
		messageQueue = new FileMessageQueueImpl();
		discColl = new DiscussionCollection(topic_id);
		discColl.addObserver(discObserver);
		// To send notifications on fetch tweet messages
		addObserver(discObserver);
		this.topic_id = topic_id;
	}

	
	@Override
	public void add(Document doc) {
		 String receivedId = doc.getString("id_str");
		 String parentId = doc.getString("in_reply_to_status_id_str");
	
		 // This represents the received tweet
		 Node<Document> child = null;
		 // This represents the parent tweet
		 Node<Document> parent = null;
		 
		 // Check if the received tweet already is in the database as a place holder
		 // because it is a tweet fetched as a reply
		 
		 if (tweetMapId.containsKey(receivedId)) {
			  // This is a tweet which is a reply to a previous tweet and therefore a node exists
			  // Update the data field of the existing entry 
			  log.debug("Tweet is already in the map... updating");
			  child = tweetMapId.get(receivedId);
			  child.setData(doc);
			  if (child.getDiscussionName() != null && parentId == null) {
				  // This is already in the discussion collection update and notify client
				  saveInDisCollNode(child);	
			  }
		 }
		
		 
		
		 if (parentId != null) {
			  // This tweet is in reply to a another tweet with id = parentId
			  // If the child does not already in the map create it
			 
			  if (child == null) {
				  log.debug("Creating child node id: " + receivedId);
				  child = new Node<Document>(doc);
			  }
			  
			  // Check if the parentId exists in the database from a previous retrieve
			  if (tweetMapId.containsKey(parentId)) {
				  parent = tweetMapId.get(parentId);
			  }
			  else {
				  // Create a dummy parent node a place holder
				  Document docNew =  new Document("id_str", parentId)
				  			.append("text", "TO_BE_RETRIEVED");
  				
				  parent = new Node<Document>(docNew);
				  
				  // Write the id to the message queue to be retrieved
				  // log.debug("Insert parent id to message queue for retrieval: " + parentId);	  
				  // String tidTopic = String.valueOf(parentId) + '@' + String.valueOf(topic_id);
				  // messageQueue.write(MessageQueueInterface.TID_MESSAGE_QUEUE, tidTopic);
				  
				  // Send a message to discussion queue to fetch the tweet
				  Document fetchDoc = new Document("message_type", "FETCH_TWEET");
				  fetchDoc.append("topic_id", topic_id);
				  fetchDoc.append("tid", String.valueOf(parentId) );
				  setChanged();
				  notifyObservers(fetchDoc);
				  log.debug("Generating a fetch tweet message: " + fetchDoc.toJson());
			  }
			  
			  parent.addChild(child);
			  tweetMapId.put(parentId, parent);
			  tweetMapId.put(receivedId, child);
			  
			  // TODO: Shanika Insert into the a map based on time to ease cleanup
			  //tweetMapTime.put(doc.getLong("createAtLong"), child);
			  
			  Node<Document> root = child.getRoot();
			  int treeSize = root.getNumDecendants();
	           
			  log.debug("Size of tree: " + treeSize);
			 
			  String discussionName = null;
			  if ( treeSize == DECENDENT_THRESHOLD ) {
				  // Saving the discussion for the first time. Save the tree. 
				  discussionName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
				  saveInDisCollTree(root, discussionName);  
			  }
			  else {
				  if  (treeSize > DECENDENT_THRESHOLD ) {
					  // Tree already saved in the database. 
					
					 
					  //Document docChild = dbConn.getDoc(DBInterface.DISCUSSION_STORE, "id", child.getData().getLong("id"));
					  String childDiscussionName = child.getDiscussionName();
					  String rootDiscussionName = root.getDiscussionName();
				
					  // If parent is already a part of a discussion make the discussion name
					  // equal to the parent's discussion name. Else make the discussion name the 
					  // child discussion name;
					  if (rootDiscussionName != null) {
						  discussionName = rootDiscussionName;
						  saveInDisCollNode(parent);				
						  saveInDisCollTree(child, discussionName);
					  }
					  else {
						  discussionName = childDiscussionName;
						  parent.setDiscussionName(discussionName);
						  saveInDisCollNode(parent);
						  saveInDisCollNode(child);
					  }
					   
				  }
			  }
			  
		 }
		 else {
			 log.debug("This tweet is not a reply to another tweet - no action is required");
		 }
	}
	
	private void saveInDisCollTree(Node<Document> root, String discussionName) {
		
		 root.setDiscussionName(discussionName);
		
		 saveInDisCollNode(root);
		 // Recursively traverse through the tree and save it
		 List<Node<Document>> children = root.getChildren();  
		 for (Node<Document> child: children) {
			 saveInDisCollTree(child, discussionName);
		 }
		
		 
	 }
	 
	 private void saveInDisCollNode(Node<Document> child) {
		 
		 Document doc = nodeToDoc(child);
		 discColl.addToDiscussion(doc);
	 }
	 
	 
	 private Document nodeToDoc(Node<Document> node) {
	      
		 // Save the discussion under some name
		 
		 //String discussionName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		 
		 //Document doc = new Document("discussionName", discussionName);
		
		  
		 Document doc; 
		 if (node.getParent() != null) {
			 doc = new Document("in_reply_to_status_id_str", node.getParent().getData().getString("id_str"));
		 } else {
			 doc = new Document("in_reply_to_status_id_str", null);
		 }
			
		 doc.append("id_str", node.getData().getString("id_str"));
		 doc.append("screen_name", node.getData().getString("screen_name"));
	     List<Node<Document>> children = node.getChildren();  
	     
	     List<String> child_ids = new ArrayList<String>();
	     
	     for (Node<Document> child: children) {
	    	 String childId = child.getData().getString("id_str");
	    	 child_ids.add(childId);
	     }
	     
	     // Add the list of childIds to the document
	     doc.append("children", child_ids);
	      
	     // Add the node data to document
	     doc.append("data", node.getData());
	     
	     doc.append("discussion_name", node.getDiscussionName());
	     
		 return doc;
		 
	 }
	 
	 public void print() {
		 
		  for(Entry<String, Node<Document>> entry: tweetMapId.entrySet()){
        	  log.debug("tid: " + entry.getKey()  +  "  Node: " + entry.getValue());
          }
	 }	
	

}
