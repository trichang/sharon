package storm.sharon.algorithms;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Node<T> {
	// Total number of decendents
	private static final int DEC_THRESHOLD = 8;
	private int numDecendants = 0;
	private int numChildren = 0;
	// Indicates if a parent exists 0 if one does not exist and 1 otherwise
	private int parentExists = 0;  
    private List<Node<T>> children = new ArrayList<Node<T>>();
    private Node<T> parent = null;
	private T data = null;
	private String discussionName = null;
	private Log log = LogFactory.getLog(Node.class);
	public Node() {
	        parentExists = 0;
	        numDecendants = 0;
	}
	
    public Node(T data) {
        this.data = data;
        this.parent = null;
        parentExists = 0;
        numDecendants = 0;
    }
   
    public Node(T data, Node<T> parent) {
        this.data = data;
        addParent(parent);
        parentExists = 1;
    }
    
    // This is a node which does not have data - data will be updated later.
    public Node(Node<T> parent) {
        //this.data = data;
        addParent(parent);
        parentExists = 1;
    }
    
    public int getNumDecendants() {
		return numDecendants;
	}


	public void setNumDecendants(int numDecendants) {
		this.numDecendants = numDecendants;
	}


	public int getNumChildren() {
		return numChildren;
	}


	public void setNumChildren(int numChildren) {
		this.numChildren = numChildren;
	}
	

	public String getDiscussionName() {
		return discussionName;
	}

	public void setDiscussionName(String discussionName) {
		this.discussionName = discussionName;
	}

	public int getParentExists() {
		return parentExists;
	}


	public void setParentExists(int parentExists) {
		this.parentExists = parentExists;
	}


	public void setChildren(List<Node<T>> children) {
		this.children = children;
	}


	public void setParent(Node<T> parent) {
		this.parent = parent;
	}


	public List<Node<T>> getChildren() {
        return children;
    }

    public void addParent(Node<T> parent) {
    	parent.addChild(this);
    }
    
    
    public Node<T> getParent() {
		return parent;
	}

    public void addChild(Node<T> child) {
        this.children.add(child);
        child.setParent(this);
        numChildren++;
        incrementDecendents(child.getNumDecendants());  
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isRoot() {
        return (this.parent == null);
    }

    public boolean isLeaf() {
        if(this.children.size() == 0) 
            return true;
        else 
            return false;
    }

    public void removeParent() {
        this.parent = null;
    }
    
    
    private void incrementDecendents(int n){
    	numDecendants = numDecendants + n + 1;
    	if (parent != null) {
    		parent.incrementDecendents(n);
    	}
    	else {
    		if ( numDecendants >  DEC_THRESHOLD) {
    			log.debug("Created dicussion: "+discussionName);
    		}
    	}
    }
    
    
	public Node<T> getRoot() {
    	
    	Node<T> parent;
    	
    	Node<T> root = this;
    	parent = this.getParent();
    	
    	while (parent != null) {
    		root = parent;
    		parent = parent.getParent();
    	}
    	
    	return root;
    }
    
    public String toString(){
    	String s = "Data: " + this.data;
    	if (this.getParent() != null) {
    		s = s + " Parent: " + this.getParent().getData();
    	}
    	else {
    		s = s + " Parent: this node has no parent.";
    	}
    	
    	//s = s + " Children: " + this.getChildren();
    	s = s + " NumChildren: " + numChildren;
    	s = s + " NumDecendents: " + numDecendants;
    	return s;
    }
    
  
}
