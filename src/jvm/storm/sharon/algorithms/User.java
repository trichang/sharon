package storm.sharon.algorithms;

import java.util.ArrayList;
import java.util.HashMap;

import org.bson.Document;

import scala.Tuple2;

public class User {
	
	
	private String screen_name;
	private String last_tid;
	private int totalMentions = 0;
	private int totalReplies = 0;
	private boolean trackedUser = false;
	private HashMap<String, Integer> mentionReceived = new HashMap<String, Integer>();
	private HashMap<String, Integer> replyReceived = new HashMap<String, Integer>();
	private ArrayList<Tuple2>  mentioned = new ArrayList<Tuple2>();
	private ArrayList<Tuple2>  hashtags = new ArrayList<Tuple2>();
	private ArrayList<Tuple2>  repliedTo = new ArrayList<Tuple2>();
	private ArrayList<Tuple2>  retweeted = new ArrayList<Tuple2>();

	public User(String screen_name) {
		this.screen_name = screen_name;
		last_tid = "0";
	}
	
	public User(Document doc) {
		screen_name = doc.getString("user_name");
		totalMentions = doc.getInteger("total_mentions");
		totalReplies = doc.getInteger("total_replies");
		ArrayList<Document> mentionedBy = (ArrayList<Document>)doc.get("mentioning_list");
		
		if (mentionedBy != null) {
			for (Document d: mentionedBy ) {
				mentionReceived.put(d.getString("mentioning_user_name"), d.getInteger("mention_count"));
			}
		}
	
		ArrayList<Document> repliedTo = (ArrayList<Document>)doc.get("replying_list");
		
		if (repliedTo != null) {
			for (Document d: repliedTo ) {
				replyReceived.put(d.getString("replying_user_name"), d.getInteger("reply_count"));
			}
		}
	
		last_tid = "0";
	}

	public ArrayList<Tuple2>  getMentioned() {
		return mentioned;
	}
	
	public ArrayList<Tuple2>  getHashtags() {
		return hashtags;
	}
	
	
	public ArrayList<Tuple2>  getRepliedTo() {
		return repliedTo;
	}
	
	public ArrayList<Tuple2>  getRetweeted() {
		return retweeted;
	}
	
	public HashMap<String, Integer> getMentionReceived() {
		return mentionReceived;
	}
	
	public HashMap<String, Integer> getReplyReceived() {
		return replyReceived;
	}
	
	public int getTotalMentions() {
		return totalMentions;
	}
	
	public boolean getTrackedUser() {
		return trackedUser;
	}
	public void incrementMentions() {
		totalMentions++;
	}
	
	public void incremenReplies() {
		totalReplies++;
	}

	public void setTrackedUser(boolean b) {
		trackedUser = b;
	}

	public int getTotalReplies() {
		return totalReplies;
	}
	
	
	
}
