package storm.sharon.algorithms;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import scala.Tuple2;
import storm.sharon.util.FileMessageQueueImpl;
import storm.sharon.util.MessageQueueInterface;


public class CommunityStrategyA implements CommunityInterface  {

	
	private Log log = LogFactory.getLog(CommunityStrategyA.class);
	private MessageQueueInterface messageQueue = null;
	private HashMap<String, User> userMap = null;
	private HashMap<String, User> replyMap = null;
	private final static int MENTION_THRESHOLD = 20;
	private final static int MENTION_USER_THRESHOLD = 10;
	private final static int MENTION_USER_THRESHOLD_2 = 2;
	private static final int REPLY_THRESHOLD = 2;
	private static final int REPLY_USER_THRESHOLD = 2;
	
	private int topic_id;
	private CommunityCollection communityColl;
	
	/*private  HashMap<String, Tuple2> mentionCount = null;
	private  HashMap<String, Tuple2> hashCount = null;
	private  HashMap<String, Tuple2> retweetCount = null;
	private  HashMap<String, Tuple2> repliedCount = null;*/
	 
	public CommunityStrategyA(Observer commObserver, int topic_id) {
		messageQueue = new FileMessageQueueImpl();
		userMap = new HashMap<String, User>();
		replyMap = new HashMap<String, User>();
		this.topic_id = topic_id;
		communityColl = new CommunityCollection(commObserver, topic_id);
		/*mentionCount = new HashMap<String, Tuple2>();
		hashCount = new HashMap<String, Tuple2>();
		retweetCount = new HashMap<String, Tuple2>();
		repliedCount = new HashMap<String, Tuple2>();*/
	}

	
	@Override
	public void process(Document doc) {
		
		String screen_name =  ((Document)doc.get("user")).getString("screen_name");
		
		List<Document> mentions = (List<Document>)((Document)doc.get("entities")).get("user_mentions");
		
		if (mentions != null) {
			for (Document m: mentions) {
				String mention = m.getString("screen_name");
				//user.getMentioned().add(new Tuple2(timestamp, mention));
			    User mentionedUser = null;
				if (userMap.containsKey(mention)) {
					mentionedUser = userMap.get(mention);
				} else {
					mentionedUser = new User(mention);
				    userMap.put(mention, mentionedUser);
				}
				int count = 0;
				if (mentionedUser.getMentionReceived().containsKey(screen_name)) {
					count = mentionedUser.getMentionReceived().get(screen_name);
					count++;
				} else {
					count = 1;
				}
				mentionedUser.getMentionReceived().put(screen_name, count);
				mentionedUser.incrementMentions();
				
				User tUser;
				int uCount = 0;
				// Check if the user has mentioned the user
				/*if (userMap.containsKey(screen_name)) {
					tUser = userMap.get(screen_name);
				    if (tUser.getMentionReceived().containsKey(mention)) {
						uCount =  tUser.getMentionReceived().get(mention);
					}
				} */
				
				log.debug("MENTION TopicID: " + topic_id + " User: " + screen_name + " Mentioned: " + mention + " total Mentions: " + mentionedUser.getTotalMentions()
						+ " number of mentioning users: " + mentionedUser.getMentionReceived().size()
						  + " number of mentions received " + uCount) ;
				
				if ((mentionedUser.getTotalMentions() >=  MENTION_THRESHOLD) 
						&& (mentionedUser.getMentionReceived().size() >= MENTION_USER_THRESHOLD)){
					//	&& (uCount >= MENTION_USER_THRESHOLD_2)) {
		
					Document docRet =  new Document("user_name", mention);
					docRet.append("total_mentions", mentionedUser.getTotalMentions());
					docRet.append("total_replies", 0);
					ArrayList<Document> mentionedBy = new ArrayList<Document>();
					for (Entry<String, Integer> u: mentionedUser.getMentionReceived().entrySet() ) {
						Document doc2 = new Document("mentioning_user_name", u.getKey());
						doc2.append("mention_count", u.getValue());
						mentionedBy.add(doc2);
					}
					docRet.append("mentioning_list", mentionedBy);
					docRet.append("topic_id", topic_id);
					
					log.debug("MENTION TopicID: Adding " + mention + " to Community Collection");
						
					communityColl.addToCommunity(docRet, true);
					
				}
		
			}
	
		}
		
		String inreplyto_name = doc.getString("in_reply_to_screen_name");
		
		User repliedToUser = null;
		if (inreplyto_name != null) {
			if (replyMap.containsKey(inreplyto_name)) {
				repliedToUser = replyMap.get(inreplyto_name);
			} else {
				repliedToUser = new User(inreplyto_name);
			    replyMap.put(inreplyto_name, repliedToUser);
			}
			
			int count = 0;
			if (repliedToUser.getReplyReceived().containsKey(screen_name)) {
				count = repliedToUser.getReplyReceived().get(screen_name);
				count++;
			} else {
				count = 1;
			}
			repliedToUser.getReplyReceived().put(screen_name, count);
			repliedToUser.incremenReplies();
			
			log.debug("REPLY TopicID: " + topic_id + " User: " + screen_name + " Replied to: " + inreplyto_name + " "
					+ "total Replies: " + repliedToUser.getTotalReplies()
					+ " number of replying users: " + repliedToUser.getReplyReceived().size());
			
			if ((repliedToUser.getTotalReplies() >=  REPLY_THRESHOLD) 
					&& (repliedToUser.getReplyReceived().size() >= REPLY_USER_THRESHOLD)) {
	
				Document docRet =  new Document("user_name", inreplyto_name);
				docRet.append("total_replies", repliedToUser.getTotalReplies());
				docRet.append("total_mentions", 0);
				ArrayList<Document> repliedBy = new ArrayList<Document>();
				for (Entry<String, Integer> u: repliedToUser.getReplyReceived().entrySet() ) {
					Document doc2 = new Document("replying_user_name", u.getKey());
					doc2.append("reply_count", u.getValue());
					repliedBy.add(doc2);
				}
				docRet.append("replying_list", repliedBy);
				docRet.append("topic_id", topic_id);
				
				log.debug("REPLY TopicID: Adding " + inreplyto_name + " to Community Collection");
					
				communityColl.addToCommunity(docRet, true);
				
			}
					  
			
		}
		

	}


	@Override
	public void clearUserMap() {
		userMap = new HashMap<String, User>();	
	}


	@Override
	public void clearReplyMap() {
		replyMap = new HashMap<String, User>();
	}

}
