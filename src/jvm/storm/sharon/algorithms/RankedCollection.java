package storm.sharon.algorithms;

import java.util.HashMap;
import java.util.Observable;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

// We might not really need this class,  just a place holder in case we need
// persist ranked tweets.

public class RankedCollection extends Observable {
	
	private Log log = LogFactory.getLog(RankedCollection.class);
	
	
	public RankedCollection() {
		
	}
	
	
	
	public void add(Document doc) {
		setChanged();
		notifyObservers(doc);
	}
	

}
