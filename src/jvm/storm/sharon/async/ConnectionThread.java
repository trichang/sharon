package storm.sharon.async;

import org.graphstream.graph.Graph;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.apache.storm.shade.org.joda.time.DateTime;
import org.graphstream.graph.Node;

/**
 * Created by tri on 17/04/16.
 */
public class ConnectionThread extends Thread {
    private PrintWriter outStream;
    private BufferedReader inStream;
    private static double sumDuration = 0;
    private static int countDuration = 0;

    private Graph g;
    private ConcurrentHashMap<String, HashSet<String>> communityList;

    public ConnectionThread(Graph g, ConcurrentHashMap<String, HashSet<String>> communityList, PrintWriter out, BufferedReader in) {
        super();

        this.inStream = in;
        this.outStream = out;
        this.communityList = communityList;

        this.g = g;
    }
    public ConnectionThread(ConcurrentHashMap<String, HashSet<String>> communityList, PrintWriter out, BufferedReader in) {
        super();

        this.inStream = in;
        this.outStream = out;
        this.communityList = communityList;

        this.g = g;
    }

    public void run() {
        String inputLine, outputLine;

        try {
            while ((inputLine = inStream.readLine()) != null) {
//                JSONObject obj = (JSONObject) (new JSONParser().parse(inputLine));

                System.err.println("INV TYPE: " + inputLine);
//                switch (obj.get("type").toString()) {
//                    case "edge":
//                        System.out.println("Edge: " + obj);
//                        addEdge(obj.get("from").toString(), obj.get("to").toString());
//                        break;
//                    case "community":
//                        String seed = obj.get("seed").toString();
//                        String[] nodeList = obj.get("value").toString().replace("[", "").replace("]", "").split(",");
//
//                        DateTime start = DateTime.parse(obj.get("start").toString());
//                        DateTime end = DateTime.parse(obj.get("end").toString());
//                        double duration = (end.getMillis() - start.getMillis())/1000.;
//                        countDuration++;
//                        sumDuration += duration;
//
//                        System.out.println("Community: " + obj);
//                        System.out.printf("[avg:%f] Sequence %s running in %f seconds\n", (sumDuration/countDuration), obj.get("sequence"), duration);
//
//                        HashSet<String> community = communityList.get(seed);
//                        if (community == null) {
//                            community = new HashSet<String>();
//                            communityList.put(seed, community);
//                        }
//
//                        community.addAll(new HashSet<String>(Arrays.asList(nodeList)));
//                        break;
//                    default:
//                        System.err.println("INV TYPE: " + inputLine);
//                        break;
//                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private synchronized void addEdge(String a, String b) {
        Node A = g.getNode(a);
        if (A == null) A = g.addNode(a);
        A.setAttribute("ui.label", a);

        Node B = g.getNode(b);
        if (B == null) B = g.addNode(b);
        B.setAttribute("ui.label", b);

        try {
            if (a.compareTo(b) > 0) {
                g.addEdge(b + a, B, A);
            } else {
                g.addEdge(a + b, A, B);
            }
        } catch (Exception ignored) {}
    }
}
