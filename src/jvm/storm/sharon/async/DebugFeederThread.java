package storm.sharon.async;

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.graphstream.graph.Graph;
import storm.sharon.CommunityGUI;
import storm.sharon.gui.core.CommandQueueListener;
import storm.sharon.gui.core.MessageQueueListener;
import storm.sharon.util.MessageQueueInterface;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by tri on 26/04/16.
 */
public class DebugFeederThread extends Thread {
//    private static Logger log = LogManager.getLogger(DebugFeederThread.class.getName());
    protected String topic;
    protected Observable guiSubject;
    protected Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap;
    public boolean ready=false;
    private Graph g;
    class myObserver extends Observable {

        @Override
        public synchronized void addObserver(Observer o) {
            super.addObserver(o);
        }

        public void setData(Document d) {
            setChanged();
            notifyObservers(d);
        }
    }

    public DebugFeederThread(String topic, Graph g) {
        this.topic = topic;
        this.g = g;
        switch(topic){
            case MessageQueueInterface.KEYWORD_TOPIC_MESSAGE_QUEUE: // the keyword-topic is also where we receive commands
                consumerMap = CommandQueueListener.getConsumerMap();
                break;
            default:
                consumerMap = MessageQueueListener.getConsumerMap();
        }
        guiSubject = new DebugFeederThread.myObserver();
    }

    public Observable getGuiSubject() {
        return guiSubject;
    }

    public void register(Observer obs) {
        guiSubject.addObserver(obs);
    }

    @Override
    public void run() {
        KafkaStream<byte[], byte[]> stream = consumerMap.get(topic).get(0);
        ConsumerIterator<byte[], byte[]> it = stream.iterator();
        ready=true;
        while (it.hasNext()) {
            String receivedData = new String(it.next().message());

            System.out.println("["+topic+"] Received: " + receivedData);
        }
    }
}
