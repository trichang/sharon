package storm.sharon.async;

import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.swingViewer.Viewer;
import org.graphstream.ui.swingViewer.ViewerListener;
import org.graphstream.ui.swingViewer.ViewerPipe;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import storm.sharon.util.Settings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tri on 17/04/16.
 */
public class CommunityViewer extends Thread {
    private Settings setting;
    private ConcurrentHashMap<String, HashSet<String>> communityList = new ConcurrentHashMap<String, HashSet<String>>();

    private String styleSheet="node {"+
            "fill-mode: dyn-plain;"+
            " fill-color: #999, #F53, #55F;"+
            " size: 30px;"+
            " stroke-mode: plain;"+
            " stroke-color: black;"+
            " stroke-width: 1px;"+
            "}"+
            "node.important {"+
            " fill-color: red;"+
            //" size: 30px;"+
            "}";

    public CommunityViewer() {
        super();

        setting = Settings.getInstance();

        System.setProperty("sun.java2d.directx", "True");
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
    }

    public void run() {
//        try {
//            String inputLine = "{ \"count\" : 4, \"actualWindowLength\" : 0, \"topic_id\" : 1, \"dest_node\" : \"Smallzy\", \"src_node\" : \"cubamelodies\", \"time\" : { \"$numberLong\" : \"1460894043131\" }, \"query_id\" : \"window_mention\" }";
//            JSONObject obj = (JSONObject) (new JSONParser().parse(inputLine));
//            System.out.println(obj.get("src_node"));
//            System.out.println(obj.get("dest_node"));
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }

        System.out.println("Graphic server started.");
        Graph g = display();

        try {
            ServerSocket serverSocket = new ServerSocket(setting.port);
            while (true) {
                try {
                    Socket clientSocket = serverSocket.accept();
                    System.out.println("New connection accepted.");

                    PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                    BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

//                    Thread thread = new ConnectionThread(g, communityList, out, in);
                    Thread thread = new ConnectionThread(communityList, out, in);
                    thread.setDaemon(true);

                    thread.run();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception ex) { ex.printStackTrace(); }
    }

    public Graph display() {
        final Graph graph = new SingleGraph("Community View");
        graph.addAttribute("ui.stylesheet", styleSheet);

//        Viewer viewer = graph.display();
        graph.display();

//        final ViewerPipe fromViewer = viewer.newViewerPipe();
//
//        fromViewer.addSink(graph);
//
//        new Thread() {
//            volatile boolean isRunning = true;
//
//            @Override
//            public void run() {
//                fromViewer.addViewerListener(new ViewerListener() {
//                    @Override
//                    public void viewClosed(String s) {
//                        isRunning = false;
//                    }
//
//                    @Override
//                    public void buttonPushed(String s) {
//                        if (communityList.get(s) != null)
//                            for (String i : communityList.get(s)) {
//                                graph.getNode(i).addAttribute("ui.color", 0.5);
//                            }
//                    }
//
//                    @Override
//                    public void buttonReleased(String s) {
//                        if (communityList.get(s) != null)
//                            for (String i : communityList.get(s)) {
//                                graph.getNode(i).addAttribute("ui.color", 0);
//                            }
//                    }
//                });
//
//                while (isRunning) {
//                    fromViewer.pump();
//
//                    try {
//                        Thread.sleep(100);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//        }.start();

        return graph;
    }

    public static void main(String[] args) {
        CommunityViewer server = new CommunityViewer();
        server.start();
    }
}
