package storm.sharon.async;

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import storm.sharon.CommunityGUI;
import storm.sharon.gui.core.CommandQueueListener;
import storm.sharon.gui.core.MessageQueueListener;
import storm.sharon.util.MessageQueueInterface;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by tri on 25/04/16.
 */
public class GraphFeederThread extends Thread {
    private static Logger log = LogManager.getLogger(GraphFeederThread.class.getName());
    protected String topic;
    protected Observable guiSubject;
    protected Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap;
    public boolean ready=false;
    private Graph g;
    class myObserver extends Observable {

        @Override
        public synchronized void addObserver(Observer o) {
            super.addObserver(o);
        }

        public void setData(Document d) {
            setChanged();
            notifyObservers(d);
        }
    }

    public GraphFeederThread(String topic, Graph g) {
        this.topic = topic;
        this.g = g;
        switch(topic){
            case MessageQueueInterface.KEYWORD_TOPIC_MESSAGE_QUEUE: // the keyword-topic is also where we receive commands
                consumerMap = CommandQueueListener.getConsumerMap();
                break;
            default:
                consumerMap = MessageQueueListener.getConsumerMap();
        }
        guiSubject = new GraphFeederThread.myObserver();
    }

    public Observable getGuiSubject() {
        return guiSubject;
    }

    public void register(Observer obs) {
        guiSubject.addObserver(obs);
    }

    @Override
    public void run() {
        KafkaStream<byte[], byte[]> stream = consumerMap.get(topic).get(0);

        ConsumerIterator<byte[], byte[]> it = stream.iterator();
        log.debug("reading Kafka topic: " + topic);
        ready=true;
        while (it.hasNext()) {
            String receivedData = new String(it.next().message());

            Document d = Document.parse(receivedData);
            d.append("rapid_queue_name", topic);
            ((GraphFeederThread.myObserver) guiSubject).setData(d);

            String src= "" , dest = "", count="";
            try {
                JSONObject obj = (JSONObject) (new JSONParser().parse(receivedData));
                JSONObject edgeObj = (JSONObject) (new JSONParser().parse(obj.get("tuples").toString()));
                src = edgeObj.get("src_node").toString();
                dest = edgeObj.get("dest_node").toString();
                count = edgeObj.get("count").toString();
System.out.println(src+ " .. " + dest);
//                addEdge(src, dest);
//                addEdge(dest, src);
            } catch (Exception ex) { System.err.println(ex.getMessage()); }
        }
        log.warn("terminating Kafka topic: "+topic);
    }

    private synchronized void addEdge(String a, String b) {
        Node A = g.getNode(a);
        if (A == null) A = g.addNode(a);
        A.setAttribute("ui.label", a);

        Node B = g.getNode(b);
        if (B == null) B = g.addNode(b);
        B.setAttribute("ui.label", b);

        try {
            if (a.compareTo(b) > 0) {
                g.addEdge(b + a, B, A);
            } else {
                g.addEdge(a + b, A, B);
            }
        } catch (Exception ignored) {}
    }
}
