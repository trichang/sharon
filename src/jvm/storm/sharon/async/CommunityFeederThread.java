package storm.sharon.async;

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.graphstream.graph.Graph;
import storm.sharon.CommunityGUI;
import storm.sharon.gui.core.CommandQueueListener;
import storm.sharon.gui.core.MessageQueueListener;
import storm.sharon.util.MessageQueueInterface;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by tri on 25/04/16.
 */
public class CommunityFeederThread extends Thread {
    private static Logger log = LogManager.getLogger(CommunityFeederThread.class.getName());
    protected String topic;
    protected Observable guiSubject;
    protected Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap;
    public boolean ready=false;
    private Graph g;
    class myObserver extends Observable {

        @Override
        public synchronized void addObserver(Observer o) {
            super.addObserver(o);
        }

        public void setData(Document d) {
            setChanged();
            notifyObservers(d);
        }
    }

    public CommunityFeederThread(String topic, Graph g) {
        this.topic = topic;
        this.g = g;
        switch(topic){
            case MessageQueueInterface.KEYWORD_TOPIC_MESSAGE_QUEUE: // the keyword-topic is also where we receive commands
                consumerMap = CommandQueueListener.getConsumerMap();
                break;
            default:
                consumerMap = MessageQueueListener.getConsumerMap();
        }
        guiSubject = new CommunityFeederThread.myObserver();
    }

    public Observable getGuiSubject() {
        return guiSubject;
    }

    public void register(Observer obs) {
        guiSubject.addObserver(obs);
    }

    @Override
    public void run() {
        KafkaStream<byte[], byte[]> stream = consumerMap.get(topic).get(0);
        ConsumerIterator<byte[], byte[]> it = stream.iterator();
        log.debug("reading Kafka topic: " + topic);
        ready=true;
        while (it.hasNext()) {
            String receivedData = new String(it.next().message());
            //TODO: logic to get GUI

            Document d = Document.parse(receivedData);
            d.append("rapid_queue_name", topic);
            ((CommunityFeederThread.myObserver) guiSubject).setData(d);
        }
        log.warn("terminating Kafka topic: "+topic);
    }
}
