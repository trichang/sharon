package storm.sharon.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.Random;

import org.bson.Document;

import storm.sharon.gui.core.MessageQueueListener;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;


public class TestConsumer extends Thread {
	
	private final ConsumerConnector consumer;
	private final String zookeeper;
	private String topic;
	private Observable guiSubject;
	
	class myObserver extends Observable {
		
		public void setData(Document d) {
			setChanged();
			notifyObservers(d);
		}
		
	}
	
	public TestConsumer(String zookeeper, String topic) {
		this.topic = topic;
		this.zookeeper = zookeeper;
		consumer = kafka.consumer.Consumer.createJavaConsumerConnector(createConsumerConfig());
		guiSubject = new myObserver();
		//
	}
	
	public Observable getGuiSubject() {
		return guiSubject;
	}

	public void register(Observer obs) {
		guiSubject.addObserver(obs);
	}
	/**
	 * Creates the consumer config.
	 *
	 * @return the consumer config
	 */
	private ConsumerConfig createConsumerConfig() {
		Properties props = new Properties();
		//props.put("zookeeper.connect", KafkaMailProperties.zkConnect);
		//props.put("group.id", KafkaMailProperties.groupId);
		//props.put("zookeeper.connect", "localhost:2181");
		props.put("zookeeper.connect", zookeeper);
		props.put("group.id", "kafka-consumer-" + new Random().nextInt(100000));
		//props.put("group.id", "group4");
		props.put("zookeeper.session.timeout.ms", "400");
		props.put("zookeeper.sync.time.ms", "200");
		props.put("auto.commit.interval.ms", "1000");

		return new ConsumerConfig(props);

	}


	@Override
	public void run() {

		Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
		topicCountMap.put(topic, new Integer(1));
		Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer.createMessageStreams(topicCountMap);
		KafkaStream<byte[], byte[]> stream = consumerMap.get(topic).get(0);
		ConsumerIterator<byte[], byte[]> it = stream.iterator();
		System.out.println("GuiApplication: Listening to topic: " + topic);
		while (it.hasNext()) {
			String receivedData = new String(it.next().message());
			System.out.println(receivedData);
			Document d = Document.parse(receivedData);
			((myObserver) guiSubject).setData(d);	
		}
	}
	
	public static void main(String[] args) {	
		
		
		System.out.println("Running");
		
		
		
	//	MessageQueueListener keyword_topic = new MessageQueueListener(args[0],  "keyword-topic");
	//	keyword_topic.start();
		
	}
		
	

}
