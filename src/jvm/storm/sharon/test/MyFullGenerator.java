package storm.sharon.test;
import java.util.Scanner;

import org.graphstream.algorithm.generator.Generator;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.SourceBase;

public class MyFullGenerator extends SourceBase
      implements Generator {
 
   int currentIndex = 0;
   int edgeId = 0;
   static String node = null;
   static String s_node = null;
   static String d_node = null;
   static String command = null;
   static Integer size = 10;
   static Graph graph = null;
 
   public void begin() {
     // addNode();
   }
 
   /*public boolean nextEvents() {
      addNode();
      return true;
   }*/
 
   public void end() {
      // Nothing to do
   }
 
  /* protected void addNode() {
      sendNodeAdded(sourceId, Integer.toString(currentIndex));
 
      for(int i = 0; i < currentIndex; i++)
         sendEdgeAdded(sourceId, Integer.toString(edgeId++),
               Integer.toString(i), Integer.toString(currentIndex), false);
 
      currentIndex++;
   }
*/
	@Override
	public boolean nextEvents() {
		// TODO Auto-generated method stub
		//addNode();
		if (command.compareTo("ADD_NODE") == 0) {
			if (graph.getNode(node) == null) {
				sendNodeAdded(sourceId, node);
				sendNodeAttributeAdded(sourceId, node, "ui.label", node);
			}
		} else if (command.compareTo("ADD_EDGE") == 0) {
			sendEdgeAdded(sourceId, s_node + d_node, s_node, d_node,  true);
		}
		else if (command.compareTo("ADD_SIZE") == 0) {
			System.out.println("Setting Node Size to " + size);
			sendNodeAttributeAdded(sourceId, node, "ui.size", size);
			//sendEdgeAdded(sourceId, s_node + d_node, s_node, d_node,  true);
		}
		return false;
	}
	
	
	public static void main(String[] args) {
		
		System.setProperty("org.graphstream.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		Generator gen = new MyFullGenerator();
		graph = new SingleGraph("Tutorial 1");
		//graph.addAttribute("ui.stylesheet", "url('file:///C:/UserPrograms/stylesheet')");
		graph.addAttribute("ui.stylesheet", "url('file:///media/karuspriv/ShanikaShared/Research/TwitterStorm/storm/examples/newclone/sharon/stylesheet')");
		gen.addSink(graph);
		graph.addNode("A");
		graph.getNode("A").addAttribute("ui.label", "A");
		graph.display();
		Scanner s = new Scanner(System.in);
		
		System.out.println("Enter Yes to cotinue");
		while ( s.next().compareTo("Yes") == 0) {
			
			System.out.println("Enter comma nd");
			command = s.next();
			if (command.compareTo("ADD_NODE") == 0) {
				System.out.println("Enter node ID to add");
				node = s.next();
			} else if (command.compareTo("ADD_EDGE") == 0) {
				System.out.println("Enter source ID");
				s_node = s.next();
				System.out.println("Enter dest ID");
				d_node = s.next();
			} else if (command.compareTo("ADD_SIZE") == 0) {
				System.out.println("Enter  node ID");
				node = s.next();
				System.out.println("Enter Size");
				size = s.nextInt();
				s.next();
			}
			
			
			gen.nextEvents();
			System.out.println("Enter Yes to cotinue");
		}
		
		
		
		
		
	
	}
}