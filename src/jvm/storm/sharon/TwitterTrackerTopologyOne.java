/**  
* TwitterTrackerTopologyOne.java - this defines the Storm Topology and excutes it.  
* @author  Shanika Karunasekera
* @version 1.0 
*/

package storm.sharon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.utils.Utils;
import storm.kafka.BrokerHosts;
import storm.kafka.SpoutConfig;
import storm.kafka.StringScheme;
import storm.kafka.ZkHosts;
import storm.kafka.bolt.KafkaBolt;
import storm.kafka.bolt.mapper.FieldNameBasedTupleToKafkaMapper;
import storm.kafka.trident.TridentKafkaState;
import storm.sharon.bolt.*;
import storm.sharon.spout.*;
import storm.sharon.util.*;


/*
 * The sharon main topology creater
 */
public class TwitterTrackerTopologyOne {
	private static Log log = LogFactory.getLog(TwitterTrackerTopologyOne.class);
	public static int MAX_NUM_TOPICS = 50;
	
	@SuppressWarnings("serial")
	public static class PrinterBolt extends BaseBasicBolt {

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
		}

		@Override
		public void execute(Tuple tuple, BasicOutputCollector collector) {
		//LOG.info(tuple.toString());
			System.out.println(tuple.toString());
		}
	}
	
	public static void InitDatabase(){
		/*
		 * If the database does not contain any topics then put a default topic.
		 */
		log.info("Initializing database.");
		DBInterface dbConn = new MongoDBImpl();
        Document doc = new Document();
        doc.append("topic_name", "Climate Change");
        doc.append("topic_id", 0);
        doc.append("isCurrent", true);
        doc.append("topic_type", 0); // make the topic public
        doc.append("topic_owner", SharonUser.DEFAULT_SHARON_USER);
        ArrayList<String> shared_users = new ArrayList<String>();
		shared_users.add(SharonUser.DEFAULT_SHARON_USER);
		doc.append("shared_users", shared_users);
        ArrayList<Document> keywords = new ArrayList<Document>();
        Document  keyword = new Document("keyword", "climatechange");
        keyword.append("type", 0);
        keyword.append("isCurrent", true);
        keyword.append("isBlacklisted", false);
        keywords.add(keyword);
        doc.append("keywords", keywords);
        if (!dbConn.containsDoc(DBInterface.TOPIC_KEYWORD_STORE, "topic_id", doc.getInteger("topic_id"))) {
			log.warn("Adding a default topic to database since it does not currently contain a topic.");
			dbConn.insertDoc(DBInterface.TOPIC_KEYWORD_STORE, doc);
		}	
        
        SharonUser user = new SharonUser(SharonUser.DEFAULT_SHARON_USER);
        user.addToAllQueues("Climate Change");
        Document doc2 = user.toDocument();
        if (!dbConn.containsDoc(DBInterface.USER_STORE, "user_name", doc2.getString("user_name"))) {
			log.warn("Adding a default user to database since it does not currently contain the user.");
			dbConn.insertDoc(DBInterface.USER_STORE, doc2);
		}	
        
        
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static TopologyBuilder BuildTopology(Config conf){
		log.info("Building toplogy."); 
		String kafkaBrokers;
		String zookeeper = null;
		int numLabellerBolts = 10; // not limit
		int numStreamTrackerSpouts = 1;  // limit to the number of credentials
		int numTweetTrackerSpouts = 1;  // limit to the number of credentials
		int numUserTimelineTrackerSpouts = 1; // limit to the number of credentials
		int numFilterBolts = 10;   // not limited 
		int numTupleGeneratorBolts = 10; // not limited
		int numGraphBolts = Settings.getInstance().numGraphBolts; // not limited
		int numClusterBolts = Settings.getInstance().numClusterBolts; // not limited
		int numRollingTupleCounterBolts = 10; // not limited
		int maxNumTopics = MAX_NUM_TOPICS;  // Maximum number of topics supported by the system 
		int numMessageQueueBolts = 1; // currently set to on, but can be unlimited, but could impact the order of messages sent to the GUI
		int numCommandHandlerBolts = 1; // must be equal to  1
		int numCommandHandlerSpouts = 1; // must be equal to 1
		
		PropertiesConfiguration config = storm.sharon.util.Properties.getConfig();
		
		if(config!=null){
			kafkaBrokers = config.getString("kafka.brokerlist");
			zookeeper = config.getString("kafka.zookeeper");
			if(config.containsKey("numLabellerBolts")) numLabellerBolts=config.getInt("numLabellerBolts");
			if(config.containsKey("numStreamTrackerSpouts")) numStreamTrackerSpouts=config.getInt("numStreamTrackerSpouts");
			if(config.containsKey("numTweetTrackerSpouts")) numTweetTrackerSpouts=config.getInt("numTweetTrackerSpouts");
			if(config.containsKey("numUserTimelineTrackerSpouts")) numUserTimelineTrackerSpouts=config.getInt("numUserTimelineTrackerSpouts");
			if(config.containsKey("numFilterBolts")) numFilterBolts=config.getInt("numFilterBolts");
			if(config.containsKey("numTupleGeneratorBolts")) numTupleGeneratorBolts=config.getInt("numTupleGeneratorBolts");
			if(config.containsKey("numRollingTupleCounterBolts")) numRollingTupleCounterBolts=config.getInt("numRollingTupleCounterBolts");
			if(config.containsKey("maxNumTopics")) maxNumTopics=config.getInt("maxNumTopics");
			if(config.containsKey("numMessageQueueBolts")) numMessageQueueBolts=config.getInt("numMessageQueueBolts");
			if(config.containsKey("numCommandHandlerBolts")) numCommandHandlerBolts=config.getInt("numCommandHandlerBolts");
			if(config.containsKey("numCommandHandlerSpouts")) numCommandHandlerSpouts=config.getInt("numCommandHandlerSpouts");
		} else {
			log.warn("using default properties: kafka.brokerlist=localhost:9092, kafka.zookeeper=localhost:2181");
			kafkaBrokers="localhost:9092";
			zookeeper = "localhost:2181";
		}
		

		numStreamTrackerSpouts = CredentialsManager.getNumTwitterCredentials();
		numTweetTrackerSpouts = numStreamTrackerSpouts;
		numUserTimelineTrackerSpouts = numStreamTrackerSpouts;
		
		// TODO: redesign to handle unlimited number of topics
		TopologyBuilder builder = new TopologyBuilder();
		
		builder.setSpout("streamTracker", new TwitterStreamTrackerSpout(zookeeper), numStreamTrackerSpouts);//.setNumTasks(2);
		builder.setSpout("userTimelineTracker", new TwitterUserTimelineSpout(zookeeper), numStreamTrackerSpouts);
		builder.setSpout("tweetIdTracker", new TwitterTweetTrackerSpout(zookeeper), numTweetTrackerSpouts);
		
		builder.setBolt("labeller", new TopicLabellerBolt(), numLabellerBolts)
			   .shuffleGrouping("streamTracker")
			   .shuffleGrouping("tweetIdTracker")
			   .allGrouping("command_handler");
		
		builder.setBolt("filter", new FilterBolt(), numFilterBolts)
			   .shuffleGrouping("labeller");	

		builder.setBolt("tupleGenerator", new MRH_TupleGeneratorBolt(), numTupleGeneratorBolts)
			   .shuffleGrouping("labeller");

		builder.setBolt("rollingTupleCounter", new WindowedCommunityDetectorBolt(), numRollingTupleCounterBolts)
				.fieldsGrouping("tupleGenerator", new Fields("tupleKey"))
				.allGrouping("command_handler");

		builder.setBolt("communityGraphBolt101", new GraphBolt(0.99, 0.01), numGraphBolts)
				.allGrouping("rollingTupleCounter");
		builder.setBolt("communityClusteringBolt101", new ClusterBolt(), numClusterBolts)
				.fieldsGrouping("communityGraphBolt101", new Fields("seed"));
		
		builder.setBolt("tweetCollector", new TweetCollectBolt(), maxNumTopics)
		       .customGrouping("labeller", new TopicGrouping());
		
		builder.setBolt("discussionAggregator", new DiscussionAggregatorBolt(), maxNumTopics)
		       .customGrouping("tweetCollector", new TopicGrouping());
		
		builder.setBolt("ranker", new RankerBolt(), maxNumTopics)
		       .customGrouping("tweetCollector", new TopicGrouping())
		       .allGrouping("command_handler"); 
		
		
		KafkaBolt<String, String> k1 = new UserMessageQueueBolt<String, String>(MessageQueueInterface.TWEET_MESSAGE_QUEUE,"sharon_tweet")
				.withTupleToKafkaMapper(new FieldNameBasedTupleToKafkaMapper());
		
		
		builder.setBolt("tweetMessageQueue", k1, numMessageQueueBolts)
		       .shuffleGrouping("tweetCollector")
				.allGrouping("command_handler");
		

		KafkaBolt<String, String> k2 = new UserMessageQueueBolt<String, String> (MessageQueueInterface.DISCUSSION_MESSAGE_QUEUE, "discussion")
				.withTupleToKafkaMapper(new FieldNameBasedTupleToKafkaMapper());
		
		builder.setBolt("discussionMessageQueue", k2, numMessageQueueBolts)
		       .shuffleGrouping("discussionAggregator")
		       .allGrouping("command_handler");
		
	    KafkaBolt<String, String> k3 = new MessageQueueBolt<String, String> (MessageQueueInterface.KEYWORD_TOPIC_MESSAGE_QUEUE, "command_response")
	    	   .withTupleToKafkaMapper(new FieldNameBasedTupleToKafkaMapper());
	    
	    builder.setBolt("keywordMessageQueue", k3, numMessageQueueBolts)
	           .shuffleGrouping("command_handler");
		
		KafkaBolt<String, String> k4 = new UserMessageQueueBolt<String, String> (MessageQueueInterface.COMMUNITY_MESSAGE_QUEUE, "community")
			   .withTupleToKafkaMapper(new FieldNameBasedTupleToKafkaMapper());
		
			
		KafkaBolt<String, String> k5 = new UserMessageQueueBolt<String, String> (MessageQueueInterface.TUPLE_MESSAGE_QUEUE, "tuples")
				.withTupleToKafkaMapper(new FieldNameBasedTupleToKafkaMapper());
		
		builder.setBolt("tupleMessageQueue", k5, numMessageQueueBolts)
		       .shuffleGrouping("rollingTupleCounter")
		       .allGrouping("command_handler");
			
		KafkaBolt<String, String> k6 = new GenericMessageQueueBolt<String, String>()
				.withTupleToKafkaMapper(new FieldNameBasedTupleToKafkaMapper());
		
		builder.setBolt("genericQueue", k6, numMessageQueueBolts)
		       .shuffleGrouping("filter");

		KafkaBolt<String, String> k7 = new ExternalMessageQueueBolt<String> (MessageQueueInterface.GRAPH_QUEUE)
				.withTupleToKafkaMapper(new FieldNameBasedTupleToKafkaMapper());
		builder.setBolt("graphMessageQueue", k7, numMessageQueueBolts)
				.shuffleGrouping("communityGraphBolt101");

		KafkaBolt<String, String> k8 = new ExternalMessageQueueBolt<String> (MessageQueueInterface.CLUSTER_QUEUE)
				.withTupleToKafkaMapper(new FieldNameBasedTupleToKafkaMapper());
		builder.setBolt("clusterMessageQueue", k8, numMessageQueueBolts)
				.shuffleGrouping("communityClusteringBolt101");

		KafkaBolt<String, String> k9 = new ExternalMessageQueueBolt<String> (MessageQueueInterface.NODE_QUEUE)
				.withTupleToKafkaMapper(new FieldNameBasedTupleToKafkaMapper());
		builder.setBolt("nodeMessageQueue", k9, numMessageQueueBolts)
				.shuffleGrouping("rollingTupleCounter");

		// Create a Kafka Spout	
		BrokerHosts brokerHosts;
		brokerHosts = new ZkHosts(zookeeper);
		SpoutConfig kafkaConfig = new SpoutConfig(brokerHosts, MessageQueueInterface.COMMAND_QUEUE, "", "storm");
		kafkaConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
			
		// TODO: Initial testing showed multiple emits for the same message from the 
		// CommandSpout - need to investigate
		// builder.setSpout("commands", new CommandSpout(kafkaConfig, "sharon_command"), 1);
		builder.setSpout("commands", new CommandHandlerSpout(zookeeper), numCommandHandlerSpouts);	
		//builder.setBolt("print", new PrinterBolt()).shuffleGrouping("commands");
		
		builder.setBolt("command_handler", new CommandHandlerBolt(), numCommandHandlerBolts)
		       .shuffleGrouping("commands", "topic_user_stream").shuffleGrouping("ranker");
		
		
		// Kafka properties
		//set producer properties.
		Properties props = new Properties();
		props.put("metadata.broker.list", kafkaBrokers);
		props.put("request.required.acks", "1");
		props.put("serializer.class", "kafka.serializer.StringEncoder");
		conf.put(TridentKafkaState.KAFKA_BROKER_PROPERTIES, props);
		
		return builder;
	}

	public static void main(String[] args) throws Exception {

		// die hard if the sharon.properties file is not available
		if(storm.sharon.util.Properties.isUsingDefaults()){
			throw new RuntimeException("Cannot access sharon.properties files.");
		}
		
		
		// Initialize the database
		InitDatabase();
        
		
		// Setup the configuration
		Config conf = new Config();
		conf.put(Config.TOPOLOGY_TRIDENT_BATCH_EMIT_INTERVAL_MILLIS, 2000);
		
		
		// Build the topology
		TopologyBuilder builder = BuildTopology(conf);

		 
		if (args != null && args.length > 0) {
			PropertiesConfiguration config = storm.sharon.util.Properties.getConfig();
			int numWorkers=10;
			if(config!=null){
				if(config.containsKey("numWorkers")) numWorkers=config.getInt("numWorkers");
			}
			conf.setNumWorkers(numWorkers);
			StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
		} else {
			conf.setNumWorkers(2);
			conf.setMaxTaskParallelism(1000);
			LocalCluster cluster = new LocalCluster();
			cluster.submitTopology("test", conf, builder.createTopology());
			while(true){ // TODO: catch CTRL-c and shutdown cluster properly
				Utils.sleep(1000 * 60 * 60);
			}
			//cluster.killTopology("test");
			//cluster.shutdown();
		}
	}
}
