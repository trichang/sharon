package storm.sharon;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;

import scala.Tuple2;
import scala.Tuple3;
import storm.sharon.util.window.SlidingWindowCounter;
import storm.sharon.util.window.SlidingWindowObjectTracker;

public class TestWindowProcessing {
	
	public static void main(String[] args) {
		
		FileReader inputStream = null;
		SlidingWindowObjectTracker<Tuple2, Tuple3> objectTracker = 
								new SlidingWindowObjectTracker<Tuple2, Tuple3>(5);
      
        try {
            inputStream = new FileReader("testFiles/topicMentionUser");
            BufferedReader br = new BufferedReader(inputStream);
            String line = null;
            int count = 0;
            int window = 0;
        	while ((line = br.readLine()) != null) {
        		//System.out.println(line);
        		StringTokenizer t = new StringTokenizer(line, ";");
        		
        		String topic_id = t.nextToken();
        		String mentioned_user = t.nextToken();
        		String user= t.nextToken();
        		
        		Tuple2<String, String> key = new Tuple2<String, String> (topic_id, mentioned_user);
        		Tuple3<String, String, String> value = new 
        				Tuple3<String, String, String> (topic_id, mentioned_user, user);
        		System.out.println("Count: " + count);
        		System.out.println("Key: " + key);
        		System.out.println("Value: " + value);
        		//System.out.println("Adding pair to object tracker");
        		objectTracker.addObject(key, value);
        		
        		count++;
        		int threshold = 3;
        		if (count == 2) {
        			count = 0;
        			window++;
        			System.out.println("****************************: " + window);
        			System.out.println("Keys Greater than threshold: " +  objectTracker.getKeysGreaterThanEqual(threshold));
        		    System.out.println("Values Greater than threshold: " + 
                            objectTracker.getObjectsThenAdvanceWindow(threshold));
        		    System.out.println("****************************");
        		}
        		//System.out.println("Added pair to object tracker");
        	}
        	br.close();
        }
        catch(Exception e) {
            System.out.println("File open failed");	
        }
        
        /*
        System.out.println("Keys Greater than threshold: " +  objectTracker.getKeysGreaterThan(3));
        
        System.out.println("Values Greater than threshold: " + 
                                    objectTracker.getObjectsThenAdvanceWindow(3));
       */
	}

}
