#!/usr/bin/env bash

mainIP="$1"

clusterIPs="$@"

echo "Setting up on servers ${clusterIPs}"

nodenum=1
for node in $clusterIPs
do
echo "=== /etc/hosts at $node"	
ssh -oStrictHostKeyChecking=no ubuntu@$node 'sudo bash -s' << END
hostname prod-${nodenum}
hostname prod-${nodenum}
echo "#RAPID configuration" > /etc/hosts
nodex=1
for server in $clusterIPs
do
	if test \$server = $node ;
	then
		echo "\${server} prod-\${nodex} localhost" >> /etc/hosts
	else 
		echo "\${server} prod-\${nodex}" >> /etc/hosts 
	fi
	nodex=\`expr \${nodex} + 1\`
done
END
nodenum=`expr $nodenum + 1`
done

# Configure and start zookeeper on each cluster node
nodenum=1
for node in $clusterIPs
do
echo "=== Zookeeper $nodenum at $node"
ssh ubuntu@$node 'bash -s' << END
cd /home/ubuntu/bin/zookeeper/bin
./zkServer.sh stop
rm -rf /var/zookeeper/*
cd /home/ubuntu/bin/zookeeper/conf
cp zoo_sample.cfg zoo.cfg
nodex=1
for server in $clusterIPs
do
	echo "server.\${nodex}=\${server}:2888:3888" >> zoo.cfg
	nodex=\`expr \${nodex} + 1\`
done
echo $nodenum > /var/zookeeper/myid
echo "My id is \`cat /var/zookeeper/myid\`"
cd ../bin
./zkServer.sh start
END
nodenum=`expr $nodenum + 1`
done

# Configure and start storm on each cluster node
nodenum=1
for node in $clusterIPs
do
echo "=== Apache Storm $nodenum at $node"
ssh ubuntu@$node 'bash -s' << END
pkill -TERM -u ubuntu -f 'backtype.storm.*'
rm -rf /var/storm/*
cd /home/ubuntu/bin/storm/conf
echo storm.zookeeper.servers: > storm.yaml
for server in $clusterIPs
do
	echo " - \"\${server}\"" >> storm.yaml
done
echo "storm.local.dir: \"/var/storm\"" >> storm.yaml
echo "nimbus.host: \"${mainIP}\"" >> storm.yaml
echo supervisor.slots.ports: >> storm.yaml
echo " - 6700" >> storm.yaml
echo " - 6701" >> storm.yaml
echo " - 6702" >> storm.yaml
echo " - 6703" >> storm.yaml
echo "nimbus.childopts: \"-Xmx1024m -Djava.net.preferIPv4Stack=true\"" >> storm.yaml
echo "ui.childopts: \"-Xmx768m -Djava.net.preferIPv4Stack=true\"" >> storm.yaml
echo "supervisor.childopts: \"-Djava.net.preferIPv4Stack=true\"" >> storm.yaml
echo "worker.childopts: \"-Xmx768m -Djava.net.preferIPv4Stack=true\"" >> storm.yaml
cd ..
if test "$node" = "$mainIP";
then
    nohup bin/storm nimbus > /dev/null 2>&1 &
    nohup bin/storm ui > /dev/null 2>&1 &
else
	nohup bin/storm supervisor > /dev/null 2>&1 &
	nohup bin/storm logviewer > /dev/null 2>&1 &
fi
END
nodenum=`expr $nodenum + 1`
done

# Configure and start kafka
nodenum=1
for node in $clusterIPs
do
echo "=== Kafka $nodenum at $node"
ssh ubuntu@$node 'bash -s' << END
cd /home/ubuntu/bin/kafka
bin/kafka-server-stop.sh > /dev/null 2>&1
rm -rf /var/kafka-logs/*
rm -rf logs/*
cd /home/ubuntu/bin/kafka/config
cp server.properties server-${nodenum}.properties
echo "broker.id=${nodenum}" >> server-${nodenum}.properties
echo "log.dirs=/var/kafka-logs" >> server-${nodenum}.properties
echo "log.retention.hours=1" >> server-${nodenum}.properties
echo "host.name=${node}" >> server-${nodenum}.properties
echo "advertised.host.name=${node}" >> server-${nodenum}.properties
echo "num.partitions=1" >> server-${nodenum}.properties
cd ..
echo "Starting kafka server"
nohup bin/kafka-server-start.sh config/server-${nodenum}.properties > /dev/null 2>&1 &
END
nodenum=`expr $nodenum + 1`
done

# Configure sharon
nodenum=1
for node in $clusterIPs
do
echo "=== Sharon $nodenum at $node"
ssh ubuntu@$node 'bash -s' << END
cd /var/sharon
test -d TwitterCredentials || mkdir TwitterCredentials
echo "#sharon properties" > sharon.properties
echo "kafka.brokerlist = localhost:9092" >> sharon.properties
echo "kafka.zookeeper = localhost:2181" >> sharon.properties
echo "database.hostname = ${mainIP}" >> sharon.properties
echo "database.port = 27017" >> sharon.properties
echo "database.name = sharonDB" >> sharon.properties
echo "database.user = sharon" >> sharon.properties
echo "database.password = sharon123" >> sharon.properties
END
nodenum=`expr $nodenum + 1`
done


