#!/usr/bin/python
"""nectar.

Usage:
  nectar.py info [-q | --quiet]
  nectar.py delete
  nectar.py allocate [--num=<num>]
  nectar.py (-h | --help)
  nectar.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  --num=<num>   Number of instances to allocate [default: 1].
  -q --quiet    Print only required information

"""
import boto

from boto.ec2.connection import EC2Connection
from boto.ec2.regioninfo import *

from pprint import pprint

from docopt import docopt



region = RegionInfo(name="NeCTAR", endpoint="nova.rc.nectar.org.au")
connection = boto.connect_ec2(
                    is_secure=True,
                    region=region,
                    validate_certs=False,
                    port=8773,
                    path="/services/Cloud")

rapidNodeImage = None
existingInstances = []
quiet=False

def ImageInfo():
    if(not quiet): print '=== IMAGE INFORMATION ==='
    global rapidNodeImage
    images = connection.get_all_images()
    for i in images:
        if(i.name=="RAPID node"): 
            if(not quiet): pprint(i.__dict__)
            rapidNodeImage = i
            break
    if(rapidNodeImage==None):
        raise RuntimeError('RAPID node image is not available.')

def InstanceInformation():
    if(not quiet): print '=== INSTANCE INFORMATION ==='
    reservations = connection.get_all_reservations()
    global existingInstances
    for r in reservations:
        instances = r.instances
        for i in instances:
            if(i.image_id==rapidNodeImage.id):
                print i.ip_address
                existingInstances.append(i);

def DestroyInstances():
    InstanceInformation()
    if len(existingInstances)>0:
        if(not quiet): print "---> TERMINATING EXISTING INSTANCES"
        connection.terminate_instances(instance_ids=[x.id for x in existingInstances])
    else:
        if(not quiet): print "No existing instances."

def AllocateInstances(numInstances=10):
    if(not quiet): print '=== ALLOCATING INSTANCES ==='
    connection.run_instances(
            rapidNodeImage.id,
            key_name='aharwood_sunrise',
            instance_type='m2.small',
            security_groups=['RAPID Security'],
            min_count=numInstances,
            max_count=numInstances)

if __name__ == '__main__':
    arguments = docopt(__doc__, version='nectar 0.1')
    if(('--quiet' in arguments and arguments['--quiet']==True) or ('-q' in arguments and arguments['-q']==True)):
        quiet=True
    if(not quiet): print(arguments)
    if(arguments['info']==True):
        ImageInfo()
        InstanceInformation()
    if(arguments['delete']==True):
        ImageInfo()
        DestroyInstances()
    if(arguments['allocate']==True):
        ImageInfo()
        AllocateInstances(int(arguments['--num']))
        